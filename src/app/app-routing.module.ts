import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {ReportVisorComponent} from './components/report-visor/report-visor.component';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: '/horizontalProperty', canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    loadChildren: () => import('./components/auth/auth.module').then(r => r.AuthModule)
  },
  {
    path: 'client',
    loadChildren: () => import('./components/authClient/auth-client.module').then(r => r.AuthClientModule)
  },
  {
    path: 'horizontalProperty',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/admin-horizontal-property/admin-horizontal-property.module')
      .then(r => r.AdminHorizontalPropertyModule)
  },
  {
    path: 'managerHorizontalProperty/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/horizontal-property/horizontal-property.module')
      .then(r => r.HorizontalPropertyModule)
  },
  {
    path: 'report-visor',
    component: ReportVisorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
