import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataGlobalService {
  private _search: string = '';
  evt = new EventEmitter();
  private isOpen: boolean = true;
  eventOpen = new EventEmitter();

  constructor() {
  }

  getSearch() {
    return this._search;
  }

  setSearch(data: string) {
    this._search = data;
    this.evt.emit(this._search);
  }

  setOpen() {
    this.isOpen = !this.isOpen;
    this.eventOpen.emit(this.isOpen);
  }


}
