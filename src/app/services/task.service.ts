import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class TaskService extends PrincipalService {
  route = 'task';

  getAllTask(): Observable<ApiResponse> {
    return this.get();
  }

  getTaskProperty(id): Observable<ApiResponse> {
    return this.get(`${id}/pending`);
  }

  taskProcess(id) {
    return this.get(`processTask/${id}`);
  }
}
