import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SNACK_CONFIG} from '../config/toast.config';
import {SnackAlertComponent} from '../components/alerts/snack-alert/snack-alert.component';
import {LoaderService} from './loader.service';
import {SnackErrorComponent} from '../components/alerts/snack-error/snack-error.component';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  re = SNACK_CONFIG;
  hostAdmin = environment.admin;
  hostClient = environment.client;

  constructor(
    private router: Router,
    private snack: MatSnackBar,
    private loaderService: LoaderService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.setLoader(true);
    const token: string = localStorage.getItem('token');
    this.re.data = 'Correcto';
    let request = req;
    if (req.method === 'POST' || req.method === 'PUT' || req.method === 'DELETE' || req.method === 'GET') {
      this.loaderService.setLoaderBlur(true);
    } else {
      this.loaderService.setLoaderBlur(false);
    }
    if (token) {
      request = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request)
      .pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            try {
              if (event.body.message) {
                this.re.data = event.body.message;
                this.snack.openFromComponent(SnackAlertComponent, this.re);
              }
            } catch (e) {
              //
            }

            this.loaderService.setLoader(false);
            this.loaderService.setLoaderBlur(false);
          }
          return event;
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            localStorage.clear();
            this.re.data = 'No Autorizado';
            this.snack.openFromComponent(SnackAlertComponent, this.re);


            if (window.location.origin.includes(this.hostAdmin)) {
              this.router.navigate(['/auth/login']);
            }
            if (window.location.origin.includes(this.hostClient)) {
              this.router.navigate(['/client/login']);
            }


          } else if (err.status >= 400 && err.status <= 500) {
            this.re.data = err.error.message;
            this.snack.openFromComponent(SnackAlertComponent, this.re);
          } else if (err.status === 0) {
            this.re.data = 'Error de Conexión';
            // this.loaderService.setLoaderBlur(false);
            // this.loaderService.setLoader(false);
            this.snack.openFromComponent(SnackErrorComponent, this.re);
          }
          this.loaderService.setLoader(false);
          this.loaderService.setLoaderBlur(false);
          return throwError(err);
        })
      );
  }
}
