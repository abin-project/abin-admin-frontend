import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  evt = new EventEmitter();
  evtBlur = new EventEmitter();
  private visible = false;
  private visibleBlur = false;
  constructor() {
  }

  setLoader(state: boolean) {
    this.visible = state;
    this.evt.emit(this.visible);
  }

  getLoader() {
    this.evt.emit(this.visible);
    return this.visible;
  }

  getLoaderBlur() {
    this.evtBlur.emit(this.visible);
    return this.visibleBlur;
  }

  setLoaderBlur(state: boolean) {
    this.visibleBlur = state;
    this.evtBlur.emit(this.visibleBlur);
  }

  disabledButtons(state: boolean) {
    const r = document.getElementsByTagName('button');
    for (let a in r) {
      console.log(r[a]);
      // r[a].disabled = state;
    }
  }
}
