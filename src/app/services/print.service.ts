import {Injectable} from '@angular/core';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as moment from 'moment';
import {FONTS} from '../config/fonts.config';
import {User} from '../model/User';
import {Property} from '../model/hotizontal-property/property';
import {Transaction} from '../model/hotizontal-property/transaction';
import {formatNumber, pad, toDecimal} from './globalFunctions';
import {Bank} from '../model/hotizontal-property/bank';
import {NumeroALetras} from './numberToLetter';
import {WatterPipe} from '../pipes/watter.pipe';
import {FormIncome} from '../model/hotizontal-property/formIncome';
import {AbinDatePipe} from '../pipes/abin-date.pipe';
import {MoneyPipe} from '../pipes/money.pipe';
import {ViewTransaction} from '../components/manager-horizontal-property/expense/create-render-transaction/create-render-transaction.component';
import {HorizontalProperty} from '../model/hotizontal-property/horizontalProperty';

declare var $: any;

// tslint:disable-next-line:only-arrow-functions
(function(API) {
  // @ts-ignore
  API.textCenter = function(txt, options, x, y) {
    options = options || {};
    // tslint:disable-next-line:triple-equals
    if (options.align == 'center') {
      const fontSize = this.internal.getFontSize();
      const pageWidth = this.internal.pageSize.width;
      const txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
      x = (pageWidth - txtWidth) / 2;
    }
    this.text(txt, x, y);
  };
})(jsPDF.API);

@Injectable({
  providedIn: 'root'
})
export class PrintService {
  base64ImgLogo = new Image();
  report = new Image();
  receiptWatter = new Image();
  receiptLiquidation = new Image();
  auxLines = [];
  doc: jsPDF;
  constructor(private pipeWatter: WatterPipe, private pipeDate: AbinDatePipe, private pipeMoney: MoneyPipe) {
    this.base64ImgLogo.src = '../../assets/logoReport.jpg';
    this.report.src = '../../assets/recibo.jpg';
    this.receiptWatter.src = '../../assets/boleta_agua.jpg';
    this.receiptLiquidation.src = '../../assets/boleta_no_agua.jpg';
  }

  async printTableById(titles: string[], idTable, optionCol?, typeTable?) {
    let posStart = 18;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 9,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 15,
        right: 10,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 9
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
    };
    if (typeTable) {
      // tslint:disable-next-line
      dataOptions['tableWidth'] = typeTable;
    }

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();

  }

  async createSingleTable(title, subtitle, head, body, optionCol, typeTable?: 'wrap' | null) {
    this.doc = new jsPDF('p', 'mm', 'letter', true);
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    // @ts-ignore
    this.doc.textCenter(title, {align: 'center', font: 'OpenSansBold'}, 0, 18);
    // @ts-ignore
    this.doc.textCenter(subtitle, {align: 'center'}, 0, 24);
    this.doc.setFont('OpenSansLight');
    const dataOptions = {
      startY: 39,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      tableLineColor: [191, 191, 191],
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 7
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
    };
    if (typeTable) {
      // tslint:disable-next-line
      dataOptions['tableWidth'] = typeTable;
    }
    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }

  async createIncomeIndividual(titles: string[], head, body, property: Property) {
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    let x = 28;
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, x);
      x = x + 6;
    }
    this.doc.setFont('OpenSansLight');
    const dataOptions = {
      startY: x + 14,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      tableLineColor: [191, 191, 191],
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 8,
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        // minCellWidth: 20,
        font: 'OpenSansLight'
      },
      columnStyles: {
        0: {
          cellWidth: 13
        }
      }
    };
    const colW = 173 / (head.length - 1);
    for (let i = 1; i < head.length; i++) {
      dataOptions.columnStyles[i] = {
        cellWidth: colW
      };
    }

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    await this.insertHeaderAndFooter(this.doc);
    await this.incomePropertyTitles(property);
    // await this.setProperties(this.doc, titles, user);
    this.outputPrint();
  }

  async advancedSingleTable(title, subtitle, head, body, optionCol, owners) {
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    // @ts-ignore
    this.doc.textCenter(title, {align: 'center'}, 0, 18);
    // @ts-ignore
    this.doc.textCenter(subtitle, {align: 'center'}, 0, 24);
    this.doc.setFont('OpenSansLight', 'normal');
    const dataOptions = {
      startY: 39,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      tableLineColor: [191, 191, 191],
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 7
      },
      willDrawCell:  (data) => {
        if (data.row.section === 'body' && data.column.dataKey === 1) {
          if (owners.find(rr => rr.namePeople.toUpperCase() === data.cell.raw) !== undefined) {
            this.doc.setFont('OpenSansBold', 'bold');
          }
        }
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
    };
    // if (typeTable) {
    //   // tslint:disable-next-line
    //   dataOptions['tableWidth'] = typeTable;
    // }
    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    // const prefix = moment().unix();
    this.outputPrint();
  }

  async createIdTableIncomeExpense(titles: string[], optionCol, idTable?, typeTable?) {
    let posStart = 18;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 9,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 15,
        right: 10,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        // lineColor: [191, 191, 191],
        // lineWidth: 0.1,
        fontSize: 10
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
      didParseCell: (data) => {
        if (data.cell.raw.className.includes('bold')) {
          this.setFont(data);
        }
        if (data.cell.raw.className.includes('center')) {
          data.cell.styles.halign = 'center';
        }
        this.setRowTitles(data);
      },
      didDrawPage: (data) => {
        for (const a of data.table.body) {
          if (a.cells[11]) {
            if (a.cells[11].raw.id === 'totalOfTotal' || a.cells[11].raw.id === 'totalPeriod') {
              this.drawLine(a.cells[11]);
            }
          }
        }
      },

    };
    if (typeTable) {
      // tslint:disable-next-line
      dataOptions['tableWidth'] = typeTable;
    }

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();

  }

  async incomePropertyTitles(property: Property) {
    this.doc.setFont('OpenSansLight', 'normal');
    this.doc.setFontSize(8);
    // @ts-ignore
    this.doc.text(`${property.type}: ${property.name}`, 15, 40);
    this.doc.text(`Propietario: ${property.owner.nameOwner}`, 15, 44);
    this.doc.text(`Cel: ${property.owner.cellOwner}`, this.doc.internal.pageSize.width / 2, 44);
    if (property.tenant) {
      this.doc.text(`Inquilino: ${property.tenant.nameTenant}`, 15, 48);
      this.doc.text(`Cel: ${property.tenant.cellTenant}`, this.doc.internal.pageSize.width / 2, 48);
    }
  }

  async printProjectById(titles: string[],  idTable?, typeTable?) {
    let posStart = 18;
    this.doc = new jsPDF({
      orientation: 'landscape',
      format: 'legal'
    });
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 9,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 10,
        right: 10,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 6.5
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: {
          1: {
            halign: 'left',
          },
      },
      didParseCell: (data) => {
        if (data.row.cells[0].text[0] === '500' && data.row.index < (data.table.body.length - 2 )) {
          this.setRowFont(data);
        } else if (data.row.cells[0].text[0] !== '500'){
          this.setRowFont(data);
        }
        if (data.section === 'body' && data.column.index !== 1){
          data.cell.styles.halign = 'right';
        }
        if (data.row.raw.length < 2) {
            data.cell.styles.halign = 'center';
        }
        console.log(data);
      },
    };
    if (typeTable) {
      // tslint:disable-next-line
      dataOptions['tableWidth'] = typeTable;
    }

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }

  async printLedger(titles: string[], optionCol, idTable?, typeTable?) {
    let posStart = 18;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(12);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 9,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
    };
    if (typeTable) {
      // tslint:disable-next-line
      dataOptions['tableWidth'] = typeTable;
    }

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();

  }

  async printMultipleTable(titles: string[], keys: any[], dataOptions?) {
    let posStart = 18;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(12);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    let finalY = 24;
    for (const data of keys) {
      // @ts-ignore
      if (this.doc.previousAutoTable.finalY) {
        // @ts-ignore
        finalY = this.doc.previousAutoTable.finalY;
      }
      this.doc.setFont('OpenSansBold', 'bold');
      this.doc.setFontSize(10);
      this.doc.text(data.title, 14, finalY + 10);
      this.doc.setFont('OpenSansLight');
      // @ts-ignore
      this.doc.autoTable({
        html: `#${data.table}`,
        startY: finalY + 15,
        margin: {
          top: 30,
          left: 15,
          right: 15,
          bottom: 15
        },
        theme: 'plain',
        tableLineColor: [191, 191, 191],
        styles: {
          cellPadding: 0.5,
          lineColor: [191, 191, 191],
          lineWidth: 0.1,
          fontSize: 7
        },
        headStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansBold'
        },
        bodyStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansLight'
        },
        columnStyles: {
          0: {
            halign: 'right'
          },
          2: {
            halign: 'right'
          },
          3: {
            halign: 'right'
          },
          4: {
            halign: 'right'
          },
          5: {
            halign: 'right'
          },
          6: {
            halign: 'right'
          },
          7: {
            halign: 'right'
          },
          8: {
            halign: 'right'
          },
          9: {
            halign: 'right'
          }
        },
      });
    }
    // });
    if (dataOptions) {
      // @ts-ignore
      this.doc.autoTable(dataOptions);
    }
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }

  async printReportDebtsById(titles: string[], optionCol, idTable?, widthDoc?: number) {
    let posStart = 18;
    this.doc = new jsPDF('landscape', 'mm', [216, widthDoc], true);
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 9,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 10,
        right: 10,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 6.5
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        maxCellWidth: 10,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
      didParseCell: (data) => {
        if (data.cell.section === 'head') {
          data.cell.styles.halign = 'center';
        }
        if (data.cell.section === 'body' && isNaN(Number(data.cell.text[0])) ) {
          data.cell.styles.halign = 'right';
        }
      },
    };
    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }

  async printVoucher(titles, head, body, property: Property, transaction: Transaction, bank: Bank) {
    let posStart = 43;
    this.doc = new jsPDF('p', 'mm', 'letter');
    const width = this.doc.internal.pageSize.getWidth();
    const height = this.doc.internal.pageSize.getHeight();
    await this.addFonts(this.doc);
    this.doc.addImage(this.report, 'JPEG', 0, 0, width, height);

    // this.doc.line(15, 30, width - 15, 30);

    this.doc.setFontSize(12);
    this.doc.setFont('OpenSansBold', 'bold');
    for (const title of titles) {
      // @ts-ignore
      this.doc.textCenter(title, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setTextColor('#FF0000');
    this.doc.text('N° ' + pad(transaction.voucher.codeVoucher, 7, '0'), 179, 30);

    this.doc.setTextColor('#000000');
    this.doc.setFontSize(11);

    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.text(property.type.toUpperCase(), 53, 70.5);
    this.doc.text(property.name.toString().toUpperCase(), 142, 70.5);
    this.doc.text(`${bank.typeAccount} ${bank.nameBank} ${bank.numberAccount}`.toUpperCase(), 53, 78.7);
    this.doc.text(`${moment(transaction.date).format('dddd, MMMM DD . YYYY')}`.replace('.', 'del').toUpperCase(), 53, 86.5);
    this.doc.text(`${NumeroALetras(transaction.amount, {plural: 'bolivianos', singular: 'boliviano', centPlural: 'centavos', centSingular: 'centavo'})}  ${Number(transaction.amount).toFixed(2).split('.')[1]}/100`.toUpperCase(), 53, 94.3);
    this.doc.text(transaction.transactionCode.toUpperCase(), 65, 101.6);

    // this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      startY: posStart + 60,
      head: [
        head
      ],
      body,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 17,
        right: 17,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10,
        // overflow: 'ellipsize',
        // cellWidth: 'wrap'
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      // tableWidth: 'wrap',
      didParseCell: (data) => {
        if (data.cell.section === 'body' && !isNaN(Number(data.cell.text[0])) && data.column.index !== 0 ) {
          data.cell.styles.halign = 'right';
          data.cell.text[0] = formatNumber(data.cell.raw);
        }
      },
    };


    // @ts-ignore
    this.doc.autoTable(dataOptions);
    // this.insetHeader(this.doc);
    this.outputPrint();

  }

  async printMultipleVouchers(horizontalProperty: HorizontalProperty, datas: any[]) {

    this.doc = new jsPDF('p', 'mm', 'letter');
    await this.addFonts(this.doc);
    const width = this.doc.internal.pageSize.getWidth();
    const height = this.doc.internal.pageSize.getHeight();


    for (const data of datas) {
      let posStart = 43;
      const titles = [horizontalProperty.nameProperty, 'Recibo de Pago'];

      this.doc.addImage(this.report, 'JPEG', 0, 0, width, height);

      // this.doc.line(15, 30, width - 15, 30);

      this.doc.setFontSize(12);
      this.doc.setFont('OpenSansBold', 'bold');
      for (const title of titles) {
        // @ts-ignore
        this.doc.textCenter(title, {align: 'center'}, 0, posStart);
        posStart += 6;
      }
      this.doc.setTextColor('#FF0000');
      this.doc.text('N° ' + pad(data.transaction.voucher.codeVoucher, 7, '0'), 179, 30);

      this.doc.setTextColor('#000000');
      this.doc.setFontSize(11);

      this.doc.setFont('OpenSansBold', 'bold');
      this.doc.text(data.property.type.toUpperCase(), 53, 70.5);
      this.doc.text(data.property.name.toString().toUpperCase(), 142, 70.5);
      this.doc.text(`${data.bank.typeAccount} ${data.bank.nameBank} ${data.bank.numberAccount}`.toUpperCase(), 53, 78.7);
      this.doc.text(`${moment(data.transaction.date).format('dddd, MMMM DD . YYYY')}`.replace('.', 'del').toUpperCase(), 53, 86.5);
      this.doc.text(`${NumeroALetras(data.transaction.amount, {plural: 'bolivianos', singular: 'boliviano', centPlural: 'centavos', centSingular: 'centavo'})}  ${Number(data.transaction.amount).toFixed(2).split('.')[1]}/100`.toUpperCase(), 53, 94.3);
      this.doc.text(data.transaction.transactionCode.toUpperCase(), 65, 101.6);

      // this.doc.setFont('OpenSansLight');
      const wantedTableWidth = 120;
      const pageWidth = this.doc.internal.pageSize.width;
      const margin = (pageWidth - wantedTableWidth) / 2;
      // @ts-ignore
      const dataOptions = {
        startY: posStart + 60,
        head: [
          data.headers
        ],
        body : data.debts,
        // margin: {left: margin, right: margin}
        margin: {
          top: 30,
          left: 17,
          right: 17,
          bottom: 15
        },
        theme: 'plain',
        styles: {
          cellPadding: 0.5,
          lineColor: [191, 191, 191],
          lineWidth: 0.1,
          fontSize: 10,
          // overflow: 'ellipsize',
          // cellWidth: 'wrap'
        },
        headStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansBold'
        },
        bodyStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansLight'
        },
        // tableWidth: 'wrap',
        didParseCell: (d) => {
          if (d.cell.section === 'body' && !isNaN(Number(d.cell.text[0])) && d.column.index !== 0 ) {
            d.cell.styles.halign = 'right';
            d.cell.text[0] = formatNumber(d.cell.raw);
          }
        },
      };


      // @ts-ignore
      this.doc.autoTable(dataOptions);
      this.doc.addPage();
    }

    // this.insetHeader(this.doc);
    this.outputPrint();

  }

  async printLiquidation(titles: string[], idTable, property: Property, data) {
    let posStart = 43;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    const width = this.doc.internal.pageSize.getWidth();
    const height = this.doc.internal.pageSize.getHeight();
    this.doc.addImage(this.receiptLiquidation, 'JPEG', 0, 0, width, height);
    this.doc.setFontSize(12);
    //   lastTitle = 'Liquidación';
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    this.doc.setFontSize(11);


    this.doc.text(property.type.toUpperCase().concat(`  ${property.name.toString().toUpperCase()}`), 47, 64);
    this.doc.text(property.code, 130, 64);
    this.doc.text(moment().format('DD-MM-YYYY'), 173, 64);


    this.doc.text(property.owner.nameOwner.toUpperCase(), 47, 70);

    this.doc.setFont('OpenSansLight', 'normal');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      html: `#${idTable}`,
      startY: posStart + 20,
      // margin: {left: margin, right: margin}
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      footStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      didParseCell: (d) => {
        if (d.cell.section === 'head') {
          d.cell.styles.halign = 'center';
        }
        if (d.cell.section === 'body' && d.column.index !== 0 || d.cell.section === 'foot') {
          d.cell.styles.halign = 'right';
        }
      },
    };

    if (idTable) {
      // @ts-ignore
      this.doc.autoTable(dataOptions);
      this.doc.setFont('OpenSansBold', 'bold');
      // @ts-ignore
      let finalY = this.doc.previousAutoTable.finalY + 10;
      this.doc
        .text(`Se Solicita realizar el pago del monto detallado dentro de los plazos defindos, en caso de incumplimiento se`, 23, finalY);
      this.doc.text(`procederá conforme indica el Estatuto y/o Reglamento Interno.`, 23, finalY + 7);

      finalY = finalY + 20;
      this.doc.text('En las Cuentas:', 23, finalY);
      this.doc.setFont('OpenSansLight', 'normal');
      for (const b of data.accountsBanks) {
        finalY = finalY + 6;
        // this.doc.text(`${b.typeAccount}`, 23, finalY);
        this.doc.text(`${b.nameBank}`, 23, finalY);
        this.doc.text(`${b.numberAccount}`, 150, finalY);
      }
      this.doc.setFont('OpenSansBold', 'bold');

      this.doc.text('Con su correspondinte código de referencia:', 23, finalY + 10);
      // this.doc.setFontSize(15);
      this.doc.setTextColor('#0100ae');
      // @ts-ignore
      this.doc.text(`${property.code}`, 95, finalY + 10);
      // finalY = finalY + 20;
    } else {
      this.doc.setFillColor(255, 255, 255);
      this.doc.rect(23, 75, width - 46, 20, 'F');
      this.doc.setFont('OpenSansLight', 'normal');
      this.doc.setFontSize(12);
      const lines = this.doc.splitTextToSize(`Felicidades la propierdad ${property.name} con código ${property.code} y propietario ${property.owner.nameOwner} no tiene Deudas Pendientes y se encuentra con sus pagos al día.`, width - 46);
      this.doc.text(lines, 23, 85);
      // this.textBold(lines, 23, 85, 10, 12);
    }

    this.outputPrint();

  }

  async printVoucherPayment(titles: string[], property: Property, data: any, head: any, body: any) {
    let posStart = 43;
    const watter = data.watterTable;
    const lastForm: FormIncome = data.lastForm;
    const actualForm: FormIncome = data.actualForm;
    this.doc = new jsPDF('p', 'mm', 'letter');
    const width = this.doc.internal.pageSize.getWidth();
    const height = this.doc.internal.pageSize.getHeight();
    await this.addFonts(this.doc);
    const lastTitle = 'Boleta de Cobro';
    this.doc.addImage(this.receiptWatter, 'JPEG', 0, 0, width, height);
    titles.push(lastTitle);

    this.doc.setFontSize(12);
    this.doc.setFont('OpenSansBold', 'bold');
    for (const title of titles) {
      // @ts-ignore
      this.doc.textCenter(title, {align: 'center'}, 0, posStart);
      posStart += 6;
    }

    this.doc.setTextColor('#000000');
    this.doc.setFontSize(11);

    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.text(property.type.toUpperCase().concat(`  ${property.name.toString().toUpperCase()}`), 47, 64);
    this.doc.text(property.code, 130, 64);
    this.doc.text(moment().format('DD-MM-YYYY'), 173, 64);

    this.doc.text(property.owner.nameOwner.toUpperCase(), 47, 70);
    // if (isWatter) {
    if (lastForm) {
      this.doc.text(this.pipeDate.transform(lastForm.formDateReader), 60, 76.7);
    }
    // para la tabla del agua
    const dataOptionsWatter = {
      startY: posStart + 25,
      head: [
        watter.header
      ],
      body: watter.body,
      margin: {
        top: 30,
        left: 17,
        right: 17,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10,
        // overflow: 'ellipsize',
        // cellWidth: 'wrap'
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      // tableWidth: 'wrap',
      didParseCell: (d) => {
        if (d.cell.section === 'body' && !isNaN(Number(d.cell.text[0])) && d.column.index !== 0 ) {
          d.cell.styles.halign = 'right';
          d.cell.text[0] = formatNumber(d.cell.raw);
        }
      },
    };
    // @ts-ignore
    this.doc.autoTable(dataOptionsWatter);
    this.doc.text(this.pipeDate.transform(actualForm.formDateReader), 127, 76.7);
    // this.doc.text(toDecimal(actualForm.amountM3), 170, 76.7);
    // this.doc.text(this.pipeWatter.transform(watter.lastReader), 135, 76.7);
    // this.doc.text(this.pipeWatter.transform(watter.actualReader), 135, 83);
    // this.doc.text(this.pipeWatter.transform(watter.consumption), 135, 90);
    // }


    // this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      startY: posStart + 60,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 17,
        right: 17,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10,
        // overflow: 'ellipsize',
        // cellWidth: 'wrap'
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      // tableWidth: 'wrap',
      didParseCell: (d) => {
        if (d.cell.section === 'body' && !isNaN(Number(d.cell.text[0])) && d.column.index !== 0 ) {
          d.cell.styles.halign = 'right';
          d.cell.text[0] = formatNumber(d.cell.raw);
        }
      },
    };


    // @ts-ignore
    this.doc.autoTable(dataOptions);
    // @ts-ignore
    let finalY = this.doc.previousAutoTable.finalY + 10;
    this.doc
      .text(`Se Solicita realizar el pago del monto detallado dentro de los plazos defindos, en caso de incumplimiento se`, 23, finalY);
    this.doc.text(`procederá conforme indica el Estatuto y/o Reglamento Interno.`, 23, finalY + 7);
    finalY = finalY + 20;
    this.doc.text('En las Cuentas:', 23, finalY);
    this.doc.setFont('OpenSansLight', 'normal');
    for (const b of data.accountsBanks) {
      finalY = finalY + 6;
      // this.doc.text(`${b.typeAccount}`, 23, finalY);
      this.doc.text(`${b.nameBank}`, 23, finalY);
      this.doc.text(`${b.numberAccount}`, 150, finalY);
    }
    this.doc.setFont('OpenSansBold', 'bold');

    this.doc.text('Con su correspondinte código de referencia:', 23, finalY + 10);
    // this.doc.setFontSize(15);
    this.doc.setTextColor('#0100ae');
    // @ts-ignore
    this.doc.text(`${property.code}`, 95, finalY + 10);
    this.outputPrint();
  }

  async printAllVoucher(titles, properties: any) {
    const lastTitle = 'Boleta de Cobro';
    titles.push(lastTitle);
    this.doc = new jsPDF('p', 'mm', 'legal', true);
    await this.addFonts(this.doc);
    for (const property of properties) {
      this.createContentVoucher(titles, property, property.property, property.head, property.body);
    }
    this.outputPrint();
  }

  createContentVoucher(titles: string, data, property, head, body) {
    const defaultWatter = {
      actualReader: '0.0',
      amountPay: '0.0',
      consumption: '0.0',
      inLastForm: false,
      lastReader: '0.0',
    };
    let posStart = 43;
    this.doc.setTextColor('#000000');
    const watter = data.watterTable ? data.watterTable : defaultWatter;
    const lastForm: FormIncome = data.lastForm;
    const actualForm: FormIncome = data.actualForm;

    console.log(watter)
    console.log(lastForm)
    console.log(actualForm)

    const width = this.doc.internal.pageSize.getWidth();
    const height = this.doc.internal.pageSize.getHeight();
    // await this.addFonts(this.doc);

    this.doc.addImage(this.receiptWatter, 'JPEG', 0, 0, width, 279.4);

    this.doc.setFontSize(12);
    this.doc.setFont('OpenSansBold', 'bold');
    for (const title of titles) {
      // @ts-ignore
      this.doc.textCenter(title, {align: 'center'}, 0, posStart);
      posStart += 6;
    }

    this.doc.setFontSize(11);

    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.text(property.type.toUpperCase().concat(`  ${property.name.toString().toUpperCase()}`), 47, 64);
    this.doc.text(property.code, 130, 64);
    this.doc.text(moment().format('DD-MM-YYYY'), 170, 64);

    this.doc.text(property.owner.nameOwner.toUpperCase(), 47, 70);
    // if (isWatter) {
    if (lastForm) {
      this.doc.text(this.pipeDate.transform(lastForm.formDateReader), 60, 76.7);
    }
    // this.doc.text(this.pipeDate.transform(actualForm.formDateReader), 54, 83);
    this.doc.text(this.pipeDate.transform(actualForm.formDateReader), 127, 76.7);
    // this.doc.text(toDecimal(actualForm.amountM3), 170, 76.7);
    // this.doc.text(this.pipeWatter.transform(watter.lastReader), 135, 76.7);
    // this.doc.text(this.pipeWatter.transform(watter.actualReader), 135, 83);
    // this.doc.text(this.pipeWatter.transform(watter.consumption), 135, 90);

    const dataOptionsWatter = {
      startY: posStart + 25,
      head: [
        watter.header
      ],
      body: watter.body,
      margin: {
        top: 30,
        left: 17,
        right: 17,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10,
        // overflow: 'ellipsize',
        // cellWidth: 'wrap'
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      // tableWidth: 'wrap',
      didParseCell: (d) => {
        if (d.cell.section === 'body' && !isNaN(Number(d.cell.text[0])) && d.column.index !== 0 ) {
          d.cell.styles.halign = 'right';
          d.cell.text[0] = formatNumber(d.cell.raw);
        }
      },
    };
    // @ts-ignore
    this.doc.autoTable(dataOptionsWatter);
    // }


    // this.doc.setFont('OpenSansLight');
    const wantedTableWidth = 120;
    const pageWidth = this.doc.internal.pageSize.width;
    const margin = (pageWidth - wantedTableWidth) / 2;
    // @ts-ignore
    const dataOptions = {
      startY: posStart + 60,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 17,
        right: 17,
        bottom: 15
      },
      theme: 'plain',
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 10,
        // overflow: 'ellipsize',
        // cellWidth: 'wrap'
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      // tableWidth: 'wrap',
      didParseCell: (d) => {
        if (d.cell.section === 'body' && !isNaN(Number(d.cell.text[0])) && d.column.index !== 0 ) {
          d.cell.styles.halign = 'right';
          d.cell.text[0] = formatNumber(d.cell.raw);
        }
      },
    };


    // @ts-ignore
    this.doc.autoTable(dataOptions);
    // @ts-ignore
    let finalY = this.doc.previousAutoTable.finalY + 10;
    this.doc.text(`Se Solicita realizar el pago del monto detallado dentro de los plazos defindos, en caso de incumplimiento se
procederá conforme indica el Estatuto y/o Reglamento Interno.`, 23, finalY);
    finalY = finalY + 20;
    this.doc.text('En las Cuentas:', 23, finalY);
    this.doc.setFont('OpenSansLight', 'normal');
    for (const b of data.accountsBanks) {
      finalY = finalY + 6;
      // this.doc.text(`${b.typeAccount}`, 23, finalY);
      this.doc.text(`${b.nameBank}`, 23, finalY);
      this.doc.text(`${b.numberAccount}`, 150, finalY);
    }
    this.doc.setFont('OpenSansBold', 'bold');

    this.doc.text('Con su correspondinte código de referencia:', 23, finalY + 10);
    // this.doc.setFontSize(15);
    this.doc.setTextColor('#0100ae');
    // @ts-ignore
    this.doc.text(`${property.code}`, 95, finalY + 10);
    this.doc.addPage('legal', 'p');
  }

  // pdf de la tranccion rendida
  async createSingleTableTransaction(title, subtitle, head, body, optionCol, transaction: Transaction,
                                     bank: Bank, rendered: number) {
    this.doc = new jsPDF('p', 'mm', 'letter', true);
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(10);
    // @ts-ignore
    this.doc.textCenter(title, {align: 'center', font: 'OpenSansBold'}, 0, 18);
    // @ts-ignore
    this.doc.textCenter(subtitle, {align: 'center'}, 0, 24);
    this.doc.setFontSize(8);

    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.text('Cuenta: ', 20, 35);
    this.doc.text('Transacción: ', 20, 40);
    this.doc.text('Fecha de T.: ', 20, 45);
    this.doc.text('Monto: ', 55, 45);
    this.doc.text('Rendido: ', 80, 45);
    this.doc.text('Saldo: ', 109, 45);
    this.doc.text('Detalle: ', 20, 55);

    this.doc.setFont('OpenSansLight', 'normal');
    this.doc.text(`${bank.typeAccount}, ${bank.nameBank} N° ${bank.numberAccount}`, 37, 35);
    this.doc.text(`${transaction.transactionCode}, ${transaction.gloss}`, 37, 40);
    this.doc.text(this.pipeDate.transform(transaction.date), 37, 45);
    this.doc.text(this.pipeMoney.transform(transaction.amount, 'abs'), 65, 45);
    this.doc.text(this.pipeMoney.transform(Number(rendered), 'abs'), 92, 45);
    this.doc.text(this.pipeMoney.transform((Number(transaction.amount) + Number(rendered)), 'abs'), 118, 45);

    const dataOptions = {
      startY: 60,
      head: [
        head
      ],
      body,
      margin: {
        top: 30,
        left: 15,
        right: 15,
        bottom: 15
      },
      theme: 'plain',
      tableLineColor: [191, 191, 191],
      styles: {
        cellPadding: 0.5,
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 7
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansBold'
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: 0,
        font: 'OpenSansLight'
      },
      columnStyles: optionCol,
    };

    // @ts-ignore
    this.doc.autoTable(dataOptions);
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }


  private outputPrint() {
    const r = this.doc.output('datauristring');
    localStorage.setItem('pdf', r);
    window.open(`${window.location.origin}/report-visor?index=pdf`, '_blank');
  }

  private setFont(data) {
    data.cell.styles.font = 'OpenSansBold';
    data.cell.styles.fontStyle = 'bold';
  }

  async insertHeaderAndFooter(doc: jsPDF) {
    doc.setFont('OpenSansLight', 'normal');
    const posImg = doc.internal.pageSize.getWidth() - 35;
    const heightPage = doc.internal.pageSize.getHeight() - 5;

    const user = JSON.parse(atob(localStorage.getItem('profile')));

    for (let i = 0; i < doc.getNumberOfPages(); i++) {
      doc.setPage(i);
      doc.addImage(this.base64ImgLogo, 'JPEG', posImg, 7, 20, 20);
      doc.setFontSize(8);
      // @ts-ignore
      doc.textCenter(`${doc.getCurrentPageInfo().pageNumber} de ${doc.getNumberOfPages()}`, {align: 'center'}, 0, heightPage - 5);
      doc.text(`Usuario: ${user.username ? user.username.toUpperCase() : user.nameOwner.toUpperCase()}`, 15, heightPage);
      doc.text(`Fecha: ${moment().format('YYYY-MM-DD HH:mm')}`, 15, heightPage - 5);
      doc.setFontSize(8);
      doc.text(`Direccion: J.N. Rosales #1757 entre Av. América y Parque Fidel Anze`, 15, 10); // 280+4
      doc.text(`Ciudad Cochabamba, Bolivia`, 15, 14); // 280+4
      doc.text(`Telf: (+591)(4)4501738, Cel: (+591)72239684`, 15, 18);
      doc.text(`Correo: administracionph@abinsrl.com`, 15, 22);
      doc.text(`www.abinsrl.com`, 15, 26);
    }
  }

  private setRowTitles(data) {
    if (data.row.cells[0].text[0] === 'INGRESOS' || data.row.cells[0].text[0] === 'EGRESOS') {
      data.cell.styles.font = 'OpenSansBold';
      data.cell.styles.fontStyle = 'bold';
    }
  }

  private setRowFont(data) {
    if ( data.row.cells[0].text[0] === '100'
      || data.row.cells[0].text[0] === '200'
      || data.row.cells[0].text[0] === '210'
      || data.row.cells[0].text[0] === '220'
      || data.row.cells[0].text[0] === '230'
      || data.row.cells[0].text[0] === '240'
      || data.row.cells[0].text[0] === '300'
      || data.row.cells[0].text[0] === '400'
      || data.row.cells[0].text[0] === '500'
      || data.row.cells[0].text[0] === ''
      || data.row.cells[0].text[0] === 'Presupuesto de Gasto'
    ) {
      data.cell.styles.font = 'OpenSansBold';
      data.cell.styles.fontStyle = 'bold';
    }
  }

  private drawLine(last) {
    if (last && last.x > 0) {
      if (!this.auxLines.includes(last)) {
        this.auxLines.push(last);
        this.doc.setDrawColor(100, 100, 100);
        this.doc.setLineWidth(0.1);
        this.doc.line(last.width + last.x - 20, last.y, last.width + last.x, last.y);
        this.doc.line(last.width + last.x - 20, last.y + last.height, last.width + last.x, last.y + last.height);
        this.doc.line(last.width + last.x - 20, last.y + last.height + 0.7, last.width + last.x, last.y + last.height + 0.7);
      }
   }
  }

  private async insetHeader(doc: jsPDF) {
    doc.setFont('OpenSansLight', 'normal');
    // const posImg = doc.internal.pageSize.getWidth() - 35;
    // const heightPage = doc.internal.pageSize.getHeight() - 5;
    for (let i = 0; i < doc.getNumberOfPages(); i++) {
      doc.setPage(i);
      doc.addImage(this.base64ImgLogo, 'JPEG', 15, 7, 20, 20);
      doc.setFontSize(8);
      doc.text(`Direccion: J.N. Rosales #1757 entre Av. América y Parque Fidel Anze`, 37, 10); // 280+4
      doc.text(`Ciudad Cochabamba, Bolivia`, 37, 14); // 280+4
      doc.text(`Telf: (+591)(4)4501738, Cel: (+591)72239684`, 37, 18);
      doc.text(`Correo: administracionph@abinsrl.com`, 37, 22);
      doc.text(`www.abinsrl.com`, 37, 26);
    }
  }

  private async addFonts(doc) {
    doc.addFileToVFS(FONTS.ROBOT_CONDENSET_BOLD.name, FONTS.ROBOT_CONDENSET_BOLD.vfs);
    doc.addFileToVFS(FONTS.ROBOTO_CONDENSED_LIGHT.name, FONTS.ROBOTO_CONDENSED_LIGHT.vfs);
    doc.addFont(FONTS.ROBOTO_CONDENSED_LIGHT.name, FONTS.ROBOTO_CONDENSED_LIGHT.id, FONTS.ROBOTO_CONDENSED_LIGHT.style);
    doc.addFont(FONTS.ROBOT_CONDENSET_BOLD.name, FONTS.ROBOT_CONDENSET_BOLD.id, FONTS.ROBOT_CONDENSET_BOLD.style);
  }

  private async setProperties(doc, title, user: User) {
    doc.setDocumentProperties({
      author: user.username.toUpperCase(),
      creator: 'Abin SRL',
      title: `${title}`,
      subject: `${title}`
    });
  }

  private textBold(textMap: string[], startX, startY, lineSpacing, fontSize) {

    const startXCached = startX;
    let boldOpen = false;
    textMap.map((text, i) => {
      if (text) {
        const arrayOfNormalAndBoldText = text.split('**');
        arrayOfNormalAndBoldText.map((textItems, j) => {
          if (boldOpen) {
            this.doc.setFont('OpenSansLight', 'normal');
          } else {
            this.doc.setFont('OpenSansBold', 'bold');
          }
          if (j % 2 === 0) {
            if (boldOpen) {
              this.doc.setFont('OpenSansBold', 'bold');
            } else {
              this.doc.setFont('OpenSansLight', 'normal');
            }
          }
          this.doc.text(textItems, startX, startY);
          startX = startX + this.doc.getStringUnitWidth(textItems) * fontSize;
        });
        boldOpen = this.isBoldOpen(arrayOfNormalAndBoldText.length, boldOpen);
        startX = startXCached;
        startY += lineSpacing;
      }
    });
  }
  private setFontMultilineBold(boldOpen: boolean) {
    if (!boldOpen) {
      this.doc.setFont('OpenSansBold', 'bold');
    } else {
      this.doc.setFont('OpenSansLight', 'normal');
    }
  }
  private  isBoldOpen(arrayLength, valueBefore = false) {
    const isEven = arrayLength % 2 === 0;
    return valueBefore !== isEven;
  }

  async printTransactionExpenseDetail(transactions: ViewTransaction[], titles: string [], account: Bank) {
    this.doc = new jsPDF('p', 'mm', 'letter', true);
    let posStart = 18;
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(12);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    let finalY = 24;
    for (const [index, transaction] of transactions.entries()) {
      // @ts-ignore
      if (this.doc.previousAutoTable.finalY) {
        // @ts-ignore
        finalY = this.doc.previousAutoTable.finalY;
      }
      if ((finalY + 10) > 240) {
        finalY = 24;
        this.doc.addPage();
      }
      this.doc.setFont('OpenSansLight', 'normal');
      this.doc.setFontSize(8);
      this.doc.text(`Transaccion Código: ${transaction.transaction_transactionCode}`, 14, finalY + 10);
      this.doc.text(`Transaccion Fecha: ${this.pipeDate.transform(transaction.transaction_date)}`, 14, finalY + 14);
      this.doc.text(`Transaccion Monto: ${this.pipeMoney.transform(transaction.transaction_amount)}`, 14, finalY + 18);
      if (transaction.transaction_gloss) {
        this.doc.text(`Transaccion Detalle: ${transaction.transaction_gloss}`, 14, finalY + 22);
      }
      // @ts-ignore
      this.doc.autoTable({
        html: '#table' + index,
        startY: finalY + (transaction.transaction_gloss ? 24 : 20),
        margin: {
          top: 30,
          left: 15,
          right: 15,
          bottom: 15
        },
        theme: 'plain',
        tableLineColor: [191, 191, 191],
        styles: {
          cellPadding: 0.5,
          lineColor: [191, 191, 191],
          lineWidth: 0.1,
          fontSize: 7
        },
        headStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansBold'
        },
        bodyStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansLight'
        },
        columnStyles: {
          0: {
            cellWidth: 20,
          },
          1: {
            cellWidth: 20,
          },
          3: {
            halign: 'right',
            cellWidth: 20,
          },
          4: {
            halign: 'right',
            cellWidth: 20
          },
        },
      });
    }
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }

  async printFormWatter(titles: any[], tables: any[]) {
    let posStart = 18;
    this.doc = new jsPDF({format: 'letter'});
    await this.addFonts(this.doc);
    this.doc.setFont('OpenSansBold', 'bold');
    this.doc.setFontSize(12);
    for (const t of titles) {
      // @ts-ignore
      this.doc.textCenter(t, {align: 'center'}, 0, posStart);
      posStart += 6;
    }
    let finalY = 30;
    for (const data of tables) {
      // @ts-ignore
      if (this.doc.previousAutoTable.finalY) {
        // @ts-ignore
        finalY = this.doc.previousAutoTable.finalY;
      }
      this.doc.setFont('OpenSansBold', 'bold');
      this.doc.setFontSize(10);
      this.doc.text(data.title, 14, finalY + 10);
      this.doc.setFont('OpenSansLight');
      // @ts-ignore
      this.doc.autoTable({
        startY: finalY + 15,
        head: [
          data.head,
        ],
        body: data.body,
        margin: {
          top: 30,
          left: 15,
          right: 15,
          bottom: 15
        },
        theme: 'plain',
        tableLineColor: [191, 191, 191],
        styles: {
          cellPadding: 0.5,
          lineColor: [191, 191, 191],
          lineWidth: 0.1,
          fontSize: 9
        },
        headStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansBold'
        },
        bodyStyles: {
          fillColor: [255, 255, 255],
          textColor: 0,
          font: 'OpenSansLight'
        }
      });
    }
    this.insertHeaderAndFooter(this.doc);
    this.outputPrint();
  }
}




