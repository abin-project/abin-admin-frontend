import * as moment from 'moment';

export function compare(a: any, b: any, isAsc: boolean) {
  if (!isNaN(a) && !isNaN(b)) {
    return (Number(a) < Number(b) ? -1 : 1) * (isAsc ? 1 : -1);
  } else {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}

export function compareDate(a: any, b: any, isAsc: boolean) {
  return (moment(a) < moment(b) ? -1 : 1) * (isAsc ? 1 : -1);
}

export function toUpperCase(value: string) {
  if (typeof value === 'string') {
    return value.toUpperCase();
  } else {
    return '';
  }
}

export function toDecimal(value: string | number) {
  if (value) {
    return Number(value).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
  } else {
    return Number(0).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
  }
}

export function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

export function increment(value: number | string) {
  return Number(value) + 1;
}

export function roundTo(value, places){
  const power = Math.pow(100, places);
  return Math.round(value * power) / power;
}

export function formatNumber(value) {
  return Number(value).toFixed(2) // always two decimal digits
    .replace('.', ',') // replace decimal point character with ,
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

