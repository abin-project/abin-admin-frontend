import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  public async exportAsExcelFile(json: any[], excelFileName: string, lengthTitles: number, merges?: any[]) {

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    workbook.Sheets.data['!merges'] = [{s: {c: 0, r: 0}, e: {r: 0, c: (lengthTitles - 1)}}];

    if (merges && merges.length > 0) {
      workbook.Sheets.data['!merges'] = workbook.Sheets.data['!merges'].concat(merges);
    }

    workbook.Sheets.data.A1.v = excelFileName;
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  public async exportAsExcelFileByTable(json: any[], excelFileName: string, tableName) {

    const workbook = XLSX.utils.book_new();
    const ws1 = XLSX.utils.table_to_sheet(document.getElementById(tableName));
    XLSX.utils.book_append_sheet(workbook, ws1, 'Sheet1');
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  public async exportAsExcelManyTable(excelFileName: string, titles: any[]) {

    const workbook = XLSX.utils.book_new();
    for (let i = 0; i < titles.length; i++) {
      const ws1 = XLSX.utils.table_to_sheet(document.getElementById('table' + i));
      XLSX.utils.book_append_sheet(workbook, ws1, titles[i]);
    }
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  public exportAsExcelByJson(excelFileName, titles: string[], object: any[]) {
    const workbook = XLSX.utils.book_new();

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < titles.length; i++) {

      const ws1 = XLSX.utils.json_to_sheet(object[i], { cellDates: true });
      XLSX.utils.book_append_sheet(workbook, ws1, titles[i]);
    }
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, `${fileName}${EXCEL_EXTENSION}`);
  }
}
