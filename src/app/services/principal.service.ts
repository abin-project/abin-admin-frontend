import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {
  protected server = environment.server;
  protected route = '';

  constructor(protected http: HttpClient) {
  }

  getPartialRoute() {
    return `${this.server}/${this.route}`;
  }

  get(id?) {
    return this.http.get(`${this.server}/${this.route}${id ? '/' + id : ''}`);
  }

  getFile(params) {
    return this.http.get(`${this.server}/${this.route}${params ? '/' + params : ''}`);
  }

  put(id, data) {
    return this.http.put(`${this.server}/${this.route}/${id}`, data);
  }

  post(data) {
    return this.http.post(`${this.server}/${this.route}`, data);
  }

  delete(id) {
    return this.http.delete(`${this.server}/${this.route}/${id}`);
  }

  getCustom(param) {
    return this.http.get(`${this.server}/${this.route}/${param}`);
  }

  getQuery?(param = null, options: any) {
    if (param)
      return this.http.get(`${this.server}/${this.route}/${param}`, {params: options});
    else
      return this.http.get(`${this.server}/${this.route}`, {params: options});
  }

  putQuery?(param, data, options: any) {
    // const params = new HttpParams();
    // for (let key in options) {
    //   params.append(key, options[key]);
    // }
    return this.http.put(`${this.server}/${this.route}/${param}`, data, {params: options});
  }

  postCustom(param, data) {
    return this.http.post(`${this.server}/${this.route}/${param}`, data);
  }

  postFile(param, data) {
    const h = new HttpHeaders();
    h.append('Content-Type', 'multipart/form-data');
    h.append('Accept', 'application/json');

    return this.http.post(`${this.server}/${this.route}/${param}`, data, {headers: h});
  }

  putFile(param, data) {
    const h = new HttpHeaders();
    h.append('Content-Type', 'multipart/form-data');
    h.append('Accept', 'application/json');

    return this.http.put(`${this.server}/${this.route}/${param}`, data, {headers: h});
  }

  getUrlFile(param) {
    return `${this.server}/${this.route}/${param}`;
  }
}
