import {Pipe, PipeTransform} from '@angular/core';
import {ViewIncomeExtract} from '../model/hotizontal-property/viewIncomeExtract';
@Pipe({
  name: 'transactionColor'
})
export class TransactionColorPipe implements PipeTransform{
  transform(value: ViewIncomeExtract, text = true): string {
    if (text) {
      if (value.incomeExtract_state === 0) {
        return '#3367d6';
      } else if (value.incomeExtract_state === 1) {
        if (value.incomeExtract_amount > 0) {
          return '#ce9b00';
        } else {
          return '';
        }
      } else if ( value.incomeExtract_state === 2) {
        return '#ff2525';
      } else if (value.incomeExtract_state === 3) {
        if (value.incomeExtract_transactionDestineId) {
          return '';
        }
        return '#208620';
      } else if (value.incomeExtract_state === 4) {
        return '#3367d6';
      } else {
        return '';
      }
    } else {
      if (value.incomeExtract_state === 0) {
        return '#FFF'; // blanco
      } else if (value.incomeExtract_state === 1) {
        if (value.incomeExtract_amount > 0) {
          return 'rgb(255, 245, 215)'; // amarillo
        } else {
          return '#FFF'; // blanco
        }
      } else if ( value.incomeExtract_state === 2) {
        return 'rgb(255, 228, 224)'; // rojo
      } else if (value.incomeExtract_state === 3) {
        if (value.incomeExtract_transactionDestineId) {
          return '';
        }
        return 'rgb(214, 235, 215)'; // verde

      } else if (value.incomeExtract_state === 4) {
        return 'rgb(222, 234, 255)'; // azul
      } else {
        return '';
      }
    }
  }
}
