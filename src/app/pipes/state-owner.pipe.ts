import { Pipe, PipeTransform } from '@angular/core';
import {
  $getOwnerStates
} from '../components/manager-horizontal-property/directory/create-position-owner/create-position-owner.component';

@Pipe({
  name: 'stateOwner'
})
export class StateOwnerPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    // tslint:disable-next-line:triple-equals
    return $getOwnerStates().find(e => e.value == value)?.state;
  }

}
