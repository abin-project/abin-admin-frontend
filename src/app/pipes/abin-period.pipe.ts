import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment-timezone';
moment.locale('es');

@Pipe({
  name: 'abinPeriod'
})
export class AbinPeriodPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    const date = moment(value);
    return date.isValid() ? date.tz('America/La_Paz').format('YYYY-MMMM').toUpperCase() : 'No Actualizado';
  }

}
