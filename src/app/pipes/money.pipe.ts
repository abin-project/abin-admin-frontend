import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dataMoney'
})
export class MoneyPipe implements PipeTransform {

  transform(value: number, params?): string {
    if (value && !params) {
      // return Number(value).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
      return Number(value).toFixed(2) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    } else if (value && params) {
      // return Math.abs(Number(value)).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
      return Math.abs(Number(value)).toFixed(2) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    } else {
      // return Number(0).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
      return Number(0).toFixed(2) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
  }

}
