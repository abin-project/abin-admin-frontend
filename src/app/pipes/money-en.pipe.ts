import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dataMoneyEn'
})
export class MoneyEnPipe implements PipeTransform {

  transform(value: number, format: string): string {
    if (format === 'es') {
      if (!isNaN(value) && format) {
        return Number(value).toFixed(2) // always two decimal digits
          .replace('.', ',') // replace decimal point character with ,
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      } else {
        return Number(0).toFixed(2) // always two decimal digits
          .replace('.', ',') // replace decimal point character with ,
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');      }
    }
    if (format === 'en') {
      if (!isNaN(value) && format) {
        return Number(value).toFixed(2); // always two decimal digits
      } else {
        return Number(0).toFixed(2); // always two decimal digits
      }
    }
  }

}
