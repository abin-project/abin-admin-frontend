import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'watter'
})
export class WatterPipe implements PipeTransform {

  transform(value: number, params?): string {
    if (value && !params) {
      // return Number(value).toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2});
      const a = 0;
      return (Number(value) + Number(a)).toFixed(3) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
  }

}
