import {Pipe, PipeTransform} from '@angular/core';
import {ViewIncomeExtract} from '../model/hotizontal-property/viewIncomeExtract';

@Pipe({
  name: 'stateIncome'
})
export class StateIncomePipe implements PipeTransform {

  transform(value: ViewIncomeExtract): string {
    if (value.incomeExtract_state === 0) {
      return 'Procesado';
    } else if (value.incomeExtract_state === 1) {
       if (value.incomeExtract_amount > 0) {
        return 'Identificado';
      } else {
        return 'Egreso';
      }
    } else if ( value.incomeExtract_state === 2) {
      return 'No Identificado';
    } else if (value.incomeExtract_state === 3) {
      if (value.incomeExtract_transactionDestineId) {
        return 'Transferencia';
      }
      return 'Ignorado';
    } else if (value.incomeExtract_state === 4) {
      return 'Ingreso';
    } else {
      return '';
    }
  }

}
