import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'dateTime'
})
export class DateTimePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    const date = moment(value);
    return date.isValid() ? date.tz('America/La_Paz').format('DD-MMM-YYYY, HH:mm:ss') : 'No Actualizado';
  }

}
