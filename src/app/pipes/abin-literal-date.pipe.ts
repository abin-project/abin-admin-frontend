import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'abinLiteralDate'
})
export class AbinLiteralDatePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const date = moment(value);
    return date.isValid() ? date.tz('America/La_Paz').format('dddd DD , MMMM , YYYY')
      .replace(',', 'de')
      .replace(',', 'de') : 'No Actualizado';

  }

}
