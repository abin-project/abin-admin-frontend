import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'abinDate'
})
export class AbinDatePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): any {
    const date = moment(value);
    return date.isValid() ? date.tz('America/La_Paz').format('DD-MMM-YYYY') : 'No Actualizado';
  }

}
