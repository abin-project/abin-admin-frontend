import { Bank } from './hotizontal-property/bank';

export interface QrModel {
  alias: string;
  detalleGlosa: string;
  fechaVencimiento: string;
  monto: number;
  configDebts: any;
  accountBankId: string;
  propertyId: string;
}


export interface QrResponse {
  id: string;
  codigo: string;
  mensaje: string;
  imagenQr: any;
  idQr: any;
  fechaVencimiento: any;
  bancoDestino: any;
  cuentaDestino: any;
  idTransaccion: any;
  configDebts: any;
  pay: boolean;
  process: boolean;
  createdAt: Date;
  updatedAt: Date;

}


