export interface Tenant {
  id?: string;
  nameTenant: string;
  telephoneTenant?: number;
  cellTenant?: number;
  emailTenant?: string;
  state?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
