import {Project} from './project';
import {TypeExpense} from './typeExpense';

export interface ProjectItemExpense {
  id?: string;
  project: Project;
  typeExpense: TypeExpense;
  amountMonth: number;
  amountYear: number;
  detail: string;
  createdAt?: Date;
  updatedAt?: Date;
}
