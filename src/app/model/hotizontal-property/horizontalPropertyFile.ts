import {HorizontalProperty} from './horizontalProperty';

export interface HorizontalPropertyFile {
  id: string;
  fileName: string;
  fileNameOriginal: string;
  group?: 'Estatuto' | 'Reglamento' | 'Resolucion';
  publish?: any;
  public?: boolean;
  type?: any;
  horizontalProperty: HorizontalProperty;
  createdAt?: any;
  updatedAt?: any;
}
