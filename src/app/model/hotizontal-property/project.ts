import {HorizontalProperty} from './horizontalProperty';
import {ProjectItemExpense} from './projectItemExpense';
import {ProjectItemIncome} from './projectItemIncome';

export interface Project {
  id?: string;
  yearIni: string;
  yearEnd: string;
  incrementExpense?: number;
  incrementSure?: number;
  percentIncrementExpense?: boolean;
  percentIncrementSure?: boolean;
  state?: boolean;
  isComplete?: boolean;
  withError?: boolean;
  horizontalProperty?: HorizontalProperty;
  projectItemExpenses?: ProjectItemExpense[];
  projectItemIncomes?: ProjectItemIncome[];
  createdAt?: Date;
  updatedAt?: Date;
}
