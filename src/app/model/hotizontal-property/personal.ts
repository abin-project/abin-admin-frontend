import { HorizontalProperty } from './horizontalProperty';

export interface Personal {
    id?: string;
    name: string;
    cell: number;
    email: string;
    position: string;
    available: boolean;
    picture: string;
    pictureCI: string;
    fileBackground: string;
    horizontalProperty: HorizontalProperty;
    ci: string;
    createdAt?: Date;
    updatedAt?: Date;
}
