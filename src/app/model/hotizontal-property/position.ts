import {Owner} from './owner';

export interface Position {
  id?: string;
  name: string;
  owner: Owner;
  dateEnd: Date;
  dateIni: Date;
  createdAt?: Date;
  updatedAt?: Date;
  year?: number;
  ownerState?: string;
}
