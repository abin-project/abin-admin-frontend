import {Property} from './property';
import {HorizontalProperty} from './horizontalProperty';

export interface PropertyGroup {
  id?: string;
  name: string;
  properties?: Property[];
  horizontalProperty: HorizontalProperty;
  createdAt?: Date;
  updatedAt?: Date;
}
