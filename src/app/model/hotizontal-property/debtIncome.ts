import {Debt} from './debt';
import {Transaction} from './transaction';

export interface DebtIncome {
  id: string;
  amount: number;
  debt: Debt;
  datePay?: string;
  debtState: boolean;
  transaction: Transaction;
  createdAt: Date;
  updatedAt: Date;
}
