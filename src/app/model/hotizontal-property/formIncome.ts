import {Project} from './project';
import {TypeIncome} from './typeIncome';
import {Debt} from './debt';
import {FormIncomeProperty} from './formIncomeProperty';

export interface FormIncome {
  id?: string;
  period: any;
  ofPeriod: any;
  amountPeriod: number;
  project: Project;
  typeIncome: TypeIncome;
  amountM3: number;
  debts: Debt[];
  open: boolean;
  totalPay?: number;
  nameIncome?: string;
  formDateReader: string;
  formIncomeProperties: FormIncomeProperty[];
  createdAt?: Date;
  updatedAt?: Date;
}
