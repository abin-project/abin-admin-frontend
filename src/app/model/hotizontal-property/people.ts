import {Property} from './property';
import {Owner} from './owner';

export interface People {
  id?: string;

  name: string;

  state?: boolean;

  property?: Property;

  owner?: Owner;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
