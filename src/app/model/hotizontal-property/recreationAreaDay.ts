import {RecreationArea} from './recreationArea';

export interface RecreationAreaDay {
  id?: string;
  available: boolean;
  day: string;
  dayNumber: number;
  timeIni: string;
  timeEnd: string;
  recreationArea?: RecreationArea;
  createdAt?: Date;
  updatedAt?: Date;
}
