import {ProjectItemIncome} from './projectItemIncome';
import {ProjectModificationExpense} from './projectModificationExpense';

export interface ProjectModificationIncome {
  id?: string;
  projectItemIncome: ProjectItemIncome;
  projectModificationExpenses: ProjectModificationExpense[];
  amount: number;
  createdAt?: Date;
  updatedAt?: Date;
}
