import {HorizontalProperty} from './horizontalProperty';

export interface CalendarActivity{
  id: string;
  horizontalProperty?: HorizontalProperty;
  dateTimeActivity: string;
  allDay: boolean;
  activity: string;
  assignTo: string;
  timeIni: string;
  timeEnd: string;
  complete: boolean;
  color: string;
  createdAt: Date;
  updatedAt: Date;
}
