import {PeriodMonth} from './periodMonth';

export interface Kardex {
  period: any;
  periods: PeriodMonth;
}
