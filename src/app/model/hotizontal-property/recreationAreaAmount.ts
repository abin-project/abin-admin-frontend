import {RecreationArea} from './recreationArea';

export interface RecreationAreaAmount {
  id?: string;
  peoplesIni: number;
  peoplesEnd: number;
  amount: number;
  guarantee: number;
  recreationArea?: RecreationArea;
}
