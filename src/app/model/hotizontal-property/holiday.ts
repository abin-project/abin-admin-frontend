import {HorizontalProperty} from './horizontalProperty';

export interface Holiday {
  id?: string;
  day: number;
  month: number;
  year: number;
  detail: string;
  horizontalProperty: HorizontalProperty;
}
