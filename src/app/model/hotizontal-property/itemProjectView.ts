import {Item} from './item';
import {TypeExpense} from './typeExpense';
import {TypeIncome} from './typeIncome';

export interface ItemProjectView extends Item {
  typesExpenses?: TypeExpenseView[];
  months: PeriodMonth[];
  typesIncomes?: TypeIncomeView[];
}

export interface TypeExpenseView extends TypeExpense {
  amountMonth: number;
  amountYear: number;
  detail: string;
  months: PeriodMonth[];
  totalMonth: number;
  debt: number;
  modification?: number;
  totalModification?: number;
}

export interface TypeIncomeView extends TypeIncome {
  amountMonth: number;
  amountYear: number;
  detail: string;
  months: PeriodMonth[];
  totalMonth: number;
  debt: number;
  modification?: number;
  totalModification?: number;
}

export interface PeriodMonth {
  period: number;
  totalAmount: number;
}
