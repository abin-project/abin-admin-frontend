import {FormIncome} from './formIncome';

export interface FormIncomeReceipt {
  id?: string;
  receiptAmount: number;
  receiptFile: string | File;
  receiptOther: number;
  formIncome: FormIncome;
  createdAt?: Date;
  updatedAt?: Date;
}
