import {TypeIncomeProperty} from './typeIncome';
import {Property} from './property';
import {Owner} from './owner';
import {DebtIncome} from './debtIncome';
import {User} from '../User';

export interface Debt {
  id?: string;
  amount: number;
  date: Date;
  debtPayLimit: any;
  typeIncomeProperty: TypeIncomeProperty;
  property: Property;
  isLastDebt?: boolean;
  isManual?: boolean;
  detail?: string;
  detailVisible?: boolean;
  notPenalty?: boolean;
  user?: User;
  owner: Owner;
  debtIncomes: DebtIncome[];
  createdAt?: any;
  updatedAt?: any;
  recreationAreaReservationDetails?: any[]
}
