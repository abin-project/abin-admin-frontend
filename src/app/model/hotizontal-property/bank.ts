import {HorizontalProperty} from './horizontalProperty';
import {Income} from './income';
import {AccountHolder} from './accountHolder';

export interface Bank {
  id?: string;
  nameBank: string;
  typeAccount: string;
  numberAccount: string;
  amount: number;
  horizontalProperty?: HorizontalProperty;
  incomes?: Income[];
  createdAt?: Date;
  updateAt?: Date;
  state?: 'Vigente' | 'No Vigente';
  accountHolders?: AccountHolder[];
  nameOn?: string;
  title: string;
  qrUser?: string;
  qrPassword?: string;
  qrServiceKey?: string;
  qrUserApiKey?: string;
  qr?: boolean;
}
