import {HorizontalProperty} from './horizontalProperty';

export interface PeriodHorizontalProperty {
  id?: string;
  dateIni: Date;
  dateEnd: Date;
  state: boolean;
  horizontalProperty: HorizontalProperty;
  positions: Position[];
  createdAt?: Date;
  updatedAt?: Date;
}
