import {Property} from './property';

export interface ViewIncomeExtract {
  accountBank_nameBank: string;
  incomeExtract_agency: string;
  incomeExtract_amount: number;
  incomeExtract_createdAt: Date;
  incomeExtract_date: any;
  incomeExtract_gloss: string;
  incomeExtract_id: string;
  incomeExtract_reference: string;
  incomeExtract_state: number;
  incomeExtract_transactionCode: string;
  incomeExtract_transactionDestineId: string;
  incomeExtract_updatedAt: Date;
  userUpdated_email: string;
  incomeExtract_amount_debt?: number;
  accumulated?: number;
  property?: Property;
  incomeExtract_payOnline: boolean;
}
