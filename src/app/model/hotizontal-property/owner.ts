import {HorizontalProperty} from './horizontalProperty';
import {Property} from './property';

export class Owner {
  id?: string;
  nameOwner: string;
  telephoneOwner: number;
  cellOwner: number;
  emailOwner: string;
  password: string;
  state: boolean;
  horizontalProperty: HorizontalProperty;
  positionOwners: Position[];
  properties?: Property[];
  createdAt?: Date;
  updatedAt?: Date;
}
