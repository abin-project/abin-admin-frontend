import {FormIncome} from './formIncome';
import {PropertyType} from './propertyTypes';

export interface FormIncomeIncrements {
  id: string;
  increment: number;
  formIncome: FormIncome;
  propertyType: PropertyType;
  createdAt?: string;
  updatedAt?: string;
}
