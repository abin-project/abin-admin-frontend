import {ProjectItemExpense} from './projectItemExpense';
import {ProjectModificationIncome} from './projectModificationIncome';

export interface ProjectModificationExpense {
  id?: string;
  projectItemExpenseOrigin: ProjectItemExpense;
  projectItemExpenseDestine: ProjectItemExpense;
  projectModificationIncome?: ProjectModificationIncome;
  amount: number;
  createdAt?: Date;
  updatedAt?: Date;
}
