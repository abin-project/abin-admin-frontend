export interface Ledger {
  date: string;
  receipt: string;
  gloss: string;
  amount: number;
  debt: number;
}
