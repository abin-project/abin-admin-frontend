import {HorizontalProperty} from './horizontalProperty';

export interface Task {
  id?: string;
  detail: string;
  state: boolean;
  horizontalProperty: HorizontalProperty;
  createdAt?: Date;
  updatedAt?: Date;
}
