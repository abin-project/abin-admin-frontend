import {TypeIncome} from './typeIncome';
import {Debt} from './debt';
import {Property} from './property';

export interface TypeIncomeProperty {
  id?: string;
  amount: number;
  debts: Debt[];
  property: Property;
  typeIncome: TypeIncome;
  createdAt?: Date;
  updatedAt?: Date;
}
