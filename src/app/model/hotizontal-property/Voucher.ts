import {Transaction} from './transaction';

export interface Voucher {
  id: string;
  codeVoucher: number;
  transaction: Transaction;
  createdAt?: Date;
  updatedAt?: Date;
}
