import {TypeExpense} from './typeExpense';
import {User} from '../User';
import {Transaction} from './transaction';

export interface RenderTransaction {
  id?: string;
  amount: number;
  gloss: string;
  receiptCode: string;
  stateRender: boolean;
  date: Date;
  typeExpense: TypeExpense;
  user: User;
  transaction: Transaction;
  file: any;
  createdAt?: Date;
  updatedAt?: Date;
}
