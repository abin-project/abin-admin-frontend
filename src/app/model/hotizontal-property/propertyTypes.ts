import {Property} from './property';
import {HorizontalProperty} from './horizontalProperty';

export interface PropertyTypeDefault {
  id?: string;
  name: string;
  type: boolean;
  amount?: number;
  code?: number;
  active?: boolean;
  properties?: Property[];
  createdAt?: Date;
  updatedAt?: Date;
}

export interface PropertyType extends PropertyTypeDefault {
  properties: Property[];
  amount: number;
  horizontalProperty: HorizontalProperty;
}

