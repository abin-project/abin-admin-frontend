import {Bank} from './bank';
import {DebtIncome} from './debtIncome';
import {User} from '../User';
import {Voucher} from './Voucher';

export interface Transaction {
  id: string;
  date: Date;
  agency: string;
  gloss: string;
  reference: string;
  transactionCode: string;
  amount: number;
  balance?: number;
  totalRender?: number;
  state: number;
  accountBank: Bank;
  extractAccountFile: any;
  configDebts?: {
    debts: string[],
    onlyDebts: string[]
  };
  voucher: Voucher,
  debtIncomes: DebtIncome[];
  userUpdated: User;
}
