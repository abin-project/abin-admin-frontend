import {HorizontalProperty} from './horizontalProperty';
import {RecreationAreaAmount} from './recreationAreaAmount';
import {RecreationAreaDay} from './recreationAreaDay';
import {RecreationAreaReservation} from './recreationAreaReservation';

export interface RecreationArea {
  id?: string;
  name: string;
  unity: string;
  amount: number;
  horizontalProperty: HorizontalProperty;
  timeIni?: string;
  timeEnd?: string;
  peoplesLimit: number;
  color?: string;
  recreationAreaAmounts: RecreationAreaAmount[];
  recreationAreaDays: RecreationAreaDay[];
  recreationAreaReservation: RecreationAreaReservation[];
  isInvited: boolean;
  usersLimit: number;
  amountGuarantee: number;
  isTimeUseLimit: boolean;
  timeUseLimit: number;
  typeReservation: string| 'Dia'|'Hora'|'Mes';
  // sunday: boolean;
  // monday: boolean;
  // tuesday: boolean;
  // wednesday: boolean;
  // thursday: boolean;
  // friday: boolean;
  // saturday: boolean;
  generateReceipt: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
