import {FormIncome} from './formIncome';
import {Property} from './property';
import {DebtLiquidation} from '../../components/manager-horizontal-property/property/liquidation/liquidation.component';

export interface FormIncomeProperty {
  id?: string;
  lastReader: number;
  actualReader: number;
  inLastForm: boolean;
  consumption: number;
  amountPay: number;
  formIncome: FormIncome;
  property: Property;
  createdAt: Date;
  updatedAt: Date;
  debt?: DebtLiquidation;
}
