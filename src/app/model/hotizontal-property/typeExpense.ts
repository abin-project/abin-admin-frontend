import {DefaultItem, Item} from './item';
import {RenderTransaction} from './renderTransaction';

export interface DefaultTypeExpense {
  id?: string;
  code?: number;
  inGroup: number;
  nameExpense: string;
  activeExpense: boolean;
  item: DefaultItem;
  detail: string;
  createdAt?: Date;
  updatedAt?: Date;
}


export interface TypeExpense extends DefaultTypeExpense {
  item: Item;
  renderTransactions?: RenderTransaction[];
}
