import {Bank} from './bank';
import {Owner} from './owner';

export interface AccountHolder {
  id: string;
  accountBank: Bank;
  owner: Owner;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
