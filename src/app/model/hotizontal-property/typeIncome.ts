import {DefaultItem, Item} from './item';
import {Income} from './income';
import {Property} from './property';
import {Debt} from './debt';

export interface DefaultTypeIncome {
  id?: string;
  code?: number;
  nameIncome: string;
  activeIncome: boolean;
  item: DefaultItem;
  isGlobal?: boolean;
  amount?: number;
  detail: string;
  priority: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface TypeIncome extends DefaultTypeIncome {
  id?: string;
  item: Item;
  incomes?: Income[];
  typeIncomeProperty: TypeIncomeProperty[];
}

export interface TypeIncomeProperty {
  id?: string;
  debts: Debt[];
  property: Property;
  amount: number;
  typeIncome: TypeIncome;
}
