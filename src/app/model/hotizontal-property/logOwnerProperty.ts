import {Owner} from './owner';
import {Tenant} from './tenant';
import {Property} from './property';
import {HorizontalProperty} from './horizontalProperty';

export interface LogOwnerProperty {
  id?: string;
  owner?: Owner;
  tenantId: number;
  tenant?: Tenant;
  property?: Property;
  horizontalProperty: HorizontalProperty;
  deletedAt?: Date;
  createdAt: Date;
  updatedAt: Date;
}
