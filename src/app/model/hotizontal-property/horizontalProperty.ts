import {PropertyType} from './propertyTypes';
import {Validators} from '@angular/forms';

export interface HorizontalProperty {
  id?: string;
  nameProperty: string;
  dateExpenseLimit: number;
  dateManagementInit: Date;
  departments: number;
  commercialPremises: number;
  offices: number;
  houses: number;
  state: boolean;
  disabled: boolean;
  expiredMonth?;
  expiredMonthLimit?;
  dayPayInit: number;
  dayPayLimit: number;
  monthPayLimit: number;
  day: number;
  horizontalPropertiesUsers?: any[];
  propertyTypes?: PropertyType[];
  items?: any[];
  coOwners?: any[];
  createdAt?: Date;
  updatedAt?: Date;
}
