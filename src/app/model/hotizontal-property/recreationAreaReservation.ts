import {RecreationArea} from './recreationArea';
import {Property} from './property';

export interface RecreationAreaReservation {
  id?: string;
  date: Date;
  recreationArea: RecreationArea;
  recreationAreaReservationDetail: RecreationAreaReservationDetail;
  allDay: boolean;
  time: any;
  createdAt?: Date;
  updatedAt?: Date;
}
export interface RecreationAreaReservationDetail {
  id?: string;
  amount: number;
  guarantee: number;
  state: string;
  property: Property;
  recreationAreaReservations: RecreationAreaReservation[];
  createdAt?: Date;
  updatedAt?: Date;
}
