import {HorizontalProperty} from './horizontalProperty';
import {Owner} from './owner';
import {Tenant} from './tenant';
import {LogOwnerProperty} from './logOwnerProperty';
import {TypeIncomeProperty} from './typeIncome';
import {PropertyGroup} from './propertyGroup';
import {PropertyType} from './propertyTypes';

export interface Property {
  id?: string;
  code: string;
  name?: string;
  m2: number;
  type: string;
  expense: number;
  horizontalProperty: HorizontalProperty;
  owner: Owner;
  typeIncomeProperty?: TypeIncomeProperty[],
  log?: LogOwnerProperty;
  tenant?: Tenant;
  propertyId?: string;
  property?: Property;
  complements?: any[];
  group?: PropertyGroup;
  createdAt?: Date;
  updatedAt?: Date;
  typeProperty?: PropertyType;
  dateInitPay?: Date;
}

export interface RawProperty {
  owner_id: string;
  owner_nameOwner: string;
  owner_telephoneOwner: string;
  owner_cellOwner: string;
  owner_emailOwner: string;
  tenant_id: string;
  tenant_nameTenant: string
  tenant_emailTenant: string
  tenant_telephoneTenant: string
  tenant_cellTenant: string;
  property_code: string;
  property_expense: number;
  property_horizontalPropertyId: string;
  property_id: string;
  property_m2: number;
  property_name: string;
  property_ownerId: string;
  property_tenantId: string;
  property_type: string;
}
