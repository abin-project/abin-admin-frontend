import {HorizontalProperty} from './horizontalProperty';
import {Income} from './income';

export interface PeriodMonth {
  id?: string;
  monthLiteral: string;
  monthNum: number;
  horizontalProperty: HorizontalProperty;
  incomes: Income[];
  year?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
