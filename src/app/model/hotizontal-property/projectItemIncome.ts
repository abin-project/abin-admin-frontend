import {Project} from './project';
import {TypeIncome} from './typeIncome';

export interface ProjectItemIncome {
  id?: string;
  project: Project;
  typeIncome: TypeIncome;
  amountMonth: number;
  amountYear: number;
  detail: string;
  createdAt?: Date;
  updatedAt?: Date;
}
