import {Property} from './property';
import {Owner} from './owner';
import {HorizontalProperty} from './horizontalProperty';

export interface CoOwnerProperty {
  id?: string;
  coOwnerId: number;
  propertyId: number;
  coOwner?: Owner;
  property?: Property;
  horizontalProperty?: HorizontalProperty;
  createdAt?: Date;
  updatedAt?: Date;

}
