import {HorizontalProperty} from './horizontalProperty';
import {DefaultTypeExpense, TypeExpense} from './typeExpense';
import {DefaultTypeIncome, TypeIncome} from './typeIncome';

export interface DefaultItem {
  id?: string;
  nameItem: string;
  type: string;
  detail: string;
  typesExpenses?: DefaultTypeExpense[];
  typesIncomes?: DefaultTypeIncome[];
  createdAt?: Date;
  updatedAt?: Date;
}


export interface Item extends DefaultItem {
  id?: string;
  nameItem: string;
  type: string;
  detail: string;
  typesExpenses?: TypeExpense[];
  typesIncomes?: TypeIncome[];
  total?: number;
  horizontalProperty: HorizontalProperty;
  createdAt?: Date;
  updatedAt?: Date;
}
