import {TypeIncome} from './typeIncome';
import {Owner} from './owner';
import {Bank} from './bank';
import {PeriodMonth} from './periodMonth';
import {Property} from './property';

export interface Income {
  id?: string;
  amount: number;
  code: number;
  date: Date;
  accountBank: Bank;
  typeIncome: TypeIncome;
  owner: Owner;
  property: Property;
  periodMonth: PeriodMonth;
  createdAt?: Date;
  updatedAt?: Date;
}
