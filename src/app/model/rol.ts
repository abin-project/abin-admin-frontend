import {User} from './User';

export interface Rol {
  id?: string;
  rolName: 'Root'|'Administrador del Sistema'|'Administrador'|'Directorio'|'Co-Propietario';
  users: User[];
}

