import {Rol} from './rol';
import {Validators} from '@angular/forms';

export class User {
  id?: string;
  email: string;
  cell: string;
  username: string;
  password?: string;
  roles: Rol[];
  picture?: string;
  pictureCI?: string;
  cv?: string;
  location?: string;
  locationMap?: any;
  defaultPassword?: false;
  deletedAt?: any;
  updatedAt?: any;
  createdAt?: any;
}
