export interface QueryTransaction {
  type?: 'income' | 'expense';
  year?: number;
  month?: number;
  accountBank?: string;
  amount?: number;
  minAmount?: number;
  maxAmount?: number;
  dateIni?: string;
  dateEnd?: string;
  onlyExpense?: boolean;
}
