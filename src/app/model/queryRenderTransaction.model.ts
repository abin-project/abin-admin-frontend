export interface QueryRenderTransaction {
  typeExpenseId?: string;
  dateIni?: any;
  dateEnd?: any;
  accountBankId?: any;
  isAll?: any;
  view?: string;
}
