import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {AuthGuard} from './guards/auth.guard';
import {AuthLoadChildGuard} from './guards/auth-load-child.guard';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptorService} from './services/authInterceptor.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {AppRoutingModule} from './app-routing.module';
import {LoaderService} from './services/loader.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {ReportVisorComponent} from './components/report-visor/report-visor.component';
import {MAT_PAGINATOR_DEFAULT_OPTIONS, MatPaginatorIntl} from '@angular/material/paginator';
import {getDutchPaginatorIntl} from './config/dutch-paginator-intl';
import {DataGlobalService} from './services/data-global.service';
import {TaskService} from './services/task.service';
import {PrintService} from './services/print.service';
import {ExcelService} from './services/excel.service';
import {WatterPipe} from './pipes/watter.pipe';
import {AbinDatePipe} from './pipes/abin-date.pipe';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {MoneyPipe} from './pipes/money.pipe';
import {MoneyEnPipe} from './pipes/money-en.pipe';
import { StateOwnerPipe } from './pipes/state-owner.pipe';
import { DateTimePipe } from './pipes/date-time.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ReportVisorComponent,
    StateOwnerPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    AppRoutingModule,
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es_ES'},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true, disableClose: true}},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl()},
    {provide: MAT_PAGINATOR_DEFAULT_OPTIONS, useValue: {pageSizeOptions: [50, 100, 200]}},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    LoaderService,
    TaskService,
    PrintService,
    DataGlobalService,
    WatterPipe,
    MoneyPipe,
    MoneyEnPipe,
    AbinDatePipe,
    DateTimePipe,
    ExcelService,
    AuthGuard,
    AuthLoadChildGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
