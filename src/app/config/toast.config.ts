import {MatSnackBarConfig} from '@angular/material/snack-bar';

export const SNACK_CONFIG: MatSnackBarConfig = {
  duration: 2000,
  data: null,
};
