import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SendCodeEmailComponent} from './send-code-email/send-code-email.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ChangeDefaultPasswordComponent} from './chage-default-password/change-default-password.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'changePassword', component: ChangeDefaultPasswordComponent},
  {path: 'sendEmail', component: SendCodeEmailComponent},
  {path: 'resetPassword', component: ResetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
