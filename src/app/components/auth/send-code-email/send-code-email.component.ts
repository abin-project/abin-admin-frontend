import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';

@Component({
  selector: 'abin-send-code-email',
  templateUrl: './send-code-email.component.html',
  styleUrls: ['./send-code-email.component.css']
})
export class SendCodeEmailComponent implements OnInit {
  reset: FormGroup;
  c = SNACK_CONFIG;

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private snack: MatSnackBar) {
    this.reset = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
    });
    this.c.data = 'Llene los campos requeridos';
  }

  ngOnInit() {
  }

  send() {
    if (this.reset.valid) {
      this.authService.requestResetPassword(this.reset.value).subscribe(() => {
        localStorage.setItem('email', this.reset.get('email').value);
        this.router.navigate(['/auth/resetPassword']);
      });
    } else {
      this.snack.openFromComponent(SnackErrorComponent, this.c);
    }
  }

  cancel() {
    this.router.navigate(['/auth/login']);
  }
}
