import {PrincipalService} from '../../services/principal.service';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends PrincipalService {
  protected route = 'auth';

  login(user): Observable<any> {
    return this.postCustom('login', user).pipe(map((r: any) => {
      localStorage.setItem('token', r.data.access_token);
      return r;
    }));
  }

  logout() {
    localStorage.clear();
    return new Promise((resolve, reject) => {
      resolve(() => {
        // localStorage.removeItem('token');
        return true;
      });
      reject(() => {
        return new Error('error');
      });
    });
  }

  profile() {
    return this.getCustom('profile').pipe(map(d => {
      localStorage.setItem('profile', btoa(JSON.stringify(d)));
    }));
  }

  resetPassword(data) {
    return this.put('resetPassword', data);
  }

  requestResetPassword(data) {
    return this.postCustom('resetPassword', data);
  }

  changePassword(data) {
    return this.postCustom('changePassword', data);
  }
}
