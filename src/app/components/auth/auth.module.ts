import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {AuthRoutingModule} from './auth-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {AuthService} from './auth.service';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {SendCodeEmailComponent} from './send-code-email/send-code-email.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {AlertsModule} from '../alerts/alerts.module';
import {ChangeDefaultPasswordComponent} from './chage-default-password/change-default-password.component';

@NgModule({
  declarations: [
    LoginComponent,
    SendCodeEmailComponent,
    ResetPasswordComponent,
    ChangeDefaultPasswordComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    AlertsModule,
  ],
  providers: [
    AuthService,
  ]
})
export class AuthModule {
}
