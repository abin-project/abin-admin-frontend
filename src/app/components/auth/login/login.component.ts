import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';

@Component({
  selector: 'abin-login-client',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  user: FormGroup;
  c = SNACK_CONFIG;

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router,
              private snack: MatSnackBar) {
    this.user = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, Validators.required]
    });
    this.c.data = 'Llene los campos requeridos';
  }

  ngOnInit() {
  }

  send() {
    this.user.get('email').setValue(this.user.get('email').value.replace(/\s/g, ''));
    if (this.user.valid) {
      const value = this.user.value;
      this.authService.login(value).subscribe(r => {
        if (r.data.default === true) {
          const params = btoa(JSON.stringify(this.user.value));
          this.router.navigate(['/auth/changePassword'], {
            queryParams: {
              k: params
            }
          });
        } else {
          this.authService.profile().subscribe(() => {
            this.router.navigate(['/horizontalProperty']);
          });
        }

      });
    } else {
      this.snack.openFromComponent(SnackErrorComponent, this.c);
    }
  }

  register() {
    this.router.navigate(['/auth/sendEmail']);
  }

}
