import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {AuthClientService} from '../auth-client.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';

@Component({
  selector: 'abin-change-default-password-client',
  templateUrl: './change-default-password-client.component.html',
  styleUrls: ['./change-default-password-client.component.css']
})
export class ChangeDefaultPasswordClientComponent implements OnInit {

  hide = true;
  hideL = true;
  hideN = true;
  user: FormGroup;
  c = SNACK_CONFIG;
  paramUser;

  constructor(private authService: AuthClientService,
              private fb: FormBuilder,
              private router: Router,
              private routerActive: ActivatedRoute,
              private snack: MatSnackBar) {
    this.routerActive.queryParams.subscribe(r => this.paramUser = r);
    this.paramUser = JSON.parse(atob(this.paramUser.k));
    this.user = this.fb.group({
      email: [this.paramUser.email, [Validators.email, Validators.required]],
      defaultPassword: [this.paramUser.password, Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      passwordRepeat: ['', Validators.compose([Validators.required])]
    }, {
      validator: MustMatch('password', 'passwordRepeat')
    });
  }

  ngOnInit() {
  }

  send() {
    if (this.user.valid) {
      this.authService.changePassword(this.user.value).subscribe(() => {
        // this.authService.login(this.user.value).subscribe(() => {
        this.authService.profile().subscribe(() => {
          this.router.navigate(['/horizontalProperty']);
        });
        // });
      });
    } else {
      this.c.data = 'Llene los campos requeridos';
      this.snack.openFromComponent(SnackErrorComponent, this.c);
    }
  }

  cancel() {
    this.router.navigate(['/client/login']);
  }


}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
}
