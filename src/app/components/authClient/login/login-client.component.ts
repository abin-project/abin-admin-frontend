import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthClientService} from '../auth-client.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';

@Component({
  selector: 'abin-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.css']
})
export class LoginClientComponent implements OnInit {
  hide = true;
  user: FormGroup;
  c = SNACK_CONFIG;

  constructor(private authService: AuthClientService, private fb: FormBuilder, private router: Router,
              private snack: MatSnackBar) {
    this.user = this.fb.group({
      email: [''.replace(' ', ''), [Validators.email, Validators.required]],
      password: [null, Validators.required]
    });
    this.c.data = 'Llene los campos requeridos';
  }

  ngOnInit() {
  }

  send() {
    if (this.user.valid) {
      this.authService.login(this.user.value).subscribe(r => {
        if (r.data.default === true) {
          const params = btoa(JSON.stringify(this.user.value));
          this.router.navigate(['/client/changePassword'], {
            queryParams: {
              k: params
            }
          });
        } else {
          this.authService.profile().subscribe(() => {
            this.router.navigate(['/horizontalProperty']);
          });
        }

      });
    } else {
      this.snack.openFromComponent(SnackErrorComponent, this.c);
    }
  }

  register() {
    this.router.navigate(['/client/sendEmail']);
  }

}
