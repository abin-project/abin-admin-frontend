import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginClientComponent} from './login/login-client.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {AuthClientRoutingModule} from './auth-client-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {AuthClientService} from './auth-client.service';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {SendCodeEmailClientComponent} from './send-code-email/send-code-email-client.component';
import {ResetPasswordClientComponent} from './reset-password/reset-password-client.component';
import {AlertsModule} from '../alerts/alerts.module';
import {ChangeDefaultPasswordClientComponent} from './chage-default-password/change-default-password-client.component';

@NgModule({
  declarations: [
    LoginClientComponent,
    SendCodeEmailClientComponent,
    ResetPasswordClientComponent,
    ChangeDefaultPasswordClientComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthClientRoutingModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    AlertsModule,
  ],
  providers: [
    AuthClientService,
  ]
})
export class AuthClientModule {
}
