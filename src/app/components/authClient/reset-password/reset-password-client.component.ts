import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthClientService} from '../auth-client.service';
import {Router} from '@angular/router';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-reset-password-client',
  templateUrl: './reset-password-client.component.html',
  styleUrls: ['./reset-password-client.component.css']
})
export class ResetPasswordClientComponent implements OnInit {
  hide = true;
  user: FormGroup;
  c = SNACK_CONFIG;

  constructor(private authService: AuthClientService,
              private fb: FormBuilder,
              private router: Router,
              private snack: MatSnackBar) {
    this.user = this.fb.group({
      email: [localStorage.getItem('email'), [Validators.email, Validators.required]],
      code: [null, Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      passwordRepeat: ['', Validators.compose([Validators.required])]
    }, {
      validator: MustMatch('password', 'passwordRepeat')
    });
  }

  ngOnInit() {
  }

  send() {
    if (this.user.valid) {
      this.authService.resetPassword(this.user.value).subscribe(() => {
        this.authService.login(this.user.value).subscribe(() => {
          this.authService.profile().subscribe(() => {
            this.router.navigate(['/horizontalProperty']);
          });
        });
      });
    } else {
      this.c.data = 'Llene los campos requeridos';
      this.snack.openFromComponent(SnackErrorComponent, this.c);
    }
  }

  cancel() {
    this.router.navigate(['/client/login']);
  }


}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
}
