import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginClientComponent} from './login/login-client.component';
import {SendCodeEmailClientComponent} from './send-code-email/send-code-email-client.component';
import {ResetPasswordClientComponent} from './reset-password/reset-password-client.component';
import {ChangeDefaultPasswordClientComponent} from './chage-default-password/change-default-password-client.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginClientComponent},
  {path: 'changePassword', component: ChangeDefaultPasswordClientComponent},
  {path: 'sendEmail', component: SendCodeEmailClientComponent},
  {path: 'resetPassword', component: ResetPasswordClientComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthClientRoutingModule {
}
