import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {HorizontalPropertyService} from '../horizontal-property.service';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {DataGlobalService} from '../../../services/data-global.service';
import * as moment from 'moment';

import {environment} from '../../../../environments/environment';
import {UserProfileComponent} from '../../admin-horizontal-property/user/user-profile/user-profile.component';
import {MatDialog} from '@angular/material/dialog';
import {RolService} from '../../admin-horizontal-property/rol.service';
import {TypeIncome} from '../../../model/hotizontal-property/typeIncome';
import {MessageSuggestionComponent} from '../../manager-horizontal-property/suggestion/message-suggestion/message-suggestion.component';
import {AuthClientService} from '../../authClient/auth-client.service';


@Component({
  selector: 'abin-horizontal-property',
  templateUrl: './horizontal-property.component.html',
  styleUrls: ['./horizontal-property.component.css']
})
export class HorizontalPropertyComponent implements OnDestroy, OnInit {
  mobileQuery: MediaQueryList;
  horizontalProperty: HorizontalProperty;
  user: any;
  isOpen = localStorage.getItem('open') ? Boolean(localStorage.getItem('open')) : true;
  textSearch = '';
  year = new Date().getFullYear();
  month = moment().format('M');
  version = environment.version;
  horizontalProperties: HorizontalProperty[];
  visible = false;
  watter: TypeIncome;
  hostAdmin = environment.admin;
  hostClient = environment.client;
  mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
              private horizontalPropertyService: HorizontalPropertyService,
              private activeRouter: ActivatedRoute,
              private authService: AuthService,
              private authClientService: AuthClientService,
              private dataService: DataGlobalService,
              private dataGlobalService: DataGlobalService,
              private dialog: MatDialog,
              private router: Router,
              public rolService: RolService) {
    this.mobileQuery = media.matchMedia('(max-width: 800px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    this.user = (localStorage.getItem('profile') ? JSON.parse(atob(localStorage.getItem('profile'))) : null);
    setTimeout(() => {
      this.isOpen = true;
    }, 250);

  }

  ngOnInit(): void {
    localStorage.removeItem('p');
    localStorage.removeItem('other');
    this.activeRouter.params.subscribe(params => {
      if (window.location.origin.includes(this.hostClient)) {
        this.authClientService.profileByProperty(params.id).subscribe(a => {
          // console.log('Cliente');
        });
      }
      this.horizontalPropertyService.getInfoHorizontalProperty(params.id).subscribe((r: any) => {
        this.horizontalProperty = r.data;
        this.watter = r.other.watter;
        localStorage.setItem('p', JSON.stringify(this.horizontalProperty));
        localStorage.setItem('other', JSON.stringify(r.other.watter));
      });
    });
    this.dataGlobalService.eventOpen.subscribe(r => {
      this.isOpen = r;
    });
    this.horizontalProperties = JSON.parse(atob(localStorage.getItem('properties'))) as HorizontalProperty[];
    if (this.horizontalProperties.length >= 5) {
      this.visible = true;
      this.horizontalProperties = this.horizontalProperties.slice(0, 5);
    }
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }


  logout() {
    this.authService.logout().then(() => {
      if (window.location.origin.includes(this.hostAdmin)) {
        this.router.navigate(['/auth/login']);
      } else if (window.location.origin.includes(this.hostClient)) {
        this.router.navigate(['/client/login']);
      }
    });
  }

  search() {
    this.dataService.setSearch(this.textSearch);
  }

  reviewText() {
    if (this.textSearch === '') {
      this.dataService.setSearch(this.textSearch);
    }
  }

  isOpenManager() {
    // return this.horizontalPropertyService.getStateNavigationOption('manager');
    // return this.router.url.includes('/property')
    //   || this.router.url.includes('/complement')
    //   || this.router.url.includes('/owner')
    //   || this.router.url.includes('/tenant')
    //   || this.router.url.includes('/items')
    //   || this.router.url.includes('/directory')
    //   || this.router.url.includes('/recreationArea')
    //   || this.router.url.includes('/files')
    //   || this.router.url.includes('/otherFiles');
  }

  expandManager(evt) {
    console.log(evt);
    // this.horizontalPropertyService.setStateNavigationOption('manager');
  }

  isDebtsActive() {
    return this.router.url.includes('/debts') || this.router.url.includes('/advanced');
  }

  isOpenItems() {
    return this.router.url.includes('/items/income')
      || this.router.url.includes('/items/expense');
  }

  isOpenIncome() {
    return this.router.url.includes('/kardex') || this.router.url.includes('/debtManual');
  }

  isTransactionActive() {
    return this.router.url.includes('/transaction?') ? 'active' : '';
  }

  setOpen() {
    this.dataGlobalService.setOpen();
  }

  openProfile() {
    this.dialog.open(UserProfileComponent, {
      width: '400px',
      data: {
        user: this.user
      }
    }).afterClosed().subscribe(r => {
      console.log(r);
    });
  }

  getLetterUser() {
    if (this.user && this.user.username) {
      return this.user.username.toUpperCase().charAt(0);
    } else if (this.user && this.user.nameOwner) {
      return this.user.nameOwner.toUpperCase().charAt(0);
    }
  }

  suggestion() {
    // this.eventCloseAll()
    this.dialog.open(MessageSuggestionComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalProperty.id
      }
    });
  }

  lastEvents() {
    this.setStateMenu('last');
    this.closedState('manager');
    this.closedState('item');
    this.closedState('report');
    this.closedState('income');
  }

  managerEvent() {
    this.setStateMenu('manager');
    this.closedState('item');
    this.closedState('last');
    this.closedState('report');
    this.closedState('income');
  }

  eventReport() {
    this.setStateMenu('report');
    this.closedState('manager');
    this.closedState('last');
    this.closedState('item');
    this.closedState('income');
  }

  eventCloseAll() {
    this.closedState('report');
    this.closedState('manager');
    this.closedState('last');
    this.closedState('item');
    this.closedState('income');
  }

  eventIncome() {
    this.setStateMenu('income');
    this.closedState('report');
    this.closedState('manager');
    this.closedState('last');
    this.closedState('item');
  }

  getStateMenu(key) {
    if (sessionStorage.getItem(key)) {
      return sessionStorage.getItem(key) === 'opened';
    } else {
      return false;
    }
  }

  setStateMenu(key) {
    if (sessionStorage.getItem(key)) {
      if (sessionStorage.getItem(key) === 'opened') {
        sessionStorage.setItem(key, 'closed');
      } else {
        sessionStorage.setItem(key, 'opened');
      }
    } else {
      sessionStorage.setItem(key, 'opened');
    }
  }
  closedState(key) {
    sessionStorage.setItem(key, 'closed');
  }
}
