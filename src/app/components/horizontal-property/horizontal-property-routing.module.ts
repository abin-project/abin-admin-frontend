import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HorizontalPropertyDashboardComponent} from './horizontal-property-dasboard/horizontal-property-dashboard.component';


const routes: Routes = [
  {
    path: '', component: HorizontalPropertyDashboardComponent,
    loadChildren: () => import('../manager-horizontal-property/manager-horizontal-property.module')
      .then(r => r.ManagerHorizontalPropertyModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HorizontalPropertyRoutingModule {
}
