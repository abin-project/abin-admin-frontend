import {PrincipalService} from '../../services/principal.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class HorizontalPropertyService extends PrincipalService {
  route = 'horizontal-property';

  getInfoHorizontalProperty(id): Observable<ApiResponse> {
    return this.get(id);
  }

  setProperty(id: string, value: any): Observable<ApiResponse> {
    return this.put(`${id}`, value);
  }
}
