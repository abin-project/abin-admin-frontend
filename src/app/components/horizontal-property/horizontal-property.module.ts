import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HorizontalPropertyRoutingModule} from './horizontal-property-routing.module';
import {HorizontalPropertyDashboardComponent} from './horizontal-property-dasboard/horizontal-property-dashboard.component';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {HorizontalPropertyComponent} from './horizontal-property/horizontal-property.component';
import {MatMenuModule} from '@angular/material/menu';
import {HorizontalPropertyService} from './horizontal-property.service';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {AuthClientService} from '../authClient/auth-client.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    HorizontalPropertyDashboardComponent,
    HorizontalPropertyComponent
  ],
  imports: [
    CommonModule,
    HorizontalPropertyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatSelectModule,
    MatExpansionModule,
    CdkAccordionModule,
    MatRippleModule,
    MatTooltipModule
  ],
  exports: [
    HorizontalPropertyComponent
  ],
  providers: [
    HorizontalPropertyService,
    AuthClientService,
  ]
})
export class HorizontalPropertyModule {
}
