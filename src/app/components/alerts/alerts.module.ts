import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeleteDialogComponent} from './delete-dialog/delete-dialog.component';
import {SnackErrorComponent} from './snack-error/snack-error.component';
import {SnackDeleteComponent} from './snack-delete/snack-delete.component';
import {SnackSuccessComponent} from './snack-success/snack-success.component';
import {SnackAlertComponent} from './snack-alert/snack-alert.component';
import {A11yModule} from '@angular/cdk/a11y';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    DeleteDialogComponent,
    SnackErrorComponent,
    SnackDeleteComponent,
    SnackSuccessComponent,
    SnackAlertComponent,
  ],
  imports: [
    CommonModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    MatIconModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatDialogModule,
  ],
  entryComponents: [
    DeleteDialogComponent,
    SnackErrorComponent,
    SnackDeleteComponent,
    SnackSuccessComponent,
    SnackAlertComponent
  ]
})
export class AlertsModule {
}
