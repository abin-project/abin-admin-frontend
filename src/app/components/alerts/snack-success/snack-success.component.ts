import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-snack-success',
  templateUrl: './snack-success.component.html',
  styleUrls: ['./snack-success.component.scss']
})
export class SnackSuccessComponent {
  text = 'Guardado';

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    if (this.data) {
      this.text = data;
    }
  }
}
