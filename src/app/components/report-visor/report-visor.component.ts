import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'abin-report-visor',
  templateUrl: './report-visor.component.html',
  styleUrls: ['./report-visor.component.css']
})
export class ReportVisorComponent implements OnInit {
  pdfContent: string;
  constructor(private a: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.a.queryParams.subscribe(params => {
      if (params.index) {
        if (localStorage.getItem(params.index)) {
          this.pdfContent = localStorage.getItem(params.index);
          const ss = document.getElementById('contentPDF');
          ss.setAttribute('data', this.pdfContent);
        } else {
          this.router.navigate(['/']);
        }
      }
    });
  }
}
