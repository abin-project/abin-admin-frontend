import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportVisorComponent } from './report-visor.component';

describe('ReportVisorComponent', () => {
  let component: ReportVisorComponent;
  let fixture: ComponentFixture<ReportVisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportVisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportVisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
