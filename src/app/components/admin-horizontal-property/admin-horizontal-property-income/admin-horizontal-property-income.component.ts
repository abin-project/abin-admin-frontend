import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ItemDefaultService} from '../item-default.service';
import {Item} from '../../../model/hotizontal-property/item';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';
import {TypeIncome} from '../../../model/hotizontal-property/typeIncome';

@Component({
  selector: 'abin-admin-horizontal-property-income',
  templateUrl: './admin-horizontal-property-income.component.html',
  styleUrls: ['./admin-horizontal-property-income.component.css']
})
export class AdminHorizontalPropertyIncomeComponent implements OnInit {
  @Output() formValid = new EventEmitter();
  @Input() stepper: any;
  @Input() horizontalProperty: HorizontalProperty;
  items: Item[];

  constructor(private itemService: ItemDefaultService) {
  }

  ngOnInit(): void {
    this.itemService.getDefaultIncome().subscribe((r: any) => {
      this.items = r;
    });
  }

  isDisabled(income: TypeIncome) {
    if (this.horizontalProperty) {
      const r = this.horizontalProperty.propertyTypes.find(f => f.code == income.code);
      if (r != undefined) {
        income.activeIncome = r.active && (r.active == true);
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  accept() {
    this.formValid.emit(this.items);
    this.stepper.next();
  }
}
