import {Component, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';
import {AdminHorizontalService} from '../admin-horizontal.service';
import {ApiResponse} from '../../../model/apiResponse';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {UpdateAdministratorComponent} from '../administrators/update-administrator/update-administrator.component';
import {AdministratorService} from '../administrators/administrator.service';
import {ShowAdministratorsComponent} from '../administrators/show-administrators/show-administrators.component';
import {RolService} from '../rol.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'abin-admin-horizontal-property-list',
  templateUrl: './admin-horizontal-property-list.component.html',
  styleUrls: ['./admin-horizontal-property-list.component.css']
})
export class AdminHorizontalPropertyListComponent implements OnInit {
  horizontalProperties: HorizontalProperty[];
  horizontalPropertiesOriginal: HorizontalProperty[];
  searching = false;
  searchControl: FormControl;

  filter = false;

  constructor(private horizontalPropertyService: AdminHorizontalService,
              private dialog: MatDialog,
              private router: Router,
              public rolService: RolService) {
    this.searchControl = new FormControl('');
    this.searchControl.valueChanges.subscribe(f => {
      this.searchHp(f);
    });
  }

  ngOnInit(): void {
    if (this.rolService.getUserRol() === 'Co-Propietario' || this.rolService.getUserRol() === 'Directorio') {
      this.horizontalPropertyService.getByProperty().subscribe((r: ApiResponse) => {
        this.horizontalProperties = r.data;
        this.horizontalPropertiesOriginal = r.data;
        localStorage.setItem('properties', btoa(JSON.stringify(this.horizontalProperties)));
      });
    } else {
      this.horizontalPropertyService.getList({state: this.filter}).subscribe((r: ApiResponse) => {
        this.horizontalProperties = r.data;
        this.horizontalPropertiesOriginal = r.data;
        localStorage.setItem('properties', btoa(JSON.stringify(this.horizontalProperties)));
      });
    }
  }

  create() {
    this.router.navigate(['/horizontalProperty/create']);
  }

  updateAdministrator(horizontalPropertyId) {
    this.dialog.open(UpdateAdministratorComponent, {
      width: '400px',
      data: {
        horizontalPropertyId
      }
    });
  }

  showAdministrators(horizontalPropertyId) {
    this.dialog.open(ShowAdministratorsComponent, {
      width: '400px',
      data: {
        horizontalPropertyId,
        title: 'Administradores',
        type: 'show'
      }
    });
  }

  showAdministratorsHistory(horizontalPropertyId) {
    this.dialog.open(ShowAdministratorsComponent, {
      width: '400px',
      data: {
        horizontalPropertyId,
        title: 'Historial de Administradores',
        type: 'history'
      }
    });
  }

  searchHp(text: string) {
    if (text?.length > 0) {
      this.horizontalProperties = this.horizontalPropertiesOriginal.filter(h => h.nameProperty.toLowerCase().includes(text.toLowerCase()));
    } else {
      this.horizontalProperties = this.horizontalPropertiesOriginal;
    }

  }


  disabledProperty(hpId: string) {
    if (hpId) {
      this.horizontalPropertyService.enableOrDisableProperty(hpId).subscribe(r => {
        this.ngOnInit();
      })
    }
  }

  filterBy(state) {
    this.filter = state;
    this.ngOnInit();
    // this.horizontalProperties.
  }

}
