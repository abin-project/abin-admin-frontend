import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../guards/auth.guard';
import {AdminHorizontalPropertyCreateComponent} from './admin-horizontal-property-create/admin-horizontal-property-create.component';
import {AdminHorizontalPropertyListComponent} from './admin-horizontal-property-list/admin-horizontal-property-list.component';


const routes: Routes = [
  {path: '', component: AdminHorizontalPropertyListComponent, canActivate: [AuthGuard]},
  {path: 'create', component: AdminHorizontalPropertyCreateComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminHorizontalPropertyRoutingModule {
}
