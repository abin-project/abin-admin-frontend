import {Injectable} from '@angular/core';
import {PrincipalService} from '../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class UserService extends PrincipalService {
  route = 'user';

  registerUser(data): Observable<ApiResponse> {
    return this.postCustom(`create`, data);
  }

  updateUserProfile(data): Observable<ApiResponse> {
    return this.postFile(`update/profile`, data);
  }

  getUsers(): Observable<ApiResponse> {
    return this.get();
  }

  resetPasswordOrEmail(id) {
    return this.get(`resendEmail/${id}`);
  }

  deleted(id: any) {
    return this.delete(`${id}`);
  }

  getProfile(): Observable<ApiResponse> {
    return this.get(`profile`);
  }
}
