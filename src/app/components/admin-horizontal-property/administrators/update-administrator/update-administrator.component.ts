import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {map, startWith} from 'rxjs/operators';
import {UserService} from '../../user.service';
import {User} from '../../../../model/User';
import {AdministratorService} from '../administrator.service';

@Component({
  selector: 'abin-update-administrator',
  templateUrl: './update-administrator.component.html',
  styleUrls: ['./update-administrator.component.css']
})
export class UpdateAdministratorComponent implements OnInit {

  adminForm: FormGroup;
  users: User[];
  filteredOptions: Observable<User[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<UpdateAdministratorComponent>,
              private fb: FormBuilder,
              private userService: UserService,
              private administratorService: AdministratorService) {
    this.adminForm = this.fb.group({
      user: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(r => {
      this.users = r.data;
      this.filteredOptions = this.adminForm.get('user').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.username),
          map(name => name ? this._filter(name) : this.users.slice())
        );
    });

  }

  save() {
    if (this.adminForm.valid) {
      this.administratorService.createAdministrator(this.data.horizontalPropertyId, this.adminForm.value).subscribe(() => {
        this.dialogRef.close(this.adminForm.value);
      });
    }
  }

  displayFn(user: User): string {
    return user && user.username ? user.username : '';
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.users.filter(option => option.username.toLowerCase().indexOf(filterValue) === 0);
  }
}
