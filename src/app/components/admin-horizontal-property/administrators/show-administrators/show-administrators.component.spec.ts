import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShowAdministratorsComponent} from './show-administrators.component';

describe('ShowAdministratorsComponent', () => {
  let component: ShowAdministratorsComponent;
  let fixture: ComponentFixture<ShowAdministratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowAdministratorsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAdministratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
