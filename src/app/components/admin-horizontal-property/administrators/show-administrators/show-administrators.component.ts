import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../../../model/User';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AdministratorService} from '../administrator.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';

@Component({
  selector: 'abin-show-administrators',
  templateUrl: './show-administrators.component.html',
  styleUrls: ['./show-administrators.component.css']
})
export class ShowAdministratorsComponent implements OnInit {
  users: User[] = [];
  title: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<ShowAdministratorsComponent>,
              private administratorService: AdministratorService,
              private snack: MatSnackBar) {
    this.title = this.data.title;
  }

  ngOnInit(): void {
    if (this.data.type === 'show') {
      this.administratorService.showAdministrators(this.data.horizontalPropertyId).subscribe(r => {
        this.users = r.data;
      });
    }
    if (this.data.type === 'history') {
      this.administratorService.showAdministratorsHistory(this.data.horizontalPropertyId).subscribe(r => {
        this.users = r.data;
      });
    }
  }

  deleteUser(userId) {
    if (this.data.type === 'show') {
      this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
        duration: 5000,
      }).onAction().subscribe(a => {
        this.administratorService.deleteAdministrator(this.data.horizontalPropertyId, userId).subscribe(r => {
          this.ngOnInit();
        });
      });

    }
  }
}
