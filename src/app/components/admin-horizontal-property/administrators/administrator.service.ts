import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService extends PrincipalService {
  route = 'horizontal-property-user';

  createAdministrator(horizontalPropertyId, user) {
    return this.put(`${horizontalPropertyId}`, user);
  }

  showAdministrators(horizontalPropertyId: any): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  showAdministratorsHistory(horizontalPropertyId: any): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/history`);
  }

  deleteAdministrator(horizontalPropertyId: any, userId) {
    return this.delete(`${horizontalPropertyId}/${userId}`);
  }
}
