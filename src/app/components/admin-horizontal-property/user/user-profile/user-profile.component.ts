import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserService} from '../../user.service';
import {User} from '../../../../model/User';

@Component({
  selector: 'abin-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  formUser: FormGroup;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private snack: MatSnackBar,
              private dialogRef: MatDialogRef<UserProfileComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.formUser = this.fb.group({
      username: [{value: this.data.user.username, disabled: true}],
      email: [{value: this.data.user.email, disabled: true}],
      cell: [null, [Validators.required]],
      picture: [null, [Validators.required]],
      pictureCI: [null, [Validators.required]],
      cv: [null, [Validators.required]],
      location: [null, [Validators.required]],
      locationMap: null
    });
  }

  ngOnInit(): void {
    this.userService.getProfile().subscribe(r => {
      const user: User = r.data;
      this.formUser.get('username').setValue(user.username);
      this.formUser.get('email').setValue(user.email);
      this.formUser.get('cell').setValue(user.cell);
      this.formUser.get('picture').setValue(user.picture);
      this.formUser.get('pictureCI').setValue(user.pictureCI);
      this.formUser.get('cv').setValue(user.cv);
      this.formUser.get('location').setValue(user.location);
      this.formUser.get('locationMap').setValue(user.locationMap);
    });
  }

  save() {
    if (this.formUser.valid) {
      const res = this.formUser.value;
      const formData = new FormData();
      formData.append('cell', res.cell);
      formData.append('location', res.location);
      formData.append('locationMap', res.locationMap);
      formData.append('picture', res.picture._files[0], res.picture._filesNames);
      formData.append('pictureCI', res.pictureCI._files[0], res.pictureCI._filesNames);
      formData.append('cv', res.cv._files[0], res.cv._filesNames);
      this.userService.updateUserProfile(formData).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
