import {Injectable} from '@angular/core';
import {PrincipalService} from '../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../model/apiResponse';
import {User} from '../../model/User';
import {Owner} from '../../model/hotizontal-property/owner';

@Injectable({
  providedIn: 'root'
})
export class RolService extends PrincipalService {
  route = 'rol';

  getRoles(): Observable<ApiResponse> {
    return this.get();
  }

  getUserRol(): 'Root'|'Administrador del Sistema'|'Administrador'|'Directorio'|'Co-Propietario' {
    const user: User| Owner | any = JSON.parse(atob(localStorage.getItem('profile')));
    if (user.positionOwners) {
      if (user.positionOwners.length > 0) {
        return 'Directorio';
      }
      return 'Co-Propietario';
    }
    if (user.roles) {
      const rol = user.roles[0];
      return rol.rolName;
    }
  }

  getEmail() {
    const user: User| Owner | any = JSON.parse(atob(localStorage.getItem('profile')));
    if (user.email) {
      return user.email;
    }
    if (user.emailOwner) {
      return user.emailOwner;
    }
  }
}
