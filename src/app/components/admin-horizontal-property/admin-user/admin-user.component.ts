import {Component, OnInit} from '@angular/core';
import {User} from '../../../model/User';
import {UserService} from '../user.service';
import {MatDialog} from '@angular/material/dialog';
import {AdminUserCreateComponent} from '../admin-user-create/admin-user-create.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../model/constants';
import {RolService} from '../rol.service';

@Component({
  selector: 'abin-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {

  users: User[];

  constructor(private userService: UserService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              public rolService: RolService) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(r => {
      this.users = r.data;
    });
  }

  newUser() {
    this.dialog.open(AdminUserCreateComponent, {
      width: '400px'
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  resetPassword(user) {
    this.userService.resetPasswordOrEmail(user.id).subscribe(() => {
      this.ngOnInit();
    });
  }

  deleted(user) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.userService.deleted(user.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }
}
