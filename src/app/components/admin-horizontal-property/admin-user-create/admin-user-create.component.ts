import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {MatDialogRef} from '@angular/material/dialog';
import {Rol} from '../../../model/rol';
import {RolService} from '../rol.service';
@Component({
  selector: 'abin-admin-user-create',
  templateUrl: './admin-user-create.component.html',
  styleUrls: ['./admin-user-create.component.css']
})
export class AdminUserCreateComponent implements OnInit {
  formUser: FormGroup;
  roles: Rol[];

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private rolService: RolService,
              private dialogRef: MatDialogRef<AdminUserCreateComponent>) {
    this.formUser = this.fb.group({
      username: [null, [Validators.required]],
      email: [null, [Validators.email]],
      rol: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.rolService.getRoles().subscribe(r => {
      this.roles = r.data;
    });
  }

  save() {
    if (this.formUser.valid) {
      this.userService.registerUser(this.formUser.value).subscribe(f => {
        this.dialogRef.close(f);
      });
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}
