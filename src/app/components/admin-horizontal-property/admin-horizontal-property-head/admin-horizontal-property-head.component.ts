import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {User} from '../../../model/User';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {TaskService} from '../../../services/task.service';
import {Task} from '../../../model/hotizontal-property/task';
import {MatDialog} from '@angular/material/dialog';
import {UserProfileComponent} from '../user/user-profile/user-profile.component';
import {MediaMatcher} from '@angular/cdk/layout';
import {RolService} from '../rol.service';
import {Owner} from '../../../model/hotizontal-property/owner';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'abin-admin-horizontal-property-head',
  templateUrl: './admin-horizontal-property-head.component.html',
  styleUrls: ['./admin-horizontal-property-head.component.css']
})
export class AdminHorizontalPropertyHeadComponent implements OnInit {
  mobileQuery: MediaQueryList;
  user: any;
  @Input() withSearch = true;
  tasks: Task[] = [];
  mobileQueryListener: () => void;
  hostAdmin = environment.admin;
  hostClient = environment.client;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private authService: AuthService,
              private taskService: TaskService,
              private dialog: MatDialog,
              public rolService: RolService,
              private router: Router) {
    this.user = localStorage.getItem('profile') ? JSON.parse(atob(localStorage.getItem('profile'))) : null;
    this.mobileQuery = media.matchMedia('(max-width: 800px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  ngOnInit(): void {
    this.taskService.getAllTask().subscribe(r => {
      this.tasks = r.data;
    });
  }

  logout() {
    this.authService.logout().then(() => {
      if (window.location.origin.includes(this.hostAdmin)) {
        this.router.navigate(['/auth/login']);
      } else if (window.location.origin.includes(this.hostClient)) {
        this.router.navigate(['/client/login']);
      }
    });
  }

  search() {
    console.log('console');
  }

  openProfile() {
    this.dialog.open(UserProfileComponent, {
      width: '400px',
      data: {
        user: this.user
      }
    }).afterClosed().subscribe(r => {
      console.log(r);
    });
  }

  getLetterUser() {
    if (this.user && this.user.username) {
      return this.user.username.toUpperCase().charAt(0);
    } else if (this.user && this.user.nameOwner) {
      return this.user.nameOwner.toUpperCase().charAt(0);
    }
  }
}
