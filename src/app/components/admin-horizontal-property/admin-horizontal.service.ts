import {PrincipalService} from '../../services/principal.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminHorizontalService extends PrincipalService {
  route = 'horizontal-property';

  getList(params) {
    return this.getQuery(null, params);
  }

  getByProperty() {
    return this.get(`owner`);
  }

  createdHorizontalService(data) {
    return this.post(data);
  }


  enableOrDisableProperty(hpId: string) {
    return this.delete(hpId);
  }
}
