import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from '../../../model/hotizontal-property/item';
import {ItemDefaultService} from '../item-default.service';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-admin-horizontal-property-expense',
  templateUrl: './admin-horizontal-property-expense.component.html',
  styleUrls: ['./admin-horizontal-property-expense.component.css']
})
export class AdminHorizontalPropertyExpenseComponent implements OnInit {
  @Output() formValid = new EventEmitter();
  @Input() stepper: any;
  @Input() horizontalProperty: HorizontalProperty;
  items: Item[];

  constructor(private itemService: ItemDefaultService) {
  }

  ngOnInit(): void {
    this.itemService.getDefaultExpense().subscribe((r: any) => {
      this.items = r;
    });
  }

  accept() {
    this.formValid.emit(this.items);
    this.stepper.next();
  }
}
