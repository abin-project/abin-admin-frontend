import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminHorizontalPropertyRoutingModule} from './admin-horizontal-property-routing.module';
import {AdminHorizontalPropertyHeadComponent} from './admin-horizontal-property-head/admin-horizontal-property-head.component';
import {AdminHorizontalPropertyComponent} from './admin-horizontal-property/admin-horizontal-property.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {AdminHorizontalService} from './admin-horizontal.service';
import {AdminHorizontalPropertyListComponent} from './admin-horizontal-property-list/admin-horizontal-property-list.component';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminHorizontalPropertyCreateComponent} from './admin-horizontal-property-create/admin-horizontal-property-create.component';
import {MatStepperModule} from '@angular/material/stepper';
import {AdminHorizontalPropertyFormComponent} from './admin-horizontal-property-form/admin-horizontal-property-form.component';
import {AdminHorizontalPropertyIncomeComponent} from './admin-horizontal-property-income/admin-horizontal-property-income.component';
import {AdminHorizontalPropertyExpenseComponent} from './admin-horizontal-property-expense/admin-horizontal-property-expense.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {ItemDefaultService} from './item-default.service';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {AdminHorizontalPropertyEndingComponent} from './admin-horizontal-property-ending/admin-horizontal-property-ending.component';
import {AdminUserComponent} from './admin-user/admin-user.component';
import {AdminUserCreateComponent} from './admin-user-create/admin-user-create.component';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {PropertyTypeService} from './property-type.service';
import {CreateTypePropertyComponent} from './create-type-property/create-type-property.component';
import {MatRadioModule} from '@angular/material/radio';
import {UpdateAdministratorComponent} from './administrators/update-administrator/update-administrator.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ShowAdministratorsComponent} from './administrators/show-administrators/show-administrators.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import { DirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
    declarations: [
        AdminHorizontalPropertyHeadComponent,
        AdminHorizontalPropertyComponent,
        AdminHorizontalPropertyListComponent,
        AdminHorizontalPropertyCreateComponent,
        AdminHorizontalPropertyFormComponent,
        AdminHorizontalPropertyIncomeComponent,
        AdminHorizontalPropertyExpenseComponent,
        AdminHorizontalPropertyEndingComponent,
        AdminUserComponent,
        AdminUserCreateComponent,
        CreateTypePropertyComponent,
        UpdateAdministratorComponent,
        ShowAdministratorsComponent,
        UserProfileComponent
    ],
    imports: [
        CommonModule,
        AdminHorizontalPropertyRoutingModule,
        FormsModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatListModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatStepperModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        CdkStepperModule,
        MatBadgeModule,
        MatTabsModule,
        MatDialogModule,
        MatMenuModule,
        MatSelectModule,
        MatRadioModule,
        MatAutocompleteModule,
        MaterialFileInputModule,
        DirectivesModule,
    ],
    exports: [
        UserProfileComponent
    ],
    providers: [
        AdminHorizontalService,
        ItemDefaultService,
        PropertyTypeService,
    ]
})
export class AdminHorizontalPropertyModule {
}
