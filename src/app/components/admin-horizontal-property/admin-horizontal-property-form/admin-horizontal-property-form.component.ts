import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../alerts/snack-error/snack-error.component';
import {SNACK_CONFIG} from '../../../config/toast.config';
import {Router} from '@angular/router';
import {PropertyTypeService} from '../property-type.service';
import {PropertyType, PropertyTypeDefault} from '../../../model/hotizontal-property/propertyTypes';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {CreateTypePropertyComponent} from '../create-type-property/create-type-property.component';

@Component({
  selector: 'abin-admin-horizontal-property-form',
  templateUrl: './admin-horizontal-property-form.component.html',
  styleUrls: ['./admin-horizontal-property-form.component.css']
})
export class AdminHorizontalPropertyFormComponent implements OnInit {
  @Output() formValid = new EventEmitter();
  @Output() insertProperty = new EventEmitter();
  @Input() stepper: any;
  horizontalProperty: FormGroup;
  typesProperties: PropertyTypeDefault[];

  constructor(private fb: FormBuilder,
              private snack: MatSnackBar,
              private router: Router,
              private propertyType: PropertyTypeService,
              private dialog: MatDialog) {
    this.horizontalProperty = this.fb.group({
      id: null,
      nameProperty: [null, Validators.required],
      dayPayInit: [null, [Validators.required]],
      dayPayLimit: [null, [Validators.min(0), Validators.max(28)]],
      day: [1, [Validators.min(0), Validators.max(28)]],
      monthPayLimit: [null, [Validators.min(0), Validators.max(12)]],
      dateManagementInit: [null, Validators.required],
    });
    // this.horizontalProperty.get('dayPayInit').valueChanges.subscribe(r => {
    // });
  }

  ngOnInit(): void {
    this.propertyType.getProperties().subscribe(r => {
      this.typesProperties = r.data;
    });
  }

  accept() {
    const c = SNACK_CONFIG;
    c.data = 'Existen Campos Vacios';
    if (!this.horizontalProperty.valid) {
      this.snack.openFromComponent(SnackErrorComponent, c);
    } else if (this.horizontalProperty.valid && !this.verify()) {
      const v = this.horizontalProperty.value;
      if (v.dayPayInit === false) {
        v.monthPayLimit = v.monthPayLimit + 1;
      }
      this.formValid.emit({...v, propertyTypes: this.typesProperties});
      this.stepper.next();
    } else {
      this.snack.openFromComponent(SnackErrorComponent, c);
    }
  }

  back() {
    this.router.navigate(['/horizontalProperty']);
  }

  addNewType() {
    this.dialog.open(CreateTypePropertyComponent, {
      width: '400px',
    }).afterClosed().subscribe(r => {
      if (r) {
        this.propertyType.getProperties().subscribe(data => {
          data.data.forEach(f => {
            if (this.typesProperties.find(r => r.id == f.id) == undefined) {
              this.typesProperties.push(f);
            }
          });
          this.insertProperty.emit(true);
        });
      }
    });
  }

  verify() {
    return this.typesProperties.filter(f => (f.amount < 1 || f.amount == null) && f.active == true).length > 0 || this.typesProperties.filter(f => f.active == true).length == 0;
  }

  isDisabled() {
    if (this.horizontalProperty.get('limitIsEndMonth').value === true) {
      this.horizontalProperty.get('dateExpenseLimit').disable();
    } else {
      this.horizontalProperty.get('dateExpenseLimit').enable();
    }
  }

  isDisabledExpired() {
    if (this.horizontalProperty.get('expiredMonth').value === true) {
      this.horizontalProperty.get('expiredMonthLimit').enable();
    } else {
      this.horizontalProperty.get('expiredMonthLimit').disable();
    }
  }
}
