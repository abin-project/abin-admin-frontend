import {PrincipalService} from '../../services/principal.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ItemDefaultService extends PrincipalService {
  route = 'item';

  getDefaultIncome() {
    return this.get('default/income');
  }

  getDefaultExpense() {
    return this.get('default/expense');
  }
}
