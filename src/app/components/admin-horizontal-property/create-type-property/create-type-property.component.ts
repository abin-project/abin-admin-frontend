import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PropertyTypeService} from '../property-type.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-create-type-property',
  templateUrl: './create-type-property.component.html',
  styleUrls: ['./create-type-property.component.css']
})
export class CreateTypePropertyComponent implements OnInit {
  formPropertyType: FormGroup;

  constructor(private fb: FormBuilder,
              private propertyTypeService: PropertyTypeService,
              private dialogRef: MatDialogRef<CreateTypePropertyComponent>) {
    this.formPropertyType = this.fb.group({
      name: [null, [Validators.required]],
      type: [false, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formPropertyType.valid) {
      this.propertyTypeService.createType(this.formPropertyType.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

}
