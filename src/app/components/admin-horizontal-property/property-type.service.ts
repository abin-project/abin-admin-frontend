import {Injectable} from '@angular/core';
import {PrincipalService} from '../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class PropertyTypeService extends PrincipalService {
  route = 'property-type';

  getProperties(): Observable<ApiResponse> {
    return this.get();
  }

  createType(value: any): Observable<any> {
    return this.post(value);
  }

  getPropertiesByHorizontalProp(horizontalPropertyId: string, b: boolean): Observable<ApiResponse> {
    if (b === null) 
      return this.get(`${horizontalPropertyId}`);
    
    if (b !== null) {
      const t = b === true ? 'complement' : 'property';
      return this.getQuery(`${horizontalPropertyId}`, {type: t});
    }
  }

  getPropertyInUse(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/inUse/property`);
  }
}

