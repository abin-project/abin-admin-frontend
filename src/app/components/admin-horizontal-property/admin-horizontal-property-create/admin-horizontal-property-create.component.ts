import {Component, OnInit, ViewChild} from '@angular/core';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AdminHorizontalService} from '../admin-horizontal.service';
import {AdminHorizontalPropertyIncomeComponent} from '../admin-horizontal-property-income/admin-horizontal-property-income.component';
import {AdminHorizontalPropertyEndingComponent} from '../admin-horizontal-property-ending/admin-horizontal-property-ending.component';
import {AdminHorizontalPropertyExpenseComponent} from '../admin-horizontal-property-expense/admin-horizontal-property-expense.component';

@Component({
  selector: 'abin-admin-horizontal-property-create',
  templateUrl: './admin-horizontal-property-create.component.html',
  styleUrls: ['./admin-horizontal-property-create.component.css']
})
export class AdminHorizontalPropertyCreateComponent implements OnInit {
  isLinear = true;
  horizontalProperty: HorizontalProperty;
  form: FormGroup;
  income: FormGroup;
  expense: FormGroup;
  @ViewChild('itemsIncome') itemsIncome: AdminHorizontalPropertyIncomeComponent;
  @ViewChild('itemsExpense') itemsExpense: AdminHorizontalPropertyExpenseComponent;

  constructor(private fb: FormBuilder, private router: Router, private horizontalService: AdminHorizontalService) {
    this.form = this.fb.group({
      formCtrl: [null, Validators.required]
    });
    this.income = this.fb.group({
      incomeCtrl: [null, Validators.required]
    });
    this.expense = this.fb.group({
      expenseCtrl: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  setForm(event) {
    this.form.get('formCtrl').setValue(true);
    this.horizontalProperty = event;
  }

  setItemsIncome(list) {
    this.income.get('incomeCtrl').setValue(true);
    this.horizontalProperty.items = list;
  }

  setItemsExpense(list) {
    this.expense.get('expenseCtrl').setValue(true);
    this.horizontalProperty.items = this.horizontalProperty.items.concat(list);
  }

  register() {
    this.horizontalService.createdHorizontalService(this.horizontalProperty).subscribe(() => {
      this.router.navigate(['horizontalProperty']);
    });
  }

  back() {
    this.router.navigate(['horizontalProperty']);
  }

  reloadItems() {
    this.itemsIncome.ngOnInit();
    this.itemsExpense.ngOnInit();
  }
}
