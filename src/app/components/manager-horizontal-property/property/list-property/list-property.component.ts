import {Component, OnInit, ViewChild} from '@angular/core';
import {PropertyService} from '../property.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {MatDialog} from '@angular/material/dialog';
import {CreatePropertyComponent} from '../create-property/create-property.component';
import {Property} from '../../../../model/hotizontal-property/property';
import {DataGlobalService} from '../../../../services/data-global.service';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {FormOwnerComponent} from '../form-owner/form-owner.component';
import {FormTenantComponent} from '../form-tenant/form-tenant.component';
import {HistoryPropertyComponent} from '../history-property/history-property.component';
import {compare, increment, toDecimal, toUpperCase} from '../../../../services/globalFunctions';
import {PeoplesComponent} from '../peoples/peoples.component';
import {FormPropertyComponent} from '../form-property/form-property.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';
import {PrintService} from '../../../../services/print.service';
import {ExcelService} from '../../../../services/excel.service';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {LiquidationComponent} from '../liquidation/liquidation.component';
import {OwnerService} from '../../owner/owner.service';

@Component({
  selector: 'abin-list-property',
  templateUrl: './list-property.component.html',
  styleUrls: ['./list-property.component.css']
})

export class ListPropertyComponent implements OnInit {

  title = 'Propiedades';
  displayedColumns: string[] = [
    'pos',
    'code',
    'name',
    'type',
    'm2',
    'expense',
    'owner',
    'cell',
    'tenant',
    'complements',
    'option'
  ];
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  dataSource: MatTableDataSource<any>;
  sortedData: Property[];
  headComplement: { name: string, type: boolean }[];
  peoples: any[] = [];
  private owners = [];
  private merges = [];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(protected dialog: MatDialog,
              protected propertyService: PropertyService,
              protected dataGlobalService: DataGlobalService,
              protected routerActive: ActivatedRoute,
              protected horizontalPropertyService: HorizontalPropertyService,
              protected snack: MatSnackBar,
              protected printService: PrintService,
              protected excelService: ExcelService,
              protected ownerService: OwnerService,
              protected rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.propertyService.getPropertiesOnHorizontalProperty(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<any>(res.data);
      this.headComplement = res.other;
      this.pushedInDisplayColumn(this.headComplement);
    });
    this.propertyService.getPeoplesReport(this.horizontalPropertyId).subscribe(r => {
      this.peoples = r.data;
    });

    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
      this.horizontalProperty = r.data;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreatePropertyComponent, {
      width: '500px',
      data: this.horizontalProperty,
    });
    dialogRef.afterClosed().subscribe((result: Property) => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  indexPosData(i) {
    return i + 1;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.filteredData ? this.dataSource.filteredData.slice() : this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'code':
          return compare(a.code, b.code, isAsc);
        case 'name':
          return compare(a.name, b.name, isAsc);
        case 'type':
          return compare(a.type, b.type, isAsc);
        case 'm2':
          return compare(a.m2, b.m2, isAsc);
        // case 'expense':
        //   return compare(a.e, b.property_expense, isAsc);
        case 'owner':
          return compare(a.owner.nameOwner, b.owner.nameOwner, isAsc);
        case 'tenant': {
          if (a.tenant && b.tenant) {
            return compare(a.tenant.nameTenant, b.tenant.nameTenant, isAsc);
          } else if (a.tenant && !b.tenant) {
            return compare(a.tenant.nameTenant, '', isAsc);
          } else if (!a.tenant && b.tenant) {
            return compare('', b.tenant.nameTenant, isAsc);
          } else {
            return compare('', '', isAsc);
          }
        }
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Property>(this.sortedData);
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  show(ev) {
    console.log(ev);
  }

  peoplesShow(id) {
    this.dialog.open(PeoplesComponent, {
      width: '500px',
      data: {
        propertyId: id
      }
    }).afterClosed().subscribe(r => {

    });
  }

  delete(propertyId) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.propertyService.deleteProperty(this.horizontalPropertyId, propertyId).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  updateOwner(owner: Property) {
    this.dialog.open(FormOwnerComponent, {
      width: '500px',
      data: {
        owner: {
          id: owner.owner.id,
          nameOwner: owner.owner.nameOwner,
          telephoneOwner: owner.owner.telephoneOwner,
          cellOwner: owner.owner.cellOwner,
          emailOwner: owner.owner.emailOwner,
          propertyId: owner.id
        },
        horizontalProperty: this.horizontalProperty
      }
    }).afterClosed().subscribe(c => {
      if (c) {
        this.ngOnInit();
      }
    });
  }

  updateTenant(property: Property) {
    this.dialog.open(FormTenantComponent, {
      width: '500px',
      data: {
        tenant: {
          id: property.tenant ? property.tenant.id : null,
          nameTenant: property.tenant ? property.tenant.nameTenant : null,
          telephoneTenant: property.tenant ? property.tenant.telephoneTenant : null,
          emailTenant: property.tenant ? property.tenant.emailTenant : null,
          cellTenant: property.tenant ? property.tenant.cellTenant : null,
          propertyId: property ? property.id : null
        },
        horizontalProperty: this.horizontalProperty
      }
    }).afterClosed().subscribe(c => {
      if (c) {
        this.ngOnInit();
      }
    });
  }

  deleteTenant(property: Property) {
    this.propertyService.deleteTenant(this.horizontalPropertyId, property.id).subscribe(() => {
      this.ngOnInit();
    });
  }

  showHistory(p: Property) {
    this.dialog.open(HistoryPropertyComponent, {
      width: '700px',
      data: {
        property: p,
        horizontalPropertyId: this.horizontalPropertyId,
      }
    });
  }

  updateProperty(property: Property) {
    this.dialog.open(FormPropertyComponent, {
      width: '500px',
      data: {
        propertyId: property.id,
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  async reportFile(formatFile: string, byReport: string) {
    switch (byReport) {
      case 'Propiedades': await this.reportProperties(formatFile); break;
      case 'Co-Propietarios': await this.reportOwner(formatFile); break;
      case 'Inquilinos': await this.reportTenant(formatFile); break;
      case 'Habitantes': await this.reportPeoples(formatFile); break;
    }
  }

  showLiquidation(id) {
    this.propertyService.showLiquidation(this.horizontalPropertyId, id).subscribe((r: any) => {
        this.dialog.open(LiquidationComponent, {
          width: '800px',
          data: {
            period: r.data,
            typeIncomes: r.headers,
            property: r.property,
            horizontalProperty: this.horizontalProperty,
            ...r
          }
        });
    });
  }

  // printVoucherPayment(id) {
  //   this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
  //     const watter: TypeIncome = r.other.watter;
  //     this.propertyService.showLiquidation(this.horizontalPropertyId, id).subscribe((a: any) => {
  //       const isWatter = !! (watter && watter.code === 103);
  //       if (a.data.length  === 0) {
  //         this.snack.open('La propiedad esta al dia', 'Aceptar', {duration: 3000});
  //       } else {
  //         this.printService.printVoucherPayment([this.horizontalProperty.nameProperty],
  //           a.property,
  //           this.processHeaderLiquidation(a.headers),
  //           this.processLiquidation(a.data), isWatter);
  //       }
  //     });
  //   });
  // }

  async reportProperties(formatFile) {
    const headers: string[] = [
      '#',
      'Código',
      'Nombre',
      'tipo',
      'Sup. m2',
      'Expensa',
      'Propietario',
      'Inquilino',
    ].concat(this.headComplement.map(f => f.name));

    const body: any[][] = await this.processBody();
    const colOption = {
      0: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };
    if (formatFile === 'PDF') {
      await this.printService
        .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de ${this.title}`, headers, body, colOption);
    }
    if (formatFile === 'XLSX') {
      await this.excelService.exportAsExcelFile([headers].concat(body), `${this.horizontalProperty.nameProperty} Lista de Información de ${this.title}`, headers.length);
    }
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    for (const i in data) {
      const property = [
        increment(i),
        toUpperCase(this.dataSource.data[i].code),
        toUpperCase(this.dataSource.data[i].name),
        toUpperCase(this.dataSource.data[i].type),
        toDecimal(this.dataSource.data[i].m2),
        toDecimal(this.getExpense(this.dataSource.data[i])),
        toUpperCase(this.dataSource.data[i].owner.nameOwner),
        toUpperCase(this.dataSource.data[i].tenant ? this.dataSource.data[i].tenant.nameTenant : '')
      ];
      // tslint:disable-next-line:forin
      for (const pos in this.headComplement) {
        property.push(this.dataSource.data[i].complements[pos][this.headComplement[pos].name]);
      }
      res.push(property);
    }
    return res;
  }

  getExpense(property: any) {
    return property.typeIncomeProperty.find(f => f.nameIncome.includes('Expensas ')).amount;
  }
  updateSendEmail(ownerId) {
    console.log(ownerId);
    this.snack.open('Seguro de Automatizar Correos', 'Aceptar', {
      duration: 5000
    }).onAction().subscribe(() => {
      this.ownerService.updateSendEmail(this.horizontalPropertyId, ownerId).subscribe(a => {
        this.ngOnInit();
      });
    });

  }


  private pushedInDisplayColumn(headComplement: any[]) {
    this.displayedColumns = ['pos',
      'code',
      'name',
      'type',
      'm2',
      'expense',
      'owner',
      'tenant',
      'complements',
      'option'];
    const pos = this.displayedColumns.indexOf('complements');
    const newColumns = this.displayedColumns.slice(0, pos).concat(headComplement.map((f: { name: string, type: boolean }) => f.name));
    newColumns.push('option');
    this.displayedColumns = newColumns;
  }
//   process owner

  async reportOwner(formatFile: string) {
    const headers: string[] = [
      '#',
      'Nombre y Apellido',
      'Correo',
      'No. de Telefono',
      'No. Celular',
      // 'Estado',
    ];
    const body: any[][] = await this.processOwnerBody();
    const colOption = {
      0: {
        halign: 'right',
      },
      3: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };

    if (formatFile === 'PDF') {
      await this.printService
        .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de Co-Propietarios`, headers, body, colOption);
    }
    if (formatFile === 'XLSX') {
      await this.excelService.exportAsExcelFile([headers].concat(body), `${this.horizontalProperty.nameProperty} Lista de Información de Co-Propietarios`, headers.length);
    }
  }

  async processOwnerBody() {
    const res = [];
    const data = (this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data)
      .filter(f => f.owner)
      .map(r => r.owner);
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(data[i].nameOwner),
        data[i].emailOwner,
        data[i].telephoneOwner,
        data[i].cellOwner,
      ]);
    }
    return res;
  }
//   tenant report

  async reportTenant(formatFile) {
    const headers: string[] = [
      '#',
      'Nombre y Apellido',
      'Correo',
      'No. de Telefono',
      'No. Celular',
      // 'Estado',
    ];
    const body: any[][] = await this.processBodyTenant();
    const colOption = {
      0: {
        halign: 'right',
      },
      3: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };
    if (formatFile === 'PDF') {
      await this.printService
        .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de Inquilinos`, headers, body, colOption);
    }
    if (formatFile === 'XLSX') {
      await this.excelService.exportAsExcelFile([headers].concat(body), `${this.horizontalProperty.nameProperty} Lista de Información de Inquilinos`, headers.length);
    }
  }

  async processBodyTenant() {
    const res = [];
    const data = (this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data)
      .filter(f => f.tenant)
      .map(f => f.tenant);
    // tslint:disable-next-line:forin
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(data[i].nameTenant),
        data[i].emailTenant,
        data[i].telephoneTenant,
        data[i].cellTenant,
        // toUpperCase(this.dataSource.data[i].state ? 'Activo' : 'No Activo'),
      ]);
    }
    return res;
  }

  async reportPeoples(formatFile) {
    const headers: string[] = [
      // '#',
      'Propiedad',
      'Habitantes',
      // 'Nombre',
      // 'Es Propietario',
      // 'Es Inquilino'
      // 'Estado',
    ];
    const body: any[][] = await this.processBodyPeoples(formatFile);
    const colOption = {
      0: {
        halign: 'right',
      }
    };
    if (formatFile === 'PDF') {
      await this.printService
        .advancedSingleTable(
          this.horizontalProperty.nameProperty,
          `Lista de Información de Habitantes`,
          headers, body, colOption, this.owners);
    }
    if (formatFile === 'XLSX') {
      await this.excelService.exportAsExcelFile(
        [headers].concat(body),
        `${this.horizontalProperty.nameProperty} Lista de Información de Habitantes`,
        headers.length, this.merges);
    }
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  async processBodyPeoples(type) {
    let res = [];
    const data = [];

    for (const a of this.peoples.slice()) {
      data.push(await this.deleteOwnerAndTenant(a));
    }
    this.merges = await this.getMergesExcel(data);

    for (const property of data) {
      const a = [];
      for (const i in property) {
        const line = [];
        // @ts-ignore
        // tslint:disable-next-line:triple-equals
        if (i == 0 && type == 'PDF') {
          line.push({
            rowSpan: property.length,
            content: property[i].code + ' - ' + property[i].name,
            styles: {valign: 'middle', halign: 'center'}
          });
        }
        // @ts-ignore
        // tslint:disable-next-line:triple-equals
        if (type == 'XLSX') {
          line.push(property[i].code + ' - ' + property[i].name);
        }
        line.push(
          toUpperCase(property[i].namePeople),
        );
        a.push(line);
      }
      res = res.concat(a);
    }
    return res;
  }

  private async deleteOwnerAndTenant(peoples: any[]) {
    let owner;
    let tenant;
    let otherOwner;
    let otherTenant;
    // tslint:disable-next-line:forin
    for (const r in peoples) {
      if (peoples[r].isOwner === 'SI') {
        owner = r;
        otherOwner = peoples[r];
      }
      if (peoples[r].isTenant === 'SI') {
        tenant = r;
        otherTenant = peoples[r];
      }
    }

    if (tenant !== undefined) {
      this.owners .push(otherTenant);
      peoples.splice(owner, 1);
    } else {
      this.owners.push(otherOwner);
    }
    return peoples;
  }

  private async getMergesExcel(peoples: any[][]) {
    const t = [];
    let ini: any = 2;
    for (const i in peoples) {
      const ii = Number(i);
      const r = {s: {c: 0, r: ini}, e: {r: (peoples[i].length - 1) + ini, c: 0}};
      // const r1 = {s: {c: 0, r: ini + ii}, e: {r: (peoples[i].length * ini), c: 0}};
      ini = ini + peoples[i].length ;
      t.push(r);
    }
    return t;
  }
}
