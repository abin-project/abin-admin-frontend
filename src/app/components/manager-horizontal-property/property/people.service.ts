import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class PeopleService extends PrincipalService {
  route = 'people';

  getPeoples(id): Observable<ApiResponse> {
    return this.get(`${id}`);
  }

  createPeoples(id, peoples): Observable<ApiResponse> {
    return this.postCustom(`${id}/create`, {peoples: peoples});
  }

  createPeopleOne(id, people): Observable<ApiResponse> {
    return this.postCustom(`${id}/createOne`, people);
  }

  deletePeoples(id, peoples: string[]) {
    return this.postCustom(`${id}/delete`, {peoples: peoples});
  }

  deleteOne(id, idPeople) {
    return this.delete(`${id}/delete/${idPeople}`);
  }
}
