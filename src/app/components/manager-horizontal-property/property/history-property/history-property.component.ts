import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {PropertyService} from '../property.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {RawProperty} from '../../../../model/hotizontal-property/property';
import {ApiResponse} from '../../../../model/apiResponse';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'abin-history-property',
  templateUrl: './history-property.component.html',
  styleUrls: ['./history-property.component.css']
})
export class HistoryPropertyComponent implements OnInit {

  propertyId: string;
  horizontalPropertyId: string;
  displayedColumns: string[] = ['id', 'owner_nameOwner', 'tenant_nameTenant', 'log_initAt', 'log_deletedAt'];
  dataSource: MatTableDataSource<RawProperty>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private propertyService: PropertyService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.propertyId = data.property.id;
    this.horizontalPropertyId = data.horizontalPropertyId;
    this.dataSource = new MatTableDataSource([]);

  }


  ngOnInit() {
    this.propertyService.getHistoryProperty(this.horizontalPropertyId, this.propertyId).subscribe((r: ApiResponse) => {
      this.dataSource = new MatTableDataSource(r.data);
      this.dataSource.sort = this.sort;
    });

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
