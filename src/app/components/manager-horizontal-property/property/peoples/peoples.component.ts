import {Component, Inject, OnInit} from '@angular/core';
import {PeopleService} from '../people.service';
import {People} from '../../../../model/hotizontal-property/people';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.css']
})
export class PeoplesComponent implements OnInit {
  peoples: People[];
  propertyId;
  formPeople: FormGroup;
  inCreated = false;

  constructor(private peopleService: PeopleService,
              private fb: FormBuilder,
              private snack: MatSnackBar,
              private rolService: RolService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.propertyId = data.propertyId;
    this.formPeople = this.fb.group({
      name: [null, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit(): void {
    this.peopleService.getPeoples(this.propertyId).subscribe(r => {
      this.peoples = r.data;
    });
  }

  addPeoples() {
    const p: People = {
      name: this.formPeople.get('name').value,
      state: true,
    };
    this.peopleService.createPeopleOne(this.propertyId, p).subscribe(r => {
      this.peoples.push(r.data);
      this.formPeople.get('name').setValue(null);
    });
  }

  deletePeople(id, pos) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.peopleService.deleteOne(this.propertyId, id).subscribe(r => {
        this.peoples.splice(pos, 1);
      });
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}
