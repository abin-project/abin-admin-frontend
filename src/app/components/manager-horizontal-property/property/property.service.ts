import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';
import {TypeIncomeProperty} from '../../../model/hotizontal-property/typeIncomeProperty';

@Injectable({
  providedIn: 'root'
})
export class PropertyService extends PrincipalService {
  route = 'property';

  getPropertiesValidOnHorizontalProperty(horizontalPropertyId): Observable<any> {
    return this.get(`${horizontalPropertyId}/withCode`);
  }

  getPropertiesOnHorizontalProperty(horizontalPropertyId): Observable<any> {
    return this.get(`${horizontalPropertyId}`);
  }

  getPropertiesWithCodeOnHorizontalProperty(horizontalPropertyId): Observable<any> {
    return this.get(`${horizontalPropertyId}/whitCode`);
  }

  getPropertyOnHorizontalPropertyTypes(horizontalPropertyId, type) {
    return this.get(`${horizontalPropertyId}/type/${type}`);
  }

  getPropertiesComplementsOnHorizontalProperty(horizontalPropertyId): Observable<any> {
    return this.get(`${horizontalPropertyId}/complement`);
  }

  createProperty(id, data): Observable<any> {
    return this.postCustom(`${id}/create`, data);
  }

  updateOwnerProperty(id, data): Observable<any> {
    return this.put(`${id}/updateOwner`, data);
  }

  updateTenantProperty(id, data): Observable<any> {
    return this.put(`${id}/updateTenant`, data);
  }

  deleteTenant(horizontalPropertyId, propertyId) {
    return this.delete(`${horizontalPropertyId}/deleteTenant/${propertyId}`);
  }

  getHistoryProperty(horizontalPropertyId: string, propertyId: string) {
    return this.getCustom(`${horizontalPropertyId}/history/${propertyId}`);
  }

  getProperty(horizontalPropertyId: any, propertyId: any) {
    return this.get(`${horizontalPropertyId}/show/${propertyId}`);
  }

  getTypesProperties(horizontalPropertyId: string) {
    return this.get(`${horizontalPropertyId}/showTypesProperties`);
  }

  updateProperty(horizontalPropertyId: any, id: string, value: any) {
    return this.put(`${horizontalPropertyId}/update/${id}`, value);
  }

  deleteProperty(horizontalPropertyId, propertyId) {
    return this.delete(`${horizontalPropertyId}/delete/${propertyId}`);
  }

  getPeoplesReport(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/reportPeoples`);
  }

  updateIncomesProperty(horizontalPropertyId: any, update: any[]): Observable<ApiResponse> {
    return this.put(`${horizontalPropertyId}/updateTypeIncomes`, update);
  }

  getPropertiesByOwner(horizontalPropertyId: any, ownerId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/byOwner/${ownerId}`);
  }

  showLiquidation(horizontalPropertyId: string, id: string, query?: any) {
    if (query) {
      return this.getQuery(`${horizontalPropertyId}/liquidation/${id}`, query);
    } else {
      return this.get(`${horizontalPropertyId}/liquidation/${id}`);
    }
  }

  showLiquidationInSelectProperties(horizontalPropertyId: string, formId: string, properties: any, typeWatter: string) {
    return this.postCustom(`${horizontalPropertyId}/liquidation/${formId}`, {properties, typeWatter});
  }
}
