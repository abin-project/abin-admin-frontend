import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {Property} from '../../../../model/hotizontal-property/property';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-liquidation',
  templateUrl: './liquidation.component.html',
  styleUrls: ['./liquidation.component.css']
})
export class LiquidationComponent implements OnInit {
  periods: PeriodLiquidation[];
  typeIncomes: TypeIncome[];
  property: Property;
  horizontalProperty: HorizontalProperty;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private printService: PrintService) {
    this.periods = data.period;
    this.typeIncomes = data.typeIncomes;
    this.property = data.property;
    this.horizontalProperty = data.horizontalProperty;
  }

  ngOnInit(): void {
  }
  printPDF() {
    if (this.periods.length > 0) {
      this.printService.printLiquidation([this.horizontalProperty.nameProperty, 'Liquidación'], 'table', this.property, this.data);
    } else {
      this.printService.printLiquidation([this.horizontalProperty.nameProperty, 'Certificado'], null, this.property, this.data);
    }
  }

  getTotal(id) {
    return this.periods.map(a => a.debts.find(r => r.id === id) ? Number(a.debts.find(r => r.id === id).debtOfPay) : 0)
      .reduce((a , b) => a + b, 0);
  }

  getOfTotal() {
    return this.periods.map(a => Number(a.totalOfPay)).reduce((a, b) => a + b, 0);
  }
}
export interface PeriodLiquidation {
  period: string;
  debts: DebtLiquidation[];
  totalOfPay: number;
}
export interface DebtLiquidation {
  debtOfPay: number;
  id: string;
  nameIncome: string;
  totalDebt: number;
  totalPay: number;
}
