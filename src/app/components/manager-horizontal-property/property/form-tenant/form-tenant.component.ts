import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../property.service';
import {TenantService} from '../../tenant/tenant.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {Tenant} from '../../../../model/hotizontal-property/tenant';

export const _filter = (opt: Tenant[], value: string): Tenant[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.nameTenant.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'abin-form-tenant',
  templateUrl: './form-tenant.component.html',
  styleUrls: ['./form-tenant.component.css']
})
export class FormTenantComponent implements OnInit {
  formTenant: FormGroup;
  horizontalProperty: HorizontalProperty;
  tenantSelect: Tenant;
  tenants: Tenant[];
  groupsTenant: Observable<GroupTenant[]>;
  protected _groupsTenant: GroupTenant[];

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<FormTenantComponent>,
              protected tenantService: TenantService,
              protected propertyService: PropertyService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.tenantSelect = data.tenant;
    this.horizontalProperty = data.horizontalProperty;
    this.formTenant = this.fb.group(
      {
        id: [this.tenantSelect.id],
        nameTenant: [this.tenantSelect.nameTenant, [Validators.required, Validators.minLength(1)]],
        telephoneTenant: [this.tenantSelect.telephoneTenant, [Validators.required]],
        cellTenant: [this.tenantSelect.cellTenant, [Validators.required]],
        emailTenant: [this.tenantSelect.emailTenant, [Validators.required, Validators.email]],
      }
    );

  }


  ngOnInit() {
    this.tenantService.getOnlyTenants(this.data.horizontalProperty.id).subscribe(async (r: ApiResponse) => {
      this.tenants = r.data;
      this._groupsTenant = await this.processOwner();
    });
    this.groupsTenant = this.formTenant.get('nameTenant')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  save() {
    const o = this.formTenant.value;
    this.propertyService.updateTenantProperty(this.horizontalProperty.id, {
      propertyId: this.data.tenant.propertyId,
      tenant: this.formTenant.value,
    }).subscribe(res => {
      this.dialogRef.close(res);
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async processOwner() {
    let firstLetter = 65;
    const res: any[] = [];
    res.push({
      letter: '#',
      tenants: await this.tenants.filter(owner => {
        return !isNaN(Number(owner.nameTenant.charAt(0)));
      })
    });
    while (firstLetter <= 90) {
      const l = String.fromCharCode(firstLetter);
      const ow = await this.tenants.filter(tenant => {
        return String.fromCharCode(firstLetter) === tenant.nameTenant.charAt(0).toUpperCase();
      });
      if (ow.length > 0) {
        await res.push({
          letter: l,
          tenants: ow
        });
      }
      firstLetter++;
    }
    return res;
  }

  setDataTenant(tenant: Tenant) {
    this.formTenant.get('id').setValue(tenant.id);
    this.formTenant.get('nameTenant').setValue(tenant.nameTenant);
    this.formTenant.get('telephoneTenant').setValue(tenant.telephoneTenant);
    this.formTenant.get('cellTenant').setValue(tenant.cellTenant);
    this.formTenant.get('emailTenant').setValue(tenant.emailTenant);
  }

  protected _filterGroup(value: string): any {
    if (value) {
      return this._groupsTenant
        .map(group => ({letter: group.letter, tenants: _filter(group.tenants, value)}))
        .filter(group => group.tenants.length > 0);
    }

    return this._groupsTenant;
  }
}

export interface GroupTenant {
  letter: string;
  tenants: Tenant[];
}

