import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OwnerService} from '../../owner/owner.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {PropertyService} from '../property.service';

export const _filter = (opt: Owner[], value: string): Owner[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.nameOwner.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'abin-form-owner',
  templateUrl: './form-owner.component.html',
  styleUrls: ['./form-owner.component.css']
})
export class FormOwnerComponent implements OnInit {

  formCoOwner: FormGroup;
  horizontalProperty: HorizontalProperty;
  ownerSelect: Owner;
  owners: Owner[];
  groupsOwner: Observable<GroupOwner[]>;
  protected _groupsOwner: GroupOwner[];


  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<FormOwnerComponent>,
              private ownerService: OwnerService,
              private propertyService: PropertyService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.ownerSelect = data.owner;
    this.horizontalProperty = data.horizontalProperty;
    this.formCoOwner = this.fb.group(
      {
        id: [this.ownerSelect.emailOwner],
        nameOwner: [this.ownerSelect.nameOwner, [Validators.required, Validators.minLength(1)]],
        telephoneOwner: [this.ownerSelect.telephoneOwner, [Validators.required]],
        cellOwner: [this.ownerSelect.cellOwner, [Validators.required]],
        emailOwner: [this.ownerSelect.emailOwner, [Validators.required, Validators.email]],
      }
    );
  }

  ngOnInit() {
    this.ownerService.getOnlyOwner(this.data.horizontalProperty.id).subscribe(async (r: ApiResponse) => {
      this.owners = r.data;
      this._groupsOwner = await this.processOwner();
    });
    this.groupsOwner = this.formCoOwner.get('nameOwner')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  save() {
    this.propertyService.updateOwnerProperty(this.horizontalProperty.id, {
      propertyId: this.data.owner.propertyId,
      owner: this.formCoOwner.value
    }).subscribe(res => {
      this.dialogRef.close(res);
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async processOwner() {
    let firstLetter = 65;
    const res: any[] = [];
    res.push({
      letter: '#',
      owners: await this.owners.filter(owner => {
        return !isNaN(Number(owner.nameOwner.charAt(0)));
      })
    });
    while (firstLetter <= 90) {
      const l = String.fromCharCode(firstLetter);
      const ow = await this.owners.filter(owner => {
        return String.fromCharCode(firstLetter) === owner.nameOwner.charAt(0).toUpperCase();
      });
      if (ow.length > 0) {
        await res.push({
          letter: l,
          owners: ow
        });
      }
      firstLetter++;
    }
    return res;
  }

  setDataOwner(owner: Owner) {
    this.formCoOwner.get('id').setValue(owner.id);
    this.formCoOwner.get('nameOwner').setValue(owner.nameOwner);
    this.formCoOwner.get('telephoneOwner').setValue(owner.telephoneOwner);
    this.formCoOwner.get('cellOwner').setValue(owner.cellOwner);
    this.formCoOwner.get('emailOwner').setValue(owner.emailOwner);
  }

  protected _filterGroup(value: string): any {
    if (value) {
      return this._groupsOwner
        .map(group => ({letter: group.letter, owners: _filter(group.owners, value)}))
        .filter(group => group.owners.length > 0);
    }

    return this._groupsOwner;
  }

}

export interface GroupOwner {
  letter: string;
  owners: Owner[];
}
