import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {TypeIncome, TypeIncomeProperty} from '../../../../model/hotizontal-property/typeIncome';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../property.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {Property} from '../../../../model/hotizontal-property/property';
import {PropertyGroupService} from '../../settings/property-group.service';
import {PropertyGroup} from '../../../../model/hotizontal-property/propertyGroup';
import * as moment from 'moment';

@Component({
  selector: 'abin-form-property',
  templateUrl: './form-property.component.html',
  styleUrls: ['./form-property.component.css']
})
export class FormPropertyComponent implements OnInit {
  formProperty: FormGroup;
  propertyGroups: PropertyGroup[] = [];
  propertySelect: Property;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<FormPropertyComponent>,
              protected propertyService: PropertyService,
              protected propertyGroupService: PropertyGroupService,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    this.formProperty = this.fb.group(
      {
        code: [{value: null, disabled: false}, [Validators.required, Validators.minLength(1)]],
        name: [null, [Validators.required, Validators.minLength(1)]],
        m2: [{value: null, disabled: false}, [Validators.required, Validators.min(0)]],
        type: [{value: null, disabled: false}, [Validators.required]],
        group: null,
        dateInitPay: [null]
      }
    );


  }

  ngOnInit() {
    this.propertyGroupService.getGroups(this.data.horizontalPropertyId).subscribe(r => {
      this.propertyGroups = r.data;
    });

    this.propertyService.getProperty(this.data.horizontalPropertyId, this.data.propertyId).subscribe((r: ApiResponse) => {
      this.propertySelect = r.data as Property;
      this.formProperty.get('code').setValue(this.propertySelect.code);
      this.formProperty.get('name').setValue(this.propertySelect.name);
      this.formProperty.get('m2').setValue(this.propertySelect.m2);
      this.formProperty.get('type').setValue(this.propertySelect.type);
      this.formProperty.get('group').setValue(this.propertySelect.group ? this.propertySelect.group.id : null);
      this.formProperty.get('dateInitPay').setValue(this.propertySelect.dateInitPay ? this.propertySelect.dateInitPay : null);
      this._processItems(this.propertySelect.typeIncomeProperty);
    });


  }

  create() {
    const property: Property = this.formProperty.value;
    if (property.dateInitPay) {
      // @ts-ignore
      property.dateInitPay = moment(property.dateInitPay).format('YYYY-MM-DD');
    }

    this.propertyService.createProperty(this.data.id, property).subscribe(r => {
      this.dialogRef.close(r);
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  private _processItems(items: TypeIncomeProperty[]) {
    items.forEach(f => {
      if (f.typeIncome.nameIncome.toLowerCase().includes('expensa')) {
        if (!this.formProperty.get('expense')) {
          this.formProperty.addControl('expense', this.fb.control(f.amount, [Validators.required, Validators.min(0.01)]));
        }
      }
      if (f.typeIncome.nameIncome == 'Seguro') {
        this.formProperty.addControl('sure', this.fb.control(f.amount, [Validators.required, Validators.min(0.01)]));
      }
    });
  }

  saveProperty() {
    const property = this.formProperty.value;
    if (property.dateInitPay) {
      // @ts-ignore
      property.dateInitPay = moment(property.dateInitPay).format('YYYY-MM-DD');
    }

    property.typeIncomeProperty = this.propertySelect.typeIncomeProperty;
    this.propertyService.updateProperty(this.data.horizontalPropertyId, this.propertySelect.id, property).subscribe(r => {
      this.dialogRef.close(r);
    });
  }
}
