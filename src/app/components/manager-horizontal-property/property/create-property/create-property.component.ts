import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Property} from '../../../../model/hotizontal-property/property';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {OwnerService} from '../../owner/owner.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {PropertyService} from '../property.service';
import {ItemService} from '../../item/item.service';
import {TypeIncome} from 'src/app/model/hotizontal-property/typeIncome';
import {PropertyGroupService} from '../../settings/property-group.service';
import {PropertyGroup} from '../../../../model/hotizontal-property/propertyGroup';
import {PropertyType} from '../../../../model/hotizontal-property/propertyTypes';
import {PropertyTypeService} from '../../../admin-horizontal-property/property-type.service';
import * as moment from 'moment';

export const _filter = (opt: Owner[], value: string): Owner[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.nameOwner.toLowerCase().indexOf(filterValue) === 0);
};


@Component({
  selector: 'abin-create-property',
  templateUrl: './create-property.component.html',
  styleUrls: ['./create-property.component.css']
})
export class CreatePropertyComponent implements OnInit {

  formProperty: FormGroup;
  formCoOwner: FormGroup;
  formTenant: FormGroup;
  horizontalProperty: HorizontalProperty;
  owners: Owner[];
  groupsOwner: Observable<GroupOwner[]>;
  propertyTypes: PropertyType[];

  propertyGroups: PropertyGroup[] = [];

  protected _groupsOwner: GroupOwner[];


  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<CreatePropertyComponent>,
              protected ownerService: OwnerService,
              protected propertyService: PropertyService,
              protected itemService: ItemService,
              protected propertyGroupService: PropertyGroupService,
              protected propertyTypeService: PropertyTypeService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.formCoOwner = this.fb.group(
      {
        nameOwner: [null, [Validators.required, Validators.minLength(1)]],
        telephoneOwner: [null, [Validators.required]],
        cellOwner: [null, [Validators.required]],
        emailOwner: [null, [Validators.required, Validators.email]],
        check_tenant: [false, Validators.required]
      }
    );
    this.formProperty = this.fb.group(
      {
        code: [null, [Validators.required, Validators.minLength(1)]],
        name: [null, [Validators.required, Validators.minLength(1)]],
        m2: [null, [Validators.required, Validators.min(0)]],
        type: [null, [Validators.required]],
        group: null,
        dateInitPay: null
      }
    );
    this.formTenant = this.fb.group(
      {
        nameTenant: [null, [Validators.required, Validators.minLength(1)]],
        telephoneTenant: [null, [Validators.required]],
        cellTenant: [null, [Validators.required]],
        emailTenant: [null, [Validators.required, Validators.email]]
      }
    );
  }

  ngOnInit() {
    this.ownerService.getOnlyOwner(this.data.id).subscribe(async (r: ApiResponse) => {
      this.owners = r.data;
      this._groupsOwner = await this.processOwner();
    });
    this.itemService.getItemTypesIncomes(this.data.id).subscribe((r: ApiResponse) => {
      this._processItems(r.data);
    });
    this.propertyGroupService.getGroups(this.data.id).subscribe(r => {
      this.propertyGroups = r.data;
    });
    this.propertyTypeService.getPropertiesByHorizontalProp(this.data.id, false).subscribe(r => {
      this.propertyTypes = r.data;
    });

    this.groupsOwner = this.formCoOwner.get('nameOwner')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  create() {
    const property: Property = this.formProperty.value;
    property.owner = this.formCoOwner.value;
    property.tenant = this.formCoOwner.get('check_tenant').value === true ? this.formTenant.value : null;
    if (property.dateInitPay) {
      // @ts-ignore
      property.dateInitPay = moment(property.dateInitPay).format('YYYY-MM-DD');
    }


    if (property.dateInitPay) {
      // @ts-ignore
      property.dateInitPay = moment(property.dateInitPay).format('YYYY-MM-DD');
    }


    this.propertyService.createProperty(this.data.id, property).subscribe(r => {
      this.dialogRef.close(r);
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async processOwner() {
    let firstLetter = 65;
    const res: any[] = [];

    res.push({
      letter: '#',
      owners: await this.owners.filter(owner => {
        return !isNaN(Number(owner.nameOwner.charAt(0)));
      })
    });

    while (firstLetter <= 90) {
      const l = String.fromCharCode(firstLetter);
      const ow = await this.owners.filter(owner => {
        return String.fromCharCode(firstLetter) === owner.nameOwner.charAt(0).toUpperCase();
      });
      if (ow.length > 0) {
        await res.push({
          letter: l,
          owners: ow
        });
      }
      firstLetter++;
    }
    return res;
  }

  setDataOwner(owner: Owner) {
    this.formCoOwner.get('nameOwner').setValue(owner.nameOwner);
    this.formCoOwner.get('telephoneOwner').setValue(owner.telephoneOwner);
    this.formCoOwner.get('cellOwner').setValue(owner.cellOwner);
    this.formCoOwner.get('emailOwner').setValue(owner.emailOwner);
  }

  protected _filterGroup(value: string): any {
    if (value) {
      return this._groupsOwner
        .map(group => ({letter: group.letter, owners: _filter(group.owners, value)}))
        .filter(group => group.owners.length > 0);
    }

    return this._groupsOwner;
  }

  private _processItems(items: TypeIncome[]) {
    items.forEach(f => {
      if (f.nameIncome.toLowerCase().includes('expensa')) {
        if (!this.formProperty.get('expense')) {
          this.formProperty.addControl('expense', this.fb.control(null, [Validators.required, Validators.min(0)]));
        }
      }
      if (f.nameIncome === 'Seguro') {
        this.formProperty.addControl('sure', this.fb.control(null, [Validators.required, Validators.min(0)]));
      }
    });
  }

}

export interface GroupOwner {
  letter: string;
  owners: Owner[];
}
