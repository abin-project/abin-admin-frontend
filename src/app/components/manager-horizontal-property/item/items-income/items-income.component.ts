import {Component, OnInit} from '@angular/core';
import {Item} from '../../../../model/hotizontal-property/item';
import {ActivatedRoute} from '@angular/router';
import {ItemService} from '../item.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {MatDialog} from '@angular/material/dialog';
import {ItemCreateComponent} from '../item-create/item-create.component';

@Component({
  selector: 'abin-items-income',
  templateUrl: './items-income.component.html',
  styleUrls: ['./items-income.component.css']
})
export class ItemsIncomeComponent implements OnInit {
  items: Item[];
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty;

  constructor(private routerActive: ActivatedRoute,
              private itemService: ItemService,
              private dialog: MatDialog,
              private horizontalPropertyService: HorizontalPropertyService,
              public rolService: RolService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.itemService.getIncome(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.items = r.data;
      console.log(this.items);
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((h: any) => {
      this.horizontalProperty = h.data;
    });
  }

  isDisabled(income: TypeIncome) {
    // if (this.horizontalProperty) {
    //   switch (income.nameIncome) {
    //     case 'Expensas Departamentos' : {
    //       income.activeIncome = this.horizontalProperty.departments > 0;
    //       return true;
    //     }
    //     case 'Expensas Locales Comerciales': {
    //       income.activeIncome = this.horizontalProperty.commercialPremises > 0;
    //       return true;
    //     }
    //     case 'Expensas Oficinas': {
    //       income.activeIncome = this.horizontalProperty.offices > 0;
    //       return true;
    //     }
    //     case 'Expensas': {
    //       return true;
    //     }
    //     default :
    //       return false;
    //   }
    // } else {
      return false;
    // }
  }

  saveItems() {
    this.itemService.saveItems(this.horizontalPropertyId, this.items).subscribe(() => {
      this.ngOnInit();
    });
  }

  newItem() {
    this.dialog.open(ItemCreateComponent, {
      width: '400px',
      data: {
        items: this.items,
        isIncome: true,
      }
    }).afterClosed().subscribe(r => {
      if (r){
        this.ngOnInit();
      }
    });
  }
}
