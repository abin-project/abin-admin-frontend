import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from 'src/app/model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class ItemService extends PrincipalService {

  route = 'item-horizontal-property';

  getIncome(id) {
    return this.getCustom(`${id}/income`);
  }

  getExpense(id) {
    return this.getCustom(`${id}/expense`);
  }

  getIncomeEnable(id): Observable<ApiResponse> {
    return this.getCustom(`${id}/income/enable`);
  }

  getExpenseEnable(id) {
    return this.getCustom(`${id}/expense/enable`);
  }

  saveItems(id, items) {
    return this.put(`${id}/update`, items);
  }

  getTypesIncomes(horizontalPropertyId: any) {
    return this.get(`${horizontalPropertyId}/typesIncomes`);
  }

  getItemTypesIncomes(horizontalPropertyId: any) {
    return this.get(`${horizontalPropertyId}/itemTypesIncomes`);
  }

  getTypeIncomeByProperty(horizontalPropertyId, propertyId: any): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/typesIncomes/${propertyId}`);
  }

  getTypeIncomeByPropertyDetail(horizontalPropertyId, propertyId: any): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/typesIncomes/details/${propertyId}`);
  }

  getTypesIncomesByPriority(horizontalPropertyId: any, priority: number): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/typesIncomes/priority/${priority}`);
  }

  getTypeIncomeByCode(horizontalPropertyId, code): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/itemByCode/${code}`);
  }

  createTypeIncome(horizontalPropertyId: string, idItem: any, typeIncome: any): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/createTypeIncome/${idItem}`, typeIncome);
  }

  createTypeExpense(horizontalPropertyId: string, idItem: any, typeExpense: any): Observable<ApiResponse>{
    return this.postCustom(`${horizontalPropertyId}/createTypeExpense/${idItem}`, typeExpense);
  }

  updateItemByCode(id: string, value: any) {
    return this.put(`${id}/updateItem`, value);
  }
}
