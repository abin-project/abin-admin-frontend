import {Component, OnInit} from '@angular/core';
import {Item} from '../../../../model/hotizontal-property/item';
import {ItemService} from '../item.service';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {ItemCreateComponent} from '../item-create/item-create.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'abin-items-expense',
  templateUrl: './items-expense.component.html',
  styleUrls: ['./items-expense.component.css']
})
export class ItemsExpenseComponent implements OnInit {

  items: Item[];
  horizontalPropertyId;

  constructor(private routerActive: ActivatedRoute,
              private itemService: ItemService,
              public rolService: RolService,
              public dialog: MatDialog) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.itemService.getExpense(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.items = r.data;
    });
  }

  saveItems() {
    this.itemService.saveItems(this.horizontalPropertyId, this.items).subscribe(() => {
      this.ngOnInit();
    });
  }

  newItem() {
    this.dialog.open(ItemCreateComponent, {
      width: '400px',
      data: {
        items: this.items,
        isIncome: false,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

}
