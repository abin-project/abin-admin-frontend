import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Item} from '../../../../model/hotizontal-property/item';
import {ItemService} from '../item.service';
import {ItemsIncomeComponent} from '../items-income/items-income.component';

@Component({
  selector: 'abin-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {
  formItem: FormGroup;
  items: Item[];
  isIncome: boolean;
  horizontalPropertyId: string;
  constructor(private fb: FormBuilder,
              private itemService: ItemService,
              private dialogRef: MatDialogRef<ItemCreateComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.formItem = this.fb.group({
      code: [null, Validators.required],
      name: [null, [Validators.required, Validators.maxLength(100)]],
      type: [null, Validators.required],
    });
    this.items = this.data.items;
    this.isIncome = this.data.isIncome;
    this.horizontalPropertyId = this.data.horizontalPropertyId;
  }

  ngOnInit(): void {
  }

  create() {
    if (this.formItem.valid){
      if (this.isIncome){
        // crear un ingreso
        const value = this.formItem.value;
        const valueIncome = {
          nameIncome: value.name,
          code: value.code,
          activeIncome: true,
          isGlobal: 9
        };
        this.itemService.createTypeIncome(this.horizontalPropertyId, value.type, valueIncome).subscribe(() => {
          this.dialogRef.close(true);
        });
      } else {
        // crear un egreso
        const value = this.formItem.value;
        const valueExpense = {
          nameExpense: value.name,
          code: value.code,
          activeExpense: true,
          inGroup: 9
        };
        this.itemService.createTypeExpense(this.horizontalPropertyId, value.type, valueExpense).subscribe(() => {
          this.dialogRef.close(true);
        });

      }
    }
  }
}
