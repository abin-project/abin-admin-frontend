import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../../../services/task.service';
import {Task} from '../../../../model/hotizontal-property/task';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT} from '../../../../model/constants';

@Component({
  selector: 'abin-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  tasks: Task[];
  horizontalPropertyId;

  constructor(private taskService: TaskService,
              private routerActive: ActivatedRoute,
              private snack: MatSnackBar) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.taskService.getTaskProperty(this.horizontalPropertyId).subscribe(r => {
      this.tasks = r.data;
    });
  }

  process(id): void {
    this.snack.open('Esta Seguro de Procesar', TEXT_ACCEPT, {
      data: 5000
    }).onAction().subscribe(() => {
      this.taskService.taskProcess(id).subscribe(r => {
        this.ngOnInit();
      });
    });


  }

}
