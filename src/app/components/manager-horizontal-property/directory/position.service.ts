import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class PositionService extends PrincipalService {
  route = 'position';

  getPositionByPeriod(horizontalPropertyId, year): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/${year}`);
  }

  createPositions(horizontalPropertyId, data): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  getHistoryPositions(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  updatePosition(horizontalPropertyId: string, positionId: string, value: any) {
    return this.put(`${horizontalPropertyId}/update/${positionId}`, value);
  }

  deletePosition(horizontalPropertyId: string, positionId: string) {
    return this.delete(`${horizontalPropertyId}/delete/${positionId}`);
  }

  getNamePositions(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/name/positions`);
  }

  getYearPeriod(horizontalPropertyId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/name/years`);
  }

  getOnlyOwnerPosition(horizontalPropertyId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/owners`);
  }

}
