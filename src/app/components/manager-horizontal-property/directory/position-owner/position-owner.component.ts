import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {HolderBankComponent} from '../../banks/holder-bank/holder-bank.component';
import {PositionService} from '../position.service';
import {Position} from '../../../../model/hotizontal-property/position';
import {CreatePositionOwnerComponent} from '../create-position-owner/create-position-owner.component';
import {compare, compareDate, increment, toUpperCase} from '../../../../services/globalFunctions';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-position-owner',
  templateUrl: './position-owner.component.html',
  styleUrls: ['./position-owner.component.css']
})
export class PositionOwnerComponent implements OnInit {
  displayedColumns: string[] = [
    'pos',
    'name',
    'owner',
    'year',
    'period',
    'state',
    // 'option'
  ];
  title = 'Directorio';
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Position>;
  sortedData: Position[];
  params;
  years;
  horizontalProperty: HorizontalProperty;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private positionService: PositionService,
              private dataGlobalService: DataGlobalService,
              private matSnackBar: MatSnackBar,
              private router: Router,
              private routerActive: ActivatedRoute,
              private printService: PrintService,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
      this.loadData();
    });
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.loadYears();
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
  }

  loadYears(){
    this.positionService.getYearPeriod(this.horizontalPropertyId).subscribe(r => {
      this.years = r.data;
    });
  }

  indexPosData(i) {
    return i + 1;
  }

  loadData() {
    this.loadYears();
    this.positionService.getPositionByPeriod(this.horizontalPropertyId, this.params.year).subscribe(res => {
      this.dataSource = new MatTableDataSource<Position>(res.data);
      if (this.readNotOnly()) {
        if (this.displayedColumns.find(r => r === 'option') === undefined) {
          this.displayedColumns.push('option');
        }
      }
    });
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return compare(a.name, b.name, isAsc);
        case 'owner':
          return compare(a.owner.nameOwner, b.owner.nameOwner, isAsc);
        case 'period':
          return compareDate(a.dateIni, b.dateIni, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Position>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(CreatePositionOwnerComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.loadData();
      }
    });
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  edit(position) {
    this.dialog.open(CreatePositionOwnerComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        position: position
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.loadData();
      }
    });
  }

  delete(position) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
    }).onAction().subscribe(() => {
      this.positionService.deletePosition(this.horizontalPropertyId, position.id).subscribe(r => {
        this.loadData();
      });
    });
  }


  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  showHolder(bank: Bank) {
    this.dialog.open(HolderBankComponent, {
      width: '550px',
      data: {
        bank: bank,
        horizontalPropertyId: this.horizontalPropertyId
      }
    });
  }

  setPeriod(year) {
    this.router.navigate([], {
      queryParams: {
        year: year
      },
      skipLocationChange: false
    });
  }

  async report() {
    const headers: string[] = [
      '#',
      'Cartera / Cargo',
      'Co-Propietario',
      'Periodo',
    ];
    const body: any[][] = await this.processBody();
    const colOption = {
      0: {
        halign: 'right',
      }
    };

    await this.printService
      .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de ${this.title}`, headers, body, colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(this.dataSource.data[i].name),
        toUpperCase(this.dataSource.data[i].owner.nameOwner),
        `${this.dataSource.data[i].dateIni} - ${this.dataSource.data[i].dateEnd}`
      ]);
    }
    return res;
  }
}
