import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OwnerService} from '../../owner/owner.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {Position} from '../../../../model/hotizontal-property/position';
import {PositionService} from '../position.service';

@Component({
  selector: 'abin-create-position-owner',
  templateUrl: './create-position-owner.component.html',
  styleUrls: ['./create-position-owner.component.css']
})
export class CreatePositionOwnerComponent implements OnInit {

  formPosition: FormGroup;
  positionSelect: Position;
  horizontalPropertyId: string;
  options: any[] = [];
  filteredOptions: Observable<any[]>;
  ownersSelected: Owner[] = [];
  owners: Owner[] = [];

  // @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  ownerStates: any[] = [];

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<CreatePositionOwnerComponent>,
              protected positionService: PositionService,
              private ownerService: OwnerService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.positionSelect = this.data.position;
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formPosition = this.fb.group({
      ownerId: [this.positionSelect ? this.positionSelect.owner.id : null, [Validators.required]],
      year: [this.positionSelect ? this.positionSelect.year : null, Validators.required],
      name: [this.positionSelect ? this.positionSelect.name : null, [Validators.required]],
      dateIni: [this.positionSelect ? this.positionSelect.dateIni : null, [Validators.required]],
      dateEnd: [this.positionSelect ? this.positionSelect.dateEnd : null, [Validators.required]],
      ownerState: [this.positionSelect ? this.positionSelect.ownerState : null, [Validators.required]],
    });

    this.ownerService.getAllOwner(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.owners = r.data;
    });

    this.positionService.getNamePositions(this.horizontalPropertyId).subscribe(r => {
      this.options = r.data;
    });
  }

  ngOnInit(): void {
    this.ownerStates = this.getOwnerStates();
    this.filteredOptions = this.formPosition.get('name').valueChanges
      .pipe(
        startWith(this.positionSelect ? this.positionSelect.name : ''),
        map(value => typeof value === 'string' ? value : value.position_name),
        map(name => name ? this._filter(name) : this.options.slice())
      );
  }

  save() {
    if (this.formPosition.valid && !this.positionSelect) {
      this.positionService.createPositions(this.horizontalPropertyId, this.formPosition.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    } else if (this.formPosition.valid && this.positionSelect) {
      this.positionService.updatePosition(this.horizontalPropertyId, this.positionSelect.id, this.formPosition.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

  remove(fruit: Owner): void {
    const index = this.ownersSelected.indexOf(fruit);

    if (index >= 0) {
      this.ownersSelected.splice(index, 1);
    }
  }

  displayFn(position: any): string {
    return position && position.position_name ? position.position_name : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.position_name.toLowerCase().indexOf(filterValue) === 0);
  }

  private getOwnerStates() {
    return $getOwnerStates();
  }
}
export function $getOwnerStates(): {state: string, value: string}[]{
  return [
    {state: 'VIGENTE', value: 'VIGENTE'},
    {state: 'RENUNCIA', value: 'RENUNCIA'},
    {state: 'CONCLUIDO', value: 'CONCLUIDO'},
  ];
}
