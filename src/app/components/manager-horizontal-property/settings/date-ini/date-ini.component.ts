import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SettingService} from '../setting.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {CreateHolidayComponent} from '../create-holiday/create-holiday.component';
import {DateIniConfigComponent} from '../date-ini-config/date-ini-config.component';

@Component({
  selector: 'abin-date-ini',
  templateUrl: './date-ini.component.html',
  styleUrls: ['./date-ini.component.css']
})
export class DateIniComponent implements OnInit {
  @Input() dateIni;
  @Input() horizontalPropertyId;
  @Output() refresh = new EventEmitter();

  constructor(private holidayService: SettingService,
              private snack: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  create() {
    this.dialog.open(DateIniConfigComponent, {
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        date: this.dateIni,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.refresh.emit(true);
      }
    });
  }

}
