import {Component, Input, OnInit} from '@angular/core';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PropertyGroupService} from '../property-group.service';
import {CreatePropertyGroupComponent} from '../create-property-group/create-property-group.component';
import {PropertyGroup} from '../../../../model/hotizontal-property/propertyGroup';

@Component({
  selector: 'abin-property-group',
  templateUrl: './property-group.component.html',
  styleUrls: ['./property-group.component.css']
})
export class PropertyGroupComponent implements OnInit {
  @Input() horizontalPropertyId: string;
  groups: PropertyGroup[];

  constructor(private propertyGroupService: PropertyGroupService,
              private snack: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.propertyGroupService.getGroups(this.horizontalPropertyId).subscribe(r => {
      this.groups = r.data;
    });
  }

  create() {
    this.dialog.open(CreatePropertyGroupComponent, {
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  deleted(group: PropertyGroup) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.propertyGroupService.deleteGroup(this.horizontalPropertyId, group.id).subscribe(r => {
        this.ngOnInit();
      });
    });
  }
}
