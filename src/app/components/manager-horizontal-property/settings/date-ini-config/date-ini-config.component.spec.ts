import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DateIniConfigComponent} from './date-ini-config.component';

describe('DateIniConfigComponent', () => {
  let component: DateIniConfigComponent;
  let fixture: ComponentFixture<DateIniConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DateIniConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateIniConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
