import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SettingService} from '../setting.service';
import * as moment from 'moment';

@Component({
  selector: 'abin-date-ini-config',
  templateUrl: './date-ini-config.component.html',
  styleUrls: ['./date-ini-config.component.css']
})
export class DateIniConfigComponent implements OnInit {

  formDate: FormGroup;
  horizontalPropertyId: string;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<DateIniConfigComponent>,
              private holidayService: SettingService,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formDate = this.fb.group({
      date: [this.data.date ? this.data.date.dateIniFirst : null, [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formDate.valid) {
      const res = {
        date: moment(this.formDate.value.date).format('YYYY-MM-DD'),
        id: this.data.date ? this.data.date.id : null,
      };
      this.holidayService.createDay(this.horizontalPropertyId, res).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
