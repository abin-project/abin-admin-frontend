import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {Holiday} from '../../../../model/hotizontal-property/holiday';
import {SettingService} from '../setting.service';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CreateHolidayComponent} from '../create-holiday/create-holiday.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';

@Component({
  selector: 'abin-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.css']
})
export class HolidayComponent implements OnInit {
  @Input() holidays: Holiday[];
  @Input() horizontalPropertyId;
  @Output() refresh = new EventEmitter();

  constructor(private holidayService: SettingService,
              private snack: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  create() {
    this.dialog.open(CreateHolidayComponent, {
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.refresh.emit(true);
      }
    });
  }

  deleted(holiday: Holiday) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.holidayService.deleteHoliday(this.horizontalPropertyId, holiday.id).subscribe(r => {
        this.refresh.emit(true);
      });
    });
  }

}
