import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigHorizontalPropertyComponent } from './config-horizontal-property.component';

describe('ConfigHorizontalPropertyComponent', () => {
  let component: ConfigHorizontalPropertyComponent;
  let fixture: ComponentFixture<ConfigHorizontalPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigHorizontalPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigHorizontalPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
