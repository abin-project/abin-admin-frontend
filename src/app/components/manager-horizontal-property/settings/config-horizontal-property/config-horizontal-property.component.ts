import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {ItemService} from '../../item/item.service';
import * as moment from 'moment';

@Component({
  selector: 'abin-config-horizontal-property',
  templateUrl: './config-horizontal-property.component.html',
  styleUrls: ['./config-horizontal-property.component.css']
})
export class ConfigHorizontalPropertyComponent implements OnInit {
  @Input() horizontalPropertyId;
  horizontalProperty: HorizontalProperty;
  horizontalPropertyForm: FormGroup;
  typeIncomeForm: FormGroup;
  constructor(private fb: FormBuilder,
              private itemService: ItemService,
              private horizontalPropertyService: HorizontalPropertyService) {
    this.typeIncomeForm = this.fb.group({
      activeIncome: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      id: [null, [Validators.required]],
      isPercent: [null, [Validators.required]],
      nameIncome: [null, [Validators.required]]
    });
  }
  ngOnInit(): void {
    this.itemService.getTypeIncomeByCode(this.horizontalPropertyId, 201).subscribe(s => {
      if (s.data) {
        this.typeIncomeForm.get('id').setValue(s.data.id);
        this.typeIncomeForm.get('amount').setValue(s.data.amount);
        this.typeIncomeForm.get('activeIncome').setValue(s.data.activeIncome);
        this.typeIncomeForm.get('nameIncome').setValue(s.data.nameIncome);
        this.typeIncomeForm.get('isPercent').setValue(s.data.isPercent);
      }
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(a => {
      this.horizontalProperty = a.data;
      localStorage.setItem('p', JSON.stringify(a.data));

      this.horizontalPropertyForm = this.fb.group({
        id: this.horizontalPropertyId,
        dayPayInit: [this.horizontalProperty.dayPayInit, [Validators.required]],
        dayPayLimit: [this.horizontalProperty.dayPayLimit, [Validators.min(0), Validators.max(28)]],
        monthPayLimit: [this.horizontalProperty.monthPayLimit, [Validators.min(0), Validators.max(12)]],
        day: [this.horizontalProperty.day, [Validators.min(0), Validators.max(28)]],
      });
    });
  }

  saveConfig() {
    if (this.horizontalPropertyForm.valid) {
      const v = this.horizontalPropertyForm.value;
      // if (v.dayPayInit === false) {
      //   v.monthPayLimit = v.monthPayLimit + 1;
      // }
      this.horizontalPropertyService.setProperty(this.horizontalProperty.id, v).subscribe(r => {
        this.ngOnInit();
      });
    }
  }

  saveItem() {
    if (this.typeIncomeForm.valid) {
      this.itemService.updateItemByCode(this.horizontalPropertyId, this.typeIncomeForm.value).subscribe(a => {
        this.ngOnInit();
      });
    }
  }

  getSampleIni() {
    if (this.horizontalPropertyForm.valid) {
      const hpData = this.horizontalPropertyForm.value;
      const now = moment().format('YYYY-MM-DD');
      return `${this.getDateIni(now, hpData.day, hpData.dayPayInit)}`
    } else {
      return 'No Válido'
    }
  }

  getSempleEnd() {
    if (this.horizontalPropertyForm.valid) {
      const hpData = this.horizontalPropertyForm.value;
      const now = moment().format('YYYY-MM-DD');
      return `${this.getDatePayLimit(now, hpData.dayPayInit, hpData.monthPayLimit, hpData.dayPayLimit)}`
    }
    return 'No Válido'
  }


  getDatePayLimit(date, dayPayInit, monthPayLimit, dayPayLimit) {
    const dateInit = moment(date).set('date', 1);
    if (!dayPayInit) {
      dateInit.subtract(1, 'month');
    }
    const datePayLimit = moment(dateInit.format('YYYY-MM-DD'))
      .add(monthPayLimit, 'month')
      .subtract(1, 'day')
      .add(dayPayLimit, 'day');
    return datePayLimit.format('LL')
  }

  getDateIni(date, day, dayPayInit) {
    const dateInit = moment(date).set('date', day);
    if (!dayPayInit) {
      dateInit.subtract(1, 'month');
    }
    return dateInit.format('LL');
  }
}
