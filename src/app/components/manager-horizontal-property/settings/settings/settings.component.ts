import {Component, OnInit} from '@angular/core';
import {SettingService} from '../setting.service';
import {ActivatedRoute} from '@angular/router';
import {Holiday} from '../../../../model/hotizontal-property/holiday';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  holidays: Holiday[];
  dateIni: string;

  constructor(private holidayService: SettingService,
              private horizontalPropertyService: HorizontalPropertyService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.holidayService.getSettings(this.horizontalPropertyId).subscribe(r => {
      this.holidays = r.data.holidays;
      this.dateIni = r.data.dateIni;
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(a => {
      this.horizontalProperty = a.data;
    });
  }
}
