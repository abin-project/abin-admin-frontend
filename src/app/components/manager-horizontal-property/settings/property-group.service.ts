import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class PropertyGroupService extends PrincipalService {
  route = 'property-group';

  getGroups(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(horizontalPropertyId);
  }

  createGroup(horizontalPropertyId, params): Observable<ApiResponse> {
    return this.postCustom(horizontalPropertyId, params);
  }

  deleteGroup(horizontalPropertyId, groupId): Observable<ApiResponse> {
    return this.delete(`${horizontalPropertyId}/${groupId}`);
  }
}
