import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SettingService} from '../setting.service';
import * as moment from 'moment';
import {Holiday} from '../../../../model/hotizontal-property/holiday';
import {PropertyGroupService} from '../property-group.service';

@Component({
  selector: 'abin-create-property-group',
  templateUrl: './create-property-group.component.html',
  styleUrls: ['./create-property-group.component.css']
})
export class CreatePropertyGroupComponent implements OnInit {

  formGroup: FormGroup;
  horizontalPropertyId: string;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<CreatePropertyGroupComponent>,
              private propertyGroupService: PropertyGroupService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formGroup = this.fb.group({
      name: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formGroup.valid) {
      this.propertyGroupService.createGroup(this.horizontalPropertyId, this.formGroup.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
