import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class SettingService extends PrincipalService {
  route = 'settings';

  getSettings(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  createHoliday(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/holiday`, data);
  }

  deleteHoliday(horizontalPropertyId, id) {
    return this.delete(`${horizontalPropertyId}/holiday/${id}`);
  }

  createDay(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/day`, data);
  }

}
