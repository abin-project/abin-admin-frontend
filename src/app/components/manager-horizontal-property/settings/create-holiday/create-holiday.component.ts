import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SettingService} from '../setting.service';
import {Holiday} from '../../../../model/hotizontal-property/holiday';
import * as moment from 'moment';

@Component({
  selector: 'abin-create-holiday',
  templateUrl: './create-holiday.component.html',
  styleUrls: ['./create-holiday.component.css']
})
export class CreateHolidayComponent implements OnInit {

  formHoliday: FormGroup;
  horizontalPropertyId: string;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<CreateHolidayComponent>,
              private holidayService: SettingService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formHoliday = this.fb.group({
      date: [null, [Validators.required]],
      detail: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formHoliday.valid) {
      const date = moment(this.formHoliday.value.date);
      const res: Holiday = {
        day: Number(date.format('D')),
        month: Number(date.format('M')),
        year: Number(date.format('YYYY')),
        detail: this.formHoliday.value.detail,
        horizontalProperty: null,
      };
      this.holidayService.createHoliday(this.horizontalPropertyId, res).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
