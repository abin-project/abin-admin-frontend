import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListPropertyComponent} from './property/list-property/list-property.component';
import {PropertyService} from './property/property.service';
import {MatTableModule} from '@angular/material/table';
import {ManagerHorizontalPropertyRoutingModule} from './manager-horizontal-property-routing.module';
import {ManagerHorizontalPropertyHeaderComponent} from './manager-horizontal-property-header/manager-horizontal-property-header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {CreatePropertyComponent} from './property/create-property/create-property.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MoneyPipe} from '../../pipes/money.pipe';
import {MatMenuModule} from '@angular/material/menu';
import {ListOwnerComponent} from './owner/list-owner/list-owner.component';
import {OwnerService} from './owner/owner.service';
import {ListItemsComponent} from './item/list-items/list-items.component';
import {ItemsExpenseComponent} from './item/items-expense/items-expense.component';
import {ItemsIncomeComponent} from './item/items-income/items-income.component';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ListPropertyComplementComponent} from './property-complement/list-property-complement/list-property-complement.component';
import {CreatePropertyComplementComponent} from './property-complement/create-property-complement/create-property-complement.component';
import {TenantService} from './tenant/tenant.service';
import {ListTenantComponent} from './tenant/list-tenant/list-tenant.component';
import {FormOwnerComponent} from './property/form-owner/form-owner.component';
import {FormTenantComponent} from './property/form-tenant/form-tenant.component';
import {HistoryPropertyComponent} from './property/history-property/history-property.component';
import {ListBankComponent} from './banks/list-bank/list-bank.component';
import {BankService} from './banks/bank.service';
import {FormBankComponent} from './banks/form-bank/form-bank.component';
import {DateTimePipe} from '../../pipes/date-time.pipe';
import {ListOwnerIncomeComponent} from './income/individual/list-owner-income/list-owner-income.component';
import {KardexOwnerIncomeComponent} from './income/individual/kardex-owmer-income/kardex-owner-income.component';
import {IncomeService} from './income/income.service';
import {MatExpansionModule} from '@angular/material/expansion';
import {CdkTableModule} from '@angular/cdk/table';
import {FormIncomeComponent} from './income/form-income/form-income.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MonthIncomeComponent} from './income/individual/month-income/month-income.component';
import {PeriodService} from './period/period.service';
import {EventService} from './income/event.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {KardexGeneralIncomeComponent} from './income/general/kardex-general-income/kardex-general-income.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatGridListModule} from '@angular/material/grid-list';
import {HolderBankComponent} from './banks/holder-bank/holder-bank.component';
import {GeneralMonthIncomeComponent} from './income/general/general-month-income/general-month-income.component';
import {TransactionsIncomeComponent} from './banks/transactions-income/transactions-income.component';
import {TransactionService} from './banks/transaction.service';
import {TransactionsIncomeFilterComponent} from './banks/transactions-income-filter/transactions-income-filter.component';
import {StateIncomePipe} from '../../pipes/state-income.pipe';
import {MatRadioModule} from '@angular/material/radio';
import {TransactionsIncomeUpdateComponent} from './banks/trasactions-income-update/transactions-income-update.component';
import {PositionOwnerComponent} from './directory/position-owner/position-owner.component';
import {CreatePositionOwnerComponent} from './directory/create-position-owner/create-position-owner.component';
import {FullCalendarModule} from '@fullcalendar/angular';
import {CalendarRecreationComponent} from './calendar/calendar-recreation/calendar-recreation.component';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import {CalendarReservationComponent} from './calendar/calendar-reservation/calendar-reservation.component';
import {RecreationAreaService} from './recreationArea/recreation-area.service';
import {CalendarEventShowComponent} from './calendar/calendar-event-show/calendar-event-show.component';
import {RenderTransactionComponent} from './expense/render-transaction/render-transaction.component';
import {CreateRenderTransactionComponent} from './expense/create-render-transaction/create-render-transaction.component';
import {RenderFilterComponent} from './expense/render-filter/render-filter.component';
import {LedgerComponent} from './ledger/ledger/ledger.component';
import {CreateRecreationAreaComponent} from './recreationArea/create-recreation-area/create-recreation-area.component';
import {ListProjectComponent} from './project/list-project/list-project.component';
import {CreateProjectComponent} from './project/create-project/create-project.component';
import {ProjectService} from './project/project.service';
import {CompleteProjectComponent} from './project/complete-project/complete-project.component';
import {HolidayComponent} from './settings/holiday/holiday.component';
import {SettingsComponent} from './settings/settings/settings.component';
import {CreateHolidayComponent} from './settings/create-holiday/create-holiday.component';
import {MatTabsModule} from '@angular/material/tabs';
import {TaskComponent} from './task/task/task.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {PeoplesComponent} from './property/peoples/peoples.component';
import {FormPropertyComponent} from './property/form-property/form-property.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {TransactionFormComponent} from './banks/transaction-form/transaction-form.component';
import {TransactionFileComponent} from './banks/transaction-file/transaction-file.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {TransactionsIncomeFilterPeriodComponent} from './banks/transactions-income-filter-period/transactions-income-filter-period.component';
import {ResultLedgerComponent} from './ledger/result-ledger/result-ledger.component';
import {IntervalsAmountComponent} from './recreationArea/intervals-amount/intervals-amount.component';
import {ModificationProjectComponent} from './project/modification-project/modification-project.component';
import {CreateModificationProjectComponent} from './project/create-modification-project/create-modification-project.component';
import {CreateModificationAdditionalComponent} from './project/create-modification-additional/create-modification-additional.component';
import {DateIniComponent} from './settings/date-ini/date-ini.component';
import {DateIniConfigComponent} from './settings/date-ini-config/date-ini-config.component';
import {PropertyGroupComponent} from './settings/property-group/property-group.component';
import {CreatePropertyGroupComponent} from './settings/create-property-group/create-property-group.component';
import {UpdatePropertyComplementComponent} from './property-complement/update-property-complement/update-property-complement.component';
import {ListDebtsComponent} from './previousPayment/debts/list-debts/list-debts.component';
import {CreateDebtsComponent} from './previousPayment/debts/create-debts/create-debts.component';
import {AbinDatePipe} from '../../pipes/abin-date.pipe';
import {ScrollingModule, VIRTUAL_SCROLL_STRATEGY} from '@angular/cdk/scrolling';
import {FilterKardexComponent} from './income/filter-kardex/filter-kardex.component';
import {IncrementValueProjectComponent} from './project/increment-value-project/increment-value-project.component';
import {ListDebtDetailComponent} from './previousPayment/debts/list-debt-detail/list-debt-detail.component';
import {TransactionDebtPayComponent} from './banks/transaction-debt-pay/transaction-debt-pay.component';
import {FilterKardexDialogComponent} from './income/filter-kardex-dialog/filter-kardex-dialog.component';
import {CalendarReservationManualComponent} from './calendar/calendar-reservation-manual/calendar-reservation-manual.component';
import {TransactionExpenseComponent} from './expense/transaction-expense/transaction-expense.component';
import {ListAdvancedComponent} from './previousPayment/advanced/list-advanced/list-advanced.component';
import {CreateAdvancedComponent} from './previousPayment/advanced/create-advanced/create-advanced.component';
import {ListAdvancedDetailComponent} from './previousPayment/advanced/list-advanced-detail/list-advanced-detail.component';
import {DashboardComponent} from './dashboard/dasboard/dashboard.component';
import {HorizontalPropertyFileComponent} from './horizontalPropertyFile/horizontal-property-file/horizontal-property-file.component';
import {CreateHorizontalPropertyFileComponent} from './horizontalPropertyFile/create-horizontal-property-file/create-horizontal-property-file.component';
import {FileVisorAdminComponent} from './horizontalPropertyFile/file-visor-admin/file-visor-admin.component';
import {HorizontalPropertyFileOtherComponent} from './horizontalPropertyFile/horizontal-property-file-other/horizontal-property-file-other.component';
import {BankFilesComponent} from './banks/bank-files/bank-files.component';
import {BankFilesYearComponent} from './banks/bank-files-year/bank-files-year.component';
import {IncomeExpenseStateResultComponent} from './incomeExpenseState/income-expense-state-result/income-expense-state-result.component';
import {IncomeExpenseStateFilterComponent} from './incomeExpenseState/income-expense-state-filter/income-expense-state-filter.component';
import {IncomeExpenseStateService} from './incomeExpenseState/income-expense-state.service';
import {IncomeResultComponent} from './incomeExpenseState/income-result/income-result.component';
import {ExpenseResultComponent} from './incomeExpenseState/expense-result/expense-result.component';
import {PersonalListComponent} from './personal/personal-list/personal-list.component';
import {PersonalCreateComponent} from './personal/personal-create/personal-create.component';
import {ReportDebtsComponent} from './previousPayment/advanced/report-debts/report-debts.component';
import {ReportDebtTableComponent} from './previousPayment/advanced/report-debt-table/report-debt-table.component';
import {ConfigHorizontalPropertyComponent} from './settings/config-horizontal-property/config-horizontal-property.component';
import {IncrementManualComponent} from './project/increment-manual/increment-manual.component';
import {RenderAccountBankComponent} from './expense/render-account-bank/render-account-bank.component';
import {TransactionTransferComponent} from './banks/transaction-transfer/transaction-transfer.component';
import {FileDateTimeComponent} from './horizontalPropertyFile/file-date-time/file-date-time.component';
import {CreateFormComponent} from './forms/create-form/create-form.component';
import {FormListComponent} from './forms/form-list/form-list.component';
import {CompleteFormComponent} from './forms/complete-form/complete-form.component';
import {RecreationAreaListComponent} from './recreationArea/recreation-area-list/recreation-area-list.component';
import {RecreationAreaCreateComponent} from './recreationArea/recreation-area-create/recreation-area-create.component';
import {OwnerPropertyComponent} from './ownerProperties/owner-property/owner-property.component';
import {AbinPeriodPipe} from '../../pipes/abin-period.pipe';
import {MessageSuggestionComponent} from './suggestion/message-suggestion/message-suggestion.component';
import {MessageSuggestionService} from './suggestion/message-suggestion.service';
import {KardexOwnerFilterComponent} from './ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {WatterPipe} from '../../pipes/watter.pipe';
import {CreateFormReceiptComponent} from './forms/create-form-receipt/create-form-receipt.component';
import {DebtManualListComponent} from './income/manual/debt-manual-list/debt-manual-list.component';
import {DebtManualCreateComponent} from './income/manual/debt-manual-create/debt-manual-create.component';
import {TransactionIncomeDebtComponent} from './banks/transaction-income-debt/transaction-income-debt.component';
import {TransactionAssignComponent} from './banks/transaction-assign/transaction-assign.component';
import {UpdateDebtIncomeComponent} from './income/individual/update-debt-income/update-debt-income.component';
import {MoneyEnPipe} from '../../pipes/money-en.pipe';
import {TransactionDetailsVoucherComponent} from './banks/transaction-details-voucher/transaction-details-voucher.component';
import {TableVirtualScrollModule} from 'ng-table-virtual-scroll';
import {LiquidationComponent} from './property/liquidation/liquidation.component';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {OwnerTransactionsComponent} from './ownerTransactions/owner-transactions/owner-transactions.component';
import {DetailDebtManualComponent} from './income/manual/detail-debt-manual/detail-debt-manual.component';
import {AbinLiteralDatePipe} from '../../pipes/abin-literal-date.pipe';
import { DialogSelectPropertiesComponent } from './forms/dialog-select-properties/dialog-select-properties.component';
import { FormUpdateDateComponent } from './forms/form-update-date/form-update-date.component';
import { ReportAdvancedComponent } from './previousPayment/advanced/report-advanced/report-advanced.component';
import { ReportAdvancedTableComponent } from './previousPayment/advanced/report-advanced-table/report-advanced-table.component';
import {TransactionColorPipe} from '../../pipes/transaction-color.pipe';
import { KardexDebtManualComponent } from './income/individual/kardex-debt-manual/kardex-debt-manual.component';
import { DialogShowDetailExpenseComponent } from './expense/dialog-show-detail-expense/dialog-show-detail-expense.component';
import { TransactionIncomeUpdateOnlyComponent } from './banks/transaction-income-update-only/transaction-income-update-only.component';
import { CalendarActivityComponent } from './calendarActivity/calendar-activity/calendar-activity.component';
import { CalendarActivityCreateComponent } from './calendarActivity/calendar-activity-create/calendar-activity-create.component';
import { CalendarActivityDetailComponent } from './calendarActivity/calendar-activity-detail/calendar-activity-detail.component';
import { UserProfileComponent } from './dashboard/user-profile/user-profile.component';
import { ErrorImgDirective } from './directives/error-img.directive';
import { ItemCreateComponent } from './item/item-create/item-create.component';
import { ImportTemplateComponent } from './banks/import-template/import-template.component';
import { DialogSendTicketComponent } from './forms/dialog-send-ticket/dialog-send-ticket.component';
import { DialogPayOk, PayProcessComponent } from './ownerProperties/pay-process/pay-process.component';
import { QrPayService } from './ownerProperties/qr-pay.service';
import { DialogMonthIncomeComponent } from './income/individual/dialog-month-income/dialog-month-income.component';
import { BankQrDataComponent } from './banks/bank-qr-data/bank-qr-data.component';

FullCalendarModule.registerPlugins([
  dayGridPlugin, interactionPlugin,
  timeGridPlugin,
  // timeGridDay
]);

@NgModule({
  declarations: [
    ListPropertyComponent,
    ListPropertyComplementComponent,
    CreatePropertyComponent,
    MoneyPipe,
    MoneyEnPipe,
    WatterPipe,
    DateTimePipe,
    AbinDatePipe,
    AbinPeriodPipe,
    AbinLiteralDatePipe,
    TransactionColorPipe,
    ListOwnerComponent,
    ListItemsComponent,
    ItemsExpenseComponent,
    ItemsIncomeComponent,
    ManagerHorizontalPropertyHeaderComponent,
    CreatePropertyComplementComponent,
    ListTenantComponent,
    FormOwnerComponent,
    FormTenantComponent,
    HistoryPropertyComponent,
    ListBankComponent,
    FormBankComponent,
    ListOwnerIncomeComponent,
    KardexOwnerIncomeComponent,
    FormIncomeComponent,
    MonthIncomeComponent,
    KardexGeneralIncomeComponent,
    HolderBankComponent,
    GeneralMonthIncomeComponent,
    TransactionsIncomeComponent,
    TransactionsIncomeFilterComponent,
    StateIncomePipe,
    TransactionsIncomeUpdateComponent,
    PositionOwnerComponent,
    CreatePositionOwnerComponent,
    CalendarRecreationComponent,
    CalendarReservationComponent,
    CalendarEventShowComponent,
    RenderTransactionComponent,
    CreateRenderTransactionComponent,
    RenderFilterComponent,
    LedgerComponent,
    CreateRecreationAreaComponent,
    ListProjectComponent,
    CreateProjectComponent,
    CompleteProjectComponent,
    HolidayComponent,
    SettingsComponent,
    CreateHolidayComponent,
    TaskComponent,
    PeoplesComponent,
    FormPropertyComponent,
    TransactionFormComponent,
    TransactionFileComponent,
    TransactionsIncomeFilterPeriodComponent,
    ResultLedgerComponent,
    IntervalsAmountComponent,
    ModificationProjectComponent,
    CreateModificationProjectComponent,
    CreateModificationAdditionalComponent,
    DateIniComponent,
    DateIniConfigComponent,
    PropertyGroupComponent,
    CreatePropertyGroupComponent,
    UpdatePropertyComplementComponent,
    ListDebtsComponent,
    CreateDebtsComponent,
    FilterKardexComponent,
    IncrementValueProjectComponent,
    ListDebtDetailComponent,
    TransactionDebtPayComponent,
    FilterKardexDialogComponent,
    CalendarReservationManualComponent,
    TransactionExpenseComponent,
    ListAdvancedComponent,
    CreateAdvancedComponent,
    ListAdvancedDetailComponent,
    DashboardComponent,
    HorizontalPropertyFileComponent,
    CreateHorizontalPropertyFileComponent,
    FileVisorAdminComponent,
    HorizontalPropertyFileOtherComponent,
    BankFilesComponent,
    BankFilesYearComponent,
    IncomeExpenseStateResultComponent,
    IncomeExpenseStateFilterComponent,
    IncomeResultComponent,
    ExpenseResultComponent,
    PersonalListComponent,
    PersonalCreateComponent,
    ReportDebtsComponent,
    ReportDebtTableComponent,
    ConfigHorizontalPropertyComponent,
    IncrementManualComponent,
    RenderAccountBankComponent,
    TransactionTransferComponent,
    FileDateTimeComponent,
    CreateFormComponent,
    FormListComponent,
    CompleteFormComponent,
    RecreationAreaListComponent,
    RecreationAreaCreateComponent,
    OwnerPropertyComponent,
    MessageSuggestionComponent,
    KardexOwnerFilterComponent,
    CreateFormReceiptComponent,
    DebtManualListComponent,
    DebtManualCreateComponent,
    TransactionIncomeDebtComponent,
    TransactionAssignComponent,
    UpdateDebtIncomeComponent,
    TransactionDetailsVoucherComponent,
    LiquidationComponent,
    OwnerTransactionsComponent,
    DetailDebtManualComponent,
    DialogSelectPropertiesComponent,
    FormUpdateDateComponent,
    ReportAdvancedComponent,
    ReportAdvancedTableComponent,
    KardexDebtManualComponent,
    DialogShowDetailExpenseComponent,
    TransactionIncomeUpdateOnlyComponent,
    CalendarActivityComponent,
    CalendarActivityCreateComponent,
    CalendarActivityDetailComponent,
    UserProfileComponent,
    ErrorImgDirective,
    ItemCreateComponent,
    ImportTemplateComponent,
    DialogSendTicketComponent,
    PayProcessComponent,
    DialogPayOk,
    DialogMonthIncomeComponent,
    BankQrDataComponent,
  ],
  imports: [
    CommonModule,
    ManagerHorizontalPropertyRoutingModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatMenuModule,
    MatCardModule,
    MatDividerModule,
    FormsModule,
    ReactiveFormsModule,
    MatRippleModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatExpansionModule,
    CdkTableModule,
    MatSidenavModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatChipsModule,
    MatGridListModule,
    MatRadioModule,
    FullCalendarModule,
    MatTabsModule,
    MatSlideToggleModule,
    MaterialFileInputModule,
    MatButtonToggleModule,
    TableVirtualScrollModule,
    ScrollingModule,
    CdkAccordionModule,
  ],
  providers: [
    PropertyService,
    OwnerService,
    TenantService,
    BankService,
    PeriodService,
    IncomeService,
    TransactionService,
    RecreationAreaService,
    ProjectService,
    QrPayService,
    EventService,
    MoneyPipe,
    MoneyEnPipe,
    WatterPipe,
    AbinDatePipe,
    AbinLiteralDatePipe,
    DateTimePipe,
    TransactionColorPipe,
    StateIncomePipe,
    AbinPeriodPipe,
    IncomeExpenseStateService,
    MessageSuggestionService,
    {provide: VIRTUAL_SCROLL_STRATEGY, useValue: 10}
  ],
  exports: [
    DateTimePipe
  ]
})
export class ManagerHorizontalPropertyModule {
}
