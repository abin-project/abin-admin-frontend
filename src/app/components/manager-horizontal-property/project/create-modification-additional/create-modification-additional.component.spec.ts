import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateModificationAdditionalComponent} from './create-modification-additional.component';

describe('CreateModificationAdditionalComponent', () => {
  let component: CreateModificationAdditionalComponent;
  let fixture: ComponentFixture<CreateModificationAdditionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateModificationAdditionalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateModificationAdditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
