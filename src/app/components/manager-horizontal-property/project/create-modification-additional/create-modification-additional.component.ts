import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ItemProjectView, TypeExpenseView} from '../../../../model/hotizontal-property/itemProjectView';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProjectItemService} from '../project-item.service';
import {ProjectModificationService} from '../project-modification.service';
import {MatSelect} from '@angular/material/select';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-create-modification-additional',
  templateUrl: './create-modification-additional.component.html',
  styleUrls: ['./create-modification-additional.component.css']
})
export class CreateModificationAdditionalComponent implements OnInit {
  formModification: FormGroup;
  items: ItemProjectView[];
  itemsE: ItemProjectView[];
  itemsExpense: itemModExpense[] = [];
  @ViewChild('destine') destine: MatSelect;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateModificationAdditionalComponent>,
              private projectItemService: ProjectItemService,
              private projectModificationService: ProjectModificationService,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    this.formModification = this.fb.group({
      idItemIncome: [this.data.modification ? this.data.modification.idItemIncome : null, [Validators.required]],
      amount: [this.data.motification ? this.data.modification.amount : null, [Validators.required, Validators.min(1)]],
      itemsExpense: this.data.modification ? this.data.modification.itemsExpense : null,
      itemExpenseDestine: [null, Validators.required],
    });


  }

  ngOnInit(): void {
    this.projectItemService.getItems(this.data.horizontalPropertyId, this.data.projectId).subscribe(r => {
      this.items = r.data.income;
      this.itemsE = r.data.expense;
    });
  }


  async addItemExpense(select: TypeExpenseView) {
    return {
      id: null,
      idItemExpense: select.id,
      nameExpense: select.nameExpense,
      amount: 0
    };
  }

  async changedSelect() {
    const res: TypeExpenseView[] = this.formModification.get('itemExpenseDestine').value;
    const newItems = [];
    for (const a of res) {
      const exist = this.itemsExpense.find(f => f.idItemExpense === a.id);
      if (exist == undefined) {
        newItems.push(await this.addItemExpense(a));
      } else {
        newItems.push(exist);
      }
    }
    this.itemsExpense = newItems;
  }

  save() {
    const total = this.getTotal();
    if (this.formModification.valid) {
      if (Number(this.formModification.get('amount').value) === total) {
        const res = this.formModification.value;
        res.itemsExpense = this.itemsExpense;
        this.projectModificationService.createModificationAdditional(this.data.projectId, this.formModification.value).subscribe(r => {
          this.dialogRef.close(r);
        });
      } else {
        this.snack.open('Los montos no son válidos', null, {duration: 2000});
      }
    } else {
      this.snack.open('Campos inválidos', null, {duration: 2000});
    }
  }

  getTotal() {
    return this.itemsExpense.map(a => Number(a.amount)).reduce((a, b) => a + b);
  }
}

export interface itemModExpense {
  id?: string
  idItemExpense?: string;
  nameExpense: string;
  amount: number;
}
