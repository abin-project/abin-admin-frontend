import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateModificationProjectComponent} from './create-modification-project.component';

describe('CreateModificationProjectComponent', () => {
  let component: CreateModificationProjectComponent;
  let fixture: ComponentFixture<CreateModificationProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateModificationProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateModificationProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
