import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProjectItemService} from '../project-item.service';
import {ItemProjectView} from '../../../../model/hotizontal-property/itemProjectView';
import {ProjectModificationService} from '../project-modification.service';

@Component({
  selector: 'abin-create-modification-project',
  templateUrl: './create-modification-project.component.html',
  styleUrls: ['./create-modification-project.component.css']
})
export class CreateModificationProjectComponent implements OnInit {
  formModification: FormGroup;
  items: ItemProjectView[];

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateModificationProjectComponent>,
              private projectItemService: ProjectItemService,
              private projectModificationService: ProjectModificationService,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    this.formModification = this.fb.group({
      itemExpenseOrigin: [this.data.modification ? this.data.modification.itemExpenseOrigin : null, [Validators.required]],
      itemExpenseDestine: [this.data.motification ? this.data.modification.itemExpenseDestine : null, [Validators.required]],
      amount: [this.data.modification ? this.data.modification.amount : null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.projectItemService.getItems(this.data.horizontalPropertyId, this.data.projectId).subscribe(r => {
      this.items = r.data.expense;
    });
  }

  save() {
    if (this.formModification.valid) {
      this.projectModificationService.createModificationTransfer(this.data.horizontalPropertyId, this.formModification.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

}
