import {Component, Inject, OnInit} from '@angular/core';
import {ProjectModificationExpense} from '../../../../model/hotizontal-property/projectModificationExpense';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CreateModificationProjectComponent} from '../create-modification-project/create-modification-project.component';
import {ProjectModificationService} from '../project-modification.service';
import {CreateModificationAdditionalComponent} from '../create-modification-additional/create-modification-additional.component';
import {ProjectModificationIncome} from '../../../../model/hotizontal-property/projectModificationIncome';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';

@Component({
  selector: 'abin-modification-project',
  templateUrl: './modification-project.component.html',
  styleUrls: ['./modification-project.component.css']
})
export class ModificationProjectComponent implements OnInit {
  modificationsTransfer: ProjectModificationExpense[] = [];
  modificationsAdditional: ProjectModificationIncome[] = [];


  constructor(private projectModificationService: ProjectModificationService,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<ModificationProjectComponent>,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit(): void {
    this.projectModificationService.getModificationsTransfer(this.data.projectId).subscribe(r => {
      this.modificationsTransfer = r.data;
    });
    this.projectModificationService.getModificationsAdditional(this.data.projectId).subscribe(r => {
      this.modificationsAdditional = r.data;
    });
  }

  open() {
    this.dialog.open(CreateModificationProjectComponent, {
      width: '400px',
      data: this.data
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  openAdditional() {
    this.dialog.open(CreateModificationAdditionalComponent, {
      width: '400px',
      data: this.data
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  deleteAdditional(id) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.projectModificationService.deleteModificationAdditional(this.data.projectId, id).subscribe(r => {
        this.ngOnInit();
      });
    });
  }

  deleteTransfer(id) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.projectModificationService.deleteModificationTransfer(this.data.projectId, id).subscribe(r => {
        this.ngOnInit();
      });
    });

  }
}
