import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModificationProjectComponent} from './modification-project.component';

describe('ModificationProjectComponent', () => {
  let component: ModificationProjectComponent;
  let fixture: ComponentFixture<ModificationProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModificationProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificationProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
