import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends PrincipalService {
  route = 'project';

  getProjects(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  createProject(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  deleteProject(horizontalPropertyId, projectId) {
    return this.delete(`${horizontalPropertyId}/delete/${projectId}`);
  }

  getProjectById(id, projectId: string): Observable<ApiResponse> {
    return this.get(`${id}/show/${projectId}`);
  }

  getProperties(id): Observable<ApiResponse> {
    return this.get(`${id}/properties`);
  }

  updateState(horizontalPropertyId: string, id: any, param3: { state: boolean }) {
    return this.put(`${horizontalPropertyId}/updateState/${id}`, param3);
  }

  fixedDebts(horizontalPropertyId: string, id: any) {
    return this.get(`${horizontalPropertyId}/fixedDebts/${id}`);
  }
}
