import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class ProjectItemService extends PrincipalService {
  route = 'project-item';

  getItems(horizontalPropertyId, projectId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/items/${projectId}`);
  }

  createProjectItem(horizontalPropertyId, projectId, items): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/create/${projectId}`, items);
  }
}

