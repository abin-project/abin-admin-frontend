import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import * as moment from 'moment';
import {compare, compareDate} from '../../../../services/globalFunctions';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {Project} from '../../../../model/hotizontal-property/project';
import {ProjectService} from '../project.service';
import {CreateProjectComponent} from '../create-project/create-project.component';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {ModificationProjectComponent} from '../modification-project/modification-project.component';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-list-project',
  templateUrl: './list-project.component.html',
  styleUrls: ['./list-project.component.css']
})
export class ListProjectComponent implements OnInit {

  displayedColumns: string[] = [
    'pos',
    'yearIni',
    'progress',
    'state',
    // 'option'
  ];
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  dataSource: MatTableDataSource<Project>;
  sortedData: Project[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private projectService: ProjectService,
              private dataGlobalService: DataGlobalService,
              private matSnackBar: MatSnackBar,
              private routerActive: ActivatedRoute,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.projectService.getProjects(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<Project>(res.data);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
    if (this.readNotOnly()) {
      if (!this.displayedColumns.find(r => r === 'option')) {
        this.displayedColumns.push('option');
      }
    }
  }

  getPeriod(year) {
    return moment(this.horizontalProperty.dateManagementInit).set('year', year).format('MMM-YYYY');
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  onlyRoot() {
    return this.rolService.getUserRol() === 'Root';
  }

  indexPosData(i) {
    return i + 1;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'yearIni':
          return compare(a.yearIni, b.yearIni, isAsc);
        case 'updatedAt':
          return compareDate(a.updatedAt, b.updatedAt, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Project>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(CreateProjectComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  delete(project) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.projectService.deleteProject(this.horizontalPropertyId, project.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  complete(project) {
    this.matSnackBar.open('Marcar Como completado', TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.projectService.updateState(this.horizontalPropertyId, project.id, {state: true}).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  modification(projectId) {
    this.dialog.open(ModificationProjectComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        projectId,
      }
    });
  }

  fixedDebt(id: any) {
    this.matSnackBar.open('Seguro de Corregir', 'Aceptar', {
      duration: 5000
    }).onAction().subscribe(a => {
      this.projectService.fixedDebts(this.horizontalPropertyId, id).subscribe(a => {
        console.log('Corregido');
      });
    });
  }
}
