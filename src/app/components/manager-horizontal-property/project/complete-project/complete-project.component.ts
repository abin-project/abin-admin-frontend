import {Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../../../model/hotizontal-property/project';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from '../project.service';
import {ItemService} from '../../item/item.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {ItemProjectView} from '../../../../model/hotizontal-property/itemProjectView';
import {ProjectItemService} from '../project-item.service';
import {MatDialog} from '@angular/material/dialog';
import {ModificationProjectComponent} from '../modification-project/modification-project.component';
import {PrintService} from '../../../../services/print.service';
import * as moment from 'moment';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {ExcelService} from '../../../../services/excel.service';

@Component({
  selector: 'abin-complete-project',
  templateUrl: './complete-project.component.html',
  styleUrls: ['./complete-project.component.css']
})
export class CompleteProjectComponent implements OnInit, OnDestroy {
  project: Project;
  projectId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  itemIncomes: ItemProjectView[];
  itemExpense: ItemProjectView[];
  totalIncomes: Period[] = [];
  totalExpenses: Period[] = [];
  months: string [] = [];
  totalMonths = 0;
  visible = true;
  format = 'es';
  horizontalPropertyId: string;

  constructor(private routerActive: ActivatedRoute,
              private projectService: ProjectService,
              private itemService: ItemService,
              private projectItemService: ProjectItemService,
              private printService: PrintService,
              private excelService: ExcelService,
              private dialog: MatDialog,
              private rolService: RolService
  ) {
    this.routerActive.params.subscribe(r => {
      this.projectId = r.projectId;
    });
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });

  }

  ngOnInit(): void {
    this.projectItemService.getItems(this.horizontalPropertyId, this.projectId).subscribe(r => {
      this.itemIncomes = r.data.income;
      this.itemExpense = r.data.expense;
      this.months = r.data.months;
      this.totalExpenses = r.data.totalExpense;
      this.totalIncomes = r.data.totalIncomes;
    });
    this.projectService.getProjectById(this.horizontalPropertyId, this.projectId).subscribe(r => {
      this.project = r.data;
      this.totalMonths = moment(this.project.yearEnd).diff(moment(this.project.yearIni), 'months') + 1;
    });
  }

  ngOnDestroy(): void {
    this.routerActive.params.subscribe().unsubscribe();
  }

  getItemTotal(item: ItemProjectView, t) {
    return item[t].map(f => Number(f.amountMonth ? f.amountMonth : 0)).reduce((a, b) => a + b, 0);
  }

  getItemTotalYear(item: ItemProjectView, t) {
    return item[t].map(f => Number(f.amountYear ? f.amountYear : 0)).reduce((a, b) => a + b, 0);
  }

  getTotalMonth(items: ItemProjectView[], t) {
    return items.map(f => this.getItemTotal(f, t)).reduce((a, b) => a + b, 0);
  }

  getTotalYear(items: ItemProjectView[], t) {
    return items.map(f => this.getItemTotalYear(f, t)).reduce((a, b) => a + b, 0);
  }

  calculateYearMonth(t) {
    t.amountYear = this.totalMonths * t.amountMonth;
  }

  modifications() {
    this.dialog.open(ModificationProjectComponent, {
      width: '500px',
      data: {
        projectId: this.projectId,
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(() => {
      this.ngOnInit();
    });
  }

  save() {
    this.projectItemService.createProjectItem(this.horizontalPropertyId, this.projectId, {
      expense: this.itemExpense.map(f => f.typesExpenses).reduce((a, b) => a.concat(b)),
      income: this.itemIncomes.map(f => f.typesIncomes).reduce((a, b) => a.concat(b)),
    }).subscribe(r => {
      this.ngOnInit();
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  getHeaderMonth(m) {
    if (this.format === 'es') {
      return m.header + ' ' + m.period;
    } else {
      return m.header + ' ' + m.period.replace('.-', '._');
    }
  }

  printFormat(format: string) {
    if (format === 'PDF') {
      this.print();
    }
    if (format === 'XLSX') {
      this.format = 'en';
      this.visible = false;
      setTimeout(() => {
        this.excelService.exportAsExcelFileByTable(null, `Informe de Presupuesto de ${moment(this.project.yearIni).format('MMMM-YYYY').toUpperCase()} a ${moment(this.project.yearEnd).format('MMMM-YYYY').toUpperCase()}`, 'project')
          .finally(() => {
          this.format = 'es';
          this.visible = true;
        });
      }, 500);
    }
  }

  print() {
    this.visible = false;
    setTimeout(async e => {
      // const colOption = {
      //   0: {
      //     halign: 'right',
      //   },
      //   2: {
      //     halign: 'right',
      //   },
      //   3: {
      //     halign: 'right',
      //   },
      //   4: {
      //     halign: 'right',
      //   },
      //   5: {
      //     halign: 'right',
      //   },
      //   6: {
      //     halign: 'right',
      //   },
      //   7: {
      //     halign: 'right',
      //   },
      //   8: {
      //     halign: 'right',
      //   },
      //   9: {
      //     halign: 'right',
      //   },
      //   10: {
      //     halign: 'right',
      //   },
      //   11: {
      //     halign: 'right',
      //   },
      //   12: {
      //     halign: 'right',
      //   },
      //   13: {
      //     halign: 'right',
      //   },
      //   14: {
      //     halign: 'right',
      //   },
      //   15: {
      //     halign: 'right',
      //   },
      //   16: {
      //     halign: 'right',
      //   },
      //   17: {
      //     halign: 'right',
      //   },
      //   18: {
      //     halign: 'right',
      //   },
      //   19: {
      //     halign: 'right',
      //   },
      // };
      const titles = [
        `${this.horizontalProperty.nameProperty}`,
        `Informe de Presupuesto de ${moment(this.project.yearIni).format('MMMM-YYYY').toUpperCase()} a ${moment(this.project.yearEnd).format('MMMM-YYYY').toUpperCase()}`
      ];
      await this.printService.printProjectById(titles,  'project');
      this.visible = true;
    }, 1000);
    // this.visible = true;
  }

}
export interface Period {
  period: string;
  totalAmount: number;
}
