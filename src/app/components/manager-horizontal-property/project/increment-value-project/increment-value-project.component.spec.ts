import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IncrementValueProjectComponent} from './increment-value-project.component';

describe('IncrementValueProjectComponent', () => {
  let component: IncrementValueProjectComponent;
  let fixture: ComponentFixture<IncrementValueProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncrementValueProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncrementValueProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
