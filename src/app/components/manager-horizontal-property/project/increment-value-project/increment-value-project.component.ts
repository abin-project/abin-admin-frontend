import {Component, Inject, OnInit} from '@angular/core';
import {ItemService} from '../../item/item.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {Item} from '../../../../model/hotizontal-property/item';

@Component({
  selector: 'abin-increment-value-project',
  templateUrl: './increment-value-project.component.html',
  styleUrls: ['./increment-value-project.component.css']
})
export class IncrementValueProjectComponent implements OnInit {
  horizontalPropertyId: string;
  items: ItemIncrement[] = [];

  constructor(private itemService: ItemService,
              private dialogRef: MatDialogRef<IncrementValueProjectComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;
  }

  ngOnInit(): void {
    if (!this.data.increments) {
      this.itemService.getIncomeEnable(this.horizontalPropertyId).subscribe(r => {
        this.items = r.data as ItemIncrement[];
      });
    } else {
      this.items = this.data.increments as ItemIncrement[];
    }

  }

  accept(): void {
    this.dialogRef.close(this.items);
  }

}

export interface ItemIncrement extends Item {
  typeIncomes: TypeIncomeIncrement[];
}

export interface TypeIncomeIncrement extends TypeIncome {
  amount: number;
  isPercent: boolean;
  increment: boolean;
}
