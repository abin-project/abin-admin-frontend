import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class ProjectModificationService extends PrincipalService {
  route = 'project-modification';

  getModificationsTransfer(projectId): Observable<ApiResponse> {
    return this.get(`${projectId}/transfer`);
  }

  getModificationsAdditional(projectId): Observable<ApiResponse> {
    return this.get(`${projectId}/additional`);
  }

  createModificationTransfer(projectId, data): Observable<any> {
    return this.postCustom(`${projectId}/transfer`, data);
  }

  createModificationAdditional(projectId, data): Observable<any> {
    return this.postCustom(`${projectId}/additional`, data);
  }

  deleteModificationTransfer(projectId, transferId) {
    return this.delete(`${projectId}/transfer/${transferId}`);
  }

  deleteModificationAdditional(projectId, additionalId) {
    return this.delete(`${projectId}/additional/${additionalId}`);
  }

}
