import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ProjectService} from '../project.service';
import {ItemService} from '../../item/item.service';
import {IncrementValueProjectComponent} from '../increment-value-project/increment-value-project.component';
import {IncrementManualComponent} from '../increment-manual/increment-manual.component';

@Component({
  selector: 'abin-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {

  formProject: FormGroup;
  horizontalPropertyId: string;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateProjectComponent>,
              private dialog: MatDialog,
              private projectService: ProjectService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formProject = this.fb.group({
      yearIni: [null, [Validators.required]],
      yearEnd: [null, [Validators.required]],
      increments: null,
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formProject.valid) {
      const res = this.formProject.value;
      if (res.increments == null) {
        res.increments = [];
      }
      this.projectService.createProject(this.horizontalPropertyId, res).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

  openIncrement() {
    this.dialog.open(IncrementValueProjectComponent, {
      width: '500px',
      data: {
        ...this.data,
        increments: this.formProject.get('increments').value,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.formProject.get('increments').setValue(r);
      }
    });
  }
  openManual() {
    this.dialog.open(IncrementManualComponent, {
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      },
      width: '600px'
    });
  }

}
