import {Component, Inject, OnInit} from '@angular/core';
import {ProjectService} from '../project.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Property} from '../../../../model/hotizontal-property/property';
import {PropertyService} from '../../property/property.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-increment-manual',
  templateUrl: './increment-manual.component.html',
  styleUrls: ['./increment-manual.component.css']
})
export class IncrementManualComponent implements OnInit {
  propertiesList: InterfaceGroupProperty[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<IncrementManualComponent>,
    private projectService: ProjectService,
    private snack: MatSnackBar,
    private propertyService: PropertyService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.projectService.getProperties(this.data.horizontalPropertyId).subscribe(r => {
      this.propertiesList = r.data;
    });
  }

  async save() {
    this.snack.open('Seguro de Guardar? es Irréversible', 'ACEPTAR', {
      duration: 5000,
    }).onAction().subscribe(async r => {
      const update = await this.propertiesUpdate();
      this.propertyService.updateIncomesProperty(this.data.horizontalPropertyId, update).subscribe(() => {
        this.ngOnInit();
      });
    });

  }

  async propertiesUpdate() {
    return this.propertiesList.map(r => {
      return r.properties.map((p: Property) => {
        return p.typeIncomeProperty.map(t => {
          return {
            id: t.id,
            amount: t.amount
          };
        });
      }).reduce((a, b) => a.concat(b));
    });
  }

}

export interface InterfaceGroupProperty {
  type: string;
  properties: Property[];
}
