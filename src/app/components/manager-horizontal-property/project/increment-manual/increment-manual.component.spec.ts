import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncrementManualComponent } from './increment-manual.component';

describe('IncrementManualComponent', () => {
  let component: IncrementManualComponent;
  let fixture: ComponentFixture<IncrementManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncrementManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncrementManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
