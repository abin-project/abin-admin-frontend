import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeResultComponent } from './income-result.component';

describe('IncomeResultComponent', () => {
  let component: IncomeResultComponent;
  let fixture: ComponentFixture<IncomeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
