import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {BankBalance, ItemTotal} from '../income-expense-state-result/income-expense-state-result.component';
import {TransactionService} from '../../banks/transaction.service';

moment.locale('es');
@Component({
  selector: 'abin-income-result',
  templateUrl: './income-result.component.html',
  styleUrls: ['./income-result.component.css']
})
export class IncomeResultComponent implements OnInit {
  @Input() itemIncomes: ItemTotal;
  @Input() itemExpenses: ItemTotal;
  @Input() moreDetail: number;
  @Input() period: any;
  @Input() lastTotal: number;
  @Input() totalTransfer: number;
  @Input() format = 'es';
  @Input() accounts: BankBalance[];
  @Input() transfers: ViewTransfer[] = [];
  @Input() bankId: string = null;
  @Input() horizontalPropertyId: string = null;
  day;
  month;
  year;
  dataAccount;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.day = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('DD');
    this.month = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('MMMM');
    this.year = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('YYYY');
    if (this.bankId) {
      this.transactionService.getIncomeExtractByAccount(this.horizontalPropertyId, this.bankId, {
        year: this.year,
        month: moment(`${this.period}`, 'MMMM-YYYY').format('MM'),
        state: 4}).subscribe((a: any) => {
        this.dataAccount = a.other;
      });
    }
  }

  getDatePeriod() {

    return `Saldo al ${this.day} de ${this.month} de ${this.year}`.toUpperCase();
  }

  getDebtActualPeriod() {
    const month = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('MMMM');
    const year = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('YYYY');
    return `Saldo del Mes de ${month} ${year}`.toUpperCase();
  }

  getTotalDebt() {
    const month = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('MMMM');
    const year = moment(`${this.period}`, 'MMMM-YYYY').endOf('month').format('YYYY');
    return `Saldo Según Banco al ${this.day} de ${month} de  ${year}`.toUpperCase();
  }

  getDateLast() {
    const month = moment(`${this.period}`, 'MMMM-YYYY').subtract(1, 'month').format('MMMM');
    const year = moment(`${this.period}`, 'MMMM-YYYY').subtract(1, 'month').format('YYYY');
    return `Saldo al Mes de ${month} de ${year}`.toUpperCase();
  }

  getTransfers(t: ViewTransfer) {
    if (t.amount > 0) {
      return `Transferencia de Cta. ${t.numberAccount} en Fecha ${moment(t.date).format('DD-MM-YYYY')}`.toUpperCase();
    } else {
      return `Transferencia a Cta. ${t.numberAccount} en Fecha ${moment(t.date).format('DD-MM-YYYY')}`.toUpperCase();
    }
  }

  getTextBalance(t: BankBalance) {
    return `Saldo ${t.account.typeAccount} ${t.account.nameBank} ${t.account.numberAccount}`.toUpperCase();
  }

  getDifference() {
    if (this.dataAccount) {
      const r = ((this.itemIncomes.total - this.itemExpenses.total) + this.lastTotal + this.totalTransfer);
      const a = this.dataAccount?.lastTotal;
      if (a && r) {
        return (Number(r.toFixed(2)) - Number(a.toFixed(2))).toFixed(2);
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  equals() {
    if (this.dataAccount) {
      const r = ((this.itemIncomes.total - this.itemExpenses.total) + this.lastTotal + this.totalTransfer);
      const a = this.dataAccount?.lastTotal;
      return a.toFixed(2) === r.toFixed(2);
    } else {
      return '';
    }
  }
}

export interface ViewTransfer {
  amount: number;
  date: string;
  id: string;
  nameBank: string;
  numberAccount: string;
}
