import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseResultComponent } from './expense-result.component';

describe('ExpenseResultComponent', () => {
  let component: ExpenseResultComponent;
  let fixture: ComponentFixture<ExpenseResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
