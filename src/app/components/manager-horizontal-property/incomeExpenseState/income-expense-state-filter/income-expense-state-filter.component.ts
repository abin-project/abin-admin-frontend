import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'abin-income-expense-state-filter',
  templateUrl: './income-expense-state-filter.component.html',
  styleUrls: ['./income-expense-state-filter.component.css']
})
export class IncomeExpenseStateFilterComponent implements OnInit {
  // @Input() data: any = {
    // horizontalPropertyId: null,
  // };
  @Output() onSave = new EventEmitter();

  filterForm: FormGroup;
  horizontalPropertyId: string;
  months = [
    {monthNum: 1, month: 'Enero'},
    {monthNum: 2, month: 'Febrero'},
    {monthNum: 3, month: 'Marzo'},
    {monthNum: 4, month: 'Abril'},
    {monthNum: 5, month: 'Mayo'},
    {monthNum: 6, month: 'Junio'},
    {monthNum: 7, month: 'Julio'},
    {monthNum: 8, month: 'Agosto'},
    {monthNum: 9, month: 'Septiembre'},
    {monthNum: 10, month: 'Octubre'},
    {monthNum: 11, month: 'Noviembre'},
    {monthNum: 12, month: 'Diciembre'},
  ];
  constructor(private fb: FormBuilder,
              private routerActive: ActivatedRoute,
              private router: Router) {
    this.routerActive.queryParams.subscribe(r => {
      this.filterForm = this.fb.group({
        year: [r.year, [Validators.required]],
        month: [r.month, [Validators.required]],
      });
    });
  }

  ngOnInit(): void {

  }

  async save() {
    await this.router.navigate([], {queryParams: this.filterForm.value});
    this.onSave.emit('pressed');
  }
}

