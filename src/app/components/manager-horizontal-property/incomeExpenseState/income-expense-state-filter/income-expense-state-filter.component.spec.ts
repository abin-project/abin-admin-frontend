import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeExpenseStateFilterComponent } from './income-expense-state-filter.component';

describe('IncomeExpenseStateFilterComponent', () => {
  let component: IncomeExpenseStateFilterComponent;
  let fixture: ComponentFixture<IncomeExpenseStateFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeExpenseStateFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeExpenseStateFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
