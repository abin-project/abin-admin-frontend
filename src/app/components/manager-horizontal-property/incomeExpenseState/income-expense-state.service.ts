import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PrincipalService } from 'src/app/services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class IncomeExpenseStateService extends PrincipalService {
  route = 'income-expense-state';
  filterIncomeState(horizontalPropertyId, query): Observable<any> {
    return this.getQuery(`${horizontalPropertyId}`, query);
  }

  closeMonth(horizontalPropertyId: string, body: any) {
    return this.postCustom(`${horizontalPropertyId}/closeMonth`, body);
  }
}
