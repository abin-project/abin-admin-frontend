import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeExpenseStateResultComponent } from './income-expense-state-result.component';

describe('IncomeExpenseStateResultComponent', () => {
  let component: IncomeExpenseStateResultComponent;
  let fixture: ComponentFixture<IncomeExpenseStateResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeExpenseStateResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeExpenseStateResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
