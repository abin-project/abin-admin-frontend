import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import * as moment from 'moment';
import {Bank} from 'src/app/model/hotizontal-property/bank';
import {Item} from 'src/app/model/hotizontal-property/item';
import {IncomeExpenseStateService} from '../income-expense-state.service';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {ExcelService} from '../../../../services/excel.service';
import {ViewTransfer} from '../income-result/income-result.component';
import {ProjectService} from '../../project/project.service';
import {ProjectExtends} from '../../ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {Project} from '../../../../model/hotizontal-property/project';
import {Subscription} from 'rxjs';

moment.locale('es');

@Component({
  selector: 'abin-income-expense-state-result',
  templateUrl: './income-expense-state-result.component.html',
  styleUrls: ['./income-expense-state-result.component.css']
})
export class IncomeExpenseStateResultComponent implements OnInit, OnDestroy {
  accounts: AccountReport[] = [];
  horizontalPropertyId: string;
  horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  query: any = {};
  level = 2;
  accountSelect: AccountReport;
  projects: ProjectExtends[];
  format = 'es';
  params = {};
  events: Subscription;
  constructor(
    private routerActive: ActivatedRoute,
    private printService: PrintService,
    private stateService: IncomeExpenseStateService,
    private snack: MatSnackBar,
    private excelService: ExcelService,
    private projectService: ProjectService,
    private rolService: RolService,
    private router: Router) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.query = r;
    });
    this.events = this.router.events.subscribe(a => {
      if (a instanceof NavigationEnd) {
        this.loadData();
      }
    });
  }

  ngOnInit(): void {
    this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projects = r.data.map(a => {
        return {
          ...a,
          periods: this.processPeriods(a),
          isOpen: this.getOpen(a)
        };
      });
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe();
  }

  loadData() {
    if (this.filtered()) {
      this.stateService.filterIncomeState(this.horizontalPropertyId, this.query).subscribe(r => {
        this.accounts = r;
      });
    }
  }

  filtered() {
    if (this.query !== {}) {
      return this.query.year !== undefined && this.query.month !== undefined;
    } else {
      return false;
    }
  }

  closeMonth() {
    this.snack.open('Esta Seguro de Cerrar', 'Aceptar', {
      duration: 5000
    }).onAction().subscribe(a => {
      this.stateService.closeMonth(this.horizontalPropertyId, this.query).subscribe(r => {
        this.ngOnInit();
      });
    });
  }

  periodView() {
    return moment(`${this.query.year}-${this.query.month}`, 'YYYY-M').format('MMMM-YYYY').toLocaleUpperCase();
  }

  setLevel(l) {
    this.level = l;
  }

  changeTab(event) {
    this.accountSelect = this.accounts[event];
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  processPeriods(project: Project) {
    const res = [];
    const b = moment(project.yearIni);
    const total = moment(project.yearEnd).diff(b, 'months') + 1;
    for (let i = 0; i < total; i++) {
      res.push({
        text: b.locale('es-ES').format('YYYY-MMMM').toUpperCase(),
        monthNum: (b.month() + 1),
        year: (b.year())
      });
      b.add(1, 'month');
    }
    return res;
  }

  getOpen(a: Project) {
    const now = moment();
    return now >= moment(a.yearIni) && now <= moment(a.yearEnd);
  }

  projectSelectA(p: ProjectExtends) {
    p.isOpen = !p.isOpen;
  }

  getParams(month) {
    return {
      ...this.params,
      year: month.year,
      month: month.monthNum,
    };
  }

  async report(month) {
    const colOption = {
      8: {
        halign: 'right',
      },
      9: {
        halign: 'right',
      },
      10: {
        halign: 'right',
      },
      11: {
        halign: 'right',
      }
    };
    const titles = [
      this.horizontalProperty.nameProperty.toUpperCase(),
      this.accountSelect.account.nameBank.toUpperCase(),
    ];
    if (this.accountSelect.account.title) {
      titles.push(`${this.accountSelect.account.title}`.toUpperCase());
    }
    titles.push(`Correspondiente al mes de ${month} de ${this.query.year}`.toUpperCase());
    titles.push(`(Expresado en bolivianos)`);
    await this.printService
      .createIdTableIncomeExpense(
        titles,
        colOption,
        'table');
    this.printService.auxLines = [];
  }

  async printReport(type) {
    const month = moment(`${this.query.year}-${this.query.month}`, 'YYYY-M').format('MMMM');
    if (type === 'XLSX') {
      this.format = 'en';
      setTimeout(() => {
        this.excelService.exportAsExcelFileByTable( null, `Estado de Ingreso y Egreso ${month} de ${this.query.year}`, 'table')
          .finally(() => {
          this.format = 'es';
        });
      }, 500);
    } else if ( type === 'PDF') {
      this.format = 'es';
      this.report(month);
    }
  }

}

export interface ItemReport extends Item{
  total: number;
}

export interface AccountReport {
  account: Bank;
  incomes: ItemTotal;
  expense: ItemTotal;
  lastTotal: number;
  transfers?: ViewTransfer[];
  totalActualTransfer?: number;
  balances: BankBalance;
}

export interface ItemTotal {
  total: number;
  items: ItemReport[];
}
export interface BankBalance {
  account: Bank;
  balance: number;
}
