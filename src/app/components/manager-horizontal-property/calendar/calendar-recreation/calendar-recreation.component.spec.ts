import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarRecreationComponent} from './calendar-recreation.component';

describe('CalendarRecreationComponent', () => {
  let component: CalendarRecreationComponent;
  let fixture: ComponentFixture<CalendarRecreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarRecreationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarRecreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
