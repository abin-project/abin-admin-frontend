import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CalendarOptions, FullCalendarComponent} from '@fullcalendar/angular';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {ReservationService} from '../reservation.service';
import {RecreationAreaReservation} from '../../../../model/hotizontal-property/recreationAreaReservation';
import {RecreationAreaService} from '../../recreationArea/recreation-area.service';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CalendarEventShowComponent} from '../calendar-event-show/calendar-event-show.component';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {CalendarReservationManualComponent} from '../calendar-reservation-manual/calendar-reservation-manual.component';
import {DataGlobalService} from '../../../../services/data-global.service';
// useful for typechecking
moment().locale('es-Es');

@Component({
  selector: 'abin-calendar-recreation',
  templateUrl: './calendar-recreation.component.html',
  styleUrls: ['./calendar-recreation.component.css']
})
export class CalendarRecreationComponent implements OnInit, AfterViewInit {
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    locale: 'es',
    headerToolbar: {
      left: 'prevBtn,nextBtn today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay',
    },
    customButtons: {
      prevBtn: {
        icon: 'fc-icon-chevron-left',
        click: this.btnPrevMonth.bind(this)
      },
      nextBtn: {
        icon: 'fc-icon-chevron-right',
        click: this.btnRightMonth.bind(this)
      }
    },
    buttonText: {
      today: 'Hoy',
      month: 'Mes',
      week: 'Semana',
      day: 'Día',
      list: 'Lista'
    },
    handleWindowResize: true,
    // rerenderDelay: 1,
    height: '100%',
    eventClick: this.eventClick.bind(this),
    // eventMouseLeave: this.mouseLeave.bind(this),
    allDayText: 'Todos los Dias',
    // themeSystem: 'material',
    dateClick: this.handleDateClick.bind(this), // bind is important!
    events: [],
    eventClassNames: 'active'
  };
  horizontalPropertyId;
  reservations: RecreationAreaReservation[] = [];
  myReservations: RecreationAreaReservation[] = [];
  eventSelect: RecreationAreaReservation;
  calendarApi;
  positionsPopup;
  actualMonth;

  recreationsArea: RecreationArea[];
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  constructor(
    private routerActive: ActivatedRoute,
    private reservationService: ReservationService,
    private recreationAreaService: RecreationAreaService,
    private globalService: DataGlobalService,
    private dialog: MatDialog,
    private snack: MatSnackBar) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });

  }


  ngOnInit(): void {
    this.loadData();
    // this.globalService.setOpen();
    this.globalService.eventOpen.toPromise().finally(() => {
      setTimeout(() => {
        this.calendarApi.render();
      }, 200);
    });
  }

  ngAfterViewInit(): void {
    this.calendarApi = this.calendarComponent.getApi();
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  loadData() {
    this.reservationService.getAllReservation(this.horizontalPropertyId).subscribe(r => {
      this.reservations = r.data;
      this.processReservations();
      this.loadDateMonth(this.calendarApi.currentData.currentDate);
    });
    this.recreationAreaService.getAll(this.horizontalPropertyId).subscribe(r => {
      this.recreationsArea = r.data;
    });
  }

  btnPrevMonth(evt) {
    this.calendarApi.prev();
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  btnRightMonth(evt) {
    this.calendarApi.next();
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  loadDateMonth(date) {
    const d = moment(date).add(1, 'day').format('YYYY-MM');
    this.actualMonth = d;
    // this.myReservations = this.reservations.filter(r => {
    //   return moment(r.date).format('YYYY-MM') == d && r.property.id == 'bacc4e3a-4f17-4762-bf06-8955a9968920';
    // });

  }

  handleDateClick(arg) {
    if (moment(arg.dateStr) >= moment(moment().format('YYYY-MM-DD'))) {
      this.dialog.open(CalendarReservationManualComponent, {
        width: '400px',
        data: {
          id: this.horizontalPropertyId,
          ...arg,
        },
      }).afterClosed().subscribe(r => {
        if (r) {
          this.loadData();
        }
      });
    } else {
      this.snack.open('Fecha Invalida', null, {duration: 2000});
    }
  }

  reservationManual() {
    this.dialog.open(CalendarReservationManualComponent, {
      width: '400px',
      data: {
        id: this.horizontalPropertyId
      },
    }).afterClosed().subscribe(r => {
      if (r) {
        this.loadData();
      }
    });
  }

  eventClick(ev) {
    this.eventSelect = ev.event.extendedProps;
    this.dialog.open(CalendarEventShowComponent, {
      data: {
        event: this.eventSelect,
        horizontalPropertyId: this.horizontalPropertyId,
      },
      width: '300px'
    }).afterClosed().subscribe(a => {
      if (a === true) {
        this.ngOnInit();
      }
    });
  }

  mouseLeave(ev) {
    this.eventSelect = null;
    this.positionsPopup = null;
  }

  // delete(event: RecreationAreaReservation) {
  //   this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
  //     duration: TIME_DURATION,
  //   }).onAction().subscribe(() => {
  //     this.recreationAreaService.deleteReservation(this.horizontalPropertyId, event.id).subscribe(() => {
  //       this.loadData();
  //     });
  //     // this.reservationService.deleteReservation(this.horizontalPropertyId, event.id).subscribe(() => {
  //     //   this.loadData();
  //     // });
  //
  //   });
  //
  // }
  //
  // show(event) {
  //   this.dialog.open(CalendarEventShowComponent, {
  //     width: '354px',
  //     data: {
  //       event: event
  //     }
  //   });
  // }
  //
  // createRecreation() {
  //   this.dialog.open(CreateRecreationAreaComponent, {
  //     width: '400px',
  //     data: {
  //       horizontalPropertyId: this.horizontalPropertyId
  //     }
  //   }).afterClosed().subscribe(() => {
  //     this.loadData();
  //   });
  // }
  //
  // edit(event) {
  //   this.dialog.open(CreateRecreationAreaComponent, {
  //     width: '400px',
  //     data: {
  //       horizontalPropertyId: this.horizontalPropertyId,
  //       ...event
  //     }
  //   }).afterClosed().subscribe(() => {
  //     this.loadData();
  //   });
  // }

  getStyleEvent() {
    if (this.eventSelect && this.positionsPopup) {
      return {display: 'block', top: `${this.positionsPopup.y}px`, left: `${this.positionsPopup.x}px`};
    }
  }

  processReservations() {
    this.calendarOptions.events = this.reservations.map(r => {
      return {
        title: r.recreationArea.name,
        date: r.date,
        color: r.recreationArea.color,
        id: r.id,
        allDay: r.allDay,
        ...r.time,
        extendedProps: r,
      };
    });
  }

  datePipe(reservation: RecreationAreaReservation) {
    if (reservation.allDay) {
      return `${moment(reservation.date).format('YYYY-MMM-DD')} (Todo el día)`;
    } else {
      return moment(reservation.date).format('YYYY-MMM-DD, hh:mm A');
    }
  }
}
