import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ReservationService} from '../reservation.service';
import {RecreationAreaService} from '../../recreationArea/recreation-area.service';
import {PropertyService} from '../../property/property.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {Property} from '../../../../model/hotizontal-property/property';
import {Observable} from 'rxjs';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RolService} from '../../../admin-horizontal-property/rol.service';
moment.locale('es');

@Component({
  selector: 'abin-calendar-reservation-manual',
  templateUrl: './calendar-reservation-manual.component.html',
  styleUrls: ['./calendar-reservation-manual.component.css']
})
export class CalendarReservationManualComponent implements OnInit {

  recreationForm: FormGroup;
  recreationAreas: RecreationArea[];
  recreationSelect: RecreationArea;
  properties: Property[];
  filteredOptions: Observable<Property[]>;
  dayEquivalent = {
    0: 7,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
  };
  max = moment().format('YYYY-MM-DD');
  time = {
    start: null,
    end: null,
    total: null
  };
  reservationArea: RecreationArea;
  user;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CalendarReservationManualComponent>,
              private fb: FormBuilder,
              private propertyService: PropertyService,
              private reservationService: ReservationService,
              private recreationAreaService: RecreationAreaService,
              private snack: MatSnackBar,
              private rolService: RolService) {
    this.recreationForm = this.fb.group({
      property: [this.data.property, Validators.required],
      date: [this.data.date, [Validators.required]],
      recreationArea: [null, Validators.required],
      allDay: [this.data.allDay ? this.data.allDay : false, Validators.required],
      totalAmount: [{value: null, disabled: true}],
      amountSelect: null,
      time: null
    });
    this.user = JSON.parse(atob(localStorage.getItem('profile')));
  }

  ngOnInit(): void {
    this.recreationAreaService.getAll(this.data.id).subscribe(r => {
      this.recreationAreas = r.data;
    });
    if (this.readNotOnly()) {
      this.propertyService.getPropertiesOnHorizontalProperty(this.data.id).subscribe((res: ApiResponse) => {
        this.properties = res.data;
        this.filteredOptions = this.recreationForm.get('property').valueChanges
          .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value.code),
            map(name => name ? this._filter(name) : this.properties.slice()),
          );
      });
    } else {
      this.propertyService.getPropertiesByOwner(this.data.id, this.user.id).subscribe(r => {
        this.properties = r.data;
        this.filteredOptions = this.recreationForm.get('property').valueChanges
          .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value.code),
            map(name => name ? this._filter(name) : this.properties.slice()),
          );
      });
    }

  }

  async changeRecreation() {
    this.recreationSelect = this.recreationForm.get('recreationArea').value;
    this.recreationForm.get('amountSelect').setValue(null);
    await this.findDay();
    this.setTotalPay();
  }

  changeTime() {
    const timeIni = moment(this.time.start, 'HH:mm');
    const timeEnd = moment(this.time.start, 'HH:mm').add(this.time.total, 'hour');
    this.time.start = timeIni.format('HH:mm');
    this.time.end = timeEnd.format('HH:mm');
    this.setTotalPay();
  }

  save() {
    console.log(this.recreationForm.value)
    if (this.recreationForm.valid) {
      if (!this.recreationSelect.isTimeUseLimit) {
        this.recreationForm.get('allDay').setValue(true);
      } else {
        this.recreationForm.get('allDay').setValue(false);
        if (this.recreationSelect.isTimeUseLimit) {
          if (this.time.total > this.recreationSelect.timeUseLimit) {
            this.snack.open('Cantidad de Horas no permitidas', null, {duration: 5000});
            return;
          }
          if (this.hourNotValid()) {
            this.snack.open('Horario no permitido', null, {duration: 5000});
            return;
          }
        }
      }
      if (this.setHour()) {
        let timeIni = undefined
        let timeEnd = undefined
        const a = this.getHourDay()

        if (a) {
          timeIni = a.timeIni.split(':')
          timeEnd = a.timeEnd.split(':')
        }

        if (this.recreationSelect.isTimeUseLimit) {
          timeIni = this.time.start.split(':');
          timeEnd = this.time.end.split(':');
        }

        console.log(timeIni, timeEnd)

        this.time.start = moment(this.recreationForm.get('date').value).set({hour: Number(timeIni[0]), minute: Number(timeIni[1])}).format('YYYY-MM-DD HH:mm:ss');
        this.time.end = moment(this.recreationForm.get('date').value).set({hour: Number(timeEnd[0]), minute: Number(timeEnd[1])})

        if (Number(timeEnd[0]) < Number(timeIni[0])) {
          this.time.end = this.time.end.add( 1, 'day').format('YYYY-MM-DD HH:mm:ss')
        }

        this.recreationForm.get('time').setValue(this.time);
        this.reservationService.createReservation(this.data.id, this.recreationForm.value).subscribe(() => {
          this.dialogRef.close(this.recreationForm.value);
        });
      }
    }
  }

  findDay() {
    // 0=>D 1=>L 2=>M 3=>M 4=>J 5=>V 6=>S
    const date = moment(this.recreationForm.get('date').value);
    const dateString = date.format('YYYY-MM-DD');
    // const day = this.recreationSelect.recreationAreaDays.find(r => r.dayNumber === this.dayEquivalent[date]);
    const area = this.recreationForm.get('recreationArea').value;
    this.reservationService.getInfoRecreation(this.data.id, area.id, {start: dateString, end: dateString}).subscribe(r => {
        this.reservationArea = r.data;
    });
  }

  setHour() {
    const date = moment(this.recreationForm.get('date').value);
    if (this.recreationSelect) {
      if (this.reservationArea) {
        if (this.recreationSelect.isTimeUseLimit) {
          const dayDate = this.recreationSelect.recreationAreaDays.find(r => r.dayNumber === this.dayEquivalent[date.day()]);
          if (dayDate === undefined) {
            this.snack.open('Fecha no disponible', null, {duration: 2000});
            return false;
          } else if (this.time.total > this.recreationSelect.timeUseLimit ) {
            this.snack.open('Tiempo de uso no Valido', null, {duration: 2000});
            return false;
          } else {
            const timeIni = this.time.start.split(':');
            const timeEnd = this.time.end.split(':');
            const ini = moment(this.recreationForm.get('date').value).set({hour: timeIni[0], minute: timeIni[1]});
            const end = moment(this.recreationForm.get('date').value).set({hour: timeEnd[0], minute: timeEnd[1]});
            const exist = this.reservationArea.recreationAreaReservation.find(r => moment(r.date) >= ini && moment(r.date) <= end);
            const res = exist === undefined;
            if (!res) {
              this.snack.open('Horario no Disponible', null, {duration: 2000});
            }
            return res;
          }
        } else {
          if (this.reservationArea.recreationAreaReservation) {
            const dateMoment = date.format('YYYY-MM-DD');
            const dayDate = this.recreationSelect.recreationAreaDays.find(r => r.dayNumber === this.dayEquivalent[date.day()]);
            const dayTime = this.reservationArea.recreationAreaReservation.find(r => moment(r.date).format('YYYY-MM-DD') === dateMoment );
            const res = dayTime === undefined && dayDate !== undefined;
            if (!res) {
              this.snack.open('Fecha no disponible', null, {duration: 2000});
            }
            return res;
          } else {
            // this.snack.open('Fecha no disponible', null, {duration: 2000});
            return true;
          }
        }
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  setTotalPay() {
    if (this.recreationSelect) {
      this.recreationForm.get('totalAmount').setValue(this.time.total * this.recreationSelect.amount);
    }
  }

  getHourUse() {
    if (this.recreationSelect) {
      const date = moment(this.recreationForm.get('date').value);
      const dayDate = this.recreationSelect.recreationAreaDays.find(r => r.dayNumber === this.dayEquivalent[date.day()]);
      return dayDate !== undefined && (dayDate.timeEnd !== null && dayDate.timeIni !== null) ? `Abierto  ${date.format('dddd')} desde : ${dayDate.timeIni} hasta ${dayDate.timeEnd}` : 'No Disponible';
    } else {
      return 'No Disponible';
    }
  }

  displayFn(property: Property): string {
    return property && property.code ? property.name : '';
  }

  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }

  hourNotValid() {
    let res = true;
    const ini = moment(this.time.start, 'HH:mm');
    const end = moment(this.time.end, 'HH:mm');
    this.recreationSelect.recreationAreaDays.forEach(r =>  {
      const date = moment(this.recreationForm.get('date').value);
      if (date.day() === r.dayNumber) {
        const dateStart = moment(r.timeIni, 'HH:mm');
        const dateEnd = moment(r.timeEnd, 'HH:mm');
        if (dateStart <= ini && dateEnd >= end) {
          res = false;
        }
      }
    });
    return res;
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  getDateIni() {
    const date = moment(this.recreationForm.get('date').value);
    return date.isValid() ? date.format('DD-MMMM-YYYY').toUpperCase() : '';
  }

  getDateEnd() {
    const date = moment(this.recreationForm.get('date').value);
    return date.isValid() ? date.add( 1, 'month').subtract(1, 'day').format('DD-MMMM-YYYY').toUpperCase() : '';
  }

  getHourDay() {
    if (this.recreationSelect) {
      const date = moment(this.recreationForm.get('date').value);
      const dayDate = this.recreationSelect.recreationAreaDays.find(r => r.dayNumber === this.dayEquivalent[date.day()]);
      console.log(dayDate)
      return dayDate !== undefined && (dayDate.timeEnd !== null && dayDate.timeIni !== null) ? dayDate : undefined;
    }
    return undefined
  }
}
