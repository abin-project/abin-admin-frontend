import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarReservationManualComponent} from './calendar-reservation-manual.component';

describe('CalendarReservationManualComponent', () => {
  let component: CalendarReservationManualComponent;
  let fixture: ComponentFixture<CalendarReservationManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarReservationManualComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarReservationManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
