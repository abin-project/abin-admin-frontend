import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarReservationComponent} from './calendar-reservation.component';

describe('CalendarReservationComponent', () => {
  let component: CalendarReservationComponent;
  let fixture: ComponentFixture<CalendarReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarReservationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
