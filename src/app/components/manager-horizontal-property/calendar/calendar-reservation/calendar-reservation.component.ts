import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RecreationAreaService} from '../../recreationArea/recreation-area.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {ReservationService} from '../reservation.service';

@Component({
  selector: 'abin-calendar-reservation',
  templateUrl: './calendar-reservation.component.html',
  styleUrls: ['./calendar-reservation.component.css']
})
export class CalendarReservationComponent implements OnInit {

  recreationForm: FormGroup;
  recreationAreas: RecreationArea[];
  recreationSelect: RecreationArea;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CalendarReservationComponent>,
              private fb: FormBuilder,
              private reservationService: ReservationService,
              private recreationAreaService: RecreationAreaService) {
    this.recreationForm = this.fb.group({
      date: [this.data.date, Validators.required],
      recreationArea: [null, Validators.required],
      allDay: [this.data.allDay, Validators.required],
      quantity: [null, [Validators.required, Validators.min(1)]],
      totalAmount: [{value: null, disabled: true}, Validators.required],
      propertyId: ['bacc4e3a-4f17-4762-bf06-8955a9968920', Validators.required]
    });
    this.recreationAreaService.getAll(this.data.id).subscribe(r => {
      this.recreationAreas = r.data;
    });
    this.recreationForm.get('quantity').valueChanges.subscribe(r => {
      if (this.recreationSelect) {
        this.recreationForm.get('totalAmount').setValue(this.recreationSelect.amount * r);
      }
    });
  }

  ngOnInit(): void {
  }

  changeRecreation() {
    this.recreationSelect = this.recreationForm.get('recreationArea').value;
  }

  save() {
    if (this.recreationForm.valid) {
      this.reservationService.createReservation(this.data.id, this.recreationForm.value).subscribe(() => {
        this.dialogRef.close(this.recreationForm.value);
      });
    }
  }

}
