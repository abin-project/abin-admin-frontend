import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RecreationAreaReservation} from '../../../../model/hotizontal-property/recreationAreaReservation';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ReservationService} from '../reservation.service';

@Component({
  selector: 'abin-calendar-event-show',
  templateUrl: './calendar-event-show.component.html',
  styleUrls: ['./calendar-event-show.component.css']
})
export class CalendarEventShowComponent implements OnInit {

  eventSelect: RecreationAreaReservation;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private snack: MatSnackBar,
              private dialogReg: MatDialogRef<CalendarEventShowComponent>,
              private reservationService: ReservationService) {
    this.eventSelect = data.event;
  }

  ngOnInit(): void {
  }

  deleteEvent() {
    this.snack.open('Seguro de Eliminar', 'Aceptar', {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.reservationService.deleteReservation(this.data.horizontalPropertyId, this.eventSelect.recreationAreaReservationDetail.id)
        .subscribe(a => {
        this.dialogReg.close(true);
      });
    });
  }
}
