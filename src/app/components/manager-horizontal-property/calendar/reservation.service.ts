import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class ReservationService extends PrincipalService {
  route = 'recreation-area-reservation';

  getAllReservation(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  getReservationByProperty(horizontalPropertyId, propertyId, state): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/property/${propertyId}/${state}`);
  }

  createReservation(horizontalPropertyId, data): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  getInfoRecreation(horizontalPropertyId, recreationId, query): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/recreationInfo/${recreationId}`, query);
  }

  updateReservation(horizontalPropertyId, reservationId, data): Observable<ApiResponse> {
    return this.put(`${horizontalPropertyId}/update/${reservationId}`, data);
  }

  deleteReservation(horizontalPropertyId, reservationId): Observable<ApiResponse> {
    return this.delete(`${horizontalPropertyId}/delete/${reservationId}`);
  }

}
