import {Component, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../model/hotizontal-property/horizontalProperty';
import {User} from '../../../model/User';
import {HorizontalPropertyService} from '../../horizontal-property/horizontal-property.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {DataGlobalService} from '../../../services/data-global.service';

@Component({
  selector: 'abin-manager-horizontal-property-header',
  templateUrl: './manager-horizontal-property-header.component.html',
  styleUrls: ['./manager-horizontal-property-header.component.css']
})
export class ManagerHorizontalPropertyHeaderComponent implements OnInit {
  // mobileQuery: MediaQueryList;
  horizontalProperty: HorizontalProperty;
  user: User;
  isOpen = false;
  textSearch = '';

  // private mobileQueryListener: () => void;

  constructor(private horizontalPropertyService: HorizontalPropertyService,
              private activeRouter: ActivatedRoute,
              private authService: AuthService,
              private dataService: DataGlobalService,
              private router: Router) {
    this.user = localStorage.getItem('profile') ? JSON.parse(atob(localStorage.getItem('profile'))) : null;
    setTimeout(() => {
      this.isOpen = true;
    }, 250);
  }

  ngOnInit(): void {
    this.activeRouter.parent.params.subscribe(params => {
      this.horizontalPropertyService.getInfoHorizontalProperty(params.id).subscribe((r: any) => {
        this.horizontalProperty = r.data;
      });
    });
  }

  logout() {
    this.authService.logout().then(() => {
      this.router.navigate(['/login']);
    });
  }

  search() {
    this.dataService.setSearch(this.textSearch);
  }

  reviewText() {
    if (this.textSearch === '') {
      this.dataService.setSearch(this.textSearch);
    }
  }
}
