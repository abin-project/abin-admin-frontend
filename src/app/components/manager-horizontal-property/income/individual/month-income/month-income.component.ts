import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeIncome, TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {Debt} from '../../../../../model/hotizontal-property/debt';
import {Property} from '../../../../../model/hotizontal-property/property';
import {TransactionService} from '../../../banks/transaction.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT} from '../../../../../model/constants';
import {RolService} from '../../../../admin-horizontal-property/rol.service';
import {MatDialog} from '@angular/material/dialog';
import {UpdateDebtIncomeComponent} from '../update-debt-income/update-debt-income.component';
import {DebtManualService} from '../../manual/debt-manual.service';
import { DialogMonthIncomeComponent } from '../dialog-month-income/dialog-month-income.component';

@Component({
  selector: 'abin-month-income',
  templateUrl: './month-income.component.html',
  styleUrls: ['./month-income.component.css']
})
export class MonthIncomeComponent implements OnInit {
  @Input() itemsIncome: TypeIncomeProperty[] = [];
  @Input() stateLoad = true;
  @Input() property: Property;
  @Input() selectProject: string;
  @Input() horizontalPropertyId: string;
  @Output() reLoad = new EventEmitter();
  typesProperties = ['departamento', 'local comercial', 'oficina', 'casa'];

  constructor(private transactionService: TransactionService,
              private rolService: RolService,
              private dialog: MatDialog,
              private debtManualService: DebtManualService,
              private snack: MatSnackBar) {
  }

  ngOnInit() {
  }

  getTotal(incomes: Debt[]) {
    return incomes.map(a => a.debtIncomes).reduce((a, b) => a.concat(b), []).map(a => Number(a.amount)).reduce((a, b) => a + b, 0);
  }

  getTotalDebt(debt: TypeIncomeProperty) {
    return Number(debt.amount) - Number((debt.debts.map(r => r.debtIncomes.map(f => Number(f.amount))
      .reduce((a, b) => a + b, 0)).reduce((a, b) => a +  b, 0)).toFixed(2));
  }

  isTypeIncome(name: string) {
    if (this.property) {
      if (name.includes(this.property.type)) {
        return true;
      } else {
        const a = this.typesProperties.find(r => r.toLowerCase().includes(name.toLowerCase()));
        return !a;
      }
    }

  }

  deleteDebt(typeIncome: TypeIncome, debt: Debt) {
    if (debt.recreationAreaReservationDetails.length > 0) {
      this.snack.open('No se puede Eliminar es parte de una reservacion, eliminela desde las reservaciones');
      return;
    }
    this.snack.open('Seguro de Proceder?', TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.debtManualService.deleteDebtManual(this.horizontalPropertyId, debt.id).subscribe(a => {
        this.reLoad.emit(true);
      });
      // this.transactionService.setDebtAndreProcess(this.horizontalPropertyId, this.property.id, debt).subscribe(r => {
      //   this.reLoad.emit(true);
      // });
    });
  }

  deleteDeletePenalty(debt: Debt, state) {
    this.snack.open('Seguro de Proceder?', TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      // if (debt.notPenalty === false) {
        debt.notPenalty = !state.checked;
        this.transactionService.setDebtAndreProcess(this.horizontalPropertyId, this.property.id, debt).subscribe(r => {
          this.reLoad.emit(true);
        });
      // } else {
        // this.transactionService.setDebtAndreProcess(this.horizontalPropertyId, this.property.id, debt).subscribe(r => {
        //   this.reLoad.emit(true);
        // });
      // }
    });
  }

  show(debt, typeIncome: TypeIncome) {
    this.dialog.open(DialogMonthIncomeComponent, {
      data: {
        debt: debt,
        typeIncome,
      },
      width: '700px'
    })
  }

  updateDebt(typeIncome: TypeIncomeProperty, debt: Debt) {
    this.dialog.open(UpdateDebtIncomeComponent, {
      width: '400px',
      data: {
        debt,
        typeIncome,
        property: this.property,
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.reLoad.emit(true);
      }
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  isDebt(item: TypeIncomeProperty) {
    return item.typeIncome.priority === 1;
  }

  isDetailsVisible(debts: Debt[]) {
    return debts.map(a => a.detailVisible).reduce((a, b) => a === true && b === true, true);
  }

  amountIsValid(item: TypeIncomeProperty) {
    return item.debts.map(a => Number(a.amount)).reduce((a, b) => a + b, 0) > 0;
  }
}
