import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KardexDebtManualComponent } from './kardex-debt-manual.component';

describe('KardexDebtManualComponent', () => {
  let component: KardexDebtManualComponent;
  let fixture: ComponentFixture<KardexDebtManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KardexDebtManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KardexDebtManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
