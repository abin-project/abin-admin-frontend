import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Property} from '../../../../../model/hotizontal-property/property';
import {TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../../property/property.service';
import {ItemService} from '../../../item/item.service';
import {DebtManualService} from '../../manual/debt-manual.service';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'abin-kardex-debt-manual',
  templateUrl: './kardex-debt-manual.component.html',
  styleUrls: ['./kardex-debt-manual.component.css']
})
export class KardexDebtManualComponent implements OnInit {
  debtForm: FormGroup;
  properties: Property[];
  typesIncomesProperty: TypeIncomeProperty[];
  filteredOptions: Observable<Property[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<KardexDebtManualComponent>,
              private fb: FormBuilder,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private debtManualService: DebtManualService) {
    this.debtForm = this.fb.group({
      date: [this.data.date, Validators.required],
      typeIncomeProperty: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      propertyName: [{value: this.data.property.code, disabled: true}, Validators.required],
      property: [this.data.property],
      isManual: [false, [Validators.required]],
      detailVisible: [false, [Validators.required]],
      detail: [null],
    });


  }

  ngOnInit(): void {
    this.changeProperty();
    this.debtForm.get('typeIncomeProperty').valueChanges.subscribe(r => {
      this.debtForm.get('amount').setValue(this.typesIncomesProperty.find(a => a.id === r).amount);
    });
  }

  changeProperty() {
    if (this.data.property) {
      this.debtForm.get('property').setValue(this.data.property.id);
      this.itemService.getTypeIncomeByProperty(this.data.id, this.data.property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
      });
    } else {
      this.debtForm.get('property').setValue(null);
      this.typesIncomesProperty = [];
    }
  }

  save() {
    if (this.debtForm.valid) {
      this.debtManualService.createDebtManual(this.data.id, this.debtForm.value).subscribe(() => {
        this.dialogRef.close(this.debtForm.value);
      });
    }
  }
}
