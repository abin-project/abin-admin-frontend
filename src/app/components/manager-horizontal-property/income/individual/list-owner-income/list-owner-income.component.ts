import {Component, OnDestroy} from '@angular/core';
import {ListPropertyComponent} from '../../../property/list-property/list-property.component';
import {ApiResponse} from '../../../../../model/apiResponse';
import {MatDialog} from '@angular/material/dialog';
import {PropertyService} from '../../../property/property.service';
import {DataGlobalService} from '../../../../../services/data-global.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {HorizontalPropertyService} from '../../../../horizontal-property/horizontal-property.service';
import {MatTableDataSource} from '@angular/material/table';
import {Property} from '../../../../../model/hotizontal-property/property';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintService} from '../../../../../services/print.service';
import {Project} from '../../../../../model/hotizontal-property/project';
import {FilterKardexDialogComponent} from '../../filter-kardex-dialog/filter-kardex-dialog.component';
import {ExcelService} from '../../../../../services/excel.service';
import {RolService} from '../../../../admin-horizontal-property/rol.service';
import {PropertyTypeService} from '../../../../admin-horizontal-property/property-type.service';
import {PropertyType} from '../../../../../model/hotizontal-property/propertyTypes';
import {OwnerService} from '../../../owner/owner.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'abin-list-owner-income',
  templateUrl: './list-owner-income.component.html',
  styleUrls: ['./list-owner-income.component.css']
})
export class ListOwnerIncomeComponent extends ListPropertyComponent implements OnDestroy{
  title = 'Kardex Individual';
  displayedColumns: string[] = [
    'pos',
    'code',
    'name',
    'type',
    'm2',
    'owner',
    'tenant',
    'option'
  ];
  params: any = {};
  projectSelect: Project;
  propertyTypes: PropertyType[];
  events: Subscription;
  routerEvent: Subscription;
  horizontalPropertyEvent: Subscription;
  typePropertyEvent: Subscription;
  propertyEvent: Subscription;

  constructor(
    protected dialog: MatDialog,
    protected propertyService: PropertyService,
    protected dataGlobalService: DataGlobalService,
    protected routerActive: ActivatedRoute,
    protected horizontalPropertyService: HorizontalPropertyService,
    protected router: Router,
    protected snack: MatSnackBar,
    protected printService: PrintService,
    protected excelService: ExcelService,
    protected ownerService: OwnerService,
    protected rolService: RolService,
    private propertyTypeService: PropertyTypeService) {

    super(
      dialog,
      propertyService,
      dataGlobalService,
      routerActive,
      horizontalPropertyService,
      snack,
      printService,
      excelService,
      ownerService,
      rolService);
    this.routerEvent = this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });
    this.events = this.router.events.subscribe(a => {
      if (a instanceof NavigationEnd) {
        this.loadData();
      }
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    this.loadData();
    this.horizontalPropertyEvent = this.horizontalPropertyService
      .getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
      this.horizontalProperty = r.data;
    });
    this.typePropertyEvent = this.propertyTypeService.getPropertyInUse(this.horizontalPropertyId).subscribe(r => {
      this.propertyTypes = r.data;
      if (!this.params.type) {
        this.router.navigate([], {queryParams: {type: this.propertyTypes[0].name}});
      }
    });
  }
  ngOnDestroy(): void {
    this.events.unsubscribe();
    this.routerEvent.unsubscribe();
    this.typePropertyEvent.unsubscribe();
    this.horizontalPropertyEvent.unsubscribe();
    this.propertyEvent.unsubscribe();
  }

  loadData() {
    if (this.params && this.params.type) {
      this.loadProperties(this.params.type);
    }
  }

  loadProperties(type) {
    if (this.params.type === 'Todos') {
      this.propertyEvent = this.propertyService
        .getPropertiesOnHorizontalProperty(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
        this.dataSource = new MatTableDataSource<Property>(res.data);
      });
    } else {
      this.propertyEvent = this.propertyService
        .getPropertyOnHorizontalPropertyTypes(this.horizontalPropertyId, type).subscribe((res: ApiResponse) => {
        this.dataSource = new MatTableDataSource<Property>(res.data.entities);
      });
    }

  }

  filtered() {
    // @ts-ignore
    if (this.params !== {}) {
      return this.params.type !== undefined && this.params.projectId !== undefined;
    } else {
      return false;
    }
  }

  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        ...this.params,
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.loadData();
      }
    });
  }

  getNavigatedParams(id) {
    return {
      ...this.params,
      propertyId: id
    };
  }

  backLocation() {
    this.router.navigate([`/managerHorizontalProperty/${this.horizontalPropertyId}/kardex`]);
  }
}


