import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Property} from '../../../../../model/hotizontal-property/property';
import {TypeIncome, TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TransactionService} from '../../../banks/transaction.service';
import {Debt} from '../../../../../model/hotizontal-property/debt';

@Component({
  selector: 'abin-update-debt-income',
  templateUrl: './update-debt-income.component.html',
  styleUrls: ['./update-debt-income.component.css']
})
export class UpdateDebtIncomeComponent implements OnInit {
  debtForm: FormGroup;
  properties: Property[];
  typesIncomesProperty: TypeIncomeProperty[];
  filteredOptions: Observable<Property[]>;
  debt: Debt;
  typeIncome: TypeIncome;
  property: Property;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<UpdateDebtIncomeComponent>,
              private fb: FormBuilder,
              private transactionService: TransactionService) {
    this.debt = this.data.debt;
    this.property = this.data.property;
    this.typeIncome = this.data.typeIncome;
    this.debtForm = this.fb.group({
      date: [{value: this.debt.date, disabled: true}, Validators.required],
      typeIncomeProperty: [{value: this.typeIncome.nameIncome, disabled: true}, [Validators.required]],
      amount: [this.debt.amount, [Validators.required, Validators.min(0)]],
      property: [{value: this.property.code, disabled: true}, [Validators.required]],
    });


  }

  ngOnInit(): void {
  }

  save() {
    if (this.debtForm.valid) {
      this.transactionService.updateDebtAndreProcess(this.data.horizontalPropertyId, this.debt.id, this.debtForm.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
