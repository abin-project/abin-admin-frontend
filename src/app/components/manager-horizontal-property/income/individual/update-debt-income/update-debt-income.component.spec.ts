import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDebtIncomeComponent } from './update-debt-income.component';

describe('UpdateDebtIncomeComponent', () => {
  let component: UpdateDebtIncomeComponent;
  let fixture: ComponentFixture<UpdateDebtIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDebtIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDebtIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
