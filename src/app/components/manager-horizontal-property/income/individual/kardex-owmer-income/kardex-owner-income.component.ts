import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ApiResponse} from '../../../../../model/apiResponse';
import {Property} from '../../../../../model/hotizontal-property/property';
import {PropertyService} from '../../../property/property.service';
import {PeriodService} from '../../../period/period.service';
import {HorizontalPropertyService} from '../../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import * as moment from 'moment';
import {IncomeService} from '../../income.service';
import {PrintService} from '../../../../../services/print.service';
import {TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {ProjectService} from '../../../project/project.service';
import {Project} from '../../../../../model/hotizontal-property/project';
import {ProjectExtends} from '../../../ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {RolService} from '../../../../admin-horizontal-property/rol.service';
import {KardexDebtManualComponent} from '../kardex-debt-manual/kardex-debt-manual.component';
import {TransactionService} from '../../../banks/transaction.service';

@Component({
  selector: 'abin-kardex-owner-income',
  templateUrl: './kardex-owner-income.component.html',
  styleUrls: ['./kardex-owner-income.component.css']
})
export class KardexOwnerIncomeComponent implements OnInit, OnDestroy {
  title = 'Kardex Individual';
  property: Property;
  horizontalPropertyId;
  isOpen = true;
  periods: any[];
  displayedColumns = [
    'pos',
    'bank',
    'code',
    'amount',
    'date',
    'option'
  ];
  horizontalProperty: HorizontalProperty;
  stateLoad = true;
  itemIncomes: TypeIncomeProperty[] = [];
  projectSelect: Project;
  projects: ProjectExtends[];
  params: any = {};
  events: Subscription;
  propertyEvent: Subscription;
  hpEvent: Subscription;
  projectEvent: Subscription;
  incomeEvent: Subscription;
  properties: Property[] = [];

  constructor(private periodService: PeriodService,
              private routerActive: ActivatedRoute,
              private horizontalPropertyService: HorizontalPropertyService,
              private router: Router,
              private incomeService: IncomeService,
              private propertyService: PropertyService,
              private printService: PrintService,
              private dialog: MatDialog,
              private rolService: RolService,
              private transactionService: TransactionService,
              private projectService: ProjectService) {
    this.routerActive.parent.params.subscribe(tb => {
      this.horizontalPropertyId = tb.id;
    });
    this.routerActive.queryParams.subscribe(t => {
      this.params = t;
    });
    this.events = this.router.events.subscribe((evt: NavigationEnd) => {
      if (evt instanceof NavigationEnd) {
        this.loadIncomesByTypes();
      }
    });
  }

  ngOnInit() {
    this.hpEvent = this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.horizontalProperty = res.data;
    });

    this.propertyService.getPropertyOnHorizontalPropertyTypes(this.horizontalPropertyId, this.params.type).subscribe((res: ApiResponse) => {
      this.properties = res.data.entities
    });
    this.propertyEvent = this.propertyService.getProperty(this.horizontalPropertyId, this.params.propertyId).subscribe((s: ApiResponse) => {
      this.property = s.data;
      localStorage.setItem('property', JSON.stringify(this.property));
    });
    this.projectEvent = this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projects = r.data.map(a => {
        return {
          ...a,
          periods: this.processPeriods(a),
          isOpen: this.getOpen(a)
        };
      });
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe();
    // this.incomeEvent.unsubscribe();
    this.propertyEvent.unsubscribe();
    this.projectEvent.unsubscribe();
    this.hpEvent.unsubscribe();
  }

  getOpen(a: Project) {
    const now = moment();
    return now >= moment(a.yearIni) && now <= moment(a.yearEnd);
  }

  createDebt() {
    this.dialog.open(KardexDebtManualComponent, {
        width: '400px',
        data: {
          id: this.horizontalPropertyId,
          property: this.property,
        }
      }).afterClosed().subscribe(r => {
        if (r) {
          this.loadIncomesByTypes();
        }
    });
  }


  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  projectSelectA(p: ProjectExtends) {
    p.isOpen = !p.isOpen;
  }

  loadIncomesByTypes() {
    if (this.params.year && this.params.month) {
      this.stateLoad = true;
      this.incomeEvent = this.incomeService
        .getIncomeByProperty(this.horizontalPropertyId, this.params.propertyId, this.params.year, this.params.month)
        .subscribe((t: ApiResponse) => {
          this.itemIncomes = t.data;
          this.stateLoad = false;
        });
    }
  }

  processPeriods(project: Project) {
      const res = [];
      const b = moment(project.yearIni);
      const total = moment(project.yearEnd).diff(b, 'months') + 1;
      for (let i = 0; i < total; i++) {
        res.push({
          text: b.locale('es-ES').format('YYYY-MMMM').toUpperCase(),
          monthNum: (b.month() + 1),
          year: (b.year())
        });
        b.add(1, 'month');
      }
      return res;
  }

  getParams(month) {
    return {
      ...this.params,
      year: month.year,
      month: month.monthNum,
    };
  }

  getParamsproperty(property: Property) {
    return {
      ...this.params,
      propertyId: property.id
    }
  }

  monthYearExist() {
    // @ts-ignore
    if (this.params !== {}) {
      return this.params.month !== undefined && this.params.year !== undefined;
    } else {
      return false;
    }
  }

  async report() {
    const period = moment(`${this.params.year}-${this.params.month}`, 'YYYY-M');
    this.incomeService.reportData(this.horizontalPropertyId, {
      projectId: this.projects.find(a => period >= moment(a.yearIni) && period <= moment(a.yearEnd)).id,
      propertyId: this.params.propertyId
    }).subscribe(async (r: any) => {
      const headers: string[] = r.heads;
      headers.unshift('MES');
      const body: any[][] = r.data;
      const titles = [`${this.horizontalProperty.nameProperty} ${this.title}`, r.title];
      await this.printService.createIncomeIndividual(titles, headers, body, this.property);
    });
  }

  showGraph() {
    this.incomeService.reportDataGraph(this.horizontalPropertyId, this.property.id).subscribe(async (r: any) => {
      const headers: string[] = r.heads;
      headers.unshift('MES');
      const body: any[][] = r.data;
      const titles = [`${this.horizontalProperty.nameProperty} ${this.title}`, r.title];
      await this.printService.createIncomeIndividual(titles, headers, body, this.property);
    });
  }

  backLocation() {
    this.router.navigate([`/managerHorizontalProperty/${this.horizontalPropertyId}/kardex`], {
      queryParams: {
        ...this.params
      }
    });
  }

  changeProperty(property: Property) {
    this.router.navigate(['.'], {
      queryParams: {
        propertyId: property.id
      },
      queryParamsHandling: 'merge',
      relativeTo: this.routerActive
    }).then(() => {
      this.propertyEvent = this.propertyService.getProperty(this.horizontalPropertyId, this.params.propertyId).subscribe((s: ApiResponse) => {
        this.property = s.data;
        localStorage.setItem('property', JSON.stringify(this.property));
      });
    })
  }

  changePeriod(month) {
    this.router.navigate(['.'], {
      queryParams: {
        ...this.params,
        year: month.year,
        month: month.monthNum,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.routerActive
    });
  }
}
