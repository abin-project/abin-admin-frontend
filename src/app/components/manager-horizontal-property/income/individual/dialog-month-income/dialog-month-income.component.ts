import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Debt } from 'src/app/model/hotizontal-property/debt';
import { TypeIncome } from 'src/app/model/hotizontal-property/typeIncome';

@Component({
  selector: 'abin-dialog-month-income',
  templateUrl: './dialog-month-income.component.html',
  styleUrls: ['./dialog-month-income.component.css']
})
export class DialogMonthIncomeComponent implements OnInit {

  debt: Debt;
  typeIncome: TypeIncome;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.debt = this.data.debt;
    this.typeIncome = this.data.typeIncome;
  }

  ngOnInit(): void {
  }

}
