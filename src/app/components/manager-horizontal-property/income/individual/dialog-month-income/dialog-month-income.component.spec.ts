import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMonthIncomeComponent } from './dialog-month-income.component';

describe('DialogMonthIncomeComponent', () => {
  let component: DialogMonthIncomeComponent;
  let fixture: ComponentFixture<DialogMonthIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogMonthIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMonthIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
