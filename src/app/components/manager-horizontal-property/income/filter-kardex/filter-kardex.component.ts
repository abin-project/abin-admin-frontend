import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '../../project/project.service';
import {Project} from '../../../../model/hotizontal-property/project';
import {ActivatedRoute, Router} from '@angular/router';
import {PropertyTypeService} from '../../../admin-horizontal-property/property-type.service';
import {PropertyType} from '../../../../model/hotizontal-property/propertyTypes';

@Component({
  selector: 'abin-filter-kardex',
  templateUrl: './filter-kardex.component.html',
  styleUrls: ['./filter-kardex.component.css']
})
export class FilterKardexComponent implements OnInit {
  @Input() data: any = {
    horizontalPropertyId: null,
  };
  @Input() withType = true;
  @Output() onSave = new EventEmitter();

  filterForm: FormGroup;
  horizontalPropertyId: string;
  projects: Project[] = [];
  propertyTypes: PropertyType[];

  constructor(private fb: FormBuilder,
              private routerActive: ActivatedRoute,
              private projectService: ProjectService,
              private propertyTypeService: PropertyTypeService,
              private router: Router) {
    this.routerActive.queryParams.subscribe(r => {
      this.filterForm = this.fb.group({
        projectId: [r.projectId, [Validators.required]],
      });
      if (r.propertyId) {
        this.filterForm.addControl('propertyId', this.fb.control(r.propertyId, [Validators.required]));
      }
      if (this.withType === true) {
        this.filterForm.addControl('type', this.fb.control(r.type, [Validators.required]));
      }
    });
  }

  ngOnInit(): void {
    this.projectService.getProjects(this.data.horizontalPropertyId).subscribe(r => {
      this.projects = r.data;
    });
    this.propertyTypeService.getPropertyInUse(this.data.horizontalPropertyId).subscribe(r => {
      this.propertyTypes = r.data;
    });
  }

  async save() {
    await this.router.navigate([], {queryParams: this.filterForm.value});
    this.onSave.emit(true);
  }
}
