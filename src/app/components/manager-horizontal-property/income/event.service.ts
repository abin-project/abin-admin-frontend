import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  clickEvent = new EventEmitter();

  constructor() {
  }
}
