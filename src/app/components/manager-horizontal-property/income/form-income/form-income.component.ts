import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Income} from '../../../../model/hotizontal-property/income';
import {IncomeService} from '../income.service';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {BankService} from '../../banks/bank.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {SnackErrorComponent} from '../../../alerts/snack-error/snack-error.component';
import {SNACK_CONFIG} from '../../../../config/toast.config';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-form-income',
  templateUrl: './form-income.component.html',
  styleUrls: ['./form-income.component.css']
})
export class FormIncomeComponent implements OnInit {

  formIncome: FormGroup;
  incomeSelect: Income;
  propertyId: string;
  periodMonthId: string;
  horizontalPropertyId: string;
  banks: Bank[];
  typeIncome: TypeIncome;
  con = SNACK_CONFIG;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<FormIncomeComponent>,
              protected incomeService: IncomeService,
              private bankService: BankService,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.incomeSelect = this.data.income;
    this.typeIncome = this.data.typeIncome;
    this.periodMonthId = this.data.periodMonthId;
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.propertyId = this.data.propertyId;
    this.formIncome = this.fb.group({
      amount: [null, [Validators.required]],
      code: [null, [Validators.required]],
      date: [null, [Validators.required]],
      accountBankId: [null, [Validators.required]],
      typeIncomeId: [this.typeIncome.id],
      propertyId: [this.propertyId],
      periodMonthId: [this.periodMonthId],
    });
    this.bankService.getAccounts(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.banks = r.data;
    });
  }

  ngOnInit(): void {
    if (this.incomeSelect) {
      this.incomeService.getIncome(this.horizontalPropertyId, this.data.income.id).subscribe((r: ApiResponse) => {
        this.incomeSelect = r.data;
        this.formIncome = this.fb.group({
          amount: [this.incomeSelect.amount, [Validators.required]],
          code: [this.incomeSelect.code, [Validators.required]],
          date: [this.incomeSelect.date, [Validators.required]],
          accountBankId: [this.incomeSelect.accountBank.id, [Validators.required]],
          typeIncomeId: [this.incomeSelect.typeIncome.id],
          propertyId: [this.incomeSelect.property.id],
          periodMonthId: [this.incomeSelect.periodMonth.id],
        });
      });
    }
  }

  save() {
    if (this.formIncome.valid && !this.incomeSelect) {
      this.incomeService.createIncome(this.horizontalPropertyId, this.formIncome.value).subscribe(r => {
        this.dialogRef.close(this.typeIncome);
      });
    } else if (this.formIncome.valid && this.incomeSelect) {
      this.incomeService.updateIncome(this.horizontalPropertyId, this.incomeSelect.id, this.formIncome.value).subscribe(r => {
        this.dialogRef.close(this.typeIncome);
      });
    } else {
      this.con.data = 'Existen Campos Vacios';
      this.snack.openFromComponent(SnackErrorComponent, this.con);
    }
  }

}
