import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtManualCreateComponent } from './debt-manual-create.component';

describe('DebtManualCreateComponent', () => {
  let component: DebtManualCreateComponent;
  let fixture: ComponentFixture<DebtManualCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebtManualCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtManualCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
