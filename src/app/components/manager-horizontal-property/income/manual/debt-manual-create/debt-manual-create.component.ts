import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Property} from '../../../../../model/hotizontal-property/property';
import {TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../../property/property.service';
import {ItemService} from '../../../item/item.service';
import {map, startWith} from 'rxjs/operators';
import {DebtManualService} from '../debt-manual.service';

@Component({
  selector: 'abin-debt-manual-create',
  templateUrl: './debt-manual-create.component.html',
  styleUrls: ['./debt-manual-create.component.css']
})
export class DebtManualCreateComponent implements OnInit {
  debtForm: FormGroup;
  properties: Property[];
  typesIncomesProperty: TypeIncomeProperty[];
  filteredOptions: Observable<Property[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<DebtManualCreateComponent>,
              private fb: FormBuilder,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private debtManualService: DebtManualService) {
    this.debtForm = this.fb.group({
      date: [this.data.date, Validators.required],
      typeIncomeProperty: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      property: [null, [Validators.required]],
      detailVisible: [false, [Validators.required]],
      isManual: [true, Validators.required],
      detail: null,
      propertyCode: [null, [Validators.required]]
    });


  }

  ngOnInit(): void {
    this.debtForm.get('propertyCode').valueChanges.subscribe(r => {
      this.changeProperty(r);
    });
    this.debtForm.get('typeIncomeProperty').valueChanges.subscribe(r => {
      this.debtForm.get('amount').setValue(this.typesIncomesProperty.find(a => a.id === r).amount);
    });

    this.propertyService.getPropertiesWithCodeOnHorizontalProperty(this.data.id).subscribe(r => {
      this.properties = r.data;
      this.filteredOptions = this.debtForm.get('propertyCode').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.code),
          map(name => name ? this._filter(name) : this.properties.slice())
        );
    });

  }

  changeProperty(propertyCode) {
    const property = this.properties.find(f => f.code === propertyCode);
    if (property) {
      this.debtForm.get('property').setValue(property.id);
      this.itemService.getTypeIncomeByProperty(this.data.id, property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
      });
    } else {
      this.debtForm.get('property').setValue(null);
      this.typesIncomesProperty = [];
    }
  }

  save() {
    if (this.debtForm.valid) {
      this.debtManualService.createDebtManual(this.data.id, this.debtForm.value).subscribe(() => {
        this.dialogRef.close(this.debtForm.value);
      });
    }
  }

  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }
}
