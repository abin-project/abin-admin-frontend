import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtManualListComponent } from './debt-manual-list.component';

describe('DebtManualListComponent', () => {
  let component: DebtManualListComponent;
  let fixture: ComponentFixture<DebtManualListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebtManualListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtManualListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
