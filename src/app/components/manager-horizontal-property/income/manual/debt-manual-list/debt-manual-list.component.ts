import {Component, OnInit} from '@angular/core';
import {RolService} from '../../../../admin-horizontal-property/rol.service';
import {MatDialog} from '@angular/material/dialog';
import {DebtManualService} from '../debt-manual.service';
import {DebtManualCreateComponent} from '../debt-manual-create/debt-manual-create.component';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Debt} from '../../../../../model/hotizontal-property/debt';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../../model/constants';

@Component({
  selector: 'abin-debt-manual-list',
  templateUrl: './debt-manual-list.component.html',
  styleUrls: ['./debt-manual-list.component.css']
})
export class DebtManualListComponent implements OnInit {

  dataSource: MatTableDataSource<Debt>;
  displayedColumns: Array<string> = [
    'pos',
    'code',
    'type',
    'amount',
    'total',
    'totalDebt',
    'options'
  ];


  horizontalPropertyId: string;
  constructor(public rolService: RolService,
              private dialog: MatDialog,
              private routerActive: ActivatedRoute,
              private snack: MatSnackBar,
              private debtManualService: DebtManualService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.dataSource = new MatTableDataSource<Debt>([]);
  }

  ngOnInit(): void {
    this.debtManualService.getDebtManual(this.horizontalPropertyId).subscribe(r => {
      this.dataSource = new MatTableDataSource<Debt>(r.data);
    });
    if (this.rolService.getUserRol() === 'Root'
      || this.rolService.getUserRol() === 'Administrador del Sistema'
      || this.rolService.getUserRol() === 'Administrador') {
      if (this.displayedColumns.find(r => r === 'options') === undefined) {
        this.displayedColumns.push('options');
      }
    }
  }

  openDialog() {
    this.dialog.open(DebtManualCreateComponent, {
      width: '400px',
      data: {
        id: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  debtDelete(debt: Debt) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.debtManualService.deleteDebtManual(this.horizontalPropertyId, debt.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }


}
