import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDebtManualComponent } from './detail-debt-manual.component';

describe('DetailDebtManualComponent', () => {
  let component: DetailDebtManualComponent;
  let fixture: ComponentFixture<DetailDebtManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDebtManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDebtManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
