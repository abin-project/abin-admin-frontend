import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {DebOfPay} from '../../../banks/transaction-debt-pay/transaction-debt-pay.component';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {Debt} from '../../../../../model/hotizontal-property/debt';
import {Property} from '../../../../../model/hotizontal-property/property';
import {ActivatedRoute} from '@angular/router';
import {PropertyService} from '../../../property/property.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RolService} from '../../../../admin-horizontal-property/rol.service';
import {ApiResponse} from '../../../../../model/apiResponse';
import {CreateDebtsComponent} from '../../../previousPayment/debts/create-debts/create-debts.component';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../../model/constants';
import {DebtManualService} from '../debt-manual.service';
import {DebtManualCreateComponent} from '../debt-manual-create/debt-manual-create.component';

@Component({
  selector: 'abin-detail-debt-manual',
  templateUrl: './detail-debt-manual.component.html',
  styleUrls: ['./detail-debt-manual.component.css']
})
export class DetailDebtManualComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  debts: DebOfPay[];
  propertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Debt>;
  sortedData: DebOfPay[];
  property: Property;
  displayedColumns: Array<string> = [
    'pos',
    'name',
    'amount',
    'totalPay',
    'totalDebt',
    'date',
    // 'options'
  ];

  constructor(private debtManualService: DebtManualService,
              private routerActive: ActivatedRoute,
              private propertyService: PropertyService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private rolService: RolService) {
    this.routerActive.params.subscribe(r => {
      this.propertyId = r.propertyId;
    });
  }

  ngOnInit(): void {
    this.debtManualService.debtManualByProperty(this.horizontalProperty.id, this.propertyId).subscribe(r => {
      this.dataSource = new MatTableDataSource<Debt>(r.data);
    });
    this.propertyService.getProperty(this.horizontalPropertyId, this.propertyId).subscribe((r: ApiResponse) => {
      this.property = r.data;
    });
    if (this.readNotOnly()) {
      if (!this.displayedColumns.find(a => a === 'options')) {
        this.displayedColumns.push('options');
      }
    }
  }

  openDialog() {
    this.dialog.open(DebtManualCreateComponent, {
      width: '500px',
      data: {
        id: this.horizontalProperty.id
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  pay(debt) {

  }

  delete(debt) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.debtManualService.deleteDebtManual(this.horizontalPropertyId, debt.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}
