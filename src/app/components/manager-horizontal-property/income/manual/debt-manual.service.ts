import { Injectable } from '@angular/core';
import {PrincipalService} from '../../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class DebtManualService extends PrincipalService {
  route = 'debt-manual';

  createDebtManual(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  getDebtManual(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  deleteDebtManual(horizontalPropertyId: string, id: string) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }

  debtManualByProperty(id: string, propertyId: string): Observable<ApiResponse> {
    return this.get(`${id}/detail/${propertyId}`);
  }
}
