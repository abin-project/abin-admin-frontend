import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';

@Component({
  selector: 'abin-general-month-income',
  templateUrl: './general-month-income.component.html',
  styleUrls: ['./general-month-income.component.css']
})
export class GeneralMonthIncomeComponent implements OnInit {
  @Input() stateLoad = false;
  @Input() title;
  @Input() idTable;
  @Input() dataSource: MatTableDataSource<GeneralPeriod>;
  @Input() sortedData: GeneralPeriod[];
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @Input() displayedColumns: Array<string> = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  getRow(g: GeneralPeriod, type: string) {
    const r = g.typeIncomes.find(f => f.nameIncome == type);
    return r && r.total ? r.total : 0;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<GeneralPeriod>(this.sortedData);
  }

  getPosition(pos) {
    return pos == (this.dataSource.data.length - 1) ? '' : (pos + 1);
  }

}

export interface GeneralPeriod {
  code: string;
  name: string;
  propertyId: string;
  typeIncomes: kardexGeneralT[];
}

// tslint:disable-next-line:class-name
export interface kardexGeneralT {
  nameIncome: string;
  period: string;
  priority: number;
  property_id: string;
  total: number;
  typeIncome_id: string;
}
