import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import * as moment from 'moment';
import {ProjectService} from '../../../project/project.service';
import {Project} from '../../../../../model/hotizontal-property/project';
import {GeneralPeriod} from '../general-month-income/general-month-income.component';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {IncomeService} from '../../income.service';
import {FilterKardexDialogComponent} from '../../filter-kardex-dialog/filter-kardex-dialog.component';
import {PrintService} from '../../../../../services/print.service';
import {ProjectExtends} from '../../../ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {Bank} from '../../../../../model/hotizontal-property/bank';
import {TypeIncome} from '../../../../../model/hotizontal-property/typeIncome';

@Component({
  selector: 'abin-kardex-general-income',
  templateUrl: './kardex-general-income.component.html',
  styleUrls: ['./kardex-general-income.component.css']
})
export class KardexGeneralIncomeComponent implements OnInit {
  title = 'Kardex General';
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  stateLoad = false;
  projects: ProjectExtends[];
  params: any = {};
  totalGlobal: KardexTotalLast = {
    title: '',
    typeIncomes: [],
    columns: [],
  };


  sortedData: GeneralPeriod[];
  displayedColumns: Array<string> = [];
  listData: GeneralIncome;
  generalIncomes: GeneralIncome[] = [];

  constructor(private projectService: ProjectService,
              private routerActive: ActivatedRoute,
              private dialog: MatDialog,
              private router: Router,
              private printService: PrintService,
              private incomeService: IncomeService) {
    this.routerActive.parent.parent.params.subscribe(tb => {
      this.horizontalPropertyId = tb.id;
    });
    this.routerActive.queryParams.subscribe(t => {
      this.params = t;
    });
    this.router.events.subscribe(r => {
      if (r instanceof NavigationEnd) {
        this.reloadData();
      }
    });
  }

  ngOnInit() {
    this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projects = r.data.map(a => {
        return {
          ...a,
          periods: this.processPeriods(a),
          isOpen: this.getOpen(a)
        };
      });
    });
  }

  processPeriods(project: Project) {
    const res = [];
    const b = moment(project.yearIni);
    const total = moment(project.yearEnd).diff(b, 'months') + 1;
    for (let i = 0; i < total; i++) {
      res.push({
        text: b.locale('es-ES').format('YYYY-MMMM').toUpperCase(),
        monthNum: (b.month() + 1),
        year: (b.year())
      });
      b.add(1, 'month');
    }
    return res;
  }

  getParams(month) {
    return {
      ...this.params,
      month: month.monthNum,
      year: month.year,
    };
  }

  reloadData() {
    if (this.params.year !== undefined && this.params.month !== undefined) {
      this.stateLoad = true;
      this.incomeService.getIncomeGeneralByPeriod(this.horizontalPropertyId, this.params.year, this.params.month).subscribe(r => {
        this.generalIncomes = r.data;
        this.listData = this.generalIncomes[0];
        for (const a of this.generalIncomes) {
          for (const d of a.data) {
            d.data = new MatTableDataSource<GeneralPeriod>(d.data);
            d.headerRow = this.setDisplayedColumns(d.headerRow);
          }
        }
        this.totalGlobal = this.listData.totalLast;
        this.stateLoad = false;
      });
    }
  }

  getOpen(a: Project) {
    const now = moment();
    return now >= moment(a.yearIni) && now <= moment(a.yearEnd);
  }

  projectSelectA(p: ProjectExtends) {
    p.isOpen = !p.isOpen;
  }

  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        ...this.params,
        horizontalPropertyId: this.horizontalPropertyId,
        withType: false
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.reloadData();
      }
    });
  }

  monthYearExist() {
    if (this.params !== {}) {
      return this.params.month !== undefined && this.params.year !== undefined;
    } else {
      return false;
    }
  }

  setDisplayedColumns(header: string[]) {
    let res: string[] = [
      'pos',
      'departmentCode',
    ];
    res = res.concat(header);
    return res;
  }

  getData() {
    return {
      ...this.params,
      horizontalPropertyId: this.horizontalPropertyId,
    };
  }

  getIndex(pos: number, incomes: any[]) {
    if (pos < incomes.length - 1) {
      return pos + 1;
    } else {
      return '';
    }
  }

  async print() {
    const titles = this.listData.data.map((r, index) => {
      return {
        table: `table${index}`,
        title: r.type
      };
    });
    titles.push({
      table: 'total',
      title: 'Total Global',
    });
    const period = moment(`${this.params.year}-${this.params.month}`, 'YYYY-M').format('YYYY-MMMM').toUpperCase();
    await this.printService.printMultipleTable([
      this.horizontalProperty.nameProperty,
      `Kardex General - ${this.listData.account.nameBank}`,
      `${period}`
    ], titles);
  }
  changeTab(evt) {
    this.stateLoad = true;
    this.listData = this.generalIncomes[evt];
    this.totalGlobal = this.generalIncomes[evt].totalLast;
    this.stateLoad = false;
  }

}

export interface GeneralIncome {
  account: Bank;
  data: DataKardexGeneral[];
  totalLast: KardexTotalLast;
}

export interface DataKardexGeneral {
  data: KardexProperty[]| any;
  type: string;
  headerRow: string[];
}

export interface KardexProperty {
  code: string;
  id: string;
  name: string;
  type: string;
  typeIncomes: TypeIncome[];
}

export interface KardexTypeIncome{
  nameIncome: string;
  total: number;
  codeIncome: number;
  priority: number;
}
export interface KardexTotalLast  {
  title: string;
  typeIncomes: KardexTypeIncome[];
  columns: string[];
}
