import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class IncomeService extends PrincipalService {
  route = 'income';

  getIncomeByProperty(horizontalPropertyId, propertyId, year, month) {
    return this.get(`${horizontalPropertyId}/${propertyId}/${year}/${month}`);
  }

  getIncomeGeneralByPeriod(horizontalPropertyId, year, month): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/kardex/general/${year}/${month}`);
  }

  createIncome(horizontalPropertyId: any, value: any) {
    return this.postCustom(`${horizontalPropertyId}/create`, value);
  }

  getIncome(horizontalPropertyId, incomeId) {
    return this.get(`${horizontalPropertyId}/show/${incomeId}`);
  }

  updateIncome(horizontalPropertyId: any, id: string, value: any) {
    return this.put(`${horizontalPropertyId}/update/${id}`, value);
  }

  deleteIncome(horizontalPropertyId: any, id: string) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }

  reportData(hpId, query): Observable<ApiResponse> {
    return this.getQuery(`${hpId}/report`, query);
  }

  reportDataGraph(horizontalPropertyId: any, id: string) {
    return this.get(`${horizontalPropertyId}/reportGraph/${id}`);
  }
}
