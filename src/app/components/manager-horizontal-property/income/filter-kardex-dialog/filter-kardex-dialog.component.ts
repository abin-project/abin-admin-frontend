import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Project} from '../../../../model/hotizontal-property/project';
import {PropertyType} from '../../../../model/hotizontal-property/propertyTypes';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '../../project/project.service';
import {PropertyTypeService} from '../../../admin-horizontal-property/property-type.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-filter-kardex-dialog',
  templateUrl: './filter-kardex-dialog.component.html',
  styleUrls: ['./filter-kardex-dialog.component.css']
})
export class FilterKardexDialogComponent implements OnInit {


  filterForm: FormGroup;
  horizontalPropertyId: string;
  projects: Project[] = [];
  propertyTypes: PropertyType[];


  constructor(private fb: FormBuilder,
              private routerActive: ActivatedRoute,
              private projectService: ProjectService,
              private propertyTypeService: PropertyTypeService,
              private dialogRef: MatDialogRef<FilterKardexDialogComponent>,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.routerActive.queryParams.subscribe(r => {
      this.filterForm = this.fb.group({
        projectId: [r.projectId, [Validators.required]],
      });
      if (r.propertyId || this.data.withType != false) {
        this.filterForm.addControl('propertyId', this.fb.control(r.propertyId, [Validators.required]));
      }
      if (r.type || this.data.withType != false) {
        this.filterForm.addControl('type', this.fb.control(r.type, [Validators.required]));
      }
    });
  }

  ngOnInit(): void {
    this.projectService.getProjects(this.data.horizontalPropertyId).subscribe(r => {
      this.projects = r.data;
    });
    this.propertyTypeService.getPropertyInUse(this.data.horizontalPropertyId).subscribe(r => {
      this.propertyTypes = r.data;
    });
  }

  async save() {
    this.dialogRef.close({
      ...this.filterForm.value
    });
  }

}
