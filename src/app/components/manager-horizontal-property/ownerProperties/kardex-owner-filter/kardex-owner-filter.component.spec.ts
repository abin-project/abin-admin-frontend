import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KardexOwnerFilterComponent } from './kardex-owner-filter.component';

describe('KardexOwnerFilterComponent', () => {
  let component: KardexOwnerFilterComponent;
  let fixture: ComponentFixture<KardexOwnerFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KardexOwnerFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KardexOwnerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
