import {Component, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Project} from '../../../../model/hotizontal-property/project';
import {GeneralPeriod} from '../../income/general/general-month-income/general-month-income.component';
import {ProjectService} from '../../project/project.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {PrintService} from '../../../../services/print.service';
import {IncomeService} from '../../income/income.service';
import * as moment from 'moment';
import {ApiResponse} from '../../../../model/apiResponse';
import {FilterKardexDialogComponent} from '../../income/filter-kardex-dialog/filter-kardex-dialog.component';
import {Property} from '../../../../model/hotizontal-property/property';
import {PropertyService} from '../../property/property.service';

@Component({
  selector: 'abin-kardex-owner-filter',
  templateUrl: './kardex-owner-filter.component.html',
  styleUrls: ['./kardex-owner-filter.component.css']
})
export class KardexOwnerFilterComponent implements OnInit {
  title = 'Kardex';
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  isOpen = true;
  periods: any[];
  stateLoad = false;
  property: Property;
  projects: ProjectExtends[];
  // projectSelect: Project;
  params: any = {};
  itemIncomes;

  sortedData: GeneralPeriod[];
  displayedColumns: Array<string> = [];

  constructor(private projectService: ProjectService,
              private routerActive: ActivatedRoute,
              private dialog: MatDialog,
              private router: Router,
              private printService: PrintService,
              private incomeService: IncomeService,
              private propertyService: PropertyService) {
    this.routerActive.parent.parent.params.subscribe(tb => {
      this.horizontalPropertyId = tb.id;
    });
    this.routerActive.queryParams.subscribe(t => {
      this.params = t;
    });
    this.router.events.subscribe(r => {
      if (r instanceof NavigationEnd) {
        this.reloadData();
      }
    });
  }

  ngOnInit() {
    this.propertyService.getProperty(this.horizontalPropertyId, this.params.propertyId).subscribe((s: ApiResponse) => {
      this.property = s.data;
      localStorage.setItem('property', JSON.stringify(this.property));
    });
    this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projects = r.data.map(a => {
        return {
          ...a,
          periods: this.processPeriods(a),
          isOpen: this.getOpen(a)
        };
      });
    });
  }

  processPeriods(project: Project) {
      const res = [];
      const b = moment(project.yearIni);
      const end = moment(project.yearEnd);
      for (let i = 0; b < end; i++) {
        res.push({
          text: b.locale('es-ES').format('YYYY-MMMM').toUpperCase(),
          month: (b.month() + 1),
          year: b.year(),
        });
        b.add(1, 'month');
      }
      return res;
  }

  getParams(month) {
    return {
      propertyId: this.property ? this.property.id : null,
      month: month.month,
      year: month.year
    };
  }

  reloadData() {
    if (this.params.year !== undefined && this.params.month !== undefined) {
      this.stateLoad = true;
      this.incomeService.getIncomeByProperty(this.horizontalPropertyId, this.params.propertyId, this.params.year, this.params.month)
        .subscribe((t: ApiResponse) => {
          this.itemIncomes = t.data;
          this.stateLoad = false;
        });
    }
  }

  projectSelectA(p: ProjectExtends) {
    p.isOpen = !p.isOpen;
  }


  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        ...this.params,
        horizontalPropertyId: this.horizontalPropertyId,
        withType: false
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.reloadData();
      }
    });
  }

  // filtered() {
  //   if (this.params !== {}) {
  //     return this.params.projectId !== undefined;
  //   } else {
  //     return false;
  //   }
  // }

  monthYearExist() {
    if (this.params !== {}) {
      return this.params.month !== undefined && this.params.year !== undefined;
    } else {
      return false;
    }
  }

  getData() {
    return {
      ...this.params,
      horizontalPropertyId: this.horizontalPropertyId,
    };
  }

  async report() {
    const period = moment(`${this.params.year}-${this.params.month}`, 'YYYY-M');
    this.incomeService.reportData(this.horizontalPropertyId, {
      propertyId: this.params.propertyId,
      projectId: this.projects.find(a => period >= moment(a.yearIni) && period <= moment(a.yearEnd)).id
    }).subscribe(async (r: any) => {
      const headers: string[] = r.heads;
      headers.unshift('MES');
      const body: any[][] = r.data;
      const titles = [`${this.horizontalProperty.nameProperty} ${this.title}`, r.title];
      await this.printService.createIncomeIndividual(titles, headers, body, this.property);
    });
  }

  getOpen(a: Project) {
    const now = moment();
    return now >= moment(a.yearIni) && now <= moment(a.yearEnd);
  }
}
export interface ProjectExtends extends Project {
  periods: ProjectPeriods[];
  isOpen: boolean;
}

export interface ProjectPeriods {
  text: string;
  year: string;
  month: string;
}
