import { Injectable } from '@angular/core';
import { QrModel } from 'src/app/model/qr-model';
import { PrincipalService } from 'src/app/services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class QrPayService extends PrincipalService {
  route = 'qr-pay';
  getAllDebts(propertyId) {
    return this.get('debts/' + propertyId);
  }

  generateQR(t: QrModel) {
    return this.post(t);
  }

  getStatus(qrId: any) {
    return this.get(qrId);
  }
}
