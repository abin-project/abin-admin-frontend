import { Property } from './../../../../model/hotizontal-property/property';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { PropertyService } from '../../property/property.service';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, pipe } from 'rxjs';
import { HorizontalProperty } from 'src/app/model/hotizontal-property/horizontalProperty';
import { QrPayService } from '../qr-pay.service';
import { FormControl, Validators } from '@angular/forms';
import { QrModel, QrResponse } from 'src/app/model/qr-model';
import { BankService } from '../../banks/bank.service';
import { Bank } from 'src/app/model/hotizontal-property/bank';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { TransactionService } from '../../banks/transaction.service';
import { PrintService } from 'src/app/services/print.service';
import { HorizontalPropertyService } from 'src/app/components/horizontal-property/horizontal-property.service';

@Component({
  selector: 'abin-pay-process',
  templateUrl: './pay-process.component.html',
  styleUrls: ['./pay-process.component.css']
})
export class PayProcessComponent implements OnInit, OnDestroy {


  horizontalPropertyId: string;
  propertyId: string;

  debts: DebtForPay[] = [];
  banks: Bank[] = [];
  property: Property;
  horizontalProperty: HorizontalProperty;
  selectesForPay = {
    selected: [],
    total: 0,
    bank: null
  };


  generating = true;

  qrResponse: QrResponse = {} as QrResponse;

  intervalID: any;

  dialogOpen = false;

  controlAmount = new FormControl(null, [Validators.required, Validators.min(0.1)]);

  constructor(private propertyService: PropertyService,
              private bankService: BankService,
              private qrPayService: QrPayService,
              private routerActive: ActivatedRoute,
              private router: Router,
              private dialog: MatDialog) {
    this.routerActive.parent.params.subscribe(pipe( (r: any)=> {
      this.horizontalPropertyId = r.id;
    }));
    this.routerActive.queryParams.subscribe(pipe((r: any) => {
      this.propertyId = r.p;
    }))
  }

  ngOnInit(): void {
    forkJoin([
      this.bankService.getAccounts(this.horizontalPropertyId),
      this.qrPayService.getAllDebts(this.propertyId)
    ]).subscribe((r: any) => {
      this.banks = (r[0].data as Bank[]).filter(c => c.state == 'Vigente');
      this.debts = (r[1] as DebtForPay[]).map((b) => ({...b, disabled: true}))
    })
  }

  ngOnDestroy(): void {
    if (this.intervalID) {
      clearInterval(this.intervalID);
    }
  }

  enableDebts() {
    if (this.debts.length > 0) {
      this.debts[0].disabled = false;
    }
  }

  acceptAndGenerate() {
    this.generating = false
    const t: QrModel = {
      alias: null,
      detalleGlosa: null,
      fechaVencimiento: null,
      monto: this.controlAmount.value,
      configDebts: this.selectesForPay.selected,
      accountBankId: this.selectesForPay.bank,
      propertyId: this.propertyId
    }
    this.qrPayService.generateQR(t).subscribe(r => {
      this.qrResponse = r as QrResponse;
      this.initStatus(this.qrResponse)
    }, error => {
      this.generating = true;
    })
  }

  addOrRemove(income: any, period: any) {
    console.log(income, period);
  }

  selectDebt(debt: DebtForPay) {
    const selected = !debt.isSelect;

    const t = this.selectesForPay.selected.find(t => t.id == debt.id);
    const index = this.debts.findIndex(d => d.id == debt.id);

    if (selected) {
      if (!t) {
        debt.isSelect = true
        this.selectesForPay.selected.push(debt);
        this.selectesForPay.total = this.selectesForPay.selected.map(r => Number(r.amount) - Number(r.total)).reduce((a, b) => a + b, 0);
        this.controlAmount.setValue(this.selectesForPay.total);
        if (index != this.debts.length - 1) {
          this.debts[index + 1].disabled = false;
        }
      }
    } else {
      if (t) {
        const index = this.selectesForPay.selected.indexOf(t)
        if (index == this.selectesForPay.selected.length - 1) {
          debt.isSelect = false
          this.selectesForPay.selected.splice(index, 1);
          this.selectesForPay.total = this.selectesForPay.selected.map(r => Number(r.amount) - Number(r.total)).reduce((a, b) => a + b, 0);
          this.controlAmount.setValue(this.selectesForPay.total);
          if (index != this.debts.length - 1) {
            this.debts[index + 1].disabled = true;
          }
        }
      }
    }
  }

  initStatus(qr: QrResponse) {
    this.intervalID = setInterval(() => {
      this.qrPayService.getStatus(qr.idQr).subscribe((r: QrResponse) => {
        if (r.pay && !this.dialogOpen) {
          const diag = this.dialog.open(DialogPayOk, {
            width: '300px',
            data: {
              hpId: this.horizontalPropertyId
            }
          })

          diag.afterOpened().subscribe(() => {
            this.dialogOpen = true;
            clearInterval(this.intervalID);
          });

          diag.afterClosed().subscribe(() => {
            this.router.navigate(['../'], {
              relativeTo: this.routerActive
            })
          })
        }
      })
    }, 5000)
  }
}

export interface DebtForPay {
  amount: string
  date: string
  debtPayLimit: string
  id: string
  typeIncomeId: string,
  nameIncome: string
  priority: number
  propertyId: string
  total: string
  isSelect: boolean;
  disabled?: boolean;
}


@Component({
  selector: 'app-dialog-qr',
  template: `
    <div mat-dialog-title>Pago Realizado</div>
    <div mat-dialog-content>
      El pago se realizo con &eacute;xito,
      En unos minutos usted puede descargar el recibo de su deposito en:
        <p><b>Mis Depositos</b></p>
    </div>
    <div mat-dialog-actions>
      <button mat-button color="primary" mat-dialog-close>OK</button>
    </div>
  `
})
export class DialogPayOk implements OnInit {
  horizontalProperty: HorizontalProperty;
  constructor( @Inject(MAT_DIALOG_DATA) private data: any,
    private horizontalPropertyService: HorizontalPropertyService
  ) {

  }

  ngOnInit(): void {
    this.horizontalPropertyService.get(this.data.hpId).subscribe((r: any) => {
      this.horizontalProperty = r;
    })
  }
}
