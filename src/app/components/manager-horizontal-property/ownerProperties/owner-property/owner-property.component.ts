import {Component, OnInit} from '@angular/core';
import {PropertyService} from '../../property/property.service';
import {Property} from '../../../../model/hotizontal-property/property';
import {ActivatedRoute} from '@angular/router';
import {PeoplesComponent} from '../../property/peoples/peoples.component';
import {HistoryPropertyComponent} from '../../property/history-property/history-property.component';
import {MatDialog} from '@angular/material/dialog';
import {LiquidationComponent} from '../../property/liquidation/liquidation.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {PropertyTypeService} from "../../../admin-horizontal-property/property-type.service";
import {PropertyTypeDefault} from "../../../../model/hotizontal-property/propertyTypes";

@Component({
  selector: 'abin-owner-property',
  templateUrl: './owner-property.component.html',
  styleUrls: ['./owner-property.component.css']
})
export class OwnerPropertyComponent implements OnInit {

  properties: Property[] = [];
  propertiesComplement: Property[] = [];
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty;
  user;
  typesProperties: PropertyTypeDefault[] = [];

  constructor(private propertyService: PropertyService,
              private horizontalPropertyService: HorizontalPropertyService,
              private routerActive: ActivatedRoute,
              private propertyTypeService: PropertyTypeService,
              private snack: MatSnackBar,
              private dialog: MatDialog) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.user = JSON.parse(atob(localStorage.getItem('profile')));
  }

  ngOnInit(): void {
    this.propertyService.getPropertiesByOwner(this.horizontalPropertyId, this.user.id).subscribe(r => {
      // this.properties = r.data;
      this.getTypesProperties(r.data);
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(a => {
      this.horizontalProperty = a.data;
    });
  }

  getTypesProperties(data: Property[]): void {
    this.propertyTypeService.getPropertyInUse(this.horizontalPropertyId).subscribe(r => {
      this.typesProperties = r.data;
      this.filterPropertiesType(this.typesProperties, data);
    });
  }

  mapProperties(typeP: PropertyTypeDefault[], properties: Property[]) {
    let propertiesResult = [];
    typeP.forEach(r => {
      propertiesResult = propertiesResult.concat(this.filterProperties(properties, r.name));
    });
    return propertiesResult;
  }

  filterProperties(data: Property[], type: string) {
    return data.filter(p => p.type === type);
  }

  filterPropertiesType(data: PropertyTypeDefault[], properties: Property[]){
    this.properties = this.mapProperties(data.filter(t => t.type === false), properties);
    this.propertiesComplement = this.mapProperties(data.filter(t => t.type === true), properties);
  }

  peoplesShow(id) {
    this.dialog.open(PeoplesComponent, {
      width: '500px',
      data: {
        propertyId: id
      }
    });
  }

  showHistory(p: Property) {
    this.dialog.open(HistoryPropertyComponent, {
      width: '700px',
      data: {
        property: p,
        horizontalPropertyId: this.horizontalPropertyId,
      }
    });
  }

  showLiquidation(id) {
    this.propertyService.showLiquidation(this.horizontalPropertyId, id).subscribe((r: any) => {
      if (r.data.length === 0) {
        this.snack.open('La propiedad esta al dia', 'Aceptar', {duration: 3000});
      } else {
        this.dialog.open(LiquidationComponent, {
          width: '800px',
          data: {
            period: r.data,
            typeIncomes: r.headers,
            property: r.property,
            horizontalProperty: this.horizontalProperty,
            ...r
          }
        });
      }
    });
  }
}
