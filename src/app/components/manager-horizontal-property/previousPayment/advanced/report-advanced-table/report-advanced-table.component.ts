import {Component, Input, OnInit} from '@angular/core';
import {Property} from '../../../../../model/hotizontal-property/property';

@Component({
  selector: 'abin-report-advanced-table',
  templateUrl: './report-advanced-table.component.html',
  styleUrls: ['./report-advanced-table.component.css']
})
export class ReportAdvancedTableComponent implements OnInit {
  @Input() title = '';
  @Input() properties: Property[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
