import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAdvancedTableComponent } from './report-advanced-table.component';

describe('ReportAdvancedTableComponent', () => {
  let component: ReportAdvancedTableComponent;
  let fixture: ComponentFixture<ReportAdvancedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportAdvancedTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAdvancedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
