import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {Debt} from '../../../../../model/hotizontal-property/debt';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {Property} from '../../../../../model/hotizontal-property/property';
import {ActivatedRoute} from '@angular/router';
import {PropertyService} from '../../../property/property.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ApiResponse} from '../../../../../model/apiResponse';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../../model/constants';
import {AdvancedService} from '../advanced.service';
import {CreateAdvancedComponent} from '../create-advanced/create-advanced.component';
import {RolService} from '../../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-list-advanced-detail',
  templateUrl: './list-advanced-detail.component.html',
  styleUrls: ['./list-advanced-detail.component.css']
})
export class ListAdvancedDetailComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  debts: Debt[];
  propertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Debt>;
  sortedData: DebtExtend[];
  property: Property;
  displayedColumns: Array<string> = [
    'pos',
    'name',
    'amount',
    'totalPay',
    'totalDebt',
    'date',
    // 'options'
  ];

  constructor(private advancedService: AdvancedService,
              private routerActive: ActivatedRoute,
              private propertyService: PropertyService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private rolService: RolService) {
    this.routerActive.params.subscribe(r => {
      this.propertyId = r.propertyId;
    });
  }

  ngOnInit(): void {
    this.advancedService.getDebtsOfProperty(this.horizontalProperty.id, this.propertyId).subscribe(r => {
      this.dataSource = this.processTotalsDebt(r.data);
    });
    this.propertyService.getProperty(this.horizontalPropertyId, this.propertyId).subscribe((r: ApiResponse) => {
      this.property = r.data;
    });
    if (this.readNotOnly()) {
      if (!this.displayedColumns.find(a => a === 'options')) {
        this.displayedColumns.push('options');
      }
    }
  }

  processTotalsDebt(datas: Debt[]) {
    const debtView = [];
    for(const d of datas) {
      const totalPay = d.debtIncomes.map(r => Number(r.amount)).reduce((a, b) => a + b);
      const res = {
        ...d,
        totalPay: totalPay,
        totalDebt: d.amount - totalPay
      };
      debtView.push(res);
    }

    return new MatTableDataSource<DebtExtend> (debtView);
  }

  openDialog() {
    this.dialog.open(CreateAdvancedComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalProperty.id
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  delete(debt) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.advancedService.deleteDebt(this.horizontalPropertyId, debt.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }
}
export interface DebtExtend extends Debt {
  totalPay: number;
  totalDebt: number;
}

