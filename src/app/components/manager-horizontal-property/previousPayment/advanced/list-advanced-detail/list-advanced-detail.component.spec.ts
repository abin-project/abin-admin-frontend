import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListAdvancedDetailComponent} from './list-advanced-detail.component';

describe('ListAdvancedDetailComponent', () => {
  let component: ListAdvancedDetailComponent;
  let fixture: ComponentFixture<ListAdvancedDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListAdvancedDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAdvancedDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
