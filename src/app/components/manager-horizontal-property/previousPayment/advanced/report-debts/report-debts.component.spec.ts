import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDebtsComponent } from './report-debts.component';

describe('ReportDebtsComponent', () => {
  let component: ReportDebtsComponent;
  let fixture: ComponentFixture<ReportDebtsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDebtsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDebtsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
