import {Component, OnInit} from '@angular/core';
import {AdvancedService} from '../advanced.service';
import {ActivatedRoute} from '@angular/router';
import {PrintService} from '../../../../../services/print.service';
import {ExcelService} from '../../../../../services/excel.service';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {HorizontalPropertyService} from '../../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-report-debts',
  templateUrl: './report-debts.component.html',
  styleUrls: ['./report-debts.component.css']
})
export class ReportDebtsComponent implements OnInit {
  reports: [any][any];
  actualReport;
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  pos = 0;
  format = 'es';
  links = [];
  activeLink;
  constructor(private advancedService: AdvancedService,
              private routerActive: ActivatedRoute,
              private excelService: ExcelService,
              private horizontalPropertyService: HorizontalPropertyService,
              private pdfService: PrintService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.activeLink = null;
    this.advancedService.getReportDebts(this.horizontalPropertyId).subscribe(r => {
      this.reports = r.data;
      this.links = this.reports.map(m => m.property);
      this.actualReport = this.reports.length > 0 ? this.reports[0] : null;
      this.activeLink = this.links.length > 0 ? this.links[0] : null;
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(a => {
      this.horizontalProperty = a.data;
    });
  }

  reportFile(formatFile, ass) {
    const t = document.getElementById(`table`).offsetWidth;
    const widthDoc = (t * 0.17);
    if (formatFile === 'XLSX') {
      this.format = 'en';
      setTimeout(() => {
        this.excelService.exportAsExcelFileByTable( null, `Lista de deudores ${this.actualReport.property}`, 'table')
          .finally(() => {
            this.format = 'es';
          });
      }, 1000);

    }
    if (formatFile === 'PDF') {
      this.format = 'es';
      this.pdfService.printReportDebtsById([this.horizontalProperty.nameProperty, `Lista de deudores ${this.actualReport.property}`], {
        0: {
          columnWidth: 10,
        },
      }, 'table' , widthDoc).finally(() => {
        this.format = 'es';
      });
    }
  }
  setIndex(e: MatTabChangeEvent) {
    this.pos = e.index;
  }

  setIndexClick(link) {
    this.activeLink = link;
    this.actualReport = this.reports.find(a => a.property === this.activeLink);
  }
}
