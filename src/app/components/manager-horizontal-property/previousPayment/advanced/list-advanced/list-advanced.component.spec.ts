import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListAdvancedComponent} from './list-advanced.component';

describe('ListAdvancedComponent', () => {
  let component: ListAdvancedComponent;
  let fixture: ComponentFixture<ListAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListAdvancedComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
