import {Component, OnInit, ViewChild} from '@angular/core';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {FilterKardexDialogComponent} from '../../../income/filter-kardex-dialog/filter-kardex-dialog.component';
import {CreateAdvancedComponent} from '../create-advanced/create-advanced.component';
import {AdvancedService} from '../advanced.service';
import {Property} from '../../../../../model/hotizontal-property/property';
import {RolService} from '../../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-list-advanced',
  templateUrl: './list-advanced.component.html',
  styleUrls: ['./list-advanced.component.css']
})
export class ListAdvancedComponent implements OnInit {
  // loader = true;
  horizontalProperty: HorizontalProperty;
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Property>;
  sortedData: any[];
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: Array<string> = [
    'pos',
    'code',
    'name',
    'type',
    'options'
  ];

  // params: any = {};

  constructor(private router: Router,
              private advancedService: AdvancedService,
              private activeRouted: ActivatedRoute,
              private dialog: MatDialog,
              private rolService: RolService) {
    this.dataSource = new MatTableDataSource<Property>([]);
    this.activeRouted.parent.params.subscribe(params => {
      this.horizontalPropertyId = params.id;
    });
  }

  ngOnInit(): void {
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.loadData();
  }

  loadData() {
    this.advancedService.getProperties(this.horizontalPropertyId).subscribe((r: any) => {
      this.dataSource = new MatTableDataSource<Property>(r.data);
    });
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<any>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(CreateAdvancedComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalProperty.id
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        // ...this.params,
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.loadData();
      }
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }
}

