import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAdvancedComponent } from './report-advanced.component';

describe('ReportAdvancedComponent', () => {
  let component: ReportAdvancedComponent;
  let fixture: ComponentFixture<ReportAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportAdvancedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
