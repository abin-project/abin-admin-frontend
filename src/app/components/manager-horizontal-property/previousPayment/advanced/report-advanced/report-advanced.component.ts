import {Component, OnDestroy, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import {AdvancedService} from '../advanced.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ExcelService} from '../../../../../services/excel.service';
import {HorizontalPropertyService} from '../../../../horizontal-property/horizontal-property.service';
import {PrintService} from '../../../../../services/print.service';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {Project} from '../../../../../model/hotizontal-property/project';
import * as moment from 'moment';
import {ProjectService} from '../../../project/project.service';
import {ProjectExtends} from '../../../ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'abin-report-advanced',
  templateUrl: './report-advanced.component.html',
  styleUrls: ['./report-advanced.component.css']
})
export class ReportAdvancedComponent implements OnInit, OnDestroy {

  reports: [any][any];
  actualReport;
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  projects: ProjectExtends[];
  params: any = {};
  pos = 0;
  format = 'es';
  links = [];
  activeLink;
  events: Observable<any>;
  data = [];
  constructor(private advancedService: AdvancedService,
              private routerActive: ActivatedRoute,
              private excelService: ExcelService,
              private router: Router,
              private horizontalPropertyService: HorizontalPropertyService,
              private projectService: ProjectService,
              private pdfService: PrintService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.routerActive.queryParams.subscribe(a => {
      this.params = a;
    });
    this.events = this.router.events;
    this.events.subscribe((evt: NavigationEnd) => {
      if (evt instanceof NavigationEnd) {
        this.loadData();
      }
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.events.subscribe().unsubscribe();
  }

  loadData() {
    this.advancedService.getPropertiesOfDay(this.horizontalPropertyId, this.params).subscribe(r => {
      this.data = r.data;
      this.links = r.links;
      this.activeLink = r.data[0].property.name;
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(a => {
      this.horizontalProperty = a.data;
    });
    this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projects = r.data.map(a => {
        return {
          ...a,
          periods: this.processPeriods(a),
          isOpen: this.getOpen(a)
        };
      });
    });
  }

  reportFile(formatFile) {
    const t = document.getElementById(`table`).offsetWidth;
    const widthDoc = (t * 0.17);
    if (formatFile === 'XLSX') {
      this.format = 'en';
      setTimeout(() => {
        this.excelService.exportAsExcelFileByTable( null,  `Lista de pagos a la fecha ${moment(`${this.params.year}-${this.params.month}`, 'YYYY-M').format('MMMM YYYY')}`, 'table')
          .finally(() => {
            this.format = 'es';
          });
      }, 1000);

    }
    if (formatFile === 'PDF') {
      this.format = 'es';
      this.pdfService.printTableById([
        this.horizontalProperty.nameProperty,
        `Lista de pagos a la fecha ${moment(`${this.params.year}-${this.params.month}`, 'YYYY-M').format('MMMM YYYY')}`,
        this.activeLink
        ],
        'table',
        {
          3: {
            halign: 'left',
          },
        })
        .finally(() => {
        this.format = 'es';
      });
    }
  }
  setIndex(e: MatTabChangeEvent) {
    this.pos = e.index;
  }

  setIndexClick(link) {
    this.activeLink = link;
    this.actualReport = this.reports.find(a => a.property === this.activeLink);
  }

  processPeriods(project: Project) {
    const res = [];
    const b = moment(project.yearIni);
    const total = moment(project.yearEnd).diff(b, 'months') + 1;
    for (let i = 0; i < total; i++) {
      res.push({
        text: b.locale('es-ES').format('YYYY-MMMM').toUpperCase(),
        monthNum: (b.month() + 1),
        year: (b.year())
      });
      b.add(1, 'month');
    }
    return res;
  }

  getOpen(a: Project) {
    const now = moment();
    return now >= moment(a.yearIni) && now <= moment(a.yearEnd);
  }

  projectSelectA(p: ProjectExtends) {
    p.isOpen = !p.isOpen;
  }

  getParams(month) {
    return {
      ...this.params,
      year: month.year,
      month: month.monthNum,
    };
  }

  monthYearExist() {
    if (this.params !== {}) {
      return this.params.month !== undefined && this.params.year !== undefined;
    } else {
      return false;
    }
  }
}
