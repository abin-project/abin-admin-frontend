import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Property} from '../../../../../model/hotizontal-property/property';
import {TypeIncomeProperty} from '../../../../../model/hotizontal-property/typeIncome';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../../property/property.service';
import {ItemService} from '../../../item/item.service';
import {map, startWith} from 'rxjs/operators';
import {AdvancedService} from '../advanced.service';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-create-advanced',
  templateUrl: './create-advanced.component.html',
  styleUrls: ['./create-advanced.component.css']
})
export class CreateAdvancedComponent implements OnInit {
  debtForm: FormGroup;
  properties: Property[];
  typesIncomesProperty: TypeIncomeProperty[];
  filteredOptions: Observable<Property[]>;
  pays: PartPay[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CreateAdvancedComponent>,
              private fb: FormBuilder,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private snack: MatSnackBar,
              private advancesService: AdvancedService) {
    this.debtForm = this.fb.group({
      year: [null, Validators.required],
      month: [null, Validators.required],
      typeIncomeProperty: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      property: [null, [Validators.required]],
      propertyCode: [null, [Validators.required]]
    });


  }

  ngOnInit(): void {
    this.debtForm.get('propertyCode').valueChanges.subscribe(r => {
      this.changeProperty(r);
      this.debtForm.get('typeIncomeProperty').setValue(null);
    });

    this.propertyService.getPropertiesWithCodeOnHorizontalProperty(this.data.horizontalPropertyId).subscribe(r => {
      this.properties = r.data;
      this.filteredOptions = this.debtForm.get('propertyCode').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.code),
          map(name => name ? this._filter(name) : this.properties.slice())
        );
    });

  }

  changeProperty(propertyCode) {
    const property = this.properties.find(f => f.code == propertyCode);
    if (property) {
      this.debtForm.get('property').setValue(property.id);
      this.itemService.getTypeIncomeByProperty(this.data.horizontalPropertyId, property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
      });
    } else {
      this.debtForm.get('property').setValue(null);
      this.typesIncomesProperty = [];
    }
  }

  save() {
    if (this.debtForm.valid && this._calTotalPartial() && this._verifyEmptyValue()) {
      const debt = this.debtForm.value;
      debt.date = this.getPeriod(debt.month, debt.year);
      debt.partPay = this.pays;
      this.advancesService.createAdvanced(this.data.horizontalPropertyId, debt.property, this.debtForm.value).subscribe(() => {
        this.dialogRef.close(this.debtForm.value);
      });
    } else {
      this.snack.open('Exiten Campos Vacios o no validos', null, {
        duration: 2000
      });
    }
  }

  getPeriod(month, year) {
    return moment(`${year}-${month}-1`, 'YYYY-M-D').format('YYYY-MM-DD');
  }

  addNewPart() {
    this.pays.push({
      amount: null,
      datePay: null
    });
  }

  removePart(pos) {
    this.pays.splice(pos, 1);
  }

  private _calTotalPartial() {
    if (this.pays.length > 0) {
      const r = this.pays.map(p => Number(p.amount)).reduce((a, b) => a + b, 0);
      return Number(r.toFixed(2)) > 0 && Number(r.toFixed(2)) <= this.debtForm.get('amount').value;
    } else {
      return false;
    }
  }

  private _verifyEmptyValue() {
    if (this.pays.length > 0) {
      const r = this.pays.filter(f => f.amount == 0 || f.amount == null || f.datePay == '' || f.datePay == null);
      return r.length == 0;
    } else {
      return false;
    }
  }
  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }

}

export interface PartPay {
  amount: number;
  datePay: string;
}
