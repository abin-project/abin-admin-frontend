import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateAdvancedComponent} from './create-advanced.component';

describe('CreateAdvancedComponent', () => {
  let component: CreateAdvancedComponent;
  let fixture: ComponentFixture<CreateAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAdvancedComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
