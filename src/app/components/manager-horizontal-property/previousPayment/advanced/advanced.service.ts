import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class AdvancedService extends PrincipalService {
  route = 'advanced-payment';

  getProperties(horizontalPropertyId) {
    return this.get(`${horizontalPropertyId}`);
  }

  createAdvanced(horizontalPropertyId: any, propertyId: string, value: any): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/${propertyId}`, value);
  }

  getDebtsOfProperty(horizontalPropertyId, propertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/${propertyId}`);
  }

  deleteDebt(horizontalPropertyId: string, id: any): Observable<ApiResponse> {
    return this.delete(`${horizontalPropertyId}/${id}`);
  }

  getReportDebts(horizontalPropertyId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/property/debts`);
  }

  getPropertiesOfDay(horizontalPropertyId, params): Observable<any> {
    return this.getQuery(`${horizontalPropertyId}/property/OfDay`, params);
  }
}
