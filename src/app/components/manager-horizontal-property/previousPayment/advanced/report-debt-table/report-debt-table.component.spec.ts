import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDebtTableComponent } from './report-debt-table.component';

describe('ReportDebtTableComponent', () => {
  let component: ReportDebtTableComponent;
  let fixture: ComponentFixture<ReportDebtTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDebtTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDebtTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
