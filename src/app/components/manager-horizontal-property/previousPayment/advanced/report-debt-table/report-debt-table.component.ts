import {Component, Input, OnInit} from '@angular/core';
import {TypeIncome} from '../../../../../model/hotizontal-property/typeIncome';
import * as moment from 'moment';
moment.locale('es');
@Component({
  selector: 'abin-report-debt-table',
  templateUrl: './report-debt-table.component.html',
  styleUrls: ['./report-debt-table.component.css']
})
export class ReportDebtTableComponent implements OnInit {
  @Input() data: [][] = [];
  @Input() items: TypeIncome[] = [];
  @Input() periods: any[];
  @Input() tableId: any;
  @Input() format = 'es';
  width = 0;
  height = 0;
  constructor() { }

  ngOnInit(): void {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }

  setPeriod(period: string) {
    return moment(period, 'YYYY-MM').format('YYYY - MMMM').toString().toUpperCase();
  }

  getStyle() {
    // return {width: `${this.width}px`, height: `${this.height}px`};
    return {
      width: `${this.width}px`,
      height: `calc(${this.height}px - 196px)`
    };
  }

}
