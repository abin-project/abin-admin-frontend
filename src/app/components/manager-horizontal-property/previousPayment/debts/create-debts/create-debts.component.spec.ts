import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateDebtsComponent} from './create-debts.component';

describe('CreateDebtsComponent', () => {
  let component: CreateDebtsComponent;
  let fixture: ComponentFixture<CreateDebtsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateDebtsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDebtsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
