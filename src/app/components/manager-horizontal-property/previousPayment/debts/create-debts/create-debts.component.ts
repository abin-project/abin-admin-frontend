import {Component, OnInit, Inject} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../../property/property.service';
import {ItemService} from '../../../item/item.service';
import {Property} from 'src/app/model/hotizontal-property/property';
import {TypeIncomeProperty} from 'src/app/model/hotizontal-property/typeIncome';
import {IncomeService} from 'src/app/components/manager-horizontal-property/income/income.service';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'abin-create-debts',
  templateUrl: './create-debts.component.html',
  styleUrls: ['./create-debts.component.css']
})
export class CreateDebtsComponent implements OnInit {
  debtForm: FormGroup;
  properties: Property[];
  typesIncomesProperty: TypeIncomeProperty[];
  filteredOptions: Observable<Property[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CreateDebtsComponent>,
              private fb: FormBuilder,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private incomeService: IncomeService) {
    this.debtForm = this.fb.group({
      date: [this.data.date, Validators.required],
      // debtPayLimit: [null, Validators.required],
      typeIncomeProperty: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      property: [null, [Validators.required]],
      propertyCode: [null, [Validators.required]]
    });


  }

  ngOnInit(): void {
    this.debtForm.get('propertyCode').valueChanges.subscribe(r => {
      this.changeProperty(r);
    });

    this.propertyService.getPropertiesWithCodeOnHorizontalProperty(this.data.horizontalPropertyId).subscribe(r => {
      this.properties = r.data;
      this.filteredOptions = this.debtForm.get('propertyCode').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.code),
          map(name => name ? this._filter(name) : this.properties.slice())
        );
    });

  }

  changeProperty(propertyCode) {
    const property = this.properties.find(f => f.code == propertyCode);
    if (property) {
      this.debtForm.get('property').setValue(property.id);
      this.itemService.getTypeIncomeByProperty(this.data.horizontalPropertyId, property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
      });
    } else {
      this.debtForm.get('property').setValue(null);
      this.typesIncomesProperty = [];
    }
  }

  save() {
    if (this.debtForm.valid) {
      const debt = this.debtForm.value;
      // debt.property =
      this.incomeService.createIncome(this.data.horizontalPropertyId, this.debtForm.value).subscribe(() => {
        this.dialogRef.close(this.debtForm.value);
      });
    }
  }

  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }

}
