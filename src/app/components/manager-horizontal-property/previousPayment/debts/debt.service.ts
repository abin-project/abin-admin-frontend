import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../../model/apiResponse';
import {PrincipalService} from '../../../../services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class DebtService extends PrincipalService {
  route = 'debt';

  getDebts(horizontalPropertyId, t): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}`, {type: t});
  }

  getOnlyDebts(horizontalPropertyId, query): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/onlyDebts`, query);
  }

  getOnlyDebtsProperty(horizontalPropertyId, propertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/onlyDebts/${propertyId}`);
  }
  getOnlyDebtOfPayProperty(horizontalPropertyId, propertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/onlyDebtsOfPay/${propertyId}`);
  }

  getAllDebtProperty(horizontalPropertyId, propertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/allDebts/${propertyId}`);
  }

  deleteDebt(horizontalPropertyId: string, id: any) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }
}
