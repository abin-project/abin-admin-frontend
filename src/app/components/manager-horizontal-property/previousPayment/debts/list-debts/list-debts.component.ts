import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CreateDebtsComponent} from '../create-debts/create-debts.component';
import {ActivatedRoute, Router} from '@angular/router';
import {HorizontalProperty} from 'src/app/model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {IncomeService} from '../../../income/income.service';
import {DebtService} from '../debt.service';
import {FilterKardexDialogComponent} from '../../../income/filter-kardex-dialog/filter-kardex-dialog.component';
import {RolService} from '../../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-list-debts',
  templateUrl: './list-debts.component.html',
  styleUrls: ['./list-debts.component.css']
})
export class ListDebtsComponent implements OnInit {
  // loader = true;
  horizontalProperty: HorizontalProperty;
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<DebtsProperty>;
  sortedData: DebtsProperty[];
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: Array<string> = [
    'pos',
    'code',
    'type',
    'amount',
    'total',
    'totalDebt',
    'options'
  ];

  // params: any = {};

  constructor(private router: Router,
              private incomeService: IncomeService,
              private debtService: DebtService,
              private activeRouted: ActivatedRoute,
              private dialog: MatDialog,
              private rolService: RolService) {
    this.dataSource = new MatTableDataSource<DebtsProperty>([]);
    this.activeRouted.parent.params.subscribe(params => {
      this.horizontalPropertyId = params.id;
    });
  }

  ngOnInit(): void {
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.loadData();
  }

  loadData() {
    this.debtService.getOnlyDebts(this.horizontalPropertyId, {}).subscribe((r: any) => {
          this.dataSource = new MatTableDataSource<DebtsProperty>(r.data.debts);
        });
  }

  setDisplayedColumns(header: string[]) {
    const res: string[] = [
      'pos',
      'departmentCode',
    ];
    this.displayedColumns = res.concat(header);
    this.displayedColumns.push('options');
  }

  getRow(g: GeneralPeriod, type: string) {
    return g.typeIncomes.find(f => f.nameIncome == type).totalDebt;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<DebtsProperty>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(CreateDebtsComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalProperty.id
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  routerLoad(t) {
    this.router.navigate([], {
      queryParams: {
        type: t
      }
    }).then(r => {
      this.ngOnInit();
    });
  }

  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        // ...this.params,
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.loadData();
      }
    });
  }

  filtered() {
    // if (this.params != {}) {
    //   return this.params.type != undefined && this.params.projectId != undefined;
    // } else {
    return true;
    // }
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}

export interface GeneralPeriod {
  code: string;
  name: string;
  propertyId: string;
  typeIncomes: kardexGeneralT[];
}

export interface kardexGeneralT {
  nameIncome: string;
  period: string;
  priority: number;
  property_id: string;
  total: number;
  totalDebt?: number;
  typeIncome_id: string;
}

interface DebtsProperty {
  amount: number;
  code: number;
  horizontalPropertyId: string;
  propertyId: string;
  total: number;
  totalDebt: number;
  type: string
}
