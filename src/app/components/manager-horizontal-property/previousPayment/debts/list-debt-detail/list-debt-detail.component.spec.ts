import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListDebtDetailComponent} from './list-debt-detail.component';

describe('ListDebtDetailComponent', () => {
  let component: ListDebtDetailComponent;
  let fixture: ComponentFixture<ListDebtDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListDebtDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDebtDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
