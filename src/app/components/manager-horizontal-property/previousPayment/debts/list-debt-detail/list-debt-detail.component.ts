import {Component, OnInit, ViewChild} from '@angular/core';
import {Debt} from '../../../../../model/hotizontal-property/debt';
import {ActivatedRoute} from '@angular/router';
import {DebtService} from '../debt.service';
import {HorizontalProperty} from '../../../../../model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {CreateDebtsComponent} from '../create-debts/create-debts.component';
import {MatDialog} from '@angular/material/dialog';
import {Property} from '../../../../../model/hotizontal-property/property';
import {PropertyService} from '../../../property/property.service';
import {ApiResponse} from '../../../../../model/apiResponse';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../../model/constants';
import {DebOfPay} from '../../../banks/transaction-debt-pay/transaction-debt-pay.component';
import {RolService} from '../../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-list-debt-detail',
  templateUrl: './list-debt-detail.component.html',
  styleUrls: ['./list-debt-detail.component.css']
})
export class ListDebtDetailComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  debts: DebOfPay[];
  propertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  // loader = true;
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Debt>;
  sortedData: DebOfPay[];
  property: Property;
  displayedColumns: Array<string> = [
    'pos',
    'name',
    'amount',
    'totalPay',
    'totalDebt',
    'date',
    // 'options'
  ];

  constructor(private debtService: DebtService,
              private routerActive: ActivatedRoute,
              private propertyService: PropertyService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private rolService: RolService) {
    this.routerActive.params.subscribe(r => {
      this.propertyId = r.propertyId;
    });
  }

  ngOnInit(): void {
    this.debtService.getOnlyDebtOfPayProperty(this.horizontalProperty.id, this.propertyId).subscribe(r => {
      this.dataSource = new MatTableDataSource<Debt>(r.data);
    });
    this.propertyService.getProperty(this.horizontalPropertyId, this.propertyId).subscribe((r: ApiResponse) => {
      this.property = r.data;
    });
    if (this.readNotOnly()) {
      if (!this.displayedColumns.find(a => a === 'options')) {
        this.displayedColumns.push('options');
      }
    }
  }

  openDialog() {
    this.dialog.open(CreateDebtsComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalProperty.id
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  showPay(debt) {

  }

  pay(debt) {

  }

  delete(debt) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.debtService.deleteDebt(this.horizontalPropertyId, debt.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}
