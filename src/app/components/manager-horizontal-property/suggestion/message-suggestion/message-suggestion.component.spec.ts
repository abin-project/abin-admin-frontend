import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageSuggestionComponent } from './message-suggestion.component';

describe('MessageSuggestionComponent', () => {
  let component: MessageSuggestionComponent;
  let fixture: ComponentFixture<MessageSuggestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageSuggestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageSuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
