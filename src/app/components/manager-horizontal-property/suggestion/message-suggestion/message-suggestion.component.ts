import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageSuggestionService} from '../message-suggestion.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-message-suggestion',
  templateUrl: './message-suggestion.component.html',
  styleUrls: ['./message-suggestion.component.css']
})
export class MessageSuggestionComponent implements OnInit {

  formSuggestion: FormGroup;
  text: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private fb: FormBuilder,
    private ref: MatDialogRef<MessageSuggestionComponent>,
    private suggestionService: MessageSuggestionService) {
      this.formSuggestion = this.fb.group({
        subject: [null, [Validators.required]],
        mailContent: [null, [Validators.required]],
      });
  }
  ngOnInit(): void {
  }
  save() {
    this.formSuggestion.get('mailContent').setValue(this.text);
    this.suggestionService.createSuggestion(this.data.horizontalPropertyId, this.formSuggestion.value).subscribe(r => {
      if (r) {
        this.ref.close(true);
      }
    });
  }

}
