import { Injectable } from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class MessageSuggestionService extends PrincipalService {
  route = 'suggestion';
  createSuggestion(horizontalPropertyId, data): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}`, data);
  }
}
