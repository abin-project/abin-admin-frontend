import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListPropertyComponent} from './property/list-property/list-property.component';
import {ListOwnerComponent} from './owner/list-owner/list-owner.component';
import {ItemsExpenseComponent} from './item/items-expense/items-expense.component';
import {ItemsIncomeComponent} from './item/items-income/items-income.component';
import {ListPropertyComplementComponent} from './property-complement/list-property-complement/list-property-complement.component';
import {ListTenantComponent} from './tenant/list-tenant/list-tenant.component';
import {ListBankComponent} from './banks/list-bank/list-bank.component';
import {ListOwnerIncomeComponent} from './income/individual/list-owner-income/list-owner-income.component';
import {KardexOwnerIncomeComponent} from './income/individual/kardex-owmer-income/kardex-owner-income.component';
import {KardexGeneralIncomeComponent} from './income/general/kardex-general-income/kardex-general-income.component';
import {TransactionsIncomeComponent} from './banks/transactions-income/transactions-income.component';
import {PositionOwnerComponent} from './directory/position-owner/position-owner.component';
import {CalendarRecreationComponent} from './calendar/calendar-recreation/calendar-recreation.component';
import {LedgerComponent} from './ledger/ledger/ledger.component';
import {ListProjectComponent} from './project/list-project/list-project.component';
import {CompleteProjectComponent} from './project/complete-project/complete-project.component';
import {SettingsComponent} from './settings/settings/settings.component';
import {TaskComponent} from './task/task/task.component';
import {ListDebtsComponent} from './previousPayment/debts/list-debts/list-debts.component';
import {ListDebtDetailComponent} from './previousPayment/debts/list-debt-detail/list-debt-detail.component';
import {TransactionExpenseComponent} from './expense/transaction-expense/transaction-expense.component';
import {RenderTransactionComponent} from './expense/render-transaction/render-transaction.component';
import {ListAdvancedComponent} from './previousPayment/advanced/list-advanced/list-advanced.component';
import {ListAdvancedDetailComponent} from './previousPayment/advanced/list-advanced-detail/list-advanced-detail.component';
import {DashboardComponent} from './dashboard/dasboard/dashboard.component';
import {HorizontalPropertyFileComponent} from './horizontalPropertyFile/horizontal-property-file/horizontal-property-file.component';
import {HorizontalPropertyFileOtherComponent} from './horizontalPropertyFile/horizontal-property-file-other/horizontal-property-file-other.component';
import {BankFilesComponent} from './banks/bank-files/bank-files.component';
import {BankFilesYearComponent} from './banks/bank-files-year/bank-files-year.component';
import {IncomeExpenseStateResultComponent} from './incomeExpenseState/income-expense-state-result/income-expense-state-result.component';
import {PersonalListComponent} from './personal/personal-list/personal-list.component';
import {ReportDebtsComponent} from './previousPayment/advanced/report-debts/report-debts.component';
import {RenderAccountBankComponent} from './expense/render-account-bank/render-account-bank.component';
import {FormListComponent} from './forms/form-list/form-list.component';
import {CompleteFormComponent} from './forms/complete-form/complete-form.component';
import {RecreationAreaListComponent} from './recreationArea/recreation-area-list/recreation-area-list.component';
import {OwnerPropertyComponent} from './ownerProperties/owner-property/owner-property.component';
import {KardexOwnerFilterComponent} from './ownerProperties/kardex-owner-filter/kardex-owner-filter.component';
import {DebtManualListComponent} from './income/manual/debt-manual-list/debt-manual-list.component';
import {OwnerTransactionsComponent} from './ownerTransactions/owner-transactions/owner-transactions.component';
import {DetailDebtManualComponent} from './income/manual/detail-debt-manual/detail-debt-manual.component';
import {ReportAdvancedComponent} from './previousPayment/advanced/report-advanced/report-advanced.component';
import {CalendarActivityComponent} from './calendarActivity/calendar-activity/calendar-activity.component';
import { PayProcessComponent } from './ownerProperties/pay-process/pay-process.component';


const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  // {path: '', component: HorizontalPropertyDashboardComponent},
  {path: 'property', component: ListPropertyComponent},
  {path: 'myProperty', component: OwnerPropertyComponent},
  {path: 'myProperty/pay', component: PayProcessComponent},
  {path: 'myProperty/filter', component: KardexOwnerFilterComponent},
  {path: 'myTransactions', component: OwnerTransactionsComponent},
  {path: 'complement', component: ListPropertyComplementComponent},
  {path: 'owner', component: ListOwnerComponent},
  {path: 'directory', component: PositionOwnerComponent},
  {path: 'files', component: HorizontalPropertyFileComponent},
  {path: 'otherFiles', component: HorizontalPropertyFileOtherComponent},
  {path: 'tenant', component: ListTenantComponent},
  {path: 'items/income', component: ItemsIncomeComponent},
  {path: 'items/expense', component: ItemsExpenseComponent},
  {path: 'personal', component: PersonalListComponent},
  {path: 'accounts', component: ListBankComponent},
  {path: 'accounts/:id', component: TransactionsIncomeComponent},
  {path: 'accounts/:id/files', component: BankFilesComponent},
  {path: 'accounts/:id/files/year', component: BankFilesYearComponent},
  {path: 'kardex', component: ListOwnerIncomeComponent},
  {path: 'kardex/property', component: KardexOwnerIncomeComponent},
  {path: 'kardexGeneral', component: KardexGeneralIncomeComponent},
  {path: 'debtManual', component: DebtManualListComponent},
  {path: 'debtManual/:propertyId', component: DetailDebtManualComponent},
  {path: 'calendar', component: CalendarRecreationComponent},
  {path: 'calendar-activity', component: CalendarActivityComponent},
  {path: 'transaction', component: RenderAccountBankComponent},
  {path: 'transaction/:accountId', component: TransactionExpenseComponent},
  {path: 'transaction/:accountId/render', component: RenderTransactionComponent},
  {path: 'ledger', component: LedgerComponent},
  {path: 'projects', component: ListProjectComponent},
  {path: 'projects/:projectId', component: CompleteProjectComponent},
  {path: 'task', component: TaskComponent},
  {path: 'debts', component: ListDebtsComponent},
  {path: 'forms', component: FormListComponent},
  {path: 'forms/complete', component: CompleteFormComponent},
  {path: 'recreationArea', component: RecreationAreaListComponent},
  {path: 'debts-report', component: ReportDebtsComponent},
  {path: 'debts-report-advanced', component: ReportAdvancedComponent},
  {path: 'debts/:propertyId', component: ListDebtDetailComponent},
  {path: 'advanced', component: ListAdvancedComponent},
  {path: 'advanced/:propertyId', component: ListAdvancedDetailComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'incomeExpenseState', component: IncomeExpenseStateResultComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerHorizontalPropertyRoutingModule {
}
