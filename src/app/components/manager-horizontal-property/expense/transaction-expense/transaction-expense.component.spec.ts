import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionExpenseComponent} from './transaction-expense.component';

describe('TransactionExpenseComponent', () => {
  let component: TransactionExpenseComponent;
  let fixture: ComponentFixture<TransactionExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
