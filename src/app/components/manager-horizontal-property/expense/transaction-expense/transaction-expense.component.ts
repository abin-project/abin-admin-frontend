import {Component, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatTableDataSource} from '@angular/material/table';
import {CreateRenderTransactionComponent, ViewTransaction} from '../create-render-transaction/create-render-transaction.component';
import {PeriodService} from '../../period/period.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RenderTransactionService} from '../render-transaction.service';
import {TransactionService} from '../../banks/transaction.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {Property} from '../../../../model/hotizontal-property/property';
import {Sort} from '@angular/material/sort';
import {ProjectService} from '../../project/project.service';
import {compare, compareDate} from '../../../../services/globalFunctions';
import {BankService} from '../../banks/bank.service';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {RenderFilterComponent} from '../render-filter/render-filter.component';
import {DialogShowDetailExpenseComponent} from '../dialog-show-detail-expense/dialog-show-detail-expense.component';

@Component({
  selector: 'abin-transaction-expense',
  templateUrl: './transaction-expense.component.html',
  styleUrls: ['./transaction-expense.component.css']
})
export class TransactionExpenseComponent implements OnInit {
  title = 'Egresos';
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty;
  displayColumnsGroup: string[] = [
    'pos',
    'nameBank',
    'numberAccount',
    'transaction_transactionCode',
    'transaction_date',
    'transaction_gloss',
    'transaction_reference',
    'transaction_amount',
    'totalRender',
    'balance',
    'option',
  ];
  dataSourceGroup: MatTableDataSource<ViewTransaction>;
  sortedData;
  params = {};
  accountId: string;
  account: Bank;


  constructor(private periodService: PeriodService,
              private routerActive: ActivatedRoute,
              private router: Router,
              private horizontalPropertyService: HorizontalPropertyService,
              private dialog: MatDialog,
              private dataGlobalService: DataGlobalService,
              private snack: MatSnackBar,
              private bankService: BankService,
              private renderTransactionService: RenderTransactionService,
              private transactionService: TransactionService,
              private projectService: ProjectService,
              private rolService: RolService) {

    this.routerActive.parent.params.subscribe(tb => {
      this.horizontalPropertyId = tb.id;
    });
    this.routerActive.queryParams.subscribe(p => {
      this.params = p;
    });
    this.routerActive.params.subscribe(r => {
      this.accountId = r.accountId;
    });
  }

  ngOnInit() {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.horizontalProperty = res.data;
    });
    this.bankService.getBank(this.horizontalPropertyId, this.accountId).subscribe((a: ApiResponse) => {
      this.account = a.data;
      this.title = this.account.numberAccount;
    });
    this.loadData();
  }

  loadData() {
    if (this.params !== {}) {
      this.transactionService.getTransactionQuery(this.horizontalPropertyId, {
        onlyExpense: true,
        ...this.params,
        accountBank: this.accountId
      }).subscribe(r => {
        this.dataSourceGroup = new MatTableDataSource<ViewTransaction>(r.data);
      });
    }

  }

  openDialog(transaction: ViewTransaction): void {
    const dialogRef = this.dialog.open(CreateRenderTransactionComponent, {
      width: '500px',
      data: {
        transactionId: transaction.transaction_id,
        horizontalPropertyId: this.horizontalProperty.id
      },
    });
    dialogRef.afterClosed().subscribe((result: Property) => {
      if (result) {
        this.ngOnInit();
      }
    });
  }


  indexPosData(i) {
    return i + 1;
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  sortData(sort: Sort) {
    const data = this.dataSourceGroup.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }
    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameBank':
          return compare(a.transaction_amount, b.transaction_amount, isAsc);
        case 'numberAccount':
          return compare(a.transaction_gloss, b.transaction_gloss, isAsc);
        case 'transaction_transactionCode':
          return compare(a.transaction_gloss, b.transaction_gloss, isAsc);
        case 'transaction_date':
          return compareDate(a.transaction_date, b.transaction_date, isAsc);
        case 'transaction_gloss':
          return compare(a.transaction_gloss, b.transaction_gloss, isAsc);
        case 'transaction_reference':
          return compare(a.transaction_reference, b.transaction_reference, isAsc);
        case 'transaction_amount':
          return compare(a.transaction_amount, b.transaction_amount, isAsc);
        case 'totalRender':
          return compare(a.totalRender, b.totalRender, isAsc);
        case 'balance':
          return compare(a.balance, b.balance, isAsc);
        default:
          return 0;
      }
    });
    this.dataSourceGroup = new MatTableDataSource<ViewTransaction>(this.sortedData);
  }

  showAndPrint() {
    this.dialog.open(DialogShowDetailExpenseComponent, {
      data: {
        transactions: this.dataSourceGroup.data,
        account: this.account,
        horizontalProperty: this.horizontalProperty
      },
      width: '700px',
      height: '100%',
    });
  }

  OpenFilter() {
    this.dialog.open(RenderFilterComponent, {
      width: '300px',
      data: this.params
    }).afterClosed().subscribe(async r => {
      if (r) {
        this.router.navigate([], {
          queryParams: r,
        }).then(() => this.loadData());
      }
    });
  }

  getParams(transaction) {
    return {
      ...this.params,
      transactionId: transaction.transaction_id
    };
  }
}
