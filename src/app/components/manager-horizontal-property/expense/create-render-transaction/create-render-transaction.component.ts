import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ApiResponse} from '../../../../model/apiResponse';
import {RenderTransaction} from '../../../../model/hotizontal-property/renderTransaction';
import {TransactionService} from '../../banks/transaction.service';
import {ItemService} from '../../item/item.service';
import {Item} from '../../../../model/hotizontal-property/item';
import * as moment from 'moment';
import {RenderTransactionService} from '../render-transaction.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Transaction} from '../../../../model/hotizontal-property/transaction';

@Component({
  selector: 'abin-create-render-transaction',
  templateUrl: './create-render-transaction.component.html',
  styleUrls: ['./create-render-transaction.component.css']
})
export class CreateRenderTransactionComponent implements OnInit {

  formRender: FormGroup;
  renderSelect: RenderTransaction;
  horizontalPropertyId: string;
  typeExpenses: Item[] = [];
  changeFile = true

  transactionSelect: Transaction;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateRenderTransactionComponent>,
              private itemService: ItemService,
              private renderTransactionService: RenderTransactionService,
              private transactionService: TransactionService,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.renderSelect = this.data.render;
    this.horizontalPropertyId = this.data.horizontalPropertyId;

    this.formRender = this.fb.group({
      typeExpense: [this.renderSelect ? this.renderSelect.typeExpense.id : null, [Validators.required]],
      date: [this.renderSelect ? this.renderSelect.date : null, [Validators.required]],
      amount: [this.renderSelect ? this.renderSelect.amount : null, [Validators.required, Validators.min(0)]],
      gloss: [this.renderSelect ? this.renderSelect.gloss : null, [Validators.required]],
      file: [this.renderSelect ? this.renderSelect.file : null, [Validators.required]],
      receiptCode: [this.renderSelect ? this.renderSelect.receiptCode : null, [Validators.required]],
    });

  }

  ngOnInit(): void {
    this.transactionService.getOneTransaction(this.horizontalPropertyId, this.data.transactionId).subscribe(r => {
      this.transactionSelect = r.data;
      if (!this.renderSelect) {
        this.formRender.get('date').setValue(this.transactionSelect.date);
        this.changeFile = true;
      } else {
        this.changeFile = false;
      }
    });
    this.load();
  }

  load() {
    this.itemService.getExpense(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.typeExpenses = r.data;
    });
  }

  getMaxAmountHelp() {
    if (this.data.transaction) {
      const total = Math.abs(Number(this.data.transaction.balance)) + Number(this.renderSelect.amount);
      return this.transactionSelect ? total : '0';

    } else {
      // if (this.transactionSelect) {
      return this.transactionSelect ? Math.abs(Number(this.transactionSelect.balance)) : '0';
    }
  }

  async save() {
    try {
      const tr = this.formRender.value;
      const formData = new FormData();
      formData.append('transaction', this.transactionSelect.id);
      formData.append('typeExpense', tr.typeExpense);
      formData.append('receiptCode', tr.receiptCode);
      formData.append('date', moment(tr.date).format('YYYY-MM-DD'));
      formData.append('amount', tr.amount);
      formData.append('gloss', tr.gloss);
      if (this.changeFile) {
        formData.append('file', tr.file._files[0], tr.file._filesNames);
      }
      if (this.formRender.valid && !this.renderSelect) {
        this.renderTransactionService.createRenderTransaction(this.horizontalPropertyId, formData).subscribe(r => {
          this.dialogRef.close(r);
        });
      } else if (this.formRender.valid && this.renderSelect) {
        this.renderTransactionService.updateRenderTransaction(this.horizontalPropertyId, this.renderSelect.id, formData).subscribe(r => {
          this.dialogRef.close(r);
        });
      }
    } catch (e) {
      console.log(e);
      this.snack.open('Existen Campos Vacios', null, {
        duration: 2000,
      });
    }
  }

  setChangeFile() {
    this.changeFile = !this.changeFile;

    if (this.changeFile == false) {
      this.formRender.get('file').setValue(this.renderSelect.file);
    }
  }

}

const toBase64 = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

export interface ViewTransaction {
  nameBank: string;
  numberAccount: string;
  typeAccount: string;
  balance: number;
  totalRender: number;
  transaction_accountBankId: string;
  transaction_agency: string;
  transaction_amount: number;
  transaction_balance: number;
  transaction_createdAt: Date;
  transaction_date: Date;
  transaction_gloss: string;
  transaction_id: string;
  transaction_reference: string;
  transaction_state: number;
  transaction_transactionCode: string;
  transaction_updatedAt: Date;
  transaction_userUpdatedId: string;
  render?: RenderTransaction[];
}


