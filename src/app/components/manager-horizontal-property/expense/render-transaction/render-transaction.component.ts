import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {MatDialog} from '@angular/material/dialog';
import {RenderTransactionService} from '../render-transaction.service';
import {MatTableDataSource} from '@angular/material/table';
import {RenderTransaction} from '../../../../model/hotizontal-property/renderTransaction';
import {Property} from '../../../../model/hotizontal-property/property';
import {Sort} from '@angular/material/sort';
import {compare, compareDate, increment, toUpperCase} from '../../../../services/globalFunctions';
import {SelectionModel} from '@angular/cdk/collections';
import {CreateRenderTransactionComponent} from '../create-render-transaction/create-render-transaction.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {TransactionService} from '../../banks/transaction.service';
import {Transaction} from '../../../../model/hotizontal-property/transaction';
import {ProjectService} from '../../project/project.service';
import {BankService} from '../../banks/bank.service';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {AbinDatePipe} from '../../../../pipes/abin-date.pipe';
import {MoneyPipe} from '../../../../pipes/money.pipe';

@Component({
  selector: 'abin-render-transaction',
  templateUrl: './render-transaction.component.html',
  styleUrls: ['./render-transaction.component.css']
})
export class RenderTransactionComponent implements OnInit {

  title = 'Egresos';
  horizontalPropertyId;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  displayedColumns: string[] = [
    // 'select',
    'pos',
    'reference',
    'receiptCode',
    'typeExpense',
    'gloss',
    'amount',
    'date',
    // 'stateRender',
    // 'user',
    'option'
  ];

  dataSource: MatTableDataSource<RenderTransaction>;
  sortedData: RenderTransaction[];
  transactionSelect: Transaction;
  // projectSelect: Project;
  params;
  accountId: string;
  account: Bank;

  selection = new SelectionModel<RenderTransaction>(true, []);


  constructor(private routerActive: ActivatedRoute,
              private router: Router,
              private horizontalPropertyService: HorizontalPropertyService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private renderTransactionService: RenderTransactionService,
              private bankService: BankService,
              private transactionService: TransactionService,
              private projectService: ProjectService,
              private printService: PrintService,
              private abinDatePipe: AbinDatePipe,
              private abinMoney: MoneyPipe,
              private rolService: RolService) {
    this.dataSource = new MatTableDataSource([]);

    this.routerActive.parent.params.subscribe(tb => {
      this.horizontalPropertyId = tb.id;
    });
    this.routerActive.params.subscribe(r => {
      this.accountId = r.accountId;
    });
    this.routerActive.queryParams.subscribe(p => {
      this.params = p;
    });
  }

  ngOnInit() {
    this.transactionService.getOneTransaction(this.horizontalPropertyId, this.params.transactionId).subscribe(r => {
      this.transactionSelect = r.data;
    });
    this.bankService.getBank(this.horizontalPropertyId, this.accountId).subscribe((a: ApiResponse) => {
      this.account = a.data;
      this.title = this.account.numberAccount;
    });
    this.loadData();
  }

  loadData() {
    this.renderTransactionService.getRenderTransactionFilter(this.horizontalPropertyId, this.params).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<RenderTransaction>(res.data);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateRenderTransactionComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        transactionId: this.transactionSelect.id
      },
    });

    dialogRef.afterClosed().subscribe((result: Property) => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  indexPosData(i) {
    return i + 1;
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }
    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'amount':
          return compare(a.amount, b.amount, isAsc);
        case 'gloss':
          return compare(a.gloss, b.gloss, isAsc);
        case 'stateRender':
          return compare(a.stateRender, b.stateRender, isAsc);
        case 'receiptCode':
          return compare(a.receiptCode, b.receiptCode, isAsc);
        case 'date':
          return compareDate(a.date, b.date, isAsc);
        case 'typeExpense':
          return compare(a.typeExpense.nameExpense, b.typeExpense.nameExpense, isAsc);
        case 'user':
          return compare(a.user.email, b.user.email, isAsc);
        case 'reference':
          return compare(a.transaction.reference, b.transaction.reference, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<RenderTransaction>(this.sortedData);
  }

  showFile(render) {
    window.open(this.renderTransactionService.getFileRenderTransaction(this.horizontalPropertyId, render.file), '_blank');
  }

  update(ev) {
    const dialogRef = this.dialog.open(CreateRenderTransactionComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        render: ev,
        transactionId: this.transactionSelect.id
      },
    });

    dialogRef.afterClosed().subscribe((result: Property) => {
      if (result) {
        this.loadData();
      }
    });
  }

  delete(renderTransaction: RenderTransaction) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION
    }).onAction().subscribe(() => {
      this.renderTransactionService.deleteRenderTransaction(this.horizontalPropertyId, renderTransaction.id).subscribe(r => {
        this.loadData();
      });
    });
  }

  deletes() {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
    }).onAction().subscribe(r => {
      this.renderTransactionService.deleteMultipleRenderTransaction(this.horizontalPropertyId, {selected: this.selection.selected})
        .subscribe(() => {
        this.loadData();
      });
    });
  }

  updateState(valid) {
    this.snack.open('Se Actualizara los Estados', TEXT_ACCEPT, {
      duration: TIME_DURATION,
    }).onAction().subscribe(r => {
      this.renderTransactionService.updateMultipleRenderTransaction(this.horizontalPropertyId, {
        selected: this.selection.selected,
        state: valid
      }).subscribe(() => {
        this.loadData();
      });
    });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: RenderTransaction): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.amount + 1}`;
  }

  getParams() {
    const a = {...this.params};
    delete a.transactionId;
    return a;
  }

  print(option) {
    if (option === 'PDF') {
      const t = this.processBody();
      this.printService.createSingleTableTransaction(
        this.horizontalProperty.nameProperty,
        `Transacción Código T. ${this.transactionSelect.transactionCode}`,
        this.getHead(),
        t.body,
        {
        5: {
          halign: 'right',
        }
      }, this.transactionSelect, this.account, t.total);
    }
  }

  private getHead() {
    return [
      '#',
      'Referencia/Transacción',
      'Recibo',
      'Tipo de Egreso',
      'Glosa/Detalle',
      'Monto',
      'Fecha',
    ];
  }

  private processBody() {
    const res = [];
    let total = 0;
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    // tslint:disable-next-line:forin
    for (const i in data) {
      const d = [
        increment(i),
        toUpperCase(data[i].transaction.transactionCode),
        toUpperCase(data[i].receiptCode),
        toUpperCase(data[i].typeExpense.nameExpense),
        toUpperCase(data[i].gloss),
        this.abinMoney.transform(data[i].amount),
        this.abinDatePipe.transform(data[i].date)
      ];
      total = total + Number(data[i].amount);
      res.push(d);
    }
    res.push([
      '',
      {
        content: 'Total Rendido',
        colSpan: 4
      },
      this.abinMoney.transform(total),
      ''
    ]);
    return {
      body: res,
      total,
    };
  }

}
