import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderAccountBankComponent } from './render-account-bank.component';

describe('RenderAccountBankComponent', () => {
  let component: RenderAccountBankComponent;
  let fixture: ComponentFixture<RenderAccountBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderAccountBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenderAccountBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
