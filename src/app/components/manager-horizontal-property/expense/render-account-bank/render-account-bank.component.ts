import { Component, OnInit } from '@angular/core';
import {ListBankComponent} from '../../banks/list-bank/list-bank.component';

@Component({
  selector: 'abin-render-account-bank',
  templateUrl: './render-account-bank.component.html',
  styleUrls: ['./render-account-bank.component.css']
})
export class RenderAccountBankComponent extends ListBankComponent {

}
