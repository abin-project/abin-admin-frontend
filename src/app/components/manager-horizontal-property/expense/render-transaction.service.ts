import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {QueryRenderTransaction} from '../../../model/queryRenderTransaction.model';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RenderTransactionService extends PrincipalService {
  route = 'render-transaction';

  getRenderTransactionFilter(horizontalPropertyId, params: QueryRenderTransaction): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/query`, params);
  }

  createRenderTransaction(horizontalPropertyId, data) {
    return this.postFile(`${horizontalPropertyId}/create`, data);
  }

  updateRenderTransaction(horizontalPropertyId, renderId, data) {
    return this.putFile(`${horizontalPropertyId}/update/${renderId}`, data);
  }

  deleteRenderTransaction(horizontalPropertyId: any, id: string) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }

  deleteMultipleRenderTransaction(horizontalPropertyId: any, data) {
    return this.postCustom(`${horizontalPropertyId}/delete/multiple`, data);
  }

  updateMultipleRenderTransaction(horizontalPropertyId: any, data: any) {
    return this.put(`${horizontalPropertyId}/update/state/multiple`, data);
  }

  getFileRenderTransaction(horizontalPropertyId, file) {
    return `${this.getPartialRoute()}/${horizontalPropertyId}/show/file/${file}`;
  }
}

