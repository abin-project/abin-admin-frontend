import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PrintService} from '../../../../services/print.service';
import {ViewTransaction} from '../create-render-transaction/create-render-transaction.component';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-dialog-show-detail-expense',
  templateUrl: './dialog-show-detail-expense.component.html',
  styleUrls: ['./dialog-show-detail-expense.component.css']
})
export class DialogShowDetailExpenseComponent implements OnInit {
  transactions: ViewTransaction[];
  account: Bank;
  horizontalProperty: HorizontalProperty;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private dialogRef: MatDialogRef<DialogShowDetailExpenseComponent>,
              private printService: PrintService) {
    this.transactions = this.data.transactions;
    this.account = this.data.account;
    this.horizontalProperty = this.data.horizontalProperty;
  }

  ngOnInit(): void {
  }
  printData() {
    this.printService.printTransactionExpenseDetail(this.transactions, [this.horizontalProperty.nameProperty, 'Detalle de Egresos'], this.account);
  }
}
