import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-render-filter',
  templateUrl: './render-filter.component.html',
  styleUrls: ['./render-filter.component.css']
})
export class RenderFilterComponent implements OnInit {

  formFilter: FormGroup;
  now = moment().format('YYYY-MM-DD');

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<RenderFilterComponent>,
              @Inject(MAT_DIALOG_DATA) data: any) {
    this.formFilter = this.fb.group({
      dateIni: [data.dateIni],
      dateEnd: [data.dateEnd],
      code: [data.code],
      month: [data.month ? data.month : null],
      year: [data.year ? data.year : null]
    });
  }

  ngOnInit(): void {
  }

  send() {
    if (this.formFilter.valid) {
      const res = this.formFilter.value;
      res.dateIni = res.dateIni ? moment(res.dateIni).format('YYYY-MM-DD') : null;
      res.dateEnd = res.dateEnd ? moment(res.dateEnd).format('YYYY-MM-DD') : null;
      this.dialogRef.close(res);
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
