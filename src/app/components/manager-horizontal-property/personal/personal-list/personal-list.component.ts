import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { TEXT_ACCEPT, TEXT_DELETE } from 'src/app/model/constants';
import { Personal } from 'src/app/model/hotizontal-property/personal';
import { PersonalCreateComponent } from '../personal-create/personal-create.component';
import { PersonalService } from '../personal.service';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-personal-list',
  templateUrl: './personal-list.component.html',
  styleUrls: ['./personal-list.component.css']
})
export class PersonalListComponent implements OnInit {
  propertyId: string;
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Personal>;
  displayedColumns: Array<string> = [
    'pos',
    'position',
    'name',
    'email',
    'cell',
    'ci',
    // 'options'
  ];

  constructor(private personalService: PersonalService,
              private routerActive: ActivatedRoute,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              public rolService: RolService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.personalService.getAllPersonals(this.horizontalPropertyId, true).subscribe(r => {
      this.dataSource = new MatTableDataSource<Personal>(r.data);
    });
    if (this.rolService.getUserRol() === 'Root'
      || this.rolService.getUserRol() === 'Administrador del Sistema'
      || this.rolService.getUserRol() === 'Administrador') {
      if (this.displayedColumns.find(r => r === 'options') === undefined) {
        this.displayedColumns.push('options');
      }
    }
  }

  openDialog() {
    this.dialog.open(PersonalCreateComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }

  downPersonal(personal) {
    this.snack.open('Dar de baja', TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.personalService.updatePersonal(this.horizontalPropertyId, personal.id, {available: false}).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  deleted(personal) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.personalService.deletePersonal(this.horizontalPropertyId, personal.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  update(personal: any) {
    this.dialog.open(PersonalCreateComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        personal
      }
    }).afterClosed().subscribe(r => {
      this.ngOnInit();
    });
  }
}
