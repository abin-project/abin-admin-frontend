import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PersonalListComponent } from '../personal-list/personal-list.component';
import { PersonalService } from '../personal.service';

@Component({
  selector: 'abin-personal-create',
  templateUrl: './personal-create.component.html',
  styleUrls: ['./personal-create.component.css']
})
export class PersonalCreateComponent implements OnInit {

  formPosition: FormGroup;
  horizontalPropertyId: string;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<PersonalListComponent>,
              protected personalService: PersonalService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;
    this.formPosition = this.fb.group({
      name: [this.data.personal ? this.data.personal.name : null, [Validators.required]],
      position: [this.data.personal ? this.data.personal.position : null, [Validators.required]],
      cell: [this.data.personal ? this.data.personal.cell : null, [Validators.required]],
      email: [this.data.personal ? this.data.personal.email : null, [Validators.required, Validators.email]],
      ci: [this.data.personal ? this.data.personal.ci : null, [Validators.required]],
      picture: [null],
      pictureCI: [null],
      fileBackground: [null],
    });

  }

  ngOnInit(): void {
  }

  save() {
    if (this.formPosition.valid) {
      const value = this.formPosition.value;
      const formData = new FormData();
      formData.append('name', value.name);
      formData.append('position', value.position);
      formData.append('cell', value.cell);
      formData.append('ci', value.ci);
      formData.append('email', value.email);
      if (value.picture) {
        formData.append('picture', value.picture._files[0], value.picture._filesNames);
      }
      if (value.pictureCI) {
        formData.append('pictureCI', value.pictureCI._files[0], value.pictureCI._filesNames);
      }
      if (value.fileBackground) {
        formData.append('fileBackground', value.fileBackground._files[0], value.fileBackground._filesNames);
      }

      if (!this.data.personal) {
        this.personalService.createPersonal(this.horizontalPropertyId, formData).subscribe(r => {
          this.dialogRef.close(r);
        });
      } else {
        this.personalService.updatePersonalData(this.horizontalPropertyId, this.data.personal.id, formData).subscribe(r => {
          this.dialogRef.close(r);
        });
      }
    }
  }
}
