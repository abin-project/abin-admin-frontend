import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/app/model/apiResponse';
import { PrincipalService } from 'src/app/services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalService extends PrincipalService {
  route = 'personal';

  getAllPersonals(horizontalPropertyId, state?): Observable<ApiResponse> {
    if (state) {
      return this.getQuery(`${horizontalPropertyId}`, { state: state});
    } else {
      return this.getCustom(`${horizontalPropertyId}`);
    }
  }

  createPersonal(horizontalPropertyId, data) {
    return this.postFile(`${horizontalPropertyId}/create`, data);
  }

  updatePersonalData(horizontalPropertyId, personalId, data) {
    return this.putFile(`${horizontalPropertyId}/update/${personalId}`, data);
  }

  updatePersonal(horizontalPropertyId, personalId, data) {
    return this.put(`${horizontalPropertyId}/update/${personalId}`, data);
  }

  deletePersonal(horizontalPropertyId, personalId) {
    return this.delete(`${horizontalPropertyId}/delete/${personalId}`);
  }

  getUrlFileS(id: string, name: string) {
    return this.getUrlFile(`${id}/file/${name}`);
  }
}
