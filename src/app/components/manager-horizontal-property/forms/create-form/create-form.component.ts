import {Component, Inject, OnInit} from '@angular/core';
import {ItemService} from '../../item/item.service';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Project} from '../../../../model/hotizontal-property/project';
import * as moment from 'moment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormIncomeService} from '../form-income.service';
import {PropertyType} from '../../../../model/hotizontal-property/propertyTypes';
import {CreateFormReceiptComponent, FileReceipt} from '../create-form-receipt/create-form-receipt.component';

moment.locale('es');

@Component({
  selector: 'abin-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css']
})
export class CreateFormComponent implements OnInit {
  typeIncomes: TypeIncome[];
  periods: any[];
  project: Project;
  properties: PropertyTypeEx[] = [];
  formForm: FormGroup;
  formReceiptAmount: FormGroup;
  formReceipts: FileReceipt[] = [];
  formIncomePropertyOfCreates = [
    {value: 'Agua fria', select: false},
    {value: 'Agua caliente', select: false},
  ];

  months: any[] = [];
  constructor(@Inject(MAT_DIALOG_DATA) private data,
              private dialogRef: MatDialogRef<CreateFormComponent>,
              private itemService: ItemService,
              private formIncomeService: FormIncomeService,
              private dialog: MatDialog,
              private fb: FormBuilder) {
    this.project = this.data.project;
    this.formForm = this.fb.group({
      period: [null, [Validators.required]],
      ofPeriod: null,
      ofMonth: [null, [Validators.required]],
      ofYear: [null, [Validators.required]],
      amountPeriod: null,
      typeIncomeId: [null, [Validators.required]],
      formDateReader: [null, [Validators.required]],
      projectId: [this.project.id, [Validators.required]]
    });
    this.formReceiptAmount = this.fb.group({
      amountPeriod: [null, [Validators.required]]
    });
    this.months = moment.months().map(r => r.toUpperCase());
  }

  ngOnInit(): void {
    this.formIncomeService.getProperties(this.data.horizontalPropertyId).subscribe(r => {
      // console.log(r);
      this.properties = r.data;
    });
    this.itemService.getTypesIncomesByPriority(this.data.horizontalPropertyId, 2).subscribe(r => {
      this.typeIncomes = r.data;
    });
    this.loadPeriods();
  }

  loadPeriods() {
    this.periods = [];
    const dateIni = moment(this.project.yearIni);
    const dateEnd = moment(this.project.yearEnd);
    while (dateIni < dateEnd) {
      const m = dateIni.format('MMMM');
      const y = dateIni.format('YYYY');
      const p = dateIni.format('YYYY-MM-DD');
      this.periods.push({
        month: m.toUpperCase(),
        year: y,
        period: p
      });
      dateIni.add(1, 'month');
    }
  }

  setPeriod() {
    const p = moment(this.formForm.get('period').value).subtract(1, 'month');
    this.formForm.get('ofYear').setValue(p.format('YYYY'));
    this.formForm.get('ofMonth').setValue(Number(p.format('M')));
  }

  save() {
    if (this.formForm.valid) {
      const form = {
        ...this.formForm.value,
        ...this.formReceiptAmount.value
      };
      form.increments = this.properties;
      form.formIncomeReceipts = this.formReceipts.map(a => {
        return {
          receiptAmount: a.receiptAmount,
          receiptOther : a.receiptOther,
        };
      });
      form.ofPeriod = moment(`${form.ofYear}-${form.ofMonth}-1`, 'YYYY-M-D').format('YYYY-MM-DD');
      const data = new FormData();
      data.append('form', JSON.stringify(form));
      data.append('typesForms', JSON.stringify(this.formIncomePropertyOfCreates));
      // tslint:disable-next-line:forin
      for (const i in this.formReceipts) {
        data.append('file' + i, this.formReceipts[i]);
      }
      this.formIncomeService.createFormIncome(this.data.horizontalPropertyId, data).subscribe(r => {
        if (r) {
          this.dialogRef.close(r);
        }
      });
    }
  }

  addReceipt() {
    this.dialog.open(CreateFormReceiptComponent, {
      width: '400px',
    }).afterClosed().subscribe(r => {
      if (r) {
        this.formReceipts.push(r);
        this.calcAmountPeriod();
      }
    });
  }

  calcAmountPeriod() {
    const t = this.formReceipts.map(a => Number(a.receiptAmount)).reduce((a, b) => a + b);
    this.formReceiptAmount.get('amountPeriod').setValue(t);
  }

  deleteReceipt(pos) {
    this.formReceipts.splice(pos, 1);
    this.calcAmountPeriod();
  }
}

export interface PropertyTypeEx extends PropertyType{
  increment: number;
  selected: boolean;
}
