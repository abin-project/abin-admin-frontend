import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class FormIncomeReceiptService extends PrincipalService{
  route = 'form-income-receipt';

  showFile(horizontalPropertyId, fileName) {
    return `${this.getPartialRoute()}/${horizontalPropertyId}/file/show/${fileName}`;
  }
}
