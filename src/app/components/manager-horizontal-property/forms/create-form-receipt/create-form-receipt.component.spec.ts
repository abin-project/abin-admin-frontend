import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFormReceiptComponent } from './create-form-receipt.component';

describe('CreateFormReceiptComponent', () => {
  let component: CreateFormReceiptComponent;
  let fixture: ComponentFixture<CreateFormReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFormReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFormReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
