import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-create-form-receipt',
  templateUrl: './create-form-receipt.component.html',
  styleUrls: ['./create-form-receipt.component.css']
})
export class CreateFormReceiptComponent implements OnInit {
  formReceipt: FormGroup;
  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateFormReceiptComponent>) {
    this.formReceipt = this.fb.group({
      receiptAmount: [null, [Validators.required, Validators.min(1)]],
      receiptFile: null,
      receiptOther: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formReceipt.valid) {
      const val = this.formReceipt.value;
      const res = val.receiptFile.files[0] as FileReceipt;
      res.receiptAmount = val.receiptAmount;
      res.receiptOther = val.receiptOther;
      res.receiptFile = val.receiptFile.files[0];
      this.dialogRef.close(res);
    }
  }
}

export interface FileReceipt extends Blob{
  readonly lastModified: number;
  readonly name: string;
  receiptAmount: number;
  receiptOther: string;
  receiptFile: any;
}
