import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormIncome} from '../../../../model/hotizontal-property/formIncome';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AbinPeriodPipe} from '../../../../pipes/abin-period.pipe';
import {FormIncomeService} from '../form-income.service';

@Component({
  selector: 'abin-form-update-date',
  templateUrl: './form-update-date.component.html',
  styleUrls: ['./form-update-date.component.css']
})
export class FormUpdateDateComponent implements OnInit {
  formIncome: FormIncome;
  horizontalPropertyId;
  formPeriod: FormGroup;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private abinPeriod: AbinPeriodPipe,
              private formIncomeService: FormIncomeService,
              private dialogRef: MatDialogRef<FormUpdateDateComponent>,
              private fb: FormBuilder) {
    this.formIncome = data.period;
    this.horizontalPropertyId = data.horizontalPropertyId;
    this.formPeriod = this.fb.group({
      period: [ {value: this.abinPeriod.transform(this.formIncome.period), disabled: true}],
      periodOf: [ {value: this.abinPeriod.transform(this.formIncome.ofPeriod), disabled: true}],
      formDateReader: [ this.formIncome.formDateReader, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  accept() {
    if (this.formPeriod.valid) {
      this.formIncomeService.updateForm(this.horizontalPropertyId, this.formIncome.id, this.formPeriod.value).subscribe(a => {
        this.dialogRef.close(true);
      });
    }
  }

}
