import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormUpdateDateComponent } from './form-update-date.component';

describe('FormUpdateDateComponent', () => {
  let component: FormUpdateDateComponent;
  let fixture: ComponentFixture<FormUpdateDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormUpdateDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUpdateDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
