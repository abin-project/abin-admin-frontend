import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Property} from '../../../../model/hotizontal-property/property';
import {SelectionModel} from '@angular/cdk/collections';
import {PropertyService} from '../../property/property.service';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormIncomeService} from "../form-income.service";

@Component({
  selector: 'abin-dialog-send-ticket',
  templateUrl: './dialog-send-ticket.component.html',
  styleUrls: ['./dialog-send-ticket.component.css']
})
export class DialogSendTicketComponent implements OnInit {
  displayedColumns: string[] = ['select', 'position', 'code', 'name'];
  dataSource = new MatTableDataSource<Property>([]);
  selection = new SelectionModel<Property>(true, []);

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private formIncomeService: FormIncomeService,
              private dialogRef: MatDialogRef<DialogSendTicketComponent>,
              private propertyService: PropertyService) { }

  ngOnInit(): void {
    this.propertyService.getPropertiesOnHorizontalProperty(this.data.horizontalPropertyId).subscribe(a => {
      this.dataSource = new MatTableDataSource<Property>(a.data);
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Property): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  accept() {
    this.formIncomeService.postSendEmail(this.data.horizontalPropertyId, this.data.formId, {
      properties: this.selection.selected.map(p => p.id)
    }).subscribe(r => {
      this.dialogRef.close(true);
    });
    // if (!this.selection.isEmpty()) {
    //   this.dialogRef.close(this.selection.selected.map(a => a.id));
    // } else {
    //   this.dialogRef.close(null);
    // }
  }
}
