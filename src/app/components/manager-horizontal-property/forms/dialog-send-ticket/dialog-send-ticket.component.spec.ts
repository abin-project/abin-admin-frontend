import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSendTicketComponent } from './dialog-send-ticket.component';

describe('DialogSendTicketComponent', () => {
  let component: DialogSendTicketComponent;
  let fixture: ComponentFixture<DialogSendTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSendTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSendTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
