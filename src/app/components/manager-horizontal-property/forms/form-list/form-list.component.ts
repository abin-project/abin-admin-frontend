import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE} from '../../../../model/constants';
import {FilterKardexDialogComponent} from '../../income/filter-kardex-dialog/filter-kardex-dialog.component';
import {CreateFormComponent} from '../create-form/create-form.component';
import {FormIncomeService} from '../form-income.service';
import {Project} from '../../../../model/hotizontal-property/project';
import {ProjectService} from '../../project/project.service';
import {FormIncome} from '../../../../model/hotizontal-property/formIncome';
import {yearsPerRow} from '@angular/material/datepicker';
import {FormUpdateDateComponent} from '../form-update-date/form-update-date.component';

@Component({
  selector: 'abin-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})
export class FormListComponent implements OnInit {
  projectId: string;
  project: Project;
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<FormIncome>;
  displayedColumns: Array<string> = [
    'pos',
    'period',
    'ofPeriod',
    'typeIncome',
    'amountPeriod',
    'amountReal',
    'options'
  ];
  params;

  constructor(private formIncomeService: FormIncomeService,
              private routerActive: ActivatedRoute,
              private projectService: ProjectService,
              private dialog: MatDialog,
              private router: Router,
              private snack: MatSnackBar) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });
  }

  ngOnInit(): void {
    if (this.params !== {}) {
      this.loadData();
    }
  }

  loadData() {
    if (this.params.projectId) {
      this.projectService.getProjectById(this.horizontalPropertyId, this.params.projectId).subscribe(r => {
        this.project = r.data;
      });

      this.formIncomeService.getForms(this.horizontalPropertyId, this.params).subscribe(r => {
        this.dataSource = new MatTableDataSource<FormIncome>(r.data);
      });
    }
  }

  openDialog() {
    this.dialog.open(CreateFormComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        project: this.project
      }
    }).afterClosed().subscribe(r => {
      this.loadData();
    });
  }

  closedForm(form: FormIncome) {
    this.snack.open('Es Irreversible, proceder de todas maneras?', TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.formIncomeService.closedFormIncome(this.horizontalPropertyId, form.id).subscribe(r => {
        this.ngOnInit();
      });
    });
  }

  getData() {
    return {
      ...this.params,
      horizontalPropertyId: this.horizontalPropertyId,
    };
  }

  completePeriod(period: FormIncome) {
    this.router.navigate([`/managerHorizontalProperty/${this.horizontalPropertyId}/forms/complete`], {
      queryParams: {
        projectId: this.project.id,
        formId: period.id
      }
    });
  }

  editForm(period: FormIncome) {
    this.dialog.open(FormUpdateDateComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        period,
      }
    }).afterClosed().subscribe(r => {
      this.loadData();
    });
  }

  deleteForm(form: FormIncome) {
    this.snack.open('tambien se eliminaran las deudas de agua', TEXT_ACCEPT, {
      duration: 5000
    }).onAction().subscribe(() => {
      this.formIncomeService.deleteForm(this.horizontalPropertyId, form.id).subscribe(r => {
        this.ngOnInit();
      });
    });
  }


}
