import { Injectable } from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class FormIncomeService extends PrincipalService {
  route = 'form-income';

  getForms(horizontalPropertyId, query): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}`, query);
  }

  getProperties(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/properties`);
  }

  createFormIncome(horizontalPropertyId: string, value: any): Observable<ApiResponse> {
    return this.postFile(`${horizontalPropertyId}/create`, value);
  }

  completeFormIncome(horizontalPropertyId, formId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/complete/${formId}`);
  }

  updateCompleteFormIncome(horizontalPropertyId, formId, data): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/complete/${formId}`, data);
  }

  getFormById(horizontalPropertyId, formId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/show/${formId}`);
  }

  deleteForm(horizontalPropertyId: string, idForm: string): Observable<ApiResponse> {
    return this.delete(`${horizontalPropertyId}/${idForm}`);
  }

  closedFormIncome(horizontalPropertyId: string, id: string) {
    return this.get(`${horizontalPropertyId}/closed/${id}`);
  }

  updateForm(horizontalPropertyId: string, id: string, data) {
    return this.put(`${horizontalPropertyId}/update/${id}`, data);
  }

  postSendEmail(horizontalPropertyId: string, idForm: string, property) {
    return this.postCustom(`${horizontalPropertyId}/sendEmail/${idForm}`, property);
  }
}
