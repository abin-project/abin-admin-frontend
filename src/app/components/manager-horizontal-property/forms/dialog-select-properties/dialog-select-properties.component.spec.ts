import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSelectPropertiesComponent } from './dialog-select-properties.component';

describe('DialogSelectPropertiesComponent', () => {
  let component: DialogSelectPropertiesComponent;
  let fixture: ComponentFixture<DialogSelectPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSelectPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSelectPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
