import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Property} from '../../../../model/hotizontal-property/property';
import {SelectionModel} from '@angular/cdk/collections';
import {PropertyService} from '../../property/property.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-dialog-select-properties',
  templateUrl: './dialog-select-properties.component.html',
  styleUrls: ['./dialog-select-properties.component.css']
})
export class DialogSelectPropertiesComponent implements OnInit {
  displayedColumns: string[] = ['select', 'position', 'code', 'name'];
  dataSource = new MatTableDataSource<Property>([]);
  selection = new SelectionModel<Property>(true, []);
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private propertyService: PropertyService,
              private dialogRef: MatDialogRef<DialogSelectPropertiesComponent>) {
  }

  ngOnInit(): void {
    this.propertyService.getPropertiesOnHorizontalProperty(this.data.horizontalPropertyId).subscribe(a => {
      this.dataSource = new MatTableDataSource<Property>(a.data);
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Property): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  accept() {
    if (!this.selection.isEmpty()) {
      this.dialogRef.close(this.selection.selected.map(a => a.id));
    } else {
      this.dialogRef.close(null);
    }
  }
}
