/* tslint:disable:no-string-literal */
import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../project/project.service';
import {FormIncomeService} from '../form-income.service';
import {ActivatedRoute} from '@angular/router';
import {FormIncome} from '../../../../model/hotizontal-property/formIncome';
import {FormIncomeProperty} from '../../../../model/hotizontal-property/formIncomeProperty';
import {FormIncomeIncrements} from '../../../../model/hotizontal-property/formIncomeIncrements';
import {roundTo, toUpperCase} from '../../../../services/globalFunctions';
import {FormIncomeReceiptService} from '../form-income-receipt.service';
import {FormIncomeReceipt} from '../../../../model/hotizontal-property/formIncomeReceipt';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';
import {PropertyService} from '../../property/property.service';
import {DataGlobalService} from '../../../../services/data-global.service';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintService} from '../../../../services/print.service';
import {WatterPipe} from '../../../../pipes/watter.pipe';
import {MoneyPipe} from '../../../../pipes/money.pipe';
import * as moment from 'moment';
import {MatDialog} from '@angular/material/dialog';
import {DialogSelectPropertiesComponent} from '../dialog-select-properties/dialog-select-properties.component';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {AbinPeriodPipe} from '../../../../pipes/abin-period.pipe';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {AbinDatePipe} from '../../../../pipes/abin-date.pipe';
import {ExcelService} from '../../../../services/excel.service';
import {processRules} from '@angular/compiler/src/shadow_css';
import {DialogSendTicketComponent} from '../dialog-send-ticket/dialog-send-ticket.component';

@Component({
  selector: 'abin-complete-form',
  templateUrl: './complete-form.component.html',
  styleUrls: ['./complete-form.component.css']
})
export class CompleteFormComponent implements OnInit {
  properties: FormIncomeProperty[];
  horizontalPropertyId: string;
  increments: FormIncomeIncrements[] = [];
  formIncomeReceipts: FormIncomeReceipt[] = [];
  params: any;
  form = {
    id: null,
    open: false,
    amountPeriod: 0
  } as FormIncome;
  lastReader;
  typeForms = [];
  forms = [];
  defaultPosition = 0 ;
  dateLastReader;
  dateActualReader;
  // amountM3 = 0;
  constructor(private projectService: ProjectService,
              private formIncomeService: FormIncomeService,
              private formIncomeReceiptService: FormIncomeReceiptService,
              private propertyService: PropertyService,
              private dataGlobalService: DataGlobalService,
              private horizontalPropertyService: HorizontalPropertyService,
              private snack: MatSnackBar,
              private pipeNumber: MoneyPipe,
              private watterPipe: WatterPipe,
              private datePipe: AbinDatePipe,
              private periodPipe: AbinPeriodPipe,
              private printService: PrintService,
              private dialog: MatDialog,
              private routerActive: ActivatedRoute,
              private excelService: ExcelService,
              public  rolService: RolService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });

    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });
  }

  ngOnInit(): void {
    this.formIncomeService.completeFormIncome(this.horizontalPropertyId, this.params.formId).subscribe(r => {
      // this.properties = r.data.properties;
      // this.lastReader = r.data.lastReader;
      this.forms = r.data;
      this.typeForms = r.other.typesForms;
      this.properties = this.forms[this.defaultPosition].properties;
      this.lastReader = this.forms[this.defaultPosition].lastReader;
      this.dateLastReader = this.forms[this.defaultPosition].lastDate;
    });
    this.formIncomeService.getFormById(this.horizontalPropertyId, this.params.formId).subscribe(r => {
      this.form = r.data;
      // this.amountM3 = this.form.amountM3;
      this.increments = r.other.increments;
      this.formIncomeReceipts = r.other.receipts;
      this.dateActualReader = this.form.formDateReader;
    });
  }

  async calcAmount(p: FormIncomeProperty) {
    const t = roundTo(p.actualReader - p.lastReader, 2);
    p.consumption = Number(p.actualReader === p.lastReader ? 0 : t);
    await this.calcAmountM3();
    await this.reCalcAmounts();
  }

  save() {
    this.formIncomeService.updateCompleteFormIncome(this.horizontalPropertyId, this.form.id,
      {properties: this.properties, increments: this.increments, formIncome: this.form}).subscribe(r => {
        this.ngOnInit();
    });
  }

  async calcAmountM3() {
    // if (this.getTotal() > 0) {
    //   this.form.amountM3 = Number(this.getTotal()) / this.properties.map( f => Number(f.consumption)).reduce((a, b) => a + b);
    // } else {
      this.form.amountM3 = Number(this.form.amountPeriod) / this.properties.map( f => Number(f.consumption)).reduce((a, b) => a + b);
    // }
  }

  async reCalcAmounts() {
    for (const r of this.properties) {
      const type = this.increments.find(t => t.propertyType.name === r.property.type);
      r.amountPay = Number(r.consumption) * (Number(this.form.amountM3) * (Number(type.increment) / 100));
      r.amountPay = await this.getRealAmount(r.amountPay);
    }
  }

  async getRealAmount(r: number): Promise<number> {
    const n = Number(r);
    if (n !== 0) {
      const a = Math.trunc(n);
      const b = Number((n % 1).toFixed(1));
      const c = b + 0.1;
      return Number(Number(a + c).toFixed(1));
    } else {
      return 0;
    }

  }

  getTotal() {
    return this.properties.map( f => Number(f.amountPay)).reduce((a, b) => a + b);
  }

  getTotalM3() {
    return this.properties.map( f => Number(f.consumption)).reduce((a, b) => a + b);
  }

  enterEvent(event: KeyboardEvent, el: Element) {
    if (event.code === 'Enter') {
      const evt = new KeyboardEvent('keypress', {
        key: 'Tab'
      });
      el.dispatchEvent(evt);
    }
  }

  showFile(fileName) {
    window.open(this.formIncomeReceiptService.showFile(this.horizontalPropertyId, fileName), '_blank');
  }

  send() {
    this.dialog.open(DialogSendTicketComponent, {
      width: '500px',
      data: {
        formId: this.form.id,
        horizontalPropertyId: this.horizontalPropertyId,
      }
    });
  }
  selectProperties() {
    this.dialog.open(DialogSelectPropertiesComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(a => {
      if (a) {
        this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
          const watter: TypeIncome = r.other.watter;
          const horizontalProperty = r.data;
          const typeWatter = this.typeForms[this.defaultPosition].typeWatter;
          this.propertyService.showLiquidationInSelectProperties(this.horizontalPropertyId, this.form.id, a, typeWatter)
            .subscribe((res: any) => {
            const properties = res.data.map(data => {
              return {
                ...data,
                head: this.processHeaderLiquidation(data.headers),
                body: this.processLiquidation(data.data)
              };
            });
            this.printService.printAllVoucher([horizontalProperty.nameProperty], properties);
          });
        });
      }
    });
  }

  printVoucherPayment(id) {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
      const watter: TypeIncome = r.other.watter;
      const horizontalProperty = r.data;
      this.propertyService.showLiquidation(this.horizontalPropertyId, id, {formId: this.form.id}).subscribe((a: any) => {
        const isWatter = !! (watter && watter.code === 103);
        if (a.data.length  === 0) {
          this.snack.open('La propiedad esta al dia', 'Aceptar', {duration: 3000});
        } else {
          this.printService.printVoucherPayment([horizontalProperty.nameProperty],
            a.property,
            a,
            this.processHeaderLiquidation(a.headers),
            this.processLiquidation(a.data));
        }
      });
    });
  }

  editableForm(property: FormIncomeProperty) {
    if (this.form.open === true) {
      if (this.lastReader) {
        return property.inLastForm === false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  private processHeaderLiquidation(headers: any) {
    let h = ['Periodo'];
    h = h.concat(headers.map(a => a.nameIncome));
    h.push('Total');
    return h;
  }

  private processLiquidation(data: any) {
    const res = [];
    for (const period of data) {
      const p = [];
      p.push(moment(period.period).format('YYYY-MMMM').toUpperCase());
      // tslint:disable-next-line:forin
      for (const i in period.debts) {
        p.push(period.debts[i].debtOfPay);
      }
      p.push(period.totalOfPay);
      res.push(p);
    }
    if (res.length > 1) {
      const total = res.reduce((a, b) => {
        const r = new Array(a.length);
        for (const i in a) {
          // @ts-ignore
          if (i > 0) {
            r[i] = Number(a[i]) + Number(b[i]);
          }
        }
        return r;
      });
      res.push(total);
    } else {
      const a = res[0].slice();
      a[0] = '';
      res.push(a);
    }
    return res;
  }


//  imprime la planilla
  printForm() {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
      const horizontalProperty = r.data as HorizontalProperty;
      const titles = [
        horizontalProperty.nameProperty.toUpperCase(),
        `Planilla de Agua ${this.periodPipe.transform(this.form.period)}`.toUpperCase(),
        `Correspondiente a  ${this.periodPipe.transform(this.form.ofPeriod)}`.toUpperCase()
      ];
      const p = this.processTables();
      this.printService.printFormWatter(titles, p);
    });

  }

  private processTables() {
    const tables = [];
    tables.push({
      title: `Planilla ${this.typeForms[this.defaultPosition].typeWatter}`,
      head: ['#',
        'Código',
        'Tipo',
        'Propiedad',
        `Lectura Acterior ${this.dateLastReader ? this.datePipe.transform(this.dateLastReader) : ''}`,
        `Lectura Actual ${this.dateActualReader ? this.datePipe.transform(this.dateActualReader) : ''}`,
        'Consumo M3',
        'Monto A Pagar',
        'Monto Pendiente'
      ],
      body: this.getWatterFormBody(),
    });
    tables.push({
      title: 'Factor de Cobro',
      head: ['Código', 'Tipo', 'Precio por M3', 'Incremento %', 'Precio Total'],
      body: this.getIncrements(),
    });
    if (this.formIncomeReceipts.length > 0) {
      tables.push({
        title: 'Recibos',
        head: ['#', 'Detalle', 'Monto'],
        body: this.getFiles(),
      });
    }

    return tables;
  }

  private getWatterFormBody() {
    const s = [
      {
        content: 'TOTAL', colSpan: 6,
        styles: {
          font: 'OpenSansBold',
          fontStyle: 'bold',
          halign: 'right'
        }
      },
      {
        content: this.pipeNumber.transform(this.getTotalM3()),
        styles: {
          font: 'OpenSansBold',
          fontStyle: 'bold',
          halign: 'right'
        }
      },
      {
        content: this.pipeNumber.transform(this.getTotal()),
        styles: {
          font: 'OpenSansBold',
          fontStyle: 'bold',
          halign: 'right'
        }
      }
    ];
    const bo: any = this.properties.map((a, n) => [
      n + 1,
      a.property.code,
      toUpperCase(a.property.type),
      toUpperCase(a.property.name),
      {
        content: this.watterPipe.transform(a.lastReader),
        styles: {
          halign: 'right'
        }
      },
      {
        content:    this.watterPipe.transform(a.actualReader),
        styles: {
          halign: 'right'
        }
      },      {
        content: this.pipeNumber.transform(a.consumption),
        styles: {
          halign: 'right'
        }
      },
      {
        content: this.pipeNumber.transform(a.amountPay),
        styles: {
          halign: 'right'
        }
      },
      {
        content: this.pipeNumber.transform(a.debt.debtOfPay),
        styles: {
          halign: 'right'
        }
      }
    ]);
    bo.push(s);
    return bo;
  }

  private getIncrements() {
    return this.increments.map(a => {
      return [
        a.propertyType.code,
        a.propertyType.name,
        {
          content: this.pipeNumber.transform(this.form.amountM3),
          styles: {
            halign: 'right',
          }
        },
        {
          content: this.pipeNumber.transform(a.increment),
          styles: {
            halign: 'right',
          }
        },
        {
          content: (this.form.amountM3 * (a.increment / 100)),
          styles: {
            halign: 'right',
          }
        }
      ];
    });
  }

  private getFiles() {
    const t: any[] = this.formIncomeReceipts.map((a, index) => {
      return [
        index + 1,
        a.receiptOther,
        {
          content: this.pipeNumber.transform(a.receiptAmount),
          styles: {
            halign: 'right'
          }
        },
      ];
    });
    t.push([
      {
        content: 'Total',
        styles: {
          font: 'OpenSansBold',
          fontStyle: 'bold',
          halign: 'right'
        },
        colSpan: 2,
      },
      {
        content: this.pipeNumber.transform(this.formIncomeReceipts.map(a => Number(a.receiptAmount)).reduce((a, b) => a + b, 0)),
        styles: {
          font: 'OpenSansBold',
          fontStyle: 'bold',
          halign: 'right'
        },
      }
    ]);
    return t;
  }

  setPosition(i: number) {
    this.defaultPosition = i;
    this.properties = this.forms[i].properties;
    this.lastReader = this.forms[i].lastReader;
  }

  printFormXLXS() {
    this.excelService.exportAsExcelByJson(
      'Planilla de Agua' + new Date().getTime(),
      this.typeForms.map(a => a.typeWatter),
      this.processPropertiesForXlsx());
  }
  private processPropertiesForXlsx() {
    const res = [];
    for (const form of this.forms) {
      console.log(form);
      let p = form.properties.map((a, index) => {
        const s = {};
        s['#'] = index + 1;
        s['Código'] = a.property.code;
        s['Tipo'] = a.property.type;
        s['Propiedad'] = a.property.name;
        s['Lectura Anterior ' + this.datePipe.transform(form.lastDate)] = this.watterPipe.transform(a.lastReader);
        s['Lectura Actual ' + this.datePipe.transform(form.actualDate)] = this.watterPipe.transform(a.actualReader);
        s['Consumo m3'] = this.pipeNumber.transform(a.consumption);
        s['Monto a Pagar'] = this.pipeNumber.transform(a.amountPay);
        s['Monto Pendiente'] = this.pipeNumber.transform(a.debt.debtOfPay);
        return s;
      });
      p.push({});
      p.push({});
      p.push({
        Código: 'Factor de Cobro'
      });

      p = p.concat(this.increments.map(a => {
        return {
          Tipo: a.propertyType.code,
          Propiedad: a.propertyType.name,
          'Monto a Pagar': this.pipeNumber.transform(a.increment),
          'Monto Pendiente': (this.form.amountM3 * (a.increment / 100))
        };
      }));
      p.push({});
      p.push({});
      p.push({
        Código: 'Recibos'
      });
      p = p.concat(this.formIncomeReceipts.map(a => {
        return {
          Tipo: a.receiptOther,
          'Monto a Pagar': this.pipeNumber.transform(a.receiptAmount)
        };
      }));
      res.push(p);
    }
    return res;
  }
}
