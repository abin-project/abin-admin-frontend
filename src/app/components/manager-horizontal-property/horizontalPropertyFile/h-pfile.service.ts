import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class HPFileService extends PrincipalService {
  route = 'horizontal-property-file';

  getFiles(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  getOtherFiles(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/others`);
  }

  uploadFile(horizontalPropertyId, file): Observable<ApiResponse> {
    return this.postFile(`${horizontalPropertyId}/uploadFile`, file);
  }

  deleteFile(horizontalPropertyId, fileId) {
    return this.delete(`${horizontalPropertyId}/${fileId}`);
  }

  showFile(horizontalPropertyId, fileName) {
    return `${this.getPartialRoute()}/${horizontalPropertyId}/file/${fileName}`;
  }

  updateFile(horizontalPropertyId, fileId, data): Observable<ApiResponse> {
    return this.put(`${horizontalPropertyId}/update/${fileId}`, data);
  }
}
