import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-file-date-time',
  templateUrl: './file-date-time.component.html',
  styleUrls: ['./file-date-time.component.css']
})
export class FileDateTimeComponent implements OnInit {
  formDate: FormGroup;
  now = new Date();
  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<FileDateTimeComponent>) {
    this.formDate = this.fb.group({
      date: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formDate.valid) {
      this.dialogRef.close({...this.formDate.value});
    }
  }

  cancelDate() {
    this.dialogRef.close();
  }
}
