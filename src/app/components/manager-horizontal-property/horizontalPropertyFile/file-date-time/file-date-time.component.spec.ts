import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileDateTimeComponent } from './file-date-time.component';

describe('FileDateTimeComponent', () => {
  let component: FileDateTimeComponent;
  let fixture: ComponentFixture<FileDateTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileDateTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileDateTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
