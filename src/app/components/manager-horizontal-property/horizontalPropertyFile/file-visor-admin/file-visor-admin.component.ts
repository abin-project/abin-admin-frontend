import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {HorizontalPropertyFile} from '../../../../model/hotizontal-property/horizontalPropertyFile';
import {MatDialog} from '@angular/material/dialog';
import {FileDateTimeComponent} from '../file-date-time/file-date-time.component';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-file-visor-admin',
  templateUrl: './file-visor-admin.component.html',
  styleUrls: ['./file-visor-admin.component.css']
})
export class FileVisorAdminComponent implements OnInit {
  @Input() title;
  @Input() files: HorizontalPropertyFile[] = [];
  @Output() clickDelete = new EventEmitter();
  @Output() clickFile = new EventEmitter();
  @Output() publicChange = new EventEmitter();
  @Output() cancelDate = new EventEmitter();

  constructor(private dialog: MatDialog,
              private rolService: RolService) {
  }

  ngOnInit(): void {
  }

  deleteFile(fileId) {
    this.clickDelete.emit(fileId);
  }

  showFile(name) {
    this.clickFile.emit(name);
  }

  changeState(evt: HorizontalPropertyFile) {
    if (evt.public) {
      this.dialog.open(FileDateTimeComponent, {
        width: '300px',
      }).afterClosed().subscribe(r => {
        if (r !== undefined) {
          evt.publish = r.date;
          this.publicChange.emit(evt);
        } else if (r === undefined){
          this.cancelDate.emit(r);
        }
      });
    } else {
      this.publicChange.emit(evt);
    }
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}
