import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import * as moment from 'moment';
import {HPFileService} from '../h-pfile.service';

@Component({
  selector: 'abin-create-horizontal-property-file',
  templateUrl: './create-horizontal-property-file.component.html',
  styleUrls: ['./create-horizontal-property-file.component.css']
})
export class CreateHorizontalPropertyFileComponent {

  formRender: FormGroup;
  horizontalPropertyId: string;
  groups;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateHorizontalPropertyFileComponent>,
              private hpFileService: HPFileService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.groups = data.groups;
    this.horizontalPropertyId = this.data.horizontalPropertyId;
    this.formRender = this.fb.group({
      group: [null, [Validators.required]],
      file: [null, [Validators.required]],
      send: false,
      publish: null,
      public: false,
    });
  }

  async save() {
    if (this.formRender.valid) {
      const tr = this.formRender.value;
      const formData = new FormData();
      formData.append('group', tr.group);

      formData.append('public', tr.public);
      formData.append('file', tr.file._files[0], tr.file._filesNames);
      if (this.data.send == true) {
        formData.append('send', tr.send);
      }
      if (tr.publish) {
        formData.append('publish', moment(tr.publish).format('YYYY-MM-DD'));
      }
      this.hpFileService.uploadFile(this.horizontalPropertyId, formData).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

}
