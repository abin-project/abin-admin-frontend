import { Component, OnInit } from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import {HPFileService} from '../h-pfile.service';
import {CreateHorizontalPropertyFileComponent} from '../create-horizontal-property-file/create-horizontal-property-file.component';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {FileGroup} from '../horizontal-property-file/horizontal-property-file.component';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-horizontal-property-file-other',
  templateUrl: './horizontal-property-file-other.component.html',
  styleUrls: ['./horizontal-property-file-other.component.css']
})
export class HorizontalPropertyFileOtherComponent implements OnInit {

  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  fileGroups: FileGroup[];


  constructor(public dialog: MatDialog,
              private matSnackBar: MatSnackBar,
              private routerActive: ActivatedRoute,
              private hpFileService: HPFileService,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
  }

  ngOnInit() {
    this.hpFileService.getOtherFiles(this.horizontalPropertyId).subscribe(r => {
      this.fileGroups = r.data;
    });

  }

  openDialog() {
    this.dialog.open(CreateHorizontalPropertyFileComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        groups: ['Comunicados', 'Circulares', 'Convocatorias'],
        send: true
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  delete(fileId) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.hpFileService.deleteFile(this.horizontalPropertyId, fileId).subscribe(r => {
        this.ngOnInit();
      });
    });
  }

  showFile(fileName) {
    window.open(this.hpFileService.showFile(this.horizontalPropertyId, fileName), '_blank');
  }

  updateFile(event) {
    this.hpFileService.updateFile(this.horizontalPropertyId, event.id, {...event}).subscribe(r => {
      // this.ngOnInit();
    }, error => {
      this.ngOnInit();
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}
