import {Component, OnInit} from '@angular/core';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {CreateHorizontalPropertyFileComponent} from '../create-horizontal-property-file/create-horizontal-property-file.component';
import {HPFileService} from '../h-pfile.service';
import {HorizontalPropertyFile} from '../../../../model/hotizontal-property/horizontalPropertyFile';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-horizontal-property-file',
  templateUrl: './horizontal-property-file.component.html',
  styleUrls: ['./horizontal-property-file.component.css']
})
export class HorizontalPropertyFileComponent implements OnInit {

  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty = JSON.parse(localStorage.getItem('p'));
  fileGroups: FileGroup[];

  constructor(public dialog: MatDialog,
              private matSnackBar: MatSnackBar,
              private routerActive: ActivatedRoute,
              private hpFileService: HPFileService,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
  }

  ngOnInit() {
    this.hpFileService.getFiles(this.horizontalPropertyId).subscribe(r => {
      this.fileGroups = r.data;
    });

  }

  openDialog() {
    this.dialog.open(CreateHorizontalPropertyFileComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        groups: ['Estatutos y Reglamentos', 'Resoluciones de Asamblea Ordinaria', 'Resoluciones de Asamblea Extraordinaria']
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  delete(fileId) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.hpFileService.deleteFile(this.horizontalPropertyId, fileId).subscribe(r => {
        this.ngOnInit();
      });
    });
  }

  showFile(fileName) {
    window.open(this.hpFileService.showFile(this.horizontalPropertyId, fileName), '_blank');
  }

  updateFile(event) {
    if (event) {
      this.hpFileService.updateFile(this.horizontalPropertyId, event.id, {...event}).subscribe(r => {
        this.ngOnInit();
      }, error => {
        this.ngOnInit();
      });
    } else {
      this.ngOnInit();
    }
  }

  cancelDate() {
    this.ngOnInit();
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

}

export interface FileGroup {
  title: string;
  files: HorizontalPropertyFile[];
}
