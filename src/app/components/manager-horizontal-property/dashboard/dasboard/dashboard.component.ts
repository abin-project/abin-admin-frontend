import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {HorizontalProperty} from 'src/app/model/hotizontal-property/horizontalProperty';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {ActivatedRoute} from '@angular/router';
import {PersonalService} from '../../personal/personal.service';
import {Personal} from 'src/app/model/hotizontal-property/personal';
import {User} from '../../../../model/User';

@Component({
  selector: 'abin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {

  property: HorizontalProperty;
  horizontalPropertyId;
  personals: Personal[] = [];

  // = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  constructor(private horizontalPropertyService: HorizontalPropertyService,
              private personalService: PersonalService,
              private routerActive: ActivatedRoute,
              private changeDetector: ChangeDetectorRef) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
  }

  ngOnInit(): void {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(r => {
      this.property = r.data;
      this.property.horizontalPropertiesUsers = this.mapUrl(r.data.horizontalPropertiesUsers.map(u => u.user));
      this.changeDetector.detectChanges();
    });
    this.personalService.getAllPersonals(this.horizontalPropertyId, true).subscribe(r => {
      this.personals = this.mapUrl(r.data);
      this.changeDetector.detectChanges();
    });
  }

  mapUrl(p: Personal[]|User[]|any) {
    return p.map(r => {
      return {
        ...r,
        picture: r?.picture ? this.personalService.getUrlFileS(this.horizontalPropertyId, r?.picture) : null,
        pictureCI: r?.pictureCI ? this.personalService.getUrlFileS(this.horizontalPropertyId, r?.pictureCI) : null,
        cv: r?.cv ? this.personalService.getUrlFileS(this.horizontalPropertyId, r?.cv) : null,
        fileBackground: r?.fileBackground ? this.personalService.getUrlFileS(this.horizontalPropertyId, r?.fileBackground) : null,
        position: r?.position ? r?.position : 'ADMINISTRADOR',
        name: r?.name ? r?.name : r?.username,
        username: r?.username ? r?.username : null,
      };
    });
  }

  getUrl(url) {
    if (url) {
      return null;
    }
    return this.personalService.getUrlFileS(this.horizontalPropertyId, url);
  }
}
