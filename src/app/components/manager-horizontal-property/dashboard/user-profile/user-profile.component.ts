import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../model/User';
import {Personal} from '../../../../model/hotizontal-property/personal';

@Component({
  selector: 'abin-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent {
  @Input() user: User|Personal|any;
  constructor() { }
}
