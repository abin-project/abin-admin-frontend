import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PropertyGroup} from '../../../../model/hotizontal-property/propertyGroup';
import {Property} from '../../../../model/hotizontal-property/property';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../property/property.service';
import {PropertyGroupService} from '../../settings/property-group.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'abin-update-property-complement',
  templateUrl: './update-property-complement.component.html',
  styleUrls: ['./update-property-complement.component.css']
})
export class UpdatePropertyComplementComponent implements OnInit {

  formProperty: FormGroup;
  propertyGroups: PropertyGroup[] = [];
  properties: Property[] = [];
  propertySelect: Property;
  filteredOptions: Observable<Property[]>;

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<UpdatePropertyComplementComponent>,
              protected propertyService: PropertyService,
              protected propertyGroupService: PropertyGroupService,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    this.formProperty = this.fb.group(
      {
        code: null,
        name: [null, [Validators.required, Validators.minLength(1)]],
        m2: [null, [Validators.required, Validators.min(1)]],
        type: [{value: null, disabled: true}, [Validators.required]],
        property: null,
        // expense: [null, [Validators.required, Validators.min(0)]],
        dateInitPay: [null],
        group: null
      }
    );


  }

  ngOnInit() {
    this.propertyGroupService.getGroups(this.data.horizontalPropertyId).subscribe(r => {
      this.propertyGroups = r.data;
    });
    this.propertyService.getPropertiesOnHorizontalProperty(this.data.horizontalPropertyId).subscribe(r => {
      this.properties = r.data;
    });
    this.propertyService.getProperty(this.data.horizontalPropertyId, this.data.propertyId).subscribe((r: ApiResponse) => {
      this.propertySelect = r.data as Property;
      this.formProperty.get('code').setValue(this.propertySelect.code);
      this.formProperty.get('name').setValue(this.propertySelect.name);
      this.formProperty.get('m2').setValue(this.propertySelect.m2);
      this.formProperty.get('type').setValue(this.propertySelect.type);
      this.formProperty.get('group').setValue(this.propertySelect.group ? this.propertySelect.group.id : null);
      this.formProperty.get('property').setValue(this.propertySelect.property ? this.propertySelect.property.code : null);
      this.formProperty.get('dateInitPay').setValue(this.propertySelect.dateInitPay ? this.propertySelect.dateInitPay : null);
      // this._processItems(this.propertySelect.typeIncomeProperty);
    });

    this.filteredOptions = this.formProperty.get('property').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    // this.formProperty.get('property').valueChanges.subscribe(r => {
    //   this.formProperty.get('code').setValue(null);
    // })
  }

  cancel() {
    this.dialogRef.close(null);
  }

  saveProperty() {
    const property: Property = this.formProperty.value;
    property.typeIncomeProperty = this.propertySelect.typeIncomeProperty;
    if (property.dateInitPay) {
      // @ts-ignore
      property.dateInitPay = moment(property.dateInitPay).format('YYYY-MM-DD');
    }

    this.propertyService.updateProperty(this.data.horizontalPropertyId, this.propertySelect.id, property).subscribe(r => {
      this.dialogRef.close(r);
    });
  }

  private _filter(value: string): Property[] {
    try {
      const filterValue = value.toLowerCase();

      return this.properties.filter(option => option.code.toLowerCase().includes(filterValue));
    } catch (e) {
      return [];
    }
  }
}
