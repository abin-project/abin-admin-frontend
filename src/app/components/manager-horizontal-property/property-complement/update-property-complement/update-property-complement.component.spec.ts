import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdatePropertyComplementComponent} from './update-property-complement.component';

describe('UpdatePropertyComplementComponent', () => {
  let component: UpdatePropertyComplementComponent;
  let fixture: ComponentFixture<UpdatePropertyComplementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdatePropertyComplementComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePropertyComplementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
