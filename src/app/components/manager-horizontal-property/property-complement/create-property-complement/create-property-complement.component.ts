import {Component, Inject} from '@angular/core';
import {_filter, CreatePropertyComponent} from '../../property/create-property/create-property.component';
import {TenantService} from '../../tenant/tenant.service';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OwnerService} from '../../owner/owner.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {Tenant} from '../../../../model/hotizontal-property/tenant';
import {Observable} from 'rxjs';
import {PropertyService} from '../../property/property.service';
import {ItemService} from '../../item/item.service';
import {Property} from '../../../../model/hotizontal-property/property';
import {PropertyGroupService} from '../../settings/property-group.service';
import {PropertyTypeService} from '../../../admin-horizontal-property/property-type.service';

@Component({
  selector: 'abin-create-property-complement',
  templateUrl: './create-property-complement.component.html',
  styleUrls: ['./create-property-complement.component.css']
})

export class CreatePropertyComplementComponent extends CreatePropertyComponent {


  tenants: Tenant[];
  groupsTenant: Observable<GroupTenant[]>;
  private _groupsTenants: GroupTenant[];

  properties: Property[];
  groupsProperty: Observable<GroupProperty[]>;
  private _groupsProperty: GroupProperty[];

  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<CreatePropertyComponent>,
              protected ownerService: OwnerService,
              protected propertyService: PropertyService,
              protected itemService: ItemService,
              protected propertyGroupService: PropertyGroupService,
              protected propertyTypeService: PropertyTypeService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private tenantService: TenantService) {
    super(fb, dialogRef, ownerService, propertyService, itemService, propertyGroupService, propertyTypeService, data);
    this.formProperty = this.fb.group(
      {
        code: null,
        name: [null, [Validators.required, Validators.minLength(1)]],
        m2: [null, [Validators.required, Validators.min(1)]],
        type: [null, [Validators.required]],
        property: null,
        expense: [null, [Validators.required, Validators.min(0)]],
        sure: [null, [Validators.required, Validators.min(0)]],
        group: null,
        dateInitPay: [null]
      }
    );
    // this.formCoOwner = this.fb.group(
    //   {
    //     nameOwner: [null, [Validators.required, Validators.minLength(1)]],
    //     telephoneOwner: [null, [Validators.required]],
    //     emailOwner: [null, [Validators.required, Validators.email]],
    //     check_tenant: [false, Validators.required]
    //   }
    // , {updateOn: 'change'});
  }

  ngOnInit() {
    this.ownerService.getOnlyOwner(this.data.id).subscribe(async (r: ApiResponse) => {
      this.owners = r.data;
      this._groupsOwner = await this.processOwner();
    });
    this.tenantService.getOnlyTenants(this.data.id).subscribe(async (t: ApiResponse) => {
      this.tenants = t.data;
      this._groupsTenants = await this.processTenant();
    });
    this.propertyService.getPropertiesOnHorizontalProperty(this.data.id).subscribe(async (t: ApiResponse) => {
      this.properties = t.data;
      this._groupsProperty = await this.processProperties();
    });
    this.propertyTypeService.getPropertiesByHorizontalProp(this.data.id, true).subscribe(r => {
      this.propertyTypes = r.data;
    });
    this.propertyGroupService.getGroups(this.data.id).subscribe(r => {
      this.propertyGroups = r.data;
    });

    // this.formTenant.get('nameTenant').valueChanges.subscribe(r => {
    //
    // });

    this.groupsOwner = this.formCoOwner.get('nameOwner')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
    this.groupsTenant = this.formTenant.get('nameTenant')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroupTenant(value))
      );

    this.groupsProperty = this.formProperty.get('property')!.valueChanges
      .pipe(
        startWith(null),
        map(value => this._filterGroupProperty(value))
      );

    // this.formProperty.get('property').valueChanges.subscri
  }

  async processTenant() {
    let firstLetter = 65;
    const res: any[] = [];

    res.push({
      letter: '#',
      tenants: await this.tenants.filter(tenant => {
        return !isNaN(Number(tenant.nameTenant.charAt(0)));
      })
    });

    while (firstLetter <= 90) {
      const l = String.fromCharCode(firstLetter);
      const ow = await this.tenants.filter(tenant => {
        return String.fromCharCode(firstLetter) === tenant.nameTenant.charAt(0).toUpperCase();
      });
      if (ow.length > 0) {
        await res.push({
          letter: l,
          tenants: ow
        });
      }
      firstLetter++;
    }
    return res;
  }

  async processProperties() {
    const res = [];
    let aux = null;
    if ((aux = (this.properties.filter(r => r.type === 'Departamento'))).length > 0) {
      res.push({
        letter: 'Departamentos',
        properties: aux
      });
    }

    if ((aux = (this.properties.filter(r => r.type === 'Local Comercial'))).length > 0) {
      res.push({
        letter: 'Locales Comerciales',
        properties: aux
      });
    }

    if ((aux = (this.properties.filter(r => r.type === 'Oficina'))).length > 0) {
      res.push({
        letter: 'Oficinas',
        properties: aux
      });
    }
    if ((aux = (this.properties.filter(r => r.type === 'Casa'))).length > 0) {
      res.push({
        letter: 'Casas',
        properties: aux
      });
    }

    return res;
  }

  setDataTenant(owner: Tenant) {
    this.formTenant.get('nameTenant').setValue(owner.nameTenant);
    this.formTenant.get('telephoneTenant').setValue(owner.telephoneTenant);
    this.formTenant.get('cellTenant').setValue(owner.cellTenant);
    this.formTenant.get('emailTenant').setValue(owner.emailTenant);
  }

  setDataOwnerProperty(property: Property) {
    const res = property.owner;
    this.formProperty.get('code').setValue(null);
    this.setDataOwner(res);
  }

  protected _filterGroupTenant(value: string): any {
    if (value) {
      return this._groupsOwner
        .map(group => ({letter: group.letter, owners: _filter(group.owners, value)}))
        .filter(group => group.owners.length > 0);
    }

    return this._groupsTenants;
  }

  private _filterGroupProperty(value: string): any {
    if (value) {
      return this._groupsProperty
        .map(group => ({letter: group.letter, properties: _filterProperty(group.properties, value)}))
        .filter(group => group.properties.length > 0);
    }

    return this._groupsProperty;
  }
}

export interface GroupTenant {
  letter: string;
  tenants: Tenant[];
}

export interface GroupProperty {
  letter: string;
  properties: Property[];
}

export const _filterProperty = (opt: Property[], value: string): Property[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.code.toLowerCase().indexOf(filterValue) === 0);
};
