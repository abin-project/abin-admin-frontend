import {Component} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Property} from '../../../../model/hotizontal-property/property';
import {ApiResponse} from '../../../../model/apiResponse';
import {ListPropertyComponent} from '../../property/list-property/list-property.component';
import {CreatePropertyComplementComponent} from '../create-property-complement/create-property-complement.component';
import {UpdatePropertyComplementComponent} from '../update-property-complement/update-property-complement.component';
import {increment, toDecimal, toUpperCase} from '../../../../services/globalFunctions';
import {FormOwnerComponent} from '../../property/form-owner/form-owner.component';
import {FormTenantComponent} from '../../property/form-tenant/form-tenant.component';
import {HistoryPropertyComponent} from '../../property/history-property/history-property.component';

@Component({
  selector: 'abin-list-property-complement',
  templateUrl: './list-property-complement.component.html',
  styleUrls: ['../../property/list-property/list-property.component.css']
})
export class ListPropertyComplementComponent extends ListPropertyComponent {

  title = 'Propiedades Complementarias';
  displayedColumns: string[] = [
    'pos',
    'code',
    'name',
    'type',
    'm2',
    // 'expense',
    'owner',
    'tenant',
    'option'
  ];

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    this.propertyService.getPropertiesComplementsOnHorizontalProperty(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<any>(res.data.raw);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe((r: any) => {
      this.horizontalProperty = r.data;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreatePropertyComplementComponent, {
      width: '500px',
      data: this.horizontalProperty,
    });

    dialogRef.afterClosed().subscribe((result: Property) => {
      if (result) {
        // this.propertyService.createProperty(this.horizontalPropertyId, result).subscribe(r => {
        this.ngOnInit();
        // });
      }
    });
  }

  updateProperty(property: any) {
    this.dialog.open(UpdatePropertyComplementComponent, {
      width: '500px',
      data: {
        propertyId: property.property_id,
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  updateOwner(owner: any) {
    this.dialog.open(FormOwnerComponent, {
      width: '500px',
      data: {
        owner: {
          id: owner.owner_id,
          nameOwner: owner.owner_nameOwner,
          telephoneOwner: owner.owner_telephoneOwner,
          cellOwner: owner.owner_cellOwner,
          emailOwner: owner.owner_emailOwner,
          propertyId: owner.property_id
        },
        horizontalProperty: this.horizontalProperty
      }
    }).afterClosed().subscribe(c => {
      if (c) {
        this.ngOnInit();
      }
    });
  }

  updateTenant(property: any) {
    this.dialog.open(FormTenantComponent, {
      width: '500px',
      data: {
        tenant: {
          id: property.tenant_id,
          nameTenant: property.tenant_nameTenant,
          telephoneTenant: property.tenant_telephoneTenant,
          emailTenant: property.tenant_emailTenant,
          cellTenant: property.tenant_cellTenant,
          propertyId: property.property_id
        },
        horizontalProperty: this.horizontalProperty
      }
    }).afterClosed().subscribe(c => {
      if (c) {
        this.ngOnInit();
      }
    });
  }

  showHistory(property: any) {
    this.dialog.open(HistoryPropertyComponent, {
      width: '700px',
      data: {
        property: {id: property.property_id},
        horizontalPropertyId: this.horizontalPropertyId,
      }
    });
  }

  async reportProperties() {
    const headers: string[] = [
      '#',
      'Código',
      'Nombre',
      'tipo',
      'Sup. m2',
      // 'Expensa',
      'Propietario',
      'Inquilino',
    ];

    const body: any[][] = await this.processBody();
    const colOption = {
      0: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };

    await this.printService
      .createSingleTable(this.horizontalProperty.nameProperty , `Lista de Información de ${this.title}`, headers, body, colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(this.dataSource.data[i].property_code),
        toUpperCase(this.dataSource.data[i].property_name),
        toUpperCase(this.dataSource.data[i].property_type),
        toDecimal(this.dataSource.data[i].property_m2),
        // toDecimal(this.getExpense(this.dataSource.data[i])),
        toUpperCase(this.dataSource.data[i].owner_nameOwner),
        toUpperCase(this.dataSource.data[i].tenant_nameTenant)
      ]);
    }
    return res;
  }

}
