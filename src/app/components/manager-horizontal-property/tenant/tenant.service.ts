import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class TenantService extends PrincipalService {
  route = 'tenant';

  getTenants(id) {
    return this.getCustom(`${id}`);
  }

  getOnlyTenants(id) {
    return this.get(`${id}/only`);
  }
}
