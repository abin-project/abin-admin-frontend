import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {Tenant} from '../../../../model/hotizontal-property/tenant';
import {TenantService} from '../tenant.service';
import {compare, increment, toUpperCase} from '../../../../services/globalFunctions';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-list-tenant',
  templateUrl: './list-tenant.component.html',
  styleUrls: ['./list-tenant.component.css']
})
export class ListTenantComponent implements OnInit {

  displayedColumns: string[] = [
    'pos',
    'nameTenant',
    'emailTenant',
    'telephoneTenant',
    'cellTenant',
    'state',
    // 'option'
  ];
  tenant = 'Inquilinos';
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  dataSource: MatTableDataSource<Tenant>;
  sortedData: Tenant[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private tenantService: TenantService,
              private dataGlobalService: DataGlobalService,
              private routerActive: ActivatedRoute,
              private printService: PrintService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.tenantService.getTenants(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<Tenant>(res.data);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
  }

  indexPosData(i) {
    return i + 1;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameTenant':
          return compare(a.nameTenant, b.nameTenant, isAsc);
        case 'emailTenant':
          return compare(a.emailTenant, b.emailTenant, isAsc);
        case 'telephoneTenant':
          return compare(a.telephoneTenant, b.telephoneTenant, isAsc);
        case 'cellTenant':
          return compare(a.cellTenant, b.cellTenant, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Tenant>(this.sortedData);
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  show(ev) {
    console.log(ev);
  }

  delete(id) {
    console.log(id);
  }

  async report() {
    const headers: string[] = [
      '#',
      'Nombre y Apellido',
      'Correo',
      'No. de Telefono',
      'No. Celular',
      'Estado',
    ];
    const body: any[][] = await this.processBody();
    const colOption = {
      0: {
        halign: 'right',
      },
      3: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };

    await this.printService
      .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de ${this.tenant}`, headers, body, colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(this.dataSource.data[i].nameTenant),
        this.dataSource.data[i].emailTenant,
        this.dataSource.data[i].telephoneTenant,
        this.dataSource.data[i].cellTenant,
        toUpperCase(this.dataSource.data[i].state ? 'Activo' : 'No Activo'),
      ]);
    }
    return res;
  }
}
