import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {RecreationAreaService} from '../recreation-area.service';
import {IntervalsAmountComponent} from '../intervals-amount/intervals-amount.component';
import {RecreationAreaDay} from '../../../../model/hotizontal-property/recreationAreaDay';
import {RecreationAreaAmount} from '../../../../model/hotizontal-property/recreationAreaAmount';
import {FileInput} from 'ngx-material-file-input';

@Component({
  selector: 'abin-create-recreation-area',
  templateUrl: './create-recreation-area.component.html',
  styleUrls: ['./create-recreation-area.component.css']
})
export class CreateRecreationAreaComponent implements OnInit {
  recreationForm: FormGroup;
  intervals: RecreationAreaAmount[] = [];
  days: RecreationAreaDay[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CreateRecreationAreaComponent>,
              private dialog: MatDialog,
              private fb: FormBuilder,
              private recreationAreaService: RecreationAreaService) {
    this.days = this.recreationAreaService.getRecreationAreaDayDefault();
    this.recreationForm = this.fb.group({
      name: [this.data.name ? this.data.name : null, Validators.required],
      amount: [this.data.amount ? this.data.amount : null, Validators.required],
      typeReservation: [this.data.typeReservation ? this.data.typeReservation : false, Validators.required],
      isInvited: [this.data.isInvited ? this.data.isInvited : false, Validators.required],
      usersLimit: [this.data.usersLimit ? this.data.usersLimit : null, Validators.required],
      isTimeUseLimit: [this.data.isTimeUseLimit ? this.data.isTimeUseLimit : false, Validators.required],
      timeUseLimit: [this.data.timeUseLimit ? this.data.timeUseLimit : 1, Validators.required],
      amountGuarantee: [this.data.amountGuarantee ? this.data.amountGuarantee : null, Validators.required],
      generateReceipt: [this.data.generateReceipt ? this.data.generateReceipt : false, Validators.required],
      file: this.data.file ? this.data.file : null,
    });

    if (this.recreationForm.get('isInvited').value === true) {
      this.changeStateOptional(false);
    }

    if (this.data.recreationAreaDays) {
      this.days = this.data.recreationAreaDays;
    }
    if (this.data.recreationAreaAmounts) {
      this.intervals = this.data.recreationAreaAmounts;
    }
  }
  ngOnInit(): void {
    this.recreationForm.get('isInvited').valueChanges.subscribe(r => {
      this.changeStateOptional(!r);
    });
    this.recreationForm.get('isTimeUseLimit').valueChanges.subscribe(r => {
      this.changeStateUseTime(r);
    });
  }

  save() {
    if (this.recreationForm.valid) {
      const recreation = this.recreationForm.value;
      recreation.recreationAreaDays = this.days;
      recreation.recreationAreaAmounts =
        this.intervals.filter(f => f.guarantee >= 0 && f.peoplesIni > 0 && f.peoplesEnd > 0 && f.amount >= 0);


      const formData = new FormData();
      formData.append('recreation', JSON.stringify(recreation));
      if (recreation.file) {
        if ( recreation.file instanceof FileInput) {
          formData.append('file', recreation.file._files[0], recreation.file._filesNames);
        }
      }

      if (!this.data.id) {
        this.recreationAreaService.createRecreation(this.data.horizontalPropertyId, formData).subscribe(() => {
          this.dialogRef.close(recreation);
        });
      } else {
        this.recreationAreaService.updateRecreation(this.data.horizontalPropertyId, this.data.id, formData).subscribe(() => {
          this.dialogRef.close(recreation);
        });
      }
    }
  }

  openAmountDialog() {
    this.dialog.open(IntervalsAmountComponent, {
      width: '350px',
      data: this.recreationForm.get('recreationAreaAmounts').value ? this.recreationForm.get('recreationAreaAmounts').value : []
    }).afterClosed().subscribe(r => {
      if (r) {
        this.recreationForm.get('recreationAreaAmounts').setValue(r);
      }
    });
  }

  addInterval() {
    this.intervals.push({
      id: null,
      amount: null,
      peoplesEnd: null,
      peoplesIni: null,
      recreationArea: null,
      guarantee: null,
    });
  }

  deleteInterval(pos) {
    this.intervals.splice(pos, 1);
  }

  cancel() {
    this.dialogRef.close();
  }

  changeStateOptional(state: boolean) {
    this.recreationForm.get('amount').setValue(null);
    this.recreationForm.get('amountGuarantee').setValue(null);
    if (state == true) {
      this.recreationForm.get('amountGuarantee').enable();
      this.recreationForm.get('amount').enable();
    } else {
      this.recreationForm.get('amountGuarantee').disable();
      this.recreationForm.get('amount').disable();
    }
  }

  changeStateUseTime(state) {
    this.recreationForm.get('timeUseLimit').setValue(null);
    if (state == true) {
      this.recreationForm.get('timeUseLimit').enable();
    } else {
      this.recreationForm.get('timeUseLimit').disable();
    }
  }

  intervalValid() {
    return this.intervals.find(f => f.amount > 0 && f.peoplesEnd > 0 && f.peoplesIni > 0 && f.guarantee >= 0) != undefined;
  }

  daysValid() {
    return this.days.find(f => f.available == true && f.timeEnd != null && f.timeIni != null) != undefined;
  }

  fileDisplay() {
    return typeof this.recreationForm.get('file').value === 'string';
  }

}
