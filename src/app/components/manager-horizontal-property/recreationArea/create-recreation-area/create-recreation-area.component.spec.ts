import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateRecreationAreaComponent} from './create-recreation-area.component';

describe('CreateRecreationAreaComponent', () => {
  let component: CreateRecreationAreaComponent;
  let fixture: ComponentFixture<CreateRecreationAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateRecreationAreaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRecreationAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
