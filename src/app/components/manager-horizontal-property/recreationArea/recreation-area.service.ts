import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';
import {RecreationAreaDay} from '../../../model/hotizontal-property/recreationAreaDay';

@Injectable({
  providedIn: 'root'
})
export class RecreationAreaService extends PrincipalService {

  DAYS: RecreationAreaDay[] = [
    {
      available: false,
      day: 'Lunes',
      dayNumber: 1,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Martes',
      dayNumber: 2,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Miercoles',
      dayNumber: 3,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Jueves',
      dayNumber: 4,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Viernes',
      dayNumber: 5,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Sabado',
      dayNumber: 6,
      timeIni: null,
      timeEnd: null
    },
    {
      available: false,
      day: 'Domingo',
      dayNumber: 7,
      timeIni: null,
      timeEnd: null
    },
  ];
  route = 'recreation-area';

  getAll(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  createRecreation(horizontalPropertyId, value: any): Observable<ApiResponse> {
    return this.postFile(`${horizontalPropertyId}/create`, value);
  }

  updateRecreation(horizontalPropertyId: any, id: any, value: any) {
    return this.putFile(`${horizontalPropertyId}/update/${id}`, value);
  }

  deleteReservation(horizontalPropertyId: any, id: string) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }

  getRecreationAreaDayDefault() {
    return this.DAYS.slice();
  }
}
