import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntervalsAmountComponent} from './intervals-amount.component';

describe('IntervalsAmountComponent', () => {
  let component: IntervalsAmountComponent;
  let fixture: ComponentFixture<IntervalsAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntervalsAmountComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntervalsAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
