import {Component, Inject, OnInit} from '@angular/core';
import {RecreationAreaAmount} from '../../../../model/hotizontal-property/recreationAreaAmount';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-intervals-amount',
  templateUrl: './intervals-amount.component.html',
  styleUrls: ['./intervals-amount.component.css']
})
export class IntervalsAmountComponent implements OnInit {
  intervals: RecreationAreaAmount[] = [];

  constructor(private dialogRef: MatDialogRef<RecreationAreaAmount>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.intervals = data;
    }
  }

  ngOnInit(): void {
  }

  addInterval() {
    this.intervals.push({
      id: null,
      amount: null,
      peoplesEnd: null,
      peoplesIni: null,
      recreationArea: null,
      guarantee: null
    });
  }

  deleteInterval(pos) {
    this.intervals.splice(pos, 1);
  }

  save() {
    this.dialogRef.close(this.intervals);
  }

  cancel() {
    this.dialogRef.close();
  }
}
