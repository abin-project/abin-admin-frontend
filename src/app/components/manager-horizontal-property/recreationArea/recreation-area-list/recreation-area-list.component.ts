import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {PrintService} from '../../../../services/print.service';
import {compare, compareDate} from '../../../../services/globalFunctions';
import {CreatePositionOwnerComponent} from '../../directory/create-position-owner/create-position-owner.component';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {RecreationAreaService} from '../recreation-area.service';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {RecreationAreaReservation} from '../../../../model/hotizontal-property/recreationAreaReservation';
import {CalendarEventShowComponent} from '../../calendar/calendar-event-show/calendar-event-show.component';
import {CreateRecreationAreaComponent} from '../create-recreation-area/create-recreation-area.component';
import {RolService} from '../../../admin-horizontal-property/rol.service';

@Component({
  selector: 'abin-recreation-area-list',
  templateUrl: './recreation-area-list.component.html',
  styleUrls: ['./recreation-area-list.component.css']
})
export class RecreationAreaListComponent implements OnInit {
  displayedColumns: string[] = [
    'pos',
    'name',
    // 'owner',
    // 'period',
    // 'option'
  ];
  title = 'Areas de Recreacion';
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<RecreationArea>;
  sortedData: RecreationArea[];
  horizontalProperty: HorizontalProperty;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private recreationAreaService: RecreationAreaService,
              private dataGlobalService: DataGlobalService,
              private snack: MatSnackBar,
              private router: Router,
              private routerActive: ActivatedRoute,
              private printService: PrintService,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.loadData();
  }

  indexPosData(i) {
    return i + 1;
  }

  loadData() {
    this.recreationAreaService.getAll(this.horizontalPropertyId).subscribe(r => {
      this.dataSource = new MatTableDataSource<RecreationArea>(r.data);
      if (this.readNotOnly()) {
        if (!(this.displayedColumns.find(a => a === 'option'))) {
          this.displayedColumns.push('option');
        }
      }
    });
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return compare(a.name, b.name, isAsc);
        case 'unity':
          return compare(a.unity, b.unity, isAsc);
        case 'period':
          return compareDate(a.timeIni, b.timeIni, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<RecreationArea>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(CreatePositionOwnerComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.loadData();
      }
    });
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  delete(event: RecreationAreaReservation) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
    }).onAction().subscribe(() => {
      this.recreationAreaService.deleteReservation(this.horizontalPropertyId, event.id).subscribe(() => {
        this.loadData();
      });
      // this.reservationService.deleteReservation(this.horizontalPropertyId, event.id).subscribe(() => {
      //   this.loadData();
      // });

    });

  }

  show(event) {
    this.dialog.open(CalendarEventShowComponent, {
      width: '354px',
      data: {
        event
      }
    });
  }

  createRecreation() {
    this.dialog.open(CreateRecreationAreaComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  edit(event) {
    this.dialog.open(CreateRecreationAreaComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        ...event
      }
    }).afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  // async report() {
  //   const headers: string[] = [
  //     '#',
  //     'Cartera / Cargo',
  //     'Co-Propietario',
  //     'Periodo',
  //   ];
  //   const body: any[][] = await this.processBody();
  //   const colOption = {
  //     0: {
  //       halign: 'right',
  //     }
  //   };
  //
  //   await this.printService
  //     .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de ${this.title}`, headers, body, colOption);
  // }
  //
  // async processBody() {
  //   const res = [];
  //   const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
  //   for (const i in data) {
  //     res.push([
  //       increment(i),
  //       toUpperCase(this.dataSource.data[i].name),
  //       toUpperCase(this.dataSource.data[i].owner.nameOwner),
  //       `${this.dataSource.data[i].dateIni} - ${this.dataSource.data[i].dateEnd}`
  //     ]);
  //   }
  //   return res;
  // }
}
