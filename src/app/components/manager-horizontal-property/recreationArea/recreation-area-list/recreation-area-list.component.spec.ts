import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecreationAreaListComponent } from './recreation-area-list.component';

describe('RecreationAreaListComponent', () => {
  let component: RecreationAreaListComponent;
  let fixture: ComponentFixture<RecreationAreaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecreationAreaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecreationAreaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
