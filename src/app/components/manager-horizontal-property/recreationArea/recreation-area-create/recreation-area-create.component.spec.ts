import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecreationAreaCreateComponent } from './recreation-area-create.component';

describe('RecreationAreaCreateComponent', () => {
  let component: RecreationAreaCreateComponent;
  let fixture: ComponentFixture<RecreationAreaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecreationAreaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecreationAreaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
