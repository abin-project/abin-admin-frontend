import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[abinErrorImg]'
})
export class ErrorImgDirective {

  constructor(private elementRef: ElementRef) { }

  @HostListener('error')
  onError() {
    this.elementRef.nativeElement.src = '/assets/LOGO_cube.svg';
  }

}
