import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ViewIncomeExtract} from '../../../../model/hotizontal-property/viewIncomeExtract';
import {TableVirtualScrollDataSource} from 'ng-table-virtual-scroll';
import {SelectionModel} from '@angular/cdk/collections';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {MatSort, Sort} from '@angular/material/sort';
import {TransactionService} from '../../banks/transaction.service';
import {DataGlobalService} from '../../../../services/data-global.service';
import {BankService} from '../../banks/bank.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {PrintService} from '../../../../services/print.service';
import {StateIncomePipe} from '../../../../pipes/state-income.pipe';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {compare, compareDate, increment, toDecimal, toUpperCase} from '../../../../services/globalFunctions';
import {TransactionsIncomeFilterComponent} from '../../banks/transactions-income-filter/transactions-income-filter.component';
import * as moment from 'moment';
import {Transaction} from '../../../../model/hotizontal-property/transaction';

@Component({
  selector: 'abin-owner-transactions',
  templateUrl: './owner-transactions.component.html',
  styleUrls: ['./owner-transactions.component.css']
})
export class OwnerTransactionsComponent implements OnInit {

  params = {};
  horizontalPropertyId;
  dataSource: MatTableDataSource<Transaction> = new TableVirtualScrollDataSource([]);
  sortedData: Transaction[];
  displayedColumns: string[] = [
    'pos',
    'incomeExtract_date',
    'incomeExtract_accountBank',
    'incomeExtract_accountBank_number',
    'incomeExtract_reference',
    'incomeExtract_transactionCode',
    'incomeExtract_amount',
    'options'
  ];
  rowVisible = 0;
  horizontalProperty: HorizontalProperty;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private transactionService: TransactionService,
              private dataGlobalService: DataGlobalService,
              private bankService: BankService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private router: Router,
              private printService: PrintService,
              private pipeState: StateIncomePipe,
              private routerActive: ActivatedRoute,
              private horizontalPropertyService: HorizontalPropertyService,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });

  }

  async ngOnInit() {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(r => {
      this.horizontalProperty = r.data;
    });
    await this.loadData();
  }

  indexPosData(i) {
    return i + 1;
  }

  getIndex(evt) {
    this.rowVisible = evt;
  }

  async loadData() {

    this.transactionService.transactionByOwnerId(
      this.horizontalPropertyId, this.params).subscribe(async (res: ApiResponse) => {
      this.dataSource = new TableVirtualScrollDataSource<Transaction>(res.data);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'incomeExtract_date':
          return compareDate(a.date, b.date, isAsc);
        case 'incomeExtract_accountBank':
          return compare(a.accountBank.nameBank, b.accountBank.nameBank, isAsc);
        case 'incomeExtract_accountBank_number':
          return compare(a.accountBank.numberAccount, b.accountBank.numberAccount, isAsc);
        case 'incomeExtract_reference':
          return compare(a.reference, b.reference, isAsc);
        case 'incomeExtract_transactionCode':
          return compare(a.transactionCode, b.transactionCode, isAsc);
        case 'incomeExtract_amount':
          return compare(a.amount, b.amount, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Transaction>(this.sortedData);
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Co-Propietario' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }



  printVoucher(transaction: Transaction) {

    this.transactionService.transactionDetails(this.horizontalPropertyId, transaction.id).subscribe(async r => {
      await this.printService.printVoucher([this.horizontalProperty.nameProperty, 'Recibo de Pago'],
        r.headers,
        r.debts, r.property, r.data, r.bank);
    });
  }

  async print() {
    const headers = ['#', 'Fecha', 'Cuenta', 'N° Cuenta', 'Referencia', 'Código T.', 'Monto'];
    const colOption = {
      0: {
        halign: 'right',
      },
      6: {
        halign: 'right',
      },
      7: {
        halign: 'right',
      },
      8: {
        halign: 'right',
      },
    };
    const data = await this.processBody();
    await this.printService.createSingleTable(
      this.horizontalProperty.nameProperty,
      `Mis Depósitos`,
      headers,
      data,
      colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    // tslint:disable-next-line:forin
    for (const i in data) {
      const d = [
        increment(i),
        toUpperCase(moment(data[i].date).format('DD-MM-YYYY')),
        toUpperCase(data[i].accountBank.nameBank),
        toUpperCase(data[i].accountBank.numberAccount),
        toUpperCase(data[i].reference),
        toUpperCase(data[i].transactionCode),
        toDecimal(data[i].amount)
      ];
      res.push(d);
    }
    return res;
  }

}
