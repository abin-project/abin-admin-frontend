import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerTransactionsComponent } from './owner-transactions.component';

describe('OwnerTransactionsComponent', () => {
  let component: OwnerTransactionsComponent;
  let fixture: ComponentFixture<OwnerTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
