import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';
import {CalendarActivity} from '../../../model/hotizontal-property/calendarActivity';

@Injectable({
  providedIn: 'root'
})
export class CalendarActivityService extends PrincipalService {
  route = 'calendar-activity';

  getAllActivities(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}`);
  }

  createActivity(horizontalPropertyId, data): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}`, data);
  }

  getActivityPeriod(horizontalPropertyId, actualMonth) {
    return this.get(`${horizontalPropertyId}/${actualMonth}`);
  }

  deleteActivity(horizontalPropertyId, id: string) {
    return this.delete(`${horizontalPropertyId}/${id}`);
  }

  updateActivity(horizontalPropertyId, id: string, data: CalendarActivity) {
    return this.put(`${horizontalPropertyId}/${id}`, data);
  }
}
