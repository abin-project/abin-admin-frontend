import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CalendarActivity} from '../../../../model/hotizontal-property/calendarActivity';
import {CalendarActivityService} from '../calendar-activity.service';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'abin-calendar-activity-detail',
  templateUrl: './calendar-activity-detail.component.html',
  styleUrls: ['./calendar-activity-detail.component.css']
})
export class CalendarActivityDetailComponent implements OnInit {
  eventSelect: CalendarActivity;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private snack: MatSnackBar,
              private dialogReg: MatDialogRef<CalendarActivityDetailComponent>,
              private calendarActivityService: CalendarActivityService) {
    this.eventSelect = {...data.event};
  }

  ngOnInit(): void {
  }

  deleteEvent() {
    this.snack.open('Seguro de Eliminar', 'Aceptar', {
      duration: 5000,
    }).onAction().subscribe(() => {
      this.calendarActivityService.deleteActivity(this.data.horizontalPropertyId, this.eventSelect.id)
        .subscribe(a => {
          this.dialogReg.close(true);
        });
    });
  }

  changeEventState(event: MatCheckboxChange) {
    this.calendarActivityService.updateActivity(
      this.data.horizontalPropertyId,
      this.eventSelect.id,
      {...this.eventSelect, complete: event.checked}).subscribe((a: any) => {
          this.eventSelect.complete = event.checked;
          this.snack.open('Actualizado', null, {
            duration: 2500
          });
    }, e => {
      this.snack.open('Error al actualizar', null, {
        duration: 2500
      });
    });
  }
}
