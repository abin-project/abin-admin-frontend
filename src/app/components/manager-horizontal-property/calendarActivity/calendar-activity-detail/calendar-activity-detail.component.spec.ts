import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarActivityDetailComponent } from './calendar-activity-detail.component';

describe('CalendarActivityDetailComponent', () => {
  let component: CalendarActivityDetailComponent;
  let fixture: ComponentFixture<CalendarActivityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarActivityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarActivityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
