import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CalendarOptions, FullCalendarComponent} from '@fullcalendar/angular';
import {RecreationAreaReservation} from '../../../../model/hotizontal-property/recreationAreaReservation';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {ActivatedRoute} from '@angular/router';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
import {CalendarActivityService} from '../calendar-activity.service';
import {CalendarActivityCreateComponent} from '../calendar-activity-create/calendar-activity-create.component';
import {CalendarActivity} from '../../../../model/hotizontal-property/calendarActivity';
import {CalendarReservationManualComponent} from '../../calendar/calendar-reservation-manual/calendar-reservation-manual.component';
import {CalendarActivityDetailComponent} from '../calendar-activity-detail/calendar-activity-detail.component';

@Component({
  selector: 'abin-calendar-activity',
  templateUrl: './calendar-activity.component.html',
  styleUrls: ['./calendar-activity.component.css']
})
export class CalendarActivityComponent implements OnInit, AfterViewInit {
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    locale: 'es',
    headerToolbar: {
      left: 'prevBtn,nextBtn today activity',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay',
    },
    customButtons: {
      prevBtn: {
        icon: 'fc-icon-chevron-left',
        click: this.btnPrevMonth.bind(this)
      },
      nextBtn: {
        icon: 'fc-icon-chevron-right',
        click: this.btnRightMonth.bind(this)
      },
      activity: {
        // icon: 'fc-icon-chevron-right',
        text: 'Nueva Actividad',
        click: this.createActivity.bind(this)
      }
    },
    buttonText: {
      today: 'Hoy',
      month: 'Mes',
      week: 'Semana',
      day: 'Día',
      list: 'Lista'
    },
    handleWindowResize: true,
    rerenderDelay: 1,
    height: '100%',
    eventClick: this.eventClick.bind(this),
    // eventMouseLeave: this.mouseLeave.bind(this),
    allDayText: 'Todos los Dias',
    // themeSystem: 'material',
    events: [],
    eventClassNames: 'active'
  };
  horizontalPropertyId;
  activities: CalendarActivity[] = [];
  eventSelect: CalendarActivity;
  calendarApi;
  positionsPopup;
  actualMonth;
  isRender = false;
  recreationsArea: RecreationArea[];
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  constructor(
    private routerActive: ActivatedRoute,
    private calendarActivityService: CalendarActivityService,
    private globalService: DataGlobalService,
    private dialog: MatDialog,
    private snack: MatSnackBar) {
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });

  }


  ngOnInit(): void {

    // this.globalService.setOpen();
    this.globalService.eventOpen.toPromise().finally(() => {
      setTimeout(() => {
        this.calendarApi.render();
        this.loadDateMonth(this.calendarApi.currentData.currentDate);
      }, 500);
    });
  }

  ngAfterViewInit(): void {
    this.calendarApi = this.calendarComponent.getApi();
    this.loadData();
  }

  clickDate(arg) {
    if (moment(arg.dateStr) >= moment(moment().format('YYYY-MM-DD'))) {
      this.dialog.open(CalendarReservationManualComponent, {
        width: '400px',
        data: {
          id: this.horizontalPropertyId,
          ...arg,
        },
      }).afterClosed().subscribe(r => {
        if (r) {
          this.loadData();
        }
      });
    } else {
      this.snack.open('Fecha Invalida', null, {duration: 2000});
    }
  }
  eventClick(ev) {
    this.eventSelect = ev.event.extendedProps;
    this.dialog.open(CalendarActivityDetailComponent, {
      data: {
        event: this.eventSelect,
        horizontalPropertyId: this.horizontalPropertyId,
      },
      width: '400px'
    }).afterClosed().subscribe(a => {
        this.loadData();
    });
  }

  loadData() {
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  btnPrevMonth(evt) {
    this.calendarApi.prev();
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  btnRightMonth(evt) {
    this.calendarApi.next();
    this.loadDateMonth(this.calendarApi.currentData.currentDate);
  }

  loadDateMonth(date) {
    const d = moment(date).add(1, 'day').format('YYYY-MM');
    this.actualMonth = d;
    this.calendarActivityService.getActivityPeriod(this.horizontalPropertyId, this.actualMonth)
      .subscribe((a: any) => {
        this.activities = a;
        this.processActivities();
      });
  }

  createActivity(evt) {
    this.dialog.open(CalendarActivityCreateComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
      }
    }).afterClosed().subscribe(a => {
      if (a) {
        this.loadDateMonth(this.calendarApi.currentData.currentDate);
      }
    });
  }

  mouseLeave(ev) {
    this.eventSelect = null;
    this.positionsPopup = null;
  }

  getStyleEvent() {
    if (this.eventSelect && this.positionsPopup) {
      return {display: 'block', top: `${this.positionsPopup.y}px`, left: `${this.positionsPopup.x}px`};
    }
  }

  processActivities() {
    this.calendarOptions.events = this.activities.map(r => {
      return {
        title: r.activity,
        date: r.dateTimeActivity,
        color: r.complete ? '#007b00' : r.color,
        id: r.id,
        extendedProps: r,
        allDay: r.allDay,
        start: r.allDay === false ? `${r.dateTimeActivity} ${r.timeIni}` : `${r.dateTimeActivity}`,
        end: r.allDay === false ? `${r.dateTimeActivity} ${r.timeEnd}` : `${r.dateTimeActivity}`,
      };
    });
  }

  datePipe(reservation: RecreationAreaReservation) {
    if (reservation.allDay) {
      return `${moment(reservation.date).format('YYYY-MMM-DD')} (Todo el día)`;
    } else {
      return moment(reservation.date).format('YYYY-MMM-DD, hh:mm A');
    }
  }
}
