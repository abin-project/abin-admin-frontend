import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarActivityCreateComponent } from './calendar-activity-create.component';

describe('CalendarActivityCreateComponent', () => {
  let component: CalendarActivityCreateComponent;
  let fixture: ComponentFixture<CalendarActivityCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarActivityCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarActivityCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
