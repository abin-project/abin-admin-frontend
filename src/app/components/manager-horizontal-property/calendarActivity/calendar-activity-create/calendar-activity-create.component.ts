import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RecreationArea} from '../../../../model/hotizontal-property/recreationArea';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CalendarActivityService} from '../calendar-activity.service';

@Component({
  selector: 'abin-calendar-activity-create',
  templateUrl: './calendar-activity-create.component.html',
  styleUrls: ['./calendar-activity-create.component.css']
})
export class CalendarActivityCreateComponent implements OnInit {

  activityForm: FormGroup;

  time = {
    start: null,
    end: null,
    total: null
  };
  reservationArea: RecreationArea;
  user;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CalendarActivityCreateComponent>,
              private fb: FormBuilder,
              private calendarActivityService: CalendarActivityService,
              private snack: MatSnackBar) {
    this.activityForm = this.fb.group({
      id: [this.data?.id ? this.data.id : null],
      dateTimeActivity: [this.data?.dateTimeActivity ? this.data.dateTimeActivity : null, [Validators.required]],
      allDay: [this.data?.allDay ? this.data.allDay : null],
      timeIni: [this.data?.timeIni ? this.data.timeIni : null],
      timeEnd: [this.data?.timeEnd ? this.data.timeEnd : null],
      activity: [this.data?.activity ? this.data.activity : null, [Validators.required]],
      assignTo: [this.data?.assignTo ? this.data.assignTo : null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.activityForm.get('allDay').valueChanges.subscribe(a => {
      if (a === true) {
        this.activityForm.get('timeIni').clearValidators();
        this.activityForm.get('timeEnd').clearValidators();
        this.activityForm.get('timeIni').disable();
        this.activityForm.get('timeEnd').disable();
        this.activityForm.get('allDay').setValidators([Validators.required]);
      } else {
        this.activityForm.get('timeIni').setValidators([Validators.required]);
        this.activityForm.get('timeEnd').setValidators([Validators.required]);
        this.activityForm.get('timeIni').enable();
        this.activityForm.get('timeEnd').enable();
        this.activityForm.get('allDay').clearValidators();
      }
    });
  }

  save() {
    if (this.activityForm.valid) {
      console.log(this.activityForm.value);
      this.calendarActivityService.createActivity(this.data.horizontalPropertyId, this.activityForm.value).subscribe(a => {
        this.dialogRef.close(a);
      });
    } else {
      console.log(this.activityForm.value);
    }
  }
}
