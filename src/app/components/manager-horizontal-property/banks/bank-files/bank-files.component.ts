import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {BankService} from '../bank.service';
import {DataGlobalService} from '../../../../services/data-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {TransactionFileComponent} from '../transaction-file/transaction-file.component';
import {TransactionService} from '../transaction.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {Bank} from '../../../../model/hotizontal-property/bank';

@Component({
  selector: 'abin-bank-files',
  templateUrl: './bank-files.component.html',
  styleUrls: ['./bank-files.component.css']
})
export class BankFilesComponent implements OnInit {

  displayedColumns: string[] = [
    'pos',
    'files',
    'dateFile',
    'option'
  ];
  horizontalPropertyId: string;
  account: Bank;
  accountBankId: string;
  files: YearFile[] = [];
  params: any = {year: null};
  filesSelect: any = [];

  constructor(public dialog: MatDialog,
              private transactionService: TransactionService,
              private dataGlobalService: DataGlobalService,
              private bankService: BankService,
              private matSnackBar: MatSnackBar,
              private router: Router,
              private routerActive: ActivatedRoute) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });
    this.routerActive.params.subscribe(p => {
      this.accountBankId = p.id;
    });

    this.router.events.subscribe((evt: NavigationEnd) => {
      if (evt instanceof NavigationEnd) {
        this.loadFiles();
      }
    });
  }

  ngOnInit() {
    this.bankService.getBank(this.horizontalPropertyId, this.accountBankId).subscribe((r: ApiResponse) => {
      this.account = r.data;
    });
    this.transactionService.getFilesGroup(this.horizontalPropertyId, this.accountBankId).subscribe(r => {
      this.files = r.data;
    });
  }

  indexPosData(i) {
    return i + 1;
  }

  fromFile() {
    this.dialog.open(TransactionFileComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        accountBank: this.account
      }
    }).afterClosed().subscribe(async r => {
        if (r) {
          this.ngOnInit();
          this.loadFiles();
        }
      }
    );
  }

  delete(fileId) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.transactionService.deleteFile(this.horizontalPropertyId, fileId).subscribe(() => {
        this.ngOnInit();
        this.loadFiles();
      });
    });
  }

  showFile(fileName) {
    window.open(this.transactionService.showFile(this.horizontalPropertyId, fileName), '_blank');
  }

  yearExist() {
    return this.params.year != null;
  }

  loadFiles() {
    this.transactionService.getFilesOnYearAccount(this.horizontalPropertyId, this.accountBankId, this.params.year).subscribe(r => {
      this.filesSelect = r.data;
    });
  }
}

export interface YearFile {
  year: number;
  files: TransactionFile[];
}

export interface TransactionFile {
  id: string;
  fileName: string;
  fileNameOriginal: string;
  horizontalProperty: HorizontalProperty;
  createdAt?: any;
  updatedAt?: any;
}
