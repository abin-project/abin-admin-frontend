import {Component, Input} from '@angular/core';
import {FileVisorAdminComponent} from '../../horizontalPropertyFile/file-visor-admin/file-visor-admin.component';
import {Bank} from '../../../../model/hotizontal-property/bank';

@Component({
  selector: 'abin-bank-files-year',
  templateUrl: './bank-files-year.component.html',
  styleUrls: ['./bank-files-year.component.css']
})
export class BankFilesYearComponent extends FileVisorAdminComponent {
  @Input() files: any[] = [];
}

export interface ExtractFile {
  id: string;
  fileName: string;
  file: string;
  fileDate: string;
  accountBank: Bank;
  createdAt: Date;
  updatedAt: Date;
}
