import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BankFilesYearComponent} from './bank-files-year.component';

describe('BankFilesYearComponent', () => {
  let component: BankFilesYearComponent;
  let fixture: ComponentFixture<BankFilesYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BankFilesYearComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankFilesYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
