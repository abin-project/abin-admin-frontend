import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TransactionService} from '../transaction.service';
import {PropertyService} from '../../property/property.service';
import {Property} from '../../../../model/hotizontal-property/property';
import {ApiResponse} from '../../../../model/apiResponse';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DebtService} from '../../previousPayment/debts/debt.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {ViewTypeDebt} from '../transaction-form/transaction-form.component';
import {ItemService} from '../../item/item.service';

@Component({
  selector: 'abin-transactions-income-update',
  templateUrl: './transactions-income-update.component.html',
  styleUrls: ['./transactions-income-update.component.css']
})
export class TransactionsIncomeUpdateComponent implements OnInit {
  income: FormGroup;
  now = moment().format('YYYY-MM-DD');
  properties: Property[];
  filteredOptions: Observable<Property[]>;
  withDebts = false;
  manualDebts = false;
  hp = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  typesIncomesProperty: ViewTypeDebt[] = [];
  visibleNextButton = false;
  visibleOnlySavedButton = false;
  visibleAcceptButton = false;


  constructor(private fb: FormBuilder,
              private bankIncomeService: TransactionService,
              private dialogRef: MatDialogRef<TransactionsIncomeUpdateComponent>,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private debtService: DebtService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    console.log(data);
    this.income = this.fb.group({
      id: [{value: this.data.a.incomeExtract_id, disabled: true}, Validators.required],
      date: [this.data.a.incomeExtract_date, Validators.required],
      agency: [{value: this.data.a.incomeExtract_agency, disabled: true}, Validators.required],
      gloss: [this.data.a.incomeExtract_gloss ? this.data.a.incomeExtract_gloss : null, Validators.required],
      reference: [this.data.a.incomeExtract_reference, Validators.required],
      transactionCode: [{value: this.data.a.incomeExtract_transactionCode, disabled: true}, Validators.required],
      amount: [{value: this.data.a.incomeExtract_amount, disabled: true}, Validators.required],
      state: [{value: this.data.a.incomeExtract_state, disabled: true}, Validators.required],
      onlyDebts: null,
      payDebt: [2, [Validators.required, Validators.min(0), Validators.max(2)]]
    });
  }

  ngOnInit(): void {
    if (this.data.a.incomeExtract_reference) {
      this.income.get('reference').setValue(this.data.a.incomeExtract_reference);
    }
    this.propertyService.getPropertiesWithCodeOnHorizontalProperty(this.data.hp).subscribe((res: ApiResponse) => {
      this.properties = res.data;
      this.filteredOptions = this.income.get('reference').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.code),
          map(name => name ? this._filter(name) : this.properties.slice())
        );
    });
    this.income.get('reference').valueChanges.subscribe(r => {
      this.loadDebts();
      this.loadPropertyItems();
      this.loadAllDebts();
      this.setButtonState();
    });
  }

  send(evt) {
    let re = this.income.value;
    re.date = moment(re.date).format('YYYY-MM-DD');
    re.configDebts = {
      debts: evt.select,
      onlyDebts: re.onlyDebts,
    };
    re = {
      ...re,
      ...evt
    };
    if (this.income.valid) {
      if (re.amount > 0) {
        if (this._existProperty(re.reference).length > 0) {
          re.id = this.data.a.incomeExtract_id;
          re.state = 1;
          this.bankIncomeService.updateTransaction(this.data.hp, re).subscribe(() => {
            this.dialogRef.close(this.income.value);
          });
        } else {
          this.snack.open('La Propiedad no existe!', null, {
            duration: 2000,
          });
        }
      } else {
        re.id = this.data.a.incomeExtract_id;
        // re.state = 1;
        this.bankIncomeService.updateTransaction(this.data.hp, re).subscribe(() => {
          this.dialogRef.close(this.income.value);
        });
      }

    }
  }

  cancel() {
    this.dialogRef.close();
  }

  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }

  private _existProperty(code) {
    return this.properties.filter(r => {
      return r.code == code;
    });
  }

  private loadDebts() {
    // this.income.get('debts').setValue(null);
    const property = this.properties.find(f => f.code == this.income.get('reference').value);
    if (property != undefined) {
      this.debtService.getOnlyDebtsProperty(this.hp.id, property.id).subscribe(r => {
        this.withDebts = r.data.length > 0;
      });
    }
  }

  loadPropertyItems() {
    const property = this.properties.find(f => f.code == this.income.get('reference').value);
    if (property) {
      this.itemService.getTypeIncomeByPropertyDetail(this.data.horizontalPropertyId, property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
        this.income.get('onlyDebts').setValue(this.typesIncomesProperty.filter(f => f.selected === true).map(d => d.id));
      });
    } else {
      this.typesIncomesProperty = [];
    }
  }

  getProperty() {
    // tslint:disable-next-line:triple-equals
    const p = this.properties.find(f => f.code == this.income.get('reference').value);
    if (p !== undefined) {
      return p;
    } else {
      return {
        id: null
      }
    }
  }

  private loadAllDebts() {
    const property = this.properties.find(f => f.code == this.income.get('reference').value);
    if (property != undefined) {
      this.debtService.getAllDebtProperty(this.hp.id, property.id).subscribe(r => {
        this.manualDebts = r.data.length > 0;
      });
    }
  }


  setButtonState(): void {
    if ((this.manualDebts || this.withDebts)
      && (this.income.get('payDebt').value === 0 || this.income.get('payDebt').value === 1)
      && (this.income.get('amount').value > 0)
      // && this.formTransaction.get('onlyDebts').value
      && this.getProperty()) {
      this.visibleNextButton = true;
    } else {
      this.visibleNextButton = false;
    }

    if ((!this.manualDebts && !this.withDebts)
      || this.income.get('amount').value < 0
      || this.income.get('payDebt').value === 2) {
      this.visibleAcceptButton = true;
    } else {
      this.visibleAcceptButton = false;
    }

    if (this.income.get('amount').value > 0 && this.getProperty() && this.income.get('payDebt').value === 2) {
      this.visibleOnlySavedButton = true;
    } else {
      this.visibleOnlySavedButton = false;
    }
  }

}
