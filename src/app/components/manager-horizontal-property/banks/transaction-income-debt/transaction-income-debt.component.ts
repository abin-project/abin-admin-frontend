import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DebtService} from '../../previousPayment/debts/debt.service';

@Component({
  selector: 'abin-transaction-income-debt',
  templateUrl: './transaction-income-debt.component.html',
  styleUrls: ['./transaction-income-debt.component.css']
})
export class TransactionIncomeDebtComponent implements OnInit {
  @Input() id;
  @Input() propertyId;
  @Input() maxAmount;
  @Input() onlySave = true;
  @Output() saved = new EventEmitter();
  debts: DebtView[] = [];

  constructor(private snack: MatSnackBar,
              private debtService: DebtService) {
  }

  ngOnInit(): void {
    this.debtService.getAllDebtProperty(this.id, this.propertyId).subscribe(r => {
      this.debts = r.data;
    });
  }

  save(evt) {
    // tslint:disable-next-line:triple-equals
    const selected = this.debts.filter(f => f.amountPay > 0);
    if (selected.length > 0) {
      // tslint:disable-next-line:triple-equals
      const res = selected.map(r => Number(r.amountPay)).reduce((a, b) => a + b);
      // if (res <= this.maxAmount) {
        // tslint:disable-next-line:triple-equals
      this.saved.emit({
        select: selected,
        ...evt
      });
      // se quito el monto excedido;
      // } else {
      //   this.snack.open('Monto Excedido o no existe', null, {
      //     duration: 2000,
      //   });
      // }
    } else {
      this.saved.emit({select: null, ...evt });
    }
  }
}

export interface DebtView {
  id: string;
  amount: number;
  code: string;
  date: any;
  debtPayLimit: any;
  isGlobal: number;
  nameIncome: string;
  ownerId: string;
  priority: number;
  propertyId: string;
  totalPay: number;
  amountPay: number;
  typeIncomePropertyId: string;
  // pay: any;
}
// amount: "15.00"
// code: 205
// date: "2020-01-10T04:00:00.000Z"
// debtPayLimit: "2020-01-31T04:00:00.000Z"
// id: "faa5299d-cf1f-42f4-8dd9-8c2adef5fcb3"
// isGlobal: 3
// nameIncome: "Cuotas Extraordinarias"
// ownerId: "c45bd306-16ad-4ec3-a9f2-c852c6814296"
// priority: 5
// propertyId: "34fead43-3a5d-4947-88f0-14cffb3b8910"
// totalPay: "0"
// typeIncomePropertyId: "eeb43477-a375-4085-b2d1-00531ccabeb9"
