import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionIncomeDebtComponent } from './transaction-income-debt.component';

describe('TransactionIncomeDebtComponent', () => {
  let component: TransactionIncomeDebtComponent;
  let fixture: ComponentFixture<TransactionIncomeDebtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionIncomeDebtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionIncomeDebtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
