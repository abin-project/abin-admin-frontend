import {Component, Inject, OnInit} from '@angular/core';
import {Transaction} from '../../../../model/hotizontal-property/transaction';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Property} from '../../../../model/hotizontal-property/property';
import {Observable} from 'rxjs';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {TransactionService} from '../transaction.service';
import {PropertyService} from '../../property/property.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ItemService} from '../../item/item.service';
import {DebtService} from '../../previousPayment/debts/debt.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {TransactionDebtPayComponent} from '../transaction-debt-pay/transaction-debt-pay.component';
import {ViewTypeDebt} from '../transaction-form/transaction-form.component';
import {BankService} from '../bank.service';
import {Bank} from '../../../../model/hotizontal-property/bank';

@Component({
  selector: 'abin-transaction-transfer',
  templateUrl: './transaction-transfer.component.html',
  styleUrls: ['./transaction-transfer.component.css']
})
export class TransactionTransferComponent implements OnInit {
  transaction: Transaction;
  formTransaction: FormGroup;
  accounts: Bank[] = [];
  hp = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;

  constructor(private fb: FormBuilder,
              private transactionService: TransactionService,
              private snack: MatSnackBar,
              private dialogRef: MatDialogRef<TransactionTransferComponent>,
              private dialog: MatDialog,
              private bankService: BankService,
              @Inject(MAT_DIALOG_DATA) private  data: any) {
    this.transaction = this.data.transaction ? this.data.transaction : null;
    this.formTransaction = this.fb.group({
      date: [this.transaction ? this.transaction.date : null, [Validators.required]],
      agency: this.transaction ? this.transaction.agency : null,
      gloss: this.transaction ? this.transaction.gloss : null,
      // reference: this.transaction ? this.transaction.reference : null,
      transactionCode: [this.transaction ? this.transaction.transactionCode : null, [Validators.required]],
      amount: [this.transaction ? this.transaction.amount : null, [Validators.required]],
      accountBank: [{value: this.data.accountBank.nameBank, disabled: true}, [Validators.required]],
      accountDestine: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.loadAccounts();
  }

  loadAccounts() {
    this.bankService.getAccountsNonId(this.hp.id, this.data.accountBank.id).subscribe(r => {
      this.accounts = r.data;
    });
  }

  save() {
    if (this.formTransaction.valid) {
      if (!this.transaction) {
        this.transactionService.transferTransaction(
          this.data.horizontalPropertyId,
          this.data.accountBank.id,
          this.formTransaction.value).subscribe(r => {
          this.dialogRef.close(r);
        });
      }
      // else {
      //   this.transaction = this.formTransaction.value;
      //   this.transaction.id = this.data.transaction.id;
      //   this.transaction.accountBank = this.data.accountBank;
      //   this.transactionService.updateTransaction(this.data.horizontalPropertyId, this.data.transaction).subscribe(r => {
      //     this.dialogRef.close(r);
      //   });
      // }
    }
  }
}
