import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'abin-transactions-income-filter',
  templateUrl: './transactions-income-filter.component.html',
  styleUrls: ['./transactions-income-filter.component.css']
})
export class TransactionsIncomeFilterComponent implements OnInit {
  formFilter: FormGroup;
  now = moment();

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<TransactionsIncomeFilterComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.formFilter = this.fb.group({
      dateIni: [data.dateIni],
      dateEnd: [data.dateEnd],
      code: [data.code],
      month: [data.month ? data.month : null],
      year: [data.year ? data.year : null],
      state: [data.state],
    });
  }

  ngOnInit(): void {
  }

  send() {
    if (this.formFilter.valid) {
      const res = this.formFilter.value;
      res.dateIni = res.dateIni ? moment(res.dateIni).format('YYYY-MM-DD') : null;
      res.dateEnd = res.dateEnd ? moment(res.dateEnd).format('YYYY-MM-DD') : null;
      this.dialogRef.close(res);
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}
