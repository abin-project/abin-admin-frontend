import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TransactionService} from '../transaction.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Project} from '../../../../model/hotizontal-property/project';
import * as moment from 'moment';

@Component({
  selector: 'abin-transaction-file',
  templateUrl: './transaction-file.component.html',
  styleUrls: ['./transaction-file.component.css']
})
export class TransactionFileComponent implements OnInit {
  formTransaction: FormGroup;
  projects: Project[] = [];

  constructor(private fb: FormBuilder,
              private transactionService: TransactionService,
              private snack: MatSnackBar,
              private dialogRef: MatDialogRef<TransactionFileComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.formTransaction = this.fb.group({
      accountBank: [{value: this.data.accountBank.nameBank, disabled: true}, [Validators.required]],
      file: [null, [Validators.required]],
      fileDate: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formTransaction.valid) {
      const res = this.formTransaction.value;
      const formData = new FormData();
      formData.append('file', res.file._files[0], res.file._filesNames);
      formData.append('fileDate', moment(res.fileDate).format('YYYY-MM-DD'));
      this.transactionService.uploadFileTransaction(this.data.horizontalPropertyId, this.data.accountBank.id, formData).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
