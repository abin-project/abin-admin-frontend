import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionFileComponent} from './transaction-file.component';

describe('TransactionFileComponent', () => {
  let component: TransactionFileComponent;
  let fixture: ComponentFixture<TransactionFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionFileComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
