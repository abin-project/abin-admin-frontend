import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ApiResponse} from '../../../../model/apiResponse';
import {AccountHolder} from '../../../../model/hotizontal-property/accountHolder';
import {BankService} from '../bank.service';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {OwnerService} from '../../owner/owner.service';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';

@Component({
  selector: 'abin-holder-bank',
  templateUrl: './holder-bank.component.html',
  styleUrls: ['./holder-bank.component.css']
})
export class HolderBankComponent implements OnInit {

  bankSelect: Bank;
  displayedColumns: string[] = ['id', 'nameOwner', 'updatedAt', 'option'];
  dataSource: MatTableDataSource<AccountHolder>;
  owners: Owner[];
  inChange: any;
  ownerSelect;

  constructor(private bankService: BankService,
              private ownerService: OwnerService,
              private matSnackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.bankSelect = data.bank;
    this.dataSource = new MatTableDataSource([]);

  }


  async ngOnInit() {
    await this.bankService.getAccountHolder(this.bankSelect.id).subscribe((r: ApiResponse) => {
      this.dataSource = new MatTableDataSource(r.data);
    });
    await this.ownerService.getOnlyOwner(this.data.horizontalPropertyId).subscribe(async (res: ApiResponse) => {
      this.owners = await this.processOwner(res.data);
    });
  }

  onSave(id) {
    this.bankService.setAccountHolder(this.bankSelect.id, {
      owner: this.ownerSelect,
      holderId: id
    }).subscribe(() => {
      this.ngOnInit();
      this.inChange = null;
    });
  }

  cancel() {
    this.inChange = null;
    this.ownerSelect = null;
  }

  deleteOlder(holderId) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
    }).onAction().subscribe(r => {
      this.bankService.deleteHolder(this.bankSelect.id, holderId).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  async processOwner(owners: Owner[]) {
    const res = [];
    await owners.forEach(o => {
      const f = this.dataSource.data.find(d => d.owner.id === o.id);
      if (!f) {
        res.push(o);
      }
    });
    return res;
  }
}
