import {Component, OnInit, ViewChild} from '@angular/core';
import {TransactionService} from '../transaction.service';
import {BankService} from '../bank.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {MatDialog} from '@angular/material/dialog';
import {TransactionsIncomeFilterComponent} from '../transactions-income-filter/transactions-income-filter.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {DataGlobalService} from '../../../../services/data-global.service';
import {compare, compareDate, increment, toDecimal, toUpperCase} from '../../../../services/globalFunctions';
import {ViewIncomeExtract} from '../../../../model/hotizontal-property/viewIncomeExtract';
import {SelectionModel} from '@angular/cdk/collections';
import {TransactionsIncomeUpdateComponent} from '../trasactions-income-update/transactions-income-update.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {TransactionFormComponent} from '../transaction-form/transaction-form.component';
import {TransactionFileComponent} from '../transaction-file/transaction-file.component';
import {TransactionsIncomeFilterPeriodComponent} from '../transactions-income-filter-period/transactions-income-filter-period.component';
import {TransactionTransferComponent} from '../transaction-transfer/transaction-transfer.component';
import {PrintService} from '../../../../services/print.service';
import * as moment from 'moment';
import {StateIncomePipe} from '../../../../pipes/state-income.pipe';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import {TransactionAssignComponent} from '../transaction-assign/transaction-assign.component';
import {Transaction} from '../../../../model/hotizontal-property/transaction';
import {HorizontalPropertyService} from '../../../horizontal-property/horizontal-property.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {TableVirtualScrollDataSource} from 'ng-table-virtual-scroll';
import {TransactionIncomeUpdateOnlyComponent} from '../transaction-income-update-only/transaction-income-update-only.component';
import {ImportTemplateComponent} from '../import-template/import-template.component';

@Component({
  selector: 'abin-transactions-income',
  templateUrl: './transactions-income.component.html',
  styleUrls: ['./transactions-income.component.css']
})
export class TransactionsIncomeComponent implements OnInit {

  params;
  account: Bank;
  horizontalPropertyId;
  bankId;
  dataSource: MatTableDataSource<ViewIncomeExtract> = new TableVirtualScrollDataSource([]);
  sortedData: ViewIncomeExtract[];
  displayedColumns: string[] = [
    'select',
    'pos',
    'incomeExtract_date',
    'incomeExtract_agency',
    'incomeExtract_gloss',
    'incomeExtract_reference',
    'incomeExtract_transactionCode',
    'incomeExtract_amount_debt',
    'incomeExtract_amount',
    'accumulated',
    'incomeExtract_state',
    // 'incomeExtract_createdAt',
    // 'incomeExtract_updatedAt',
    // 'userUpdated_email',
    // 'options'
  ];
  selection = new SelectionModel<ViewIncomeExtract>(true, []);
  totals = {
    lastTotal: 0,
    totalDebts: 0,
    totalCredit: 0
  };
  rowVisible = 0;
  horizontalProperty: HorizontalProperty;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private transactionService: TransactionService,
              private dataGlobalService: DataGlobalService,
              private bankService: BankService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              private router: Router,
              private printService: PrintService,
              private pipeState: StateIncomePipe,
              private routerActive: ActivatedRoute,
              private horizontalPropertyService: HorizontalPropertyService,
              private rolService: RolService) {
    this.routerActive.params.subscribe(params => {
      this.bankId = params.id;
    });
    this.routerActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });
    this.routerActive.queryParams.subscribe(r => {
      this.params = r;
    });

  }

  async ngOnInit() {
    this.horizontalPropertyService.getInfoHorizontalProperty(this.horizontalPropertyId).subscribe(r => {
      this.horizontalProperty = r.data;
    });
    await this.loadData();
  }

  indexPosData(i) {
    return i + 1;
  }

  getIndex(evt) {
    this.rowVisible = evt;
  }

  async loadData() {
    this.bankService.getBank(this.horizontalPropertyId, this.bankId).subscribe((r: ApiResponse) => {
      this.account = r.data;
    });
    this.transactionService.getIncomeExtractByAccount(
      this.horizontalPropertyId,
      this.bankId,
      this.params).subscribe(async (res: ApiResponse) => {
      // const data: ViewIncomeExtract[] = await this.getTotal(res.data);
      this.dataSource = new TableVirtualScrollDataSource<ViewIncomeExtract>(res.data);
      this.totals = res.other;
      this.selection.clear();
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
    if (this.readNotOnly()) {
      if (!this.displayedColumns.find(r => r === 'options')) {
        this.displayedColumns.push('options');
      }
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => {
        // if (row.incomeExtract_state === 0) {
          this.selection.select(row);
        // }
      });
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ViewIncomeExtract): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'incomeExtract_date':
          return compareDate(a.incomeExtract_date, b.incomeExtract_date, isAsc);
        case 'incomeExtract_agency':
          return compare(a.incomeExtract_agency, b.incomeExtract_agency, isAsc);
        case 'incomeExtract_gloss':
          return compare(a.incomeExtract_gloss, b.incomeExtract_gloss, isAsc);
        case 'incomeExtract_reference':
          return compare(a.incomeExtract_reference, b.incomeExtract_reference, isAsc);
        case 'incomeExtract_transactionCode':
          return compare(a.incomeExtract_transactionCode, b.incomeExtract_transactionCode, isAsc);
        case 'incomeExtract_amount':
          return compare(a.incomeExtract_amount, b.incomeExtract_amount, isAsc);
        case 'incomeExtract_state':
          return compare(a.incomeExtract_state, b.incomeExtract_state, isAsc);
        case 'incomeExtract_createdAt':
          return compareDate(a.incomeExtract_createdAt, b.incomeExtract_createdAt, isAsc);
        case 'incomeExtract_updatedAt':
          return compareDate(a.incomeExtract_updatedAt, b.incomeExtract_updatedAt, isAsc);
        case 'userUpdated_email':
          return compare(a.userUpdated_email, b.userUpdated_email, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<ViewIncomeExtract>(this.sortedData);
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  addTransaction() {
    this.dialog.open(TransactionFormComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        accountBank: this.account
      }
    }).afterClosed().subscribe(async r => {
        if (r) {
          await this.loadData();
        }
      }
    );
  }

  addTransfer() {
    this.dialog.open(TransactionTransferComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        accountBank: this.account
      }
    }).afterClosed().subscribe(async r => {
        if (r) {
          await this.loadData();
        }
      }
    );
  }

  addImport() {
    this.dialog.open(ImportTemplateComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        accountBank: this.account
      }
    }).afterClosed().subscribe(async r => {
        if (r) {
          await this.loadData();
        }
      }
    );
  }

  fromFile() {
    this.dialog.open(TransactionFileComponent, {
      width: '400px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        accountBank: this.account
      }
    }).afterClosed().subscribe(async r => {
        if (r) {
          await this.loadData();
        }
      }
    );
  }

  edit(incomeExtract) {
    this.dialog.open(TransactionsIncomeUpdateComponent, {
      width: '500px',
      data: {
        hp: this.horizontalPropertyId,
        a: incomeExtract
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.loadData();
      }
    });
  }

  ignore(incomeExtract) {
    this.snack.open('Seguro de Ignorar', 'ACEPTAR', {
      duration: TIME_DURATION,
    }).onAction().subscribe(() => {
      this.transactionService.updateTransaction(this.horizontalPropertyId, {
        id: incomeExtract.incomeExtract_id,
        state: 3,
        reference: incomeExtract.incomeExtract_reference
      }).subscribe(async () => {
        await this.loadData();
      });
    });
  }

  noProcess(incomeExtract: ViewIncomeExtract) {
    this.snack.open('Seguro de Cambiar de Estado', 'ACEPTAR', {
      duration: TIME_DURATION,
    }).onAction().subscribe(() => {
      this.transactionService.updateNotProcess(this.horizontalPropertyId, incomeExtract.incomeExtract_id,
        ).subscribe(async () => {
        await this.loadData();
      });
    });
  }

  process(incomeExtract) {
    this.snack.open('Seguro de Procesar', 'ACEPTAR', {
      duration: TIME_DURATION,
    }).onAction().subscribe(() => {
      this.transactionService.processTransaction(this.horizontalPropertyId, incomeExtract.incomeExtract_id).subscribe(async () => {
        await this.loadData();
      });
    });
  }

  openFilter() {
    this.dialog.open(TransactionsIncomeFilterComponent, {
      width: '300px',
      data: this.params
    }).afterClosed().subscribe(async r => {
      if (r) {
        this.router.navigate([], {
          queryParams: r,
        }).then(() => this.loadData());
      }
    });
  }

  delete(incomeExtract) {
    this.snack.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      this.transactionService.deleteTransaction(this.horizontalPropertyId, incomeExtract.incomeExtract_id).subscribe(async () => {
        await this.loadData();
      });
    });
  }

  openFilterPeriod() {
    this.dialog.open(TransactionsIncomeFilterPeriodComponent, {
      width: '300px',
      data: this.params
    }).afterClosed().subscribe(async r => {
      if (r) {
        this.router.navigate([], {
          queryParams: r,
        }).then(() => this.loadData());
      }
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  postAll(transaction: ViewIncomeExtract) {

    this.snack.open('Seguro de Procesar', TEXT_ACCEPT, {
      duration: 5000,
    }).onAction().subscribe(r => {
      const p = {
        transactionId: transaction.incomeExtract_id,
      };
      this.transactionService.reProcessTransactions(this.horizontalPropertyId, this.account.id, p).subscribe(async (dr: any) => {
        if (dr) {
          await this.loadData();
        }
      });
    });
  }

  assignToIncome(transaction: ViewIncomeExtract) {
    this.dialog.open(TransactionAssignComponent, {
      width: '400px',
      data: {
        t: transaction,
        id: this.horizontalPropertyId
      }
    }).afterClosed().subscribe( r => {
      this.ngOnInit();
    });
  }

  printVoucher(transaction: ViewIncomeExtract) {

    this.transactionService.transactionDetails(this.horizontalPropertyId, transaction.incomeExtract_id).subscribe(async r => {
      await this.printService.printVoucher([this.horizontalProperty.nameProperty, 'Recibo de Pago'],
        r.headers,
        r.debts, r.property, r.data, r.bank);
    });
  }

  async print() {
    const headers = ['#', 'Fecha', 'Detalle', 'Referencia', 'Código T.', 'Débitos', 'Créditos', 'Saldo', 'Estado'];
    const colOption = {
      0: {
        halign: 'right',
      },
      5: {
        halign: 'right',
      },
      6: {
        halign: 'right',
      },
      7: {
        halign: 'right',
      },
    };
    const data = await this.processBody();
    await this.printService.createSingleTable(
      this.horizontalProperty.nameProperty,
      `Transacciones ${this.account.nameBank} - N. ${this.account.numberAccount}`,
      headers,
      data,
      colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    // tslint:disable-next-line:forin
    for (const i in data) {
      const d = [
        increment(i),
        toUpperCase(moment(data[i].incomeExtract_date).format('DD-MM-YYYY')),
        // toUpperCase(data[i].incomeExtract_agency),
        toUpperCase(data[i].incomeExtract_gloss),
        toUpperCase(data[i].property ? `${data[i].property.type.toUpperCase()} ${data[i].property.name}` : data[i].incomeExtract_reference),
        toUpperCase(data[i].incomeExtract_transactionCode)
      ];

      if (data[i].incomeExtract_amount_debt < 0) {
        d.push(toDecimal(Math.abs(data[i].incomeExtract_amount_debt)));
        d.push('');
      } else {
        d.push('');
        d.push(toDecimal(data[i].incomeExtract_amount));
      }
      d.push(toDecimal(data[i].accumulated));
      d.push(toUpperCase(this.pipeState.transform(data[i])));
      res.push(d);
    }
    res.push([
    {
      content: 'Total Parcial',
      colSpan: 6
    },
    toDecimal(this.totals.totalDebts),
    toDecimal(this.totals.totalCredit),
    toDecimal(this.totals.lastTotal),
    ''
    ]);
    return res;
  }

  async processDebts(transaction: Transaction) {
    const res = [];
    const debts = transaction.debtIncomes;
    // tslint:disable-next-line:forin
    for (const i in debts) {
      const d = [
        increment(i),
        toUpperCase(moment(debts[i].debt.date).format('DD-MMMM-YYYY')),
        toUpperCase(debts[i].debt.typeIncomeProperty.typeIncome.nameIncome),
        toUpperCase(toDecimal(debts[i].amount)),
      ];
      res.push(d);
    }
    res.push(['', '', 'TOTAL', toDecimal(transaction.amount)]);
    return res;
  }

  editOnly(incomeExtract: any) {
    this.dialog.open(TransactionIncomeUpdateOnlyComponent, {
      width: '500px',
      data: {
        hp: this.horizontalPropertyId,
        a: incomeExtract
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.loadData();
      }
    });
  }

  printVouchers() {
    this.snack.open('Solo se imprimirán los recibos de transacciones procesadas', 'Aceptar', {
      duration: 4000,
    }).onAction().subscribe(() => {
      this.transactionService.transactionsMultipleDetails(this.horizontalPropertyId,
        this.selection.selected.filter(a => a.incomeExtract_state === 0).map(a => a.incomeExtract_id)).subscribe(a => {
        this.printService.printMultipleVouchers(this.horizontalProperty, a.data);
      });
    });
  }

  sendVoucher(incomeExtract: ViewIncomeExtract) {
    this.transactionService.postTransactionSendEmail(this.horizontalPropertyId, [incomeExtract.incomeExtract_id]).subscribe(r => {
      this.snack.open('Enviado')
    });
  }

  sendVouchers() {
    this.transactionService.postTransactionSendEmail(this.horizontalPropertyId,
      this.selection.selected.filter(a => a.incomeExtract_state === 0).map(a => a.incomeExtract_id)).subscribe(r => {
      this.snack.open('Enviado')
    });
  }
}
