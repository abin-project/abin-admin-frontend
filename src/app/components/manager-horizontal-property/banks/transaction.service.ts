import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';
import {QueryTransaction} from '../../../model/queryTransaction.model';
import {Debt} from '../../../model/hotizontal-property/debt';

@Injectable()
export class TransactionService extends PrincipalService {
  route = 'transaction';

  getIncomesExtract(horizontalPropertyId) {
    return this.get(horizontalPropertyId);
  }

  getIncomeExtractInterval(horizontalPropertyId, dateIni, dateEnd) {
    return this.get(`${horizontalPropertyId}/${dateIni}/${dateEnd}`);
  }

  getIncomeExtractByAccount(horizontalPropertyId, bankId, params) {
    return this.getQuery(`${horizontalPropertyId}/${bankId}/query`, params);
  }

  updateTransaction(horizontalPropertyId, income) {
    return this.put(`${horizontalPropertyId}/update/${income.id}`, income);
  }

  updateTransactionOnly(horizontalPropertyId, income) {
    return this.put(`${horizontalPropertyId}/update/only/${income.id}`, income);
  }

  updateNotProcess(horizontalPropertyId, transactionId) {
    return this.put(`${horizontalPropertyId}/updateState/${transactionId}`, {});
  }

  getTransactionQuery(horizontalPropertyId: string, query: QueryTransaction): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/query`, query);
  }

  getOneTransaction(horizontalPropertyId: string, transactionId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/showBalance/${transactionId}`);
  }

  getOnlyPending(horizontalPropertyId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/onlyPending`);
  }

  processTransaction(horizontalPropertyId: string, transactionId: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/process/${transactionId}`);
  }

  reProcessTransactions(horizontalPropertyId: string, accountId: string, transactions): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/reProcess/${accountId}/selectTransactions`, transactions);
  }

  createTransaction(horizontalPropertyId: any, bankId, value: any) {
    return this.postCustom(`${horizontalPropertyId}/create/${bankId}`, value);
  }

  createTransactionByFile(horizontalPropertyId: any, id: string, formData: FormData) {
    return this.postFile(`${horizontalPropertyId}/importByFile/${id}`, formData);
  }

  uploadFileTransaction(horizontalPropertyId: any, id: string, formData: FormData) {
    return this.postFile(`${horizontalPropertyId}/uploadFile/${id}`, formData);
  }

  getFilesGroup(horizontalPropertyId: any, id: string): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/files/${id}`);
  }

  deleteTransaction(horizontalPropertyId: string, id: string) {
    return this.delete(`${horizontalPropertyId}/delete/${id}`);
  }

  getFilesOnYearAccount(horizontalPropertyId: string, accountBankId: string, year: number): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/files/${accountBankId}`, { year});
  }

  deleteFile(horizontalPropertyId: string, id: any) {
    return this.delete(`${horizontalPropertyId}/files/${id}`);
  }

  showFile(horizontalPropertyId: string, fileName: any) {
    return `${this.getPartialRoute()}/${horizontalPropertyId}/file/show/${fileName}`;
  }

  transferTransaction(horizontalPropertyId: any, id: any, value: any): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/transfer/${id}`, value);
  }

  setDebtAndreProcess(horizontalPropertyId: string, id: string, debt: Debt): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/setDebt/${id}`, debt);
  }

  updateDebtAndreProcess(horizontalPropertyId: string, id: string, debt: Debt): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/updateDebt/${id}`, debt);
  }

  transactionDetails(horizontalPropertyId, id: string): Observable<any> {
    return this.get(`${horizontalPropertyId}/details/${id}`);
  }

  transactionByOwnerId(horizontalPropertyId: string, params?): Observable<any> {
    return this.getQuery(`${horizontalPropertyId}/transactionsProperty`, params);
  }

  showGraph(horizontalPropertyId: string, id: string) {
    return this.get(`${horizontalPropertyId}/detailsGraph/${id}`);
  }

  transactionsMultipleDetails(horizontalPropertyId, viewIncomeExtractsIds: string[]): Observable<ApiResponse> {
    return this.postCustom(`${horizontalPropertyId}/details`, {transactions: viewIncomeExtractsIds});
  }

  postTransactionSendEmail(horizontalPropertyId: string, transactionIds: string[]){
    return this.postCustom(`${horizontalPropertyId}/sendEmail`,  {transactionIds});
  }
}
