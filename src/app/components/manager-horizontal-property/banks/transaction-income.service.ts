import { Injectable } from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class TransactionIncomeService extends PrincipalService {
  route = 'transaction-income';

  getItemAvailable(horizontalPropertyId): Observable<ApiResponse> {
    return this.get(`${horizontalPropertyId}/itemAvailable`);
  }

  createTransactionIncome(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  deleteTransactionIncome(horizontalPropertyId, transactionId) {
    return this.delete(`${horizontalPropertyId}/delete/${transactionId}`);
  }
}
