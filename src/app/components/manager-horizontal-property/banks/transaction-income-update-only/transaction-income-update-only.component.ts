import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Property} from '../../../../model/hotizontal-property/property';
import {Observable} from 'rxjs';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {ViewTypeDebt} from '../transaction-form/transaction-form.component';
import {TransactionService} from '../transaction.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PropertyService} from '../../property/property.service';
import {ItemService} from '../../item/item.service';
import {DebtService} from '../../previousPayment/debts/debt.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'abin-transaction-income-update-only',
  templateUrl: './transaction-income-update-only.component.html',
  styleUrls: ['./transaction-income-update-only.component.css']
})
export class TransactionIncomeUpdateOnlyComponent implements OnInit {
  income: FormGroup;
  now = moment().format('YYYY-MM-DD');
  properties: Property[];
  filteredOptions: Observable<Property[]>;
  withDebts = false;
  manualDebts = false;
  hp = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  typesIncomesProperty: ViewTypeDebt[] = [];
  visibleNextButton = false;
  visibleOnlySavedButton = false;
  visibleAcceptButton = false;


  constructor(private fb: FormBuilder,
              private bankIncomeService: TransactionService,
              private dialogRef: MatDialogRef<TransactionIncomeUpdateOnlyComponent>,
              private propertyService: PropertyService,
              private itemService: ItemService,
              private debtService: DebtService,
              private dialog: MatDialog,
              private snack: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    console.log(data);
    this.income = this.fb.group({
      id: [{value: this.data.a.incomeExtract_id, disabled: true}, Validators.required],
      date: [{value: this.data.a.incomeExtract_date, disabled: true}, Validators.required],
      agency: [this.data.a.incomeExtract_agency ? this.data.a.incomeExtract_agency : null],
      gloss: [this.data.a.incomeExtract_gloss ? this.data.a.incomeExtract_gloss : null],
      reference: [{value: this.data.a.incomeExtract_reference, disabled: true}, Validators.required],
      transactionCode: [{value: this.data.a.incomeExtract_transactionCode, disabled: true}, Validators.required],
      amount: [{value: this.data.a.incomeExtract_amount, disabled: true}, Validators.required],
      state: [{value: this.data.a.incomeExtract_state, disabled: true}, Validators.required],
    });
  }

  ngOnInit(): void {
    if (this.data.a.incomeExtract_reference) {
      this.income.get('reference').setValue(this.data.a.incomeExtract_reference);
    }
  }

  send() {
    if (this.income.valid) {
        const result = this.income.value;
        result.id = this.data.a.incomeExtract_id;
        this.bankIncomeService.updateTransactionOnly(this.data.hp, result).subscribe(() => {
          this.dialogRef.close(this.income.value);
        });
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
