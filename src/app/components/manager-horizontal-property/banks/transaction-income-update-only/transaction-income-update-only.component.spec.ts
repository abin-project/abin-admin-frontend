import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionIncomeUpdateOnlyComponent } from './transaction-income-update-only.component';

describe('TransactionIncomeUpdateOnlyComponent', () => {
  let component: TransactionIncomeUpdateOnlyComponent;
  let fixture: ComponentFixture<TransactionIncomeUpdateOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionIncomeUpdateOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionIncomeUpdateOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
