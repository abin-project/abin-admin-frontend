import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {BankService} from '../bank.service';
import {FormBankComponent} from '../form-bank/form-bank.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HolderBankComponent} from '../holder-bank/holder-bank.component';
import {compare, compareDate} from '../../../../services/globalFunctions';
import * as moment from 'moment';
import {TEXT_ACCEPT, TEXT_DELETE, TIME_DURATION} from '../../../../model/constants';
import {RolService} from '../../../admin-horizontal-property/rol.service';
import { BankQrDataComponent } from '../bank-qr-data/bank-qr-data.component';

@Component({
  selector: 'abin-list-bank',
  templateUrl: './list-bank.component.html',
  styleUrls: ['./list-bank.component.css']
})
export class ListBankComponent implements OnInit {


  displayedColumns: string[] = [
    'pos',
    'nameBank',
    'typeAccount',
    'numberAccount',
    'amount',
    'updatedAt',
    'state',
    'option'
  ];
  horizontalPropertyId: string;
  dataSource: MatTableDataSource<Bank>;
  sortedData: Bank[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private bankService: BankService,
              private dataGlobalService: DataGlobalService,
              private matSnackBar: MatSnackBar,
              private routerActive: ActivatedRoute,
              private rolService: RolService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.bankService.getAccounts(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<Bank>(res.data);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
    // if (this.readNotOnly()) {
    //   if (!this.displayedColumns.find(r => r === 'option')) {
    //     this.displayedColumns.push('option');
    //   }
    // }
  }

  getParams() {
    const date = moment();
    return {
      dateIni: date.startOf('month').format('YYYY-MM-DD'),
      dateEnd: date.endOf('month').format('YYYY-MM-DD'),
      state: 2
    };
  }


  indexPosData(i) {
    return i + 1;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameBank':
          return compare(a.nameBank, b.nameBank, isAsc);
        case 'numberAccount':
          return compare(a.numberAccount, b.numberAccount, isAsc);
        case 'typeAccount':
          return compare(a.typeAccount, b.typeAccount, isAsc);
        case 'amount':
          return compare(a.amount, b.amount, isAsc);
        case 'updatedAt':
          return compareDate(a.updateAt, b.updateAt, isAsc);
        case 'state':
          return compare(a.state, b.state, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Bank>(this.sortedData);
  }

  openDialog() {
    this.dialog.open(FormBankComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();
  }

  edit(bank) {
    this.dialog.open(FormBankComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        bank
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  updateDataQr(bank: any) {

    this.dialog.open(BankQrDataComponent, {
      width: '500px',
      data: {
        horizontalPropertyId: this.horizontalPropertyId,
        bank
      }
    }).afterClosed().subscribe(r => {
      if (r) {
        this.ngOnInit();
      }
    });
  }

  showHolder(bank: Bank) {
    this.dialog.open(HolderBankComponent, {
      width: '550px',
      data: {
        bank,
        horizontalPropertyId: this.horizontalPropertyId
      }
    });
  }

  readNotOnly() {
    return this.rolService.getUserRol() === 'Root' ||
      this.rolService.getUserRol() === 'Administrador' ||
      this.rolService.getUserRol() === 'Administrador del Sistema';
  }

  updateState(bank: Bank) {
    const state = bank.state === 'Vigente' ? 'No Vigente' : 'Vigente';
    this.matSnackBar.open(`Se Cambiara de Estado a ${state}`, TEXT_ACCEPT, {
      duration: 5000
    }).onAction().subscribe(() => {
      this.bankService.setBank(this.horizontalPropertyId, bank.id, {
        nameBank: bank.nameBank,
        typeAccount: bank.typeAccount,
        numberAccount: bank.numberAccount,
        state: state
      }).subscribe(() => {
        this.ngOnInit();
      });
    });
  }

  delete(bank) {
    this.matSnackBar.open(TEXT_DELETE, TEXT_ACCEPT, {
      duration: TIME_DURATION,
      verticalPosition: 'bottom'
    }).onAction().subscribe(() => {
      this.bankService.deleteBank(this.horizontalPropertyId, bank.id).subscribe(() => {
        this.ngOnInit();
      });
    });
  }
}
