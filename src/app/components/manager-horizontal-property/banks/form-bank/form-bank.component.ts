import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BankService} from '../bank.service';
import {Bank} from '../../../../model/hotizontal-property/bank';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {map, startWith} from 'rxjs/operators';
import {ApiResponse} from '../../../../model/apiResponse';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {PositionService} from '../../directory/position.service';

@Component({
  selector: 'abin-form-bank',
  templateUrl: './form-bank.component.html',
  styleUrls: ['./form-bank.component.css']
})
export class FormBankComponent implements OnInit {

  formBank: FormGroup;
  bankSelect: Bank;
  horizontalPropertyId: string;

  filterOwners: Observable<Owner[]>;
  ownersSelected: Owner[] = [];
  owners: Owner[] = [];

  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;


  constructor(protected fb: FormBuilder,
              protected dialogRef: MatDialogRef<FormBankComponent>,
              protected bankService: BankService,
              private positionsService: PositionService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.bankSelect = this.data.bank;
    this.horizontalPropertyId = this.data.horizontalPropertyId;


    this.formBank = this.fb.group({
      numberAccount: [this.bankSelect ? this.bankSelect.numberAccount : null, [Validators.required]],
      typeAccount: [this.bankSelect ? this.bankSelect.typeAccount : null, [Validators.required]],
      nameBank: [this.bankSelect ? this.bankSelect.nameBank : null, [Validators.required]],
      isTypeAccount: [true, [Validators.required]]
    });
    this.positionsService.getOnlyOwnerPosition(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.owners = r.data;
    });
    this.verifyState();
    this.verifyAmount();
  }

  ngOnInit(): void {
    this.formBank.get('isTypeAccount').valueChanges.subscribe(r => {
      this.verifyState();
    });
  }

  save() {
    if (this.formBank.valid && !this.bankSelect) {
      this.bankService.createBank(this.horizontalPropertyId, this.formBank.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    } else if (this.formBank.valid && this.bankSelect) {
      this.bankService.setBank(this.horizontalPropertyId, this.bankSelect.id, this.formBank.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

  private verifyState() {
    if (this.formBank.get('isTypeAccount').value === true) {
      this.formBank.removeControl('nameOn');
      this.formBank.addControl('ownerCtrl', new FormControl(null, [Validators.required, Validators.maxLength(3)]));
      this.filterOwners = this.formBank.get('ownerCtrl').valueChanges.pipe(
        startWith(null),
        map((owner: string | null) => owner ? this._filter(owner) : this.owners.slice()));
    } else {
      this.formBank.removeControl('ownerCtrl');
      this.formBank.addControl('nameOn', new FormControl(null, [Validators.required]));
    }
  }

  remove(fruit: Owner): void {
    const index = this.ownersSelected.indexOf(fruit);

    if (index >= 0) {
      this.ownersSelected.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.filterOwners.subscribe(r => {
      this.ownersSelected.push(r.find(d => {
        return d.nameOwner.toLowerCase() === event.option.viewValue.toLowerCase();
      }));
    });

    this.fruitInput.nativeElement.value = '';
    this.formBank.get('ownerCtrl').setValue(null);
  }

  private _filter(value: any): Owner[] {
    const filterValue = value.toLowerCase();

    return this.owners.filter(owner => owner.nameOwner.toLowerCase().indexOf(filterValue) === 0);
  }

  private verifyAmount() {
    if (this.bankSelect?.amount > 0) {
      this.formBank.get('numberAccount').disable();
      this.formBank.get('isTypeAccount').disable();
      if (this.formBank.contains('nameOn')) {
        this.formBank.get('nameOn').disable();
      }
      if (this.formBank.contains('ownerCtrl')) {
        this.formBank.get('ownerCtrl').disable();
      }
    }
  }
}
