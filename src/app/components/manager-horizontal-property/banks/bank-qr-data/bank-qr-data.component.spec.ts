import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankQrDataComponent } from './bank-qr-data.component';

describe('BankQrDataComponent', () => {
  let component: BankQrDataComponent;
  let fixture: ComponentFixture<BankQrDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankQrDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankQrDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
