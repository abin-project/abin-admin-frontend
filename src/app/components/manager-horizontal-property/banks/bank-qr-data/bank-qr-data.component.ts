import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Bank } from 'src/app/model/hotizontal-property/bank';
import { BankService } from '../bank.service';

@Component({
  selector: 'abin-bank-qr-data',
  templateUrl: './bank-qr-data.component.html',
  styleUrls: ['./bank-qr-data.component.css']
})
export class BankQrDataComponent implements OnInit {

  bank: Bank;

  formBank: FormGroup;

  isChange = false;

  noValid = '********************'

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private dialogRef: MatDialogRef<BankQrDataComponent>,
              private bankService: BankService,
              private fb: FormBuilder) {

    this.bank = this.data.bank;

    this.formBank = this.fb.group({
      bankName: [null, [Validators.required]],
      qrUser: [this.bank.qr ? this.noValid: null, [Validators.required]],
      qrPassword: [this.bank.qr ? this.noValid: null, [Validators.required]],
      qrServiceKey: [this.bank.qr ? this.noValid: null, [Validators.required]],
      qrUserApiKey: [this.bank.qr ? this.noValid: null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    const name = `(${this.bank.numberAccount}) ${this.bank.nameBank}`
    this.formBank.get('bankName').setValue(name);
    this.formBank.valueChanges.subscribe(r => {
      this.isChange = true;
    })
  }

  save() {
    if (this.formBank.valid && this.isChange) {
      this.bankService.updateBankQr(this.data.horizontalPropertyId, this.bank.id, this.formBank.value).subscribe(arg => {
        this.dialogRef.close(arg);
      });

    }
  }

}
