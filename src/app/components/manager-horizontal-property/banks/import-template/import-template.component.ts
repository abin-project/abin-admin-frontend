import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import * as moment from "moment";
import {TransactionService} from "../transaction.service";

@Component({
  selector: 'abin-import-template',
  templateUrl: './import-template.component.html',
  styleUrls: ['./import-template.component.css']
})
export class ImportTemplateComponent implements OnInit {
  formImport: FormGroup;
  horizontalPropertyId: string;
  constructor(private fb: FormBuilder,
              private transactionService: TransactionService,
              private dialogRef: MatDialogRef<ImportTemplateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.horizontalPropertyId = this.data.horizontalPropertyId;
    this.formImport = this.fb.group({
      file: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  save() {
    if (this.formImport.valid) {
      const res = this.formImport.value;
      const formData = new FormData();
      formData.append('file', res.file._files[0], res.file._filesNames);
      this.transactionService.createTransactionByFile(this.data.horizontalPropertyId, this.data.accountBank.id, formData).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }
}
