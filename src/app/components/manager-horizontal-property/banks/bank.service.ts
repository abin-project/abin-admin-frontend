import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class BankService extends PrincipalService {

  route = 'account-bank';

  getAccounts(horizontalPropertyId) {
    return this.get(horizontalPropertyId);
  }

  createBank(horizontalPropertyId, data) {
    return this.postCustom(`${horizontalPropertyId}/create`, data);
  }

  getBank(horizontalPropertyId, accountId) {
    return this.get(`${horizontalPropertyId}/show/${accountId}`);
  }

  setBank(horizontalPropertyId: string, id: string, value: any) {
    return this.put(`${horizontalPropertyId}/update/${id}`, value);
  }

  deleteBank(horizontalPropertyId, bankId) {
    return this.delete(`${horizontalPropertyId}/delete/${bankId}`);
  }

  getAccountHolder(bankId) {
    return this.http.get(`${this.server}/account-bank-holder/${bankId}`);
  }

  setAccountHolder(bankId, data) {
    return this.http.put(`${this.server}/account-bank-holder/${bankId}`, data);
  }

  deleteHolder(bankId, holderId) {
    return this.http.delete(`${this.server}/account-bank-holder/${bankId}/${holderId}`);
  }

  getAccountsNonId(id: string, id2: any): Observable<ApiResponse> {
    return this.get(`${id}/notId/${id2}`);
  }

  updateBankQr(horizontalPropertyId: string, idBank:string, value: any) {
    return this.postCustom(`${horizontalPropertyId}/updateQr/${idBank}`, value);
  }
}

