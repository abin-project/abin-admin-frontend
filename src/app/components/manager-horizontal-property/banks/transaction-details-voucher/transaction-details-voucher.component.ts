import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Transaction} from '../../../../model/hotizontal-property/transaction';

@Component({
  selector: 'abin-transaction-details-voucher',
  templateUrl: './transaction-details-voucher.component.html',
  styleUrls: ['./transaction-details-voucher.component.css']
})
export class TransactionDetailsVoucherComponent implements OnInit {
  horizontalProperty: HorizontalProperty;
  transaction: Transaction;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.horizontalProperty = this.data.horizontalProperty;
    this.transaction = this.data.transaction;
  }

  ngOnInit(): void {
  }

}
