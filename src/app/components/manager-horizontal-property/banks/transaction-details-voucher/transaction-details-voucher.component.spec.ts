import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionDetailsVoucherComponent } from './transaction-details-voucher.component';

describe('TransactionDetailsVoucherComponent', () => {
  let component: TransactionDetailsVoucherComponent;
  let fixture: ComponentFixture<TransactionDetailsVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionDetailsVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDetailsVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
