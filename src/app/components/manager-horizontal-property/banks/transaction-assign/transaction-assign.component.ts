import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TransactionIncomeService} from '../transaction-income.service';
import {TypeIncome} from '../../../../model/hotizontal-property/typeIncome';

@Component({
  selector: 'abin-transaction-assign',
  templateUrl: './transaction-assign.component.html',
  styleUrls: ['./transaction-assign.component.css']
})
export class TransactionAssignComponent implements OnInit {
  formIncome: FormGroup;
  types: TypeIncome[];
  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<TransactionAssignComponent>,
              private transactionIncomeService: TransactionIncomeService,
              @Inject(MAT_DIALOG_DATA) private data: any ) {
    this.formIncome = this.fb.group({
      transactionId: [this.data.t.incomeExtract_id, [Validators.required]],
      typeIncomeId: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.transactionIncomeService.getItemAvailable(this.data.id).subscribe(r => {
      this.types = r.data;
    });
  }

  save() {
    if (this.formIncome.valid) {
      this.transactionIncomeService.createTransactionIncome(this.data.id, this.formIncome.value).subscribe(r => {
        this.dialogRef.close(r);
      });
    }
  }

}
