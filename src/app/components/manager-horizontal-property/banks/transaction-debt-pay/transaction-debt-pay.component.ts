import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DebtService} from '../../previousPayment/debts/debt.service';
import {Debt} from '../../../../model/hotizontal-property/debt';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'abin-transaction-debt-pay',
  templateUrl: './transaction-debt-pay.component.html',
  styleUrls: ['./transaction-debt-pay.component.css']
})
export class TransactionDebtPayComponent implements OnInit {
  @Input() id;
  @Input() propertyId;
  @Input() maxAmount;
  @Input() onlySave = true;
  @Output() saved = new EventEmitter();
  debts: DebOfPay[];

  constructor(private snack: MatSnackBar,
              private debtService: DebtService) {
  }

  ngOnInit(): void {
    this.debtService.getOnlyDebtOfPayProperty(this.id, this.propertyId).subscribe(r => {
      this.debts = r.data;
    });
  }

  save(evt) {
    // tslint:disable-next-line:triple-equals
    const selected = this.debts.filter(f => f.pay == true);
    if (selected.length > 0) {
      // tslint:disable-next-line:triple-equals
      const res = selected.map(r => Number(r.amountPay)).reduce((a, b) => a + b);
      // if (res <= this.maxAmount) {
        // tslint:disable-next-line:triple-equals
      this.saved.emit({
        select: selected,
        ...evt
      });
      // se quito el monto excedido;
      // } else {
      //   this.snack.open('Monto Excedido o no existe', null, {
      //     duration: 2000,
      //   });
      // }
    } else {
      this.saved.emit({select: null, ...evt });
    }
  }
}

export interface DebtV extends Debt {
  pay: boolean;
  amountPay: number;
}

export interface DebOfPay {
  pay: boolean;
  amountPay: number;

  amount: number;
  code: number;
  date: any;
  debtPayLimit: string;
  id: string;
  isGlobal: number;
  nameIncome: string;
  ownerId: string;
  priority: number;
  propertyId: string;
  totalPay: number;
  typeIncomePropertyId: string;
}
