import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionDebtPayComponent} from './transaction-debt-pay.component';

describe('TransactionDebtPayComponent', () => {
  let component: TransactionDebtPayComponent;
  let fixture: ComponentFixture<TransactionDebtPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionDebtPayComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDebtPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
