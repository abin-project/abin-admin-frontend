import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'abin-transactions-income-filter-period',
  templateUrl: './transactions-income-filter-period.component.html',
  styleUrls: ['./transactions-income-filter-period.component.css']
})
export class TransactionsIncomeFilterPeriodComponent implements OnInit {
  formFilter: FormGroup;
  now = moment();

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<TransactionsIncomeFilterPeriodComponent>,
              @Inject(MAT_DIALOG_DATA) data: any) {
    this.formFilter = this.fb.group({
      month: [data.month ? data.month : this.now.format('M'), [Validators.required]],
      year: [data.year ? data.year : this.now.year(), [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  send() {
    if (this.formFilter.valid) {
      this.dialogRef.close(this.formFilter.value);
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
