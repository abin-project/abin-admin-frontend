import {Component, Inject, OnInit} from '@angular/core';
import {Transaction} from '../../../../model/hotizontal-property/transaction';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TransactionService} from '../transaction.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Property} from '../../../../model/hotizontal-property/property';
import {Observable} from 'rxjs';
import {PropertyService} from '../../property/property.service';
import {ApiResponse} from '../../../../model/apiResponse';
import {map, startWith} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DebtService} from '../../previousPayment/debts/debt.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {ItemService} from '../../item/item.service';

@Component({
  selector: 'abin-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.css']
})
export class TransactionFormComponent implements OnInit {
  transaction: Transaction;
  formTransaction: FormGroup;
  properties: Property[];
  filteredOptions: Observable<Property[]>;

  withDebts = false;
  manualDebts = false;

  hp = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
  typesIncomesProperty: ViewTypeDebt[] = [];

  visibleNextButton = false;
  visibleOnlySavedButton = false;
  visibleAcceptButton = false;
  visibleOptions = false;

  constructor(private fb: FormBuilder,
              private transactionService: TransactionService,
              private propertyService: PropertyService,
              private snack: MatSnackBar,
              private dialogRef: MatDialogRef<TransactionFormComponent>,
              private itemService: ItemService,
              private dialog: MatDialog,
              private debtService: DebtService,
              @Inject(MAT_DIALOG_DATA) private  data: any) {
    this.transaction = this.data.transaction ? this.data.transaction : null;
    this.formTransaction = this.fb.group({
      date: [this.transaction ? this.transaction.date : null, [Validators.required]],
      agency: this.transaction ? this.transaction.agency : null,
      gloss: this.transaction ? this.transaction.gloss : null,
      reference: this.transaction ? this.transaction.reference : null,
      transactionCode: [this.transaction ? this.transaction.transactionCode : null, [Validators.required]],
      amount: [this.transaction ? this.transaction.amount : null, [Validators.required]],
      accountBank: [{value: this.data.accountBank.nameBank, disabled: true}, [Validators.required]],
      state: [{value: this.data.accountBank.state, disabled: true}, [Validators.required]],
      onlyDebts: null,
      payDebt: [2, [Validators.required, Validators.min(0), Validators.max(2)]]
    });
  }

  ngOnInit(): void {
    this.propertyService.getPropertiesWithCodeOnHorizontalProperty(this.data.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.properties = res.data;
      this.filteredOptions = this.formTransaction.get('reference').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.code),
          map(name => name ? this._filter(name) : this.properties.slice()),
        );
    });
    this.formTransaction.get('reference').valueChanges.subscribe(r => {
      this.loadDebts();
      this.loadPropertyItems();
      this.loadAllDebts();
      this.setButtonState();
    });

    this.formTransaction.get('amount').valueChanges.subscribe(a => {
      if (a < 0) {
        this.formTransaction.get('payDebt').setValue(2);
      } else if (a > 0 && this.withDebts) {
        this.formTransaction.get('payDebt').setValue(0);
      } else if (a > 0 && this.manualDebts) {
        this.formTransaction.get('payDebt').setValue(1);
      }
    });
  }

  save(event) {
    let formT = this.formTransaction.value;
    formT.configDebts = {
      debts: event.select,
      onlyDebts: formT.onlyDebts,
    };
    formT = {
      ...formT,
      ...event
    };
    if (this.formTransaction.valid) {
      if (!this.transaction) {
        this.transactionService.createTransaction(
          this.data.horizontalPropertyId,
          this.data.accountBank.id,
          formT).subscribe(r => {
          this.dialogRef.close(r);
        });
      } else {
        this.transaction = this.formTransaction.value;
        this.transaction.id = this.data.transaction.id;
        this.transaction.accountBank = this.data.accountBank;
        this.transactionService.updateTransaction(this.data.horizontalPropertyId, this.data.transaction).subscribe(r => {
          this.dialogRef.close(r);
        });
      }
    }
  }

  loadPropertyItems() {
    const property = this.properties.find(f => f.code == this.formTransaction.get('reference').value);
    if (property) {
      this.itemService.getTypeIncomeByPropertyDetail(this.data.horizontalPropertyId, property.id).subscribe(r => {
        this.typesIncomesProperty = r.data;
        this.formTransaction.get('onlyDebts').setValue(this.typesIncomesProperty.filter(f => f.selected === true).map(d => d.id));
        // this.debtService.
      });
    } else {
      this.typesIncomesProperty = [];
    }
  }

  private _filter(name: string): Property[] {
    const filterValue = name.toLowerCase();

    return this.properties.filter(option => option.code.toLowerCase().indexOf(filterValue) === 0);
  }

  private _existProperty(code) {
    return this.properties.filter(r => {
      return r.code == code;
    });
  }


  private loadDebts() {
    // this.formTransaction.get('debts').setValue(null);
    const property = this.properties.find(f => f.code == this.formTransaction.get('reference').value);
    if (property != undefined) {
      this.debtService.getOnlyDebtsProperty(this.hp.id, property.id).subscribe(r => {
        this.withDebts = r.data.length > 0;
      });
    }
  }

  private loadAllDebts() {
    const property = this.properties.find(f => f.code == this.formTransaction.get('reference').value);
    if (property != undefined) {
      this.debtService.getAllDebtProperty(this.hp.id, property.id).subscribe(r => {
        this.manualDebts = r.data.length > 0;
      });
    }
  }

  setButtonState(): void {
    if ((this.manualDebts || this.withDebts)
      && (this.formTransaction.get('payDebt').value === 0 || this.formTransaction.get('payDebt').value === 1)
      && (this.formTransaction.get('amount').value > 0)
      // && this.formTransaction.get('onlyDebts').value
      && this.getProperty()) {
      this.visibleNextButton = true;
    } else {
      this.visibleNextButton = false;
    }

    if ((!this.manualDebts && !this.withDebts)
      || this.formTransaction.get('amount').value < 0
      || this.formTransaction.get('payDebt').value === 2) {
      this.visibleAcceptButton = true;
    } else {
      this.visibleAcceptButton = false;
    }

    if (this.formTransaction.get('amount').value > 0 && this.getProperty() && this.formTransaction.get('payDebt').value === 2) {
      this.visibleOnlySavedButton = true;
    } else {
      this.visibleOnlySavedButton = false;
    }
  }

  getProperty() {
    // tslint:disable-next-line:triple-equals
    return this.properties.find(f => f.code == this.formTransaction.get('reference').value);
  }


}

export interface ViewTypeDebt{
  id: string;
  amount: number;
  balance: number;
  nameIncome: string;
  propertyId: string;
  totalDebt: number;
  totalPay: number;
  selected: boolean;
  typeIncomeId: string;
}
