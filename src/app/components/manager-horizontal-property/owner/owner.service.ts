import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class OwnerService extends PrincipalService {
  route = 'co-owner';

  getAllOwner(id) {
    return this.get(`${id}`);
  }

  getOnlyOwner(id) {
    return this.get(`${id}/only`);
  }

  updateSendEmail(horizontalPropertyId: string, ownerId: any) {
    return this.get(`${horizontalPropertyId}/updateEmail/${ownerId}`);
  }
}
