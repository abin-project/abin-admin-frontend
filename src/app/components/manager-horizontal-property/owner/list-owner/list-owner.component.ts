import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DataGlobalService} from '../../../../services/data-global.service';
import {ActivatedRoute} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {Owner} from '../../../../model/hotizontal-property/owner';
import {OwnerService} from '../owner.service';
import {compare, increment, toUpperCase} from '../../../../services/globalFunctions';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';

@Component({
  selector: 'abin-list-owner',
  templateUrl: './list-owner.component.html',
  styleUrls: ['./list-owner.component.css']
})
export class ListOwnerComponent implements OnInit {

  title = 'Co-Propietarios';
  displayedColumns: string[] = [
    'pos',
    'nameOwner',
    'emailOwner',
    'telephoneOwner',
    'cellOwner',
    'state',
    // 'option'
  ];
  horizontalPropertyId: string;
  horizontalProperty: HorizontalProperty;
  dataSource: MatTableDataSource<Owner>;
  sortedData: Owner[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private ownerService: OwnerService,
              private dataGlobalService: DataGlobalService,
              private routerActive: ActivatedRoute,
              private printService: PrintService) {
    this.routerActive.parent.params.subscribe(p => {
      this.horizontalPropertyId = p.id;
    });
    this.horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.ownerService.getAllOwner(this.horizontalPropertyId).subscribe((res: ApiResponse) => {
      this.dataSource = new MatTableDataSource<Owner>(res.data);
    });
    this.dataGlobalService.evt.subscribe(r => {
      this.applyFilter(r);
    });
  }

  openDialog(): void {
    // const dialogRef = this.dialog.open(CreatePropertyComponent, {
    //   width: '500px',
    // });
    //
    // dialogRef.afterClosed().subscribe((result: Property) => {
    //   if (result) {
    //     this.ownerService.create(this.horizontalPropertyId, result).subscribe(r => {
    //       this.ngOnInit();
    //     });
    //   }
    // });
  }

  indexPosData(i) {
    return i + 1;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameOwner':
          return compare(a.nameOwner, b.nameOwner, isAsc);
        case 'emailOwner':
          return compare(a.emailOwner, b.emailOwner, isAsc);
        case 'telephoneOwner':
          return compare(a.telephoneOwner, b.telephoneOwner, isAsc);
        case 'cellOwner':
          return compare(a.cellOwner, b.cellOwner, isAsc);
        default:
          return 0;
      }
    });
    this.dataSource = new MatTableDataSource<Owner>(this.sortedData);
  }

  applyFilter(event) {
    this.dataSource.filter = event.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  show(ev) {
    console.log(ev);
  }

  delete(id) {
    console.log(id);
  }

  async report() {
    const headers: string[] = [
      '#',
      'Nombre y Apellido',
      'Correo',
      'No. de Telefono',
      'No. Celular',
      'Estado',
    ];
    const body: any[][] = await this.processBody();
    const colOption = {
      0: {
        halign: 'right',
      },
      3: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      }
    };

    await this.printService
      .createSingleTable(this.horizontalProperty.nameProperty, `Lista de Información de ${this.title}`, headers, body, colOption);
  }

  async processBody() {
    const res = [];
    const data = this.dataSource.filteredData ? this.dataSource.filteredData : this.dataSource.data;
    for (const i in data) {
      res.push([
        increment(i),
        toUpperCase(this.dataSource.data[i].nameOwner),
        this.dataSource.data[i].emailOwner,
        this.dataSource.data[i].telephoneOwner,
        this.dataSource.data[i].cellOwner,
        toUpperCase(this.dataSource.data[i].state ? 'Activo' : 'No Activo'),
      ]);
    }
    return res;
  }
}
