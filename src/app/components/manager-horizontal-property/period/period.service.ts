import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class PeriodService extends PrincipalService {
  route = 'period';

  getPeriods(horizontalPropertyId) {
    return this.get(`${horizontalPropertyId}`);
  }

  getPeriodByYear(horizontalPropertyId, year) {
    return this.get(`${horizontalPropertyId}/byYear/${year}`);
  }

  getYearsOfHorizontalProperty(horizontalPropertyId) {
    return this.get(`${horizontalPropertyId}/years`);
  }
}
