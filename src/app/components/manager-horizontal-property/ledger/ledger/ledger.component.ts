import {Component, OnDestroy, OnInit} from '@angular/core';
import {ItemService} from '../../item/item.service';
import {Item} from '../../../../model/hotizontal-property/item';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ApiResponse} from '../../../../model/apiResponse';
import {LedgerService} from '../ledger.service';
import {ProjectService} from '../../project/project.service';
import {Project} from '../../../../model/hotizontal-property/project';
import {Ledger} from '../../../../model/hotizontal-property/ledger';
import {MatDialog} from '@angular/material/dialog';
import {FilterKardexDialogComponent} from '../../income/filter-kardex-dialog/filter-kardex-dialog.component';
import {PrintService} from '../../../../services/print.service';
import {HorizontalProperty} from '../../../../model/hotizontal-property/horizontalProperty';
import {Subscription} from 'rxjs';

@Component({
  selector: 'abin-ledger',
  templateUrl: './ledger.component.html',
  styleUrls: ['./ledger.component.css']
})
export class LedgerComponent implements OnInit, OnDestroy {

  itemsIncomes: Item[];
  itemsExpenses: Item[];
  horizontalPropertyId: string;
  query: any;
  result: Ledger[];
  total = 0;
  debt = 0;
  routerAc: Subscription;
  routerEvent: Subscription;
  routerActiveEvent: Subscription;
  params: any = {};
  projectSelect: Project;
  item: any;
  horizontalProperty = JSON.parse(localStorage.getItem('p')) as HorizontalProperty;

  constructor(private itemService: ItemService,
              private routeActive: ActivatedRoute,
              private router: Router,
              private dialog: MatDialog,
              private printService: PrintService,
              private ledgerService: LedgerService,
              private projectService: ProjectService) {
    this.routerActiveEvent = this.routeActive.parent.params.subscribe(r => {
      this.horizontalPropertyId = r.id;
    });

    this.routerEvent = this.routeActive.queryParams.subscribe(r => {
      this.params = r;
    });

    this.routerAc = this.router.events.subscribe((r: NavigationEnd) => {
      if (r instanceof NavigationEnd) {
        this.loadData();
      }
    });

  }

  ngOnInit(): void {

    this.itemService.getIncomeEnable(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.itemsIncomes = r.data;
    });
    this.itemService.getExpenseEnable(this.horizontalPropertyId).subscribe((r: ApiResponse) => {
      this.itemsExpenses = r.data;
    });
    this.projectService.getProjects(this.horizontalPropertyId).subscribe(r => {
      this.projectSelect = r.data;
    });
  }

  ngOnDestroy(): void {
    this.routerAc.unsubscribe();
    this.routerActiveEvent.unsubscribe();
    this.routerEvent.unsubscribe();
  }

  loadData() {
    if (this.params.projectId && this.params.typeItem) {
      this.ledgerService.showLegerPeriod(this.horizontalPropertyId, this.params).subscribe(r => {
        this.result = r.data.result;
        this.total = r.data.total;
        this.debt = r.data.debt;
        this.item = r.data.item;
      });
      if (this.params.projectId) {
        this.projectService.getProjectById(this.horizontalPropertyId, this.params.projectId).subscribe(f => {
          this.projectSelect = f.data;
        });
      }
    }
  }

  OpenFilter() {
    this.dialog.open(FilterKardexDialogComponent, {
      width: '400px',
      data: {
        ...this.params,
        horizontalPropertyId: this.horizontalPropertyId,
        withType: false
      }
    }).afterClosed().subscribe(async r => {
      if (r) {
        await this.router.navigate([], {queryParams: r});
        this.loadData();
      }
    });
  }

  getData() {
    return {
      ...this.params,
      horizontalPropertyId: this.horizontalPropertyId,
    };
  }

  async print() {
    const colOption = {
      1: {
        halign: 'right',
      },
      4: {
        halign: 'right',
      },
      5: {
        halign: 'right',
      }
    };
    const titles = [
      `${this.horizontalProperty.nameProperty}`,
      'Libro Mayor'
    ];
    if (this.item.typeIncome) {
      titles.push(`Partida ${this.item.typeIncome.code} ${this.item.typeIncome.nameIncome}`);
    } else {
      titles.push(`Partida ${this.item.typeExpense.code} ${this.item.typeExpense.nameExpense}`);
    }
    await this.printService.printLedger(titles, colOption, 'table');
  }
}
