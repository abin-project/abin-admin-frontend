import {Injectable} from '@angular/core';
import {PrincipalService} from '../../../services/principal.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../../model/apiResponse';

@Injectable({
  providedIn: 'root'
})
export class LedgerService extends PrincipalService {
  route = 'ledger';

  showLegerPeriod(horizontalPropertyId, params): Observable<ApiResponse> {
    return this.getQuery(`${horizontalPropertyId}/show`, params);
  }
}
