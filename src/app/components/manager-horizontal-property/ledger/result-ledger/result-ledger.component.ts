import {Component, Input, OnInit} from '@angular/core';
import {Ledger} from '../../../../model/hotizontal-property/ledger';
import {Project} from '../../../../model/hotizontal-property/project';

@Component({
  selector: 'abin-result-ledger',
  templateUrl: './result-ledger.component.html',
  styleUrls: ['./result-ledger.component.css']
})
export class ResultLedgerComponent implements OnInit {
  @Input() ledgers: Ledger[];
  @Input() debt: number;
  @Input() total: number;
  @Input() item: any;
  displayedColumns: string[] = [
    'pos',
    'date',
    'receipt',
    'gloss',
    'amount',
    'debt'
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
