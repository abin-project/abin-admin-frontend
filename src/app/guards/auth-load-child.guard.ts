import {Injectable} from '@angular/core';
import {CanLoad, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthLoadChildGuard implements CanLoad {
  constructor(private router: Router) {
  }

  canLoad(): boolean {
    if (!localStorage.getItem('token')) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
