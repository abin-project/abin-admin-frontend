import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  hostAdmin = environment.admin;
  hostClient = environment.client;

  constructor(private router: Router) {
  }

  canActivate(): boolean {
    if (!localStorage.getItem('token')) {
      if (window.location.origin.includes(this.hostAdmin)) {
        this.router.navigate(['/auth/login']);
      }
      if (window.location.origin.includes(this.hostClient)) {
        this.router.navigate(['/client/login']);
      }
      return false;
    }
    return true;
  }
}
