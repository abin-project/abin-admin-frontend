import {Directive, ElementRef, OnDestroy, OnInit} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[appAutofocus]',
})
export class AutofocusDirective implements OnInit, OnDestroy {

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.element.nativeElement.focus();
    this.element.nativeElement.select();
  }
  ngOnDestroy() {
    this.element.nativeElement.blur();
  }
}
