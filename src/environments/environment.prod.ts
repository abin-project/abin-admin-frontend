// @ts-ignore
import {version} from '../../package.json';

export const environment = {
  production: true,
  version,
  server: 'https://api.abinsrl.com/v2',
  admin: 'https://beta.abinsrl.com',
  client: 'https://ph.abinsrl.com'
};
