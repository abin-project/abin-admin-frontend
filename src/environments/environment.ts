// @ts-ignore
import {version} from '../../package.json';

export const environment = {
  production: true,
  version,
  server: 'http://localhost:3000',
  admin: 'http://localhost',
  client: 'http://dev.localhost'
};
