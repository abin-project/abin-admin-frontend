# AbinAdminFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.3.

## Requirements
  - nodejs 14
  - windows, linux or mac

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:80/`. The app will automatically reload if you change any of the source files.
  - `http://localhost:80/`
  - `http://dev.localhost`

settings url admin and normal user:

```typescript
export const environment = {
  production: true,
  version,
  server: 'http://localhost:3000',
  admin: 'http://localhost', // admin url
  client: 'http://dev.localhost' // normal client
};
```

register url `dev.localhost` in `C:\Windows\System32\drivers\etc\hosts`

or `hosts` file in OS

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
